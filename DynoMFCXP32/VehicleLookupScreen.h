#pragma once


// VehicleLookupScreen dialog

class VehicleLookupScreen : public CDialogEx
{
	DECLARE_DYNAMIC(VehicleLookupScreen)

public:
	VehicleLookupScreen(CWnd* pParent = nullptr);   // standard constructor
	virtual ~VehicleLookupScreen();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_VEHICLE_LOOKUP_SCREEN };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CEdit m_vin;
	CComboBox m_make;
	CComboBox m_model;
	CComboBox m_year;

	// The model array will contain lists of models that are associated with each make
	CArray<CList<CString, CString&>, CList<CString, CString&>> modelArray;



	afx_msg void OnCbnSelchangeVehicleLookupMake();
	};
