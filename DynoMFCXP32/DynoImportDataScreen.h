#pragma once


// DynoImportDataScreen dialog

class DynoImportDataScreen : public CDialogEx
{
	DECLARE_DYNAMIC(DynoImportDataScreen)

public:
	DynoImportDataScreen(CWnd* pParent = nullptr);   // standard constructor
	virtual ~DynoImportDataScreen();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ImportDataScreen };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:

	// Variables for the calling function
	int N;
	int M;
	int Rho;

	// Internal

	CEdit m_N;
	CEdit m_M;
	CEdit m_Rho;
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	};
