#pragma once
#include <afxwin.h>

#ifndef ROUND
#define ROUND(x) (int)((x) + 0.5 - (double)((x) < 0))
#endif

#define CIRC_BOUNDARY_POINTS 100

/////////////////////////////////////////////////////////////////////////////
// CCircularMeterCtrl window


class CCircularMeterCtrl : public CStatic
	{
	// Construction
	public:
		CCircularMeterCtrl();

		// Attributes
	protected:
		int m_nBottomCX;
		int m_nBottomCY;
		int m_nBottomRadius;
		int m_nTopRadius;
		int m_nValueFontHeight;
		int m_nValueBaseline;
		int m_nValueCenter;

		double m_dLeftAngleRad;
		double m_dRightAngleRad;
		double m_dMaxValue;
		double m_dMinValue;
		double m_dCurrentValue;

		double m_needleScaler = 0.0;

		CRect m_rectFace;

		CRect m_rectCtrl;
		CRect m_rectValue;
		CRgn m_rgnBoundary;

		CFont m_fontValue;


		COLORREF m_windowColour;
		COLORREF m_graphNameColour;
		COLORREF m_faceBorderColour;

		COLORREF m_valueTextColour;
		COLORREF m_valueBackgroundColour;
		COLORREF m_valueBorderHighlite;
		COLORREF m_valueBorderShadow;

		COLORREF m_tickMarkColour;
		COLORREF m_tickLabelColour;
		COLORREF m_colorNeedle;



		CString m_strMeterName;
		int m_nScaleDecimals;
		int m_nValueDecimals;
		int tickValues[9] = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

		CDC m_dcBackground;

		CBitmap* m_pBitmapOldBackground;
		CBitmap m_bitmapBackground;

		bool drawNumberBorder = true;

		// Protected operations
		void DrawMeterBackground(CDC* pDC, CRect& rect);
		void DrawNeedle(CDC* pDC);
		void DrawValue(CDC* pDC);
		void ReconstructControl();

	public:

		// Operations
	public:

		// Range, value detail, name
		void SetName(CString& strMeterName);
		void SetRange(double dMin, double dMax);
		void SetScaleDecimals(int nDecimals);
		void SetValueDecimals(int nDecimals);
		void SetTickValues(int* newValues);

		// Colours
		void SetWindowColour(COLORREF newColour);
		void SetGraphNameColour(COLORREF newColour);
		void SetFaceBorderColour(COLORREF newColour);
		void SetValueColour(COLORREF newTextColour, COLORREF newBackGroundColour = RGB(0, 0, 0));
		void SetValueBorderColours(COLORREF highlight, COLORREF shadow);

		void SetTickMarkColour(COLORREF newColour);
		void SetTickLabelColour(COLORREF newColour);
		void SetNeedleColour(COLORREF newColour);



		// Updates
		void UpdateNeedle(double dValue);

		void SetDrawNumberBorder(bool choice)
			{
			drawNumberBorder = choice;
			ReconstructControl();
			}

		// Set the needler scaler (default to none)
		void SetNeedleScaler(double dValue = 0.0)
			{
			m_needleScaler = dValue;
			}

		bool GetDrawNumberBorder()
			{
			return drawNumberBorder;
			}

		double GetCurrentValue()
			{
			return m_dCurrentValue;
			}

		// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(C3DMeterCtrl)
	//}}AFX_VIRTUAL

// Implementation
	public:
		virtual ~CCircularMeterCtrl();

		// Generated message map functions
	protected:
		//{{AFX_MSG(C3DMeterCtrl)
		afx_msg void OnPaint();
		afx_msg void OnSize(UINT nType, int cx, int cy);
		//}}AFX_MSG

		DECLARE_MESSAGE_MAP()
	public:



	};



/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
