// DynoMasterConfigurationScreen.cpp : implementation file
//

#include "pch.h"
#include "DynoMFCXP32.h"
#include "DynoMasterConfigurationScreen.h"
#include "afxdialogex.h"


// DynoMasterConfigurationScreen dialog

IMPLEMENT_DYNAMIC(DynoMasterConfigurationScreen, CDialogEx)

DynoMasterConfigurationScreen::DynoMasterConfigurationScreen(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CONFIGURATION_SCREEN, pParent)
{

}

DynoMasterConfigurationScreen::~DynoMasterConfigurationScreen()
{
}

void DynoMasterConfigurationScreen::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CONFIGURATION_TREE, m_configTreeCtrl);
	DDX_Control(pDX, IDOK, m_saveButton);
	DDX_Control(pDX, IDC_CONFIGURATION_DEFAULTS, m_defaultsButton);
	DDX_Control(pDX, IDCANCEL, m_cancelButton);
	DDX_Control(pDX, IDC_CONFIGURATION_RESET, m_resetButton);
	DDX_Control(pDX, IDC_CONFIGURATION_VALUE, m_parameter);
	}


BEGIN_MESSAGE_MAP(DynoMasterConfigurationScreen, CDialogEx)
	ON_NOTIFY(TVN_SELCHANGED, IDC_CONFIGURATION_TREE, &DynoMasterConfigurationScreen::OnTvnSelchangedConfigurationTree)
	ON_BN_CLICKED(IDC_CONFIGURATION_RESET, &DynoMasterConfigurationScreen::OnBnClickedConfigurationReset)
	ON_EN_SETFOCUS(IDC_CONFIGURATION_VALUE, &DynoMasterConfigurationScreen::OnEnSetfocusConfigurationValue)
	ON_EN_KILLFOCUS(IDC_CONFIGURATION_VALUE, &DynoMasterConfigurationScreen::OnEnKillfocusConfigurationValue)
	ON_BN_CLICKED(IDC_CONFIGURATION_DEFAULTS, &DynoMasterConfigurationScreen::OnBnClickedConfigurationDefaults)
	ON_EN_CHANGE(IDC_CONFIGURATION_VALUE, &DynoMasterConfigurationScreen::OnEnChangeConfigurationValue)
END_MESSAGE_MAP()


// DynoMasterConfigurationScreen message handlers


BOOL DynoMasterConfigurationScreen::OnInitDialog()
	{
	CDialogEx::OnInitDialog();

	// Before doing anything, make a backup of the config so that we can restore it without exiting
	backupCfg = localCfg;

	// Set up the configuration screen

	TVINSERTSTRUCT tvInsert;
	tvInsert.hParent = NULL;
	tvInsert.hInsertAfter = NULL;
	tvInsert.item.mask = TVIF_TEXT;
	tvInsert.item.pszText = _T("Hardware options");

	HTREEITEM newItem;
	std::shared_ptr<itemMap> newMap;
	int addedCount = 0;

	HTREEITEM hardwareOptions = m_configTreeCtrl.InsertItem(&tvInsert);

	// Create the new item dynamically and add it to the vector.
	// We don't have to worry about the index in the vector because the only concern
	// is that it won't be deleted.
	newItem = m_configTreeCtrl.InsertItem(_T("Shaft diameter"), hardwareOptions, TVI_SORT);
	newMap = std::make_shared<itemMap>();
	newMap->setItem(itemMap::doubleValue, (DWORD_PTR)&localCfg.dynoShaftDiameter);
	items.push_back(newMap);
	m_configTreeCtrl.SetItemData(newItem, (DWORD_PTR)newMap.get());

	newItem = m_configTreeCtrl.InsertItem(_T("Engine RPM multiplier"), hardwareOptions, TVI_SORT);
	newMap = std::make_shared<itemMap>();
	newMap->setItem(itemMap::doubleValue, (DWORD_PTR)&localCfg.dynoToEngineRPMMultiplier);
	items.push_back(newMap);
	m_configTreeCtrl.SetItemData(newItem, (DWORD_PTR)newMap.get());

	newItem = m_configTreeCtrl.InsertItem(_T("Inertia constant (Front wheel drive - front drum)"), hardwareOptions, TVI_SORT);
	newMap = std::make_shared<itemMap>();
	newMap->setItem(itemMap::doubleValue, (DWORD_PTR)&localCfg.inertiaConstant_FWD);
	items.push_back(newMap);
	m_configTreeCtrl.SetItemData(newItem, (DWORD_PTR)newMap.get());

	newItem = m_configTreeCtrl.InsertItem(_T("Inertia constant (Rear wheel drive - rear drum)"), hardwareOptions, TVI_SORT);
	newMap = std::make_shared<itemMap>();
	newMap->setItem(itemMap::doubleValue, (DWORD_PTR)&localCfg.inertiaConstant_RWD);
	items.push_back(newMap);
	m_configTreeCtrl.SetItemData(newItem, (DWORD_PTR)newMap.get());

	newItem = m_configTreeCtrl.InsertItem(_T("Inertia constant (All wheel drive)"), hardwareOptions, TVI_SORT);
	newMap = std::make_shared<itemMap>();
	newMap->setItem(itemMap::doubleValue, (DWORD_PTR)&localCfg.inertiaConstant_AWD);
	items.push_back(newMap);
	m_configTreeCtrl.SetItemData(newItem, (DWORD_PTR)newMap.get());

	newItem = m_configTreeCtrl.InsertItem(_T("Load cell configuration - Front brake"), hardwareOptions, TVI_SORT);
	newMap = std::make_shared<itemMap>();
	newMap->setItem(itemMap::doubleValue, (DWORD_PTR)&localCfg.loadCell_brake_front_calibration);
	items.push_back(newMap);
	m_configTreeCtrl.SetItemData(newItem, (DWORD_PTR)newMap.get());

	newItem = m_configTreeCtrl.InsertItem(_T("Load cell configuration - Rear brake"), hardwareOptions, TVI_SORT);
	newMap = std::make_shared<itemMap>();
	newMap->setItem(itemMap::doubleValue, (DWORD_PTR)&localCfg.loadCell_brake_rear_calibration);
	items.push_back(newMap);
	m_configTreeCtrl.SetItemData(newItem, (DWORD_PTR)newMap.get());


	HTREEITEM communications = m_configTreeCtrl.InsertItem(TVIF_TEXT,
		_T("Communications"), 0, 0, 0, 0, 0, NULL, NULL);

	newItem = m_configTreeCtrl.InsertItem(_T("TD size"), communications, TVI_SORT);
	newMap = std::make_shared<itemMap>();
	newMap->setItem(itemMap::integer, (DWORD_PTR)&localCfg.timeDeltaByteLength);
	items.push_back(newMap);
	m_configTreeCtrl.SetItemData(newItem, (DWORD_PTR)newMap.get());

	newItem = m_configTreeCtrl.InsertItem(_T("Buffer size"), communications, TVI_SORT);
	newMap = std::make_shared<itemMap>();
	newMap->setItem(itemMap::integer, (DWORD_PTR)&localCfg.comReadBufferSize);
	items.push_back(newMap);
	m_configTreeCtrl.SetItemData(newItem, (DWORD_PTR)newMap.get());

	HTREEITEM eventSystem = m_configTreeCtrl.InsertItem(TVIF_TEXT,
		_T("Event system"), 0, 0, 0, 0, 0, NULL, NULL);

	newItem = m_configTreeCtrl.InsertItem(_T("Event queue limit"), eventSystem, TVI_SORT);
	newMap = std::make_shared<itemMap>();
	newMap->setItem(itemMap::integer, (DWORD_PTR)&localCfg.incomingEventQueueLimit);
	items.push_back(newMap);
	m_configTreeCtrl.SetItemData(newItem, (DWORD_PTR)newMap.get());

	newItem = m_configTreeCtrl.InsertItem(_T("Microseconds per tick"), eventSystem, TVI_SORT);
	newMap = std::make_shared<itemMap>();
	newMap->setItem(itemMap::doubleValue, (DWORD_PTR)&localCfg.tickMultiplier);
	items.push_back(newMap);
	m_configTreeCtrl.SetItemData(newItem, (DWORD_PTR)newMap.get());

	newItem = m_configTreeCtrl.InsertItem(_T("Smoothing window size"), eventSystem, TVI_SORT);
	newMap = std::make_shared<itemMap>();
	newMap->setItem(itemMap::integer, (DWORD_PTR)&localCfg.smoothingWindowSize);
	items.push_back(newMap);
	m_configTreeCtrl.SetItemData(newItem, (DWORD_PTR)newMap.get());
	setModified(false);


	// Expand all of the root nodes
	m_configTreeCtrl.Expand(hardwareOptions, TVE_EXPAND);
	m_configTreeCtrl.Expand(communications, TVE_EXPAND);
	m_configTreeCtrl.Expand(eventSystem, TVE_EXPAND);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
	}

// When the selection is changed, display it
void DynoMasterConfigurationScreen::OnTvnSelchangedConfigurationTree(NMHDR* pNMHDR, LRESULT* pResult)
	{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	
	
	// Grab the selected item

	HTREEITEM selected = m_configTreeCtrl.GetSelectedItem();
	DWORD_PTR mapPtr = m_configTreeCtrl.GetItemData(selected);
	CString display;
	bool editable = true;

	// If the value was changed and control came here before the killfocus() then
	// go over there temporarily


	if (mapPtr)
		{
		itemMap* map = (itemMap*)mapPtr;
		wchar_t* charValue;
		bool* boolValue;
		int* intValue;
		long* longValue;
		double* doubleValue;

		// Depending on the type of data, display it according to the appropriate format
		switch (map->type)
			{
			case itemMap::boolean:

				boolValue = (bool*)map->itemPtr;
				display.Format(_T("%s"), *boolValue ? _T("True") : _T("False"));
				break;

			case itemMap::character:

				charValue = (wchar_t*)map->itemPtr;
				display.Format(_T("%c"), *charValue);
				break;

			case itemMap::integer:

				intValue = (int*)map->itemPtr;
				display.Format(_T("%d"), *intValue);
				break;

			case itemMap::longInt:

				longValue = (long*)map->itemPtr;
				display.Format(_T("%l"), *longValue);
				break;

			case itemMap::doubleValue:

				doubleValue = (double*)map->itemPtr;
				display.Format(_T("%lf"), *doubleValue);
				break;

			default:

				display = _T("Error");
				editable = false;
			}
		}
	else
		editable = false;

	// Display the parameter
	m_parameter.SetWindowTextW(display);
	m_parameter.EnableWindow(editable);

	*pResult = 0;
	}

// Set the modified flag
void DynoMasterConfigurationScreen::setModified(bool modFlag)
	{
	modified = modFlag;

	/*
	// Modify the controls depending on the modified status
	if (modified)
		{


		}
	else
		{


		}
	*/

	m_cancelButton.EnableWindow(modified);
	m_resetButton.EnableWindow(modified);
	}


// Undo the changes that have been made so far
void DynoMasterConfigurationScreen::OnBnClickedConfigurationReset()
	{
	// Restore all of the values from the backup
	localCfg = backupCfg;

	m_parameter.SetWindowTextW(_T(""));
	m_configTreeCtrl.SelectSetFirstVisible(m_configTreeCtrl.GetRootItem());
	}


void DynoMasterConfigurationScreen::OnEnSetfocusConfigurationValue()
	{
	m_parameter.GetWindowTextW(holdString);
	valueChanged = false;
	}


void DynoMasterConfigurationScreen::OnEnKillfocusConfigurationValue()
	{
	CString newValue;

	m_parameter.GetWindowTextW(newValue);		// Get the current value

	// If it's not the same as it was when we entered the dialog then deal with it
	if (newValue.Compare(holdString))
		{
		// Depending on which item it is, we have to point to the correct variable in the configuration.
		// I'm assuming that it is valid because the box shouldn't be editable (or selectable, even) if the item isn't valid.

		// Grab the selected item

		HTREEITEM selected = m_configTreeCtrl.GetSelectedItem();
		DWORD_PTR mapPtr = m_configTreeCtrl.GetItemData(selected);
		CString display;

		if (mapPtr)
			{
			itemMap* map = (itemMap*)mapPtr;
			wchar_t* charValue;
			bool* boolValue;
			int* intValue;
			long* longValue;
			double* doubleValue;

			// Depending on the type of data, display it according to the appropriate format
			switch (map->type)
				{
				case itemMap::boolean:

					// TODO: There is no support for boolean updates at this time (because we don't have any)
					boolValue = (bool*)map->itemPtr;
					*boolValue = (newValue.CompareNoCase(_T("True")) == 0);

					break;

				case itemMap::character:

					charValue = (wchar_t*)map->itemPtr;
					
					if (newValue.GetLength())
						*charValue = newValue.GetAt(0);
					else
						*charValue = 0;
					break;

				case itemMap::integer:

					intValue = (int*)map->itemPtr;
					*intValue = _wtoi(newValue.GetBuffer());
					break;

				case itemMap::longInt:

					longValue = (long*)map->itemPtr;
					*longValue = _wtol(newValue.GetBuffer());					
					break;

				case itemMap::doubleValue:

					doubleValue = (double*)map->itemPtr;
					*doubleValue = _wtof(newValue.GetBuffer());
					break;

				default:
					// Log this if it happens
					break;
				}
			}

		setModified(true);
		}
	}

// Load all of the defaults
void DynoMasterConfigurationScreen::OnBnClickedConfigurationDefaults()
	{
	localCfg.resetDefaults();

	setModified(true);
	m_parameter.SetWindowTextW(_T(""));
	m_configTreeCtrl.SelectSetFirstVisible(m_configTreeCtrl.GetRootItem());
	}


void DynoMasterConfigurationScreen::OnEnChangeConfigurationValue()
	{
	// It is necessary to make note of changes here because when you change the tree selection,
	// it calls the next tree update before the killfocus() for this field.





	}
