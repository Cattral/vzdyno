#pragma once


// DynoMasterConfigurationScreen dialog

struct itemMap
	{
	enum datatype {boolean, character, integer, longInt, doubleValue};

	datatype type;
	DWORD_PTR itemPtr;

	void setItem(enum datatype newType, DWORD_PTR newPtr)
		{
		type = newType;
		itemPtr = newPtr;
		}
	};


class DynoMasterConfigurationScreen : public CDialogEx
{
	DECLARE_DYNAMIC(DynoMasterConfigurationScreen)

public:
	DynoMasterConfigurationScreen(CWnd* pParent = nullptr);   // standard constructor
	virtual ~DynoMasterConfigurationScreen();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CONFIGURATION_SCREEN };
#endif


	DynoConfig localCfg, backupCfg;

	// Return the value of the modified flag
	bool isModified()
		{
		return modified;
		}


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	std::vector<std::shared_ptr<itemMap>> items;	// A vector of the maps for the items
	CString holdString;								// Temp string used to see if the value has changed
	bool modified;									// Keep track of whether anything has been changed
	
	// These two variable are necessary because if you click on another item in the tree control,
	// the itemchanged() message is called before the killfocus() message on edit box.
	bool valueChanged;
	DWORD_PTR oldValueLocation;					

	void setModified(bool modFlag);


	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CTreeCtrl m_configTreeCtrl;
	afx_msg void OnTvnSelchangedConfigurationTree(NMHDR* pNMHDR, LRESULT* pResult);
	CButton m_saveButton;
	CButton m_defaultsButton;
	CButton m_cancelButton;
	CButton m_resetButton;
	CEdit m_parameter;
	afx_msg void OnBnClickedConfigurationReset();
	afx_msg void OnEnSetfocusConfigurationValue();
	afx_msg void OnEnKillfocusConfigurationValue();
	afx_msg void OnBnClickedConfigurationDefaults();
	afx_msg void OnEnChangeConfigurationValue();
	};
