#pragma once
#include <afx.h>

// This class is used to store the Dyno Configuration data
class DynoConfig : public CObject
	{
	public:


	DynoConfig();		// Constructor
	~DynoConfig();		// Destructor

	DynoConfig& operator=(const DynoConfig &cfg);


	// Non-stored variables
	CString currentFilePath;
	COleDateTime timeLoaded;
	bool updatedSinceSave;

	// Serialized variables

	// Administrative
	int cfgVersionNumber = 1;			// NOTE: Update this number if the config structure changes

	// Dyno-hardware specific
	double dynoShaftDiameter;			// The diameter of the dyno shaft in inches
	double dynoToEngineRPMMultiplier;	//

	// Dates
	COleDateTime createdDate;
	COleDateTime lastUpdatedDate;

	// Colours
	CMap<int, int, COLORREF, COLORREF> colourRefsMap;

	// Graph and chart settings
	bool showCircularMeterValueBorders;
	int units;
	int legend;

	// Speedometer
	int speedometerMax;
	int speedometerRedline;

	// Tachometer
	int rpmMax;
	int tachometerRedline;

	// Volt meter
	bool showVoltMeter;

	// AFR Ratio Value
	double AFRValue;

	// Main chart (either 'HP/Torque to RPM chart' or 'RPM/Speed to Time chart') 
	bool autoScrollChart;

	// HP/Torque to RPM chart
	bool showGrid_hpTorqueRpm;
	bool rpmXAxisVisible;

	// RPM/Speed to Time chart
	bool showGrid_rpmSpeedTime;
	bool timeAxisVisible;
	double timeAxisMax;

	// Torque
	double torqueMax;
	int torqueLineWidth;
	int torqueLineType;
	bool torqueAxisVisible;
	double inertiaConstant_FWD;		// Used to convert angular velocity (dO/dt) to torque Nm (front drum only)
	double inertiaConstant_RWD;		// Used to convert angular velocity (dO/dt) to torque Nm (rear drum only)
	double inertiaConstant_AWD;		// Used to convert angular velocity (dO/dt) to torque Nm (all wheel drive)

	// Horsepower
	double horsepowerMax;
	int hpLineWidth;
	int hpLineType;
	bool hpAxisVisible;

	// Rpm (series line)
	int rpmLineWidth;
	int rpmLineType;
	bool rpmYAxisVisible;

	// Speed (series line)
	int speedLineWidth;
	int speedLineType;
	bool speedAxisVisible;

	// Dyno hardware
	double loadCell_brake_front_calibration;
	double loadCell_brake_rear_calibration;

	// COM Port settings
	bool autoPortScan = false;
	bool autoStartPolling = false;			// Indicates port connection should be established on start
	int pollingInterval;
	CString idString;						// The Arduino identification string
	int comReadBufferSize;					// The maximum number of bytes read from the COM port at one time
	int incomingEventQueueLimit;			// The maximum number of Events to hold before pausing COM read
	double tickMultiplier;					// The number of microseconds per tick (varies based on prescaler)
	int timeDeltaByteLength;				// The number of bytes (i.e. 4 for 32-bit value) for a time delta coming from Arduino or file import

	DWORD baudRate;							// Baud rate
	int parity;								// Parity
	int wordLength;							// Word length
	int stopBits;							// Stop bits
	int flowControl;						// Flow control

	int smoothingWindowSize;				// The number of time deltas in the smoothing window

	// Authorization options
	bool autoAuthorize;						// Indicates server authorization should be attempted on start

	// Replay options
	bool showReplayTrackbar = true;			// Indicates that the trackbar should be shown during replay session

	public:

		bool isLoaded();
		void resetDefaults();

		void Serialize(CArchive& ar);
		DECLARE_SERIAL(DynoConfig);

	};

