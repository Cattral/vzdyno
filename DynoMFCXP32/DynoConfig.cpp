#include "pch.h"
#include "DynoConfig.h"

#include "DynoMFCXP32.h"

IMPLEMENT_SERIAL(DynoConfig, CObject,  VERSIONABLE_SCHEMA | 1)
DynoConfig::DynoConfig()
	{
	// Set the configuration load time to 0 (December 30, 1899, midnight) as a "not loaded" value
	timeLoaded.m_dt = 0;

	}

DynoConfig::~DynoConfig()
	{


	}

// Assignment/copy constructor
DynoConfig& DynoConfig::operator=(const DynoConfig &cfg)
	{

	// Start by checking for self-assignment
	if (&cfg == this)
		return *this;

	// Copy the non-stored variables
	currentFilePath = cfg.currentFilePath;
	timeLoaded = cfg.timeLoaded;
	updatedSinceSave = cfg.updatedSinceSave;

	// Administrative
	cfgVersionNumber = cfg.cfgVersionNumber;

	// Dyno-hardware specific
	dynoShaftDiameter = cfg.dynoShaftDiameter;
	dynoToEngineRPMMultiplier = cfg.dynoToEngineRPMMultiplier;

	// Dates
	createdDate = cfg.createdDate;
	lastUpdatedDate = cfg.lastUpdatedDate;

	// Colour map.
	// Point to the first element in the map (order is arbitrary)
	POSITION pos = cfg.colourRefsMap.GetStartPosition();
	int keyValue;
	COLORREF colour;

	// Roll through the items in the colour map and archive them
	while (pos != NULL)
		{
		cfg.colourRefsMap.GetNextAssoc(pos, keyValue, colour);		// Retrieve the key and colour
		colourRefsMap.SetAt(keyValue, colour);						// Insert the element
		}

	// Graph and chart settings
	showCircularMeterValueBorders = cfg.showCircularMeterValueBorders;
	units = cfg.units;
	legend = cfg.legend;

	// Speedometer
	speedometerMax = cfg.speedometerMax;
	speedometerRedline = cfg.speedometerRedline;

	// Tachometer
	rpmMax = cfg.rpmMax;
	tachometerRedline = cfg.tachometerRedline;

	//Volt Meter Display
	showVoltMeter = cfg.showVoltMeter;

	// AFR Value
	AFRValue = cfg.AFRValue;

	// Main chart (either 'HP/Torque to RPM chart' or 'RPM/Speed to Time chart') 
	autoScrollChart = cfg.autoScrollChart;

	// HP/Torque to RPM chart
	showGrid_hpTorqueRpm = cfg.showGrid_hpTorqueRpm;
	rpmXAxisVisible = cfg.rpmXAxisVisible;
	inertiaConstant_FWD = cfg.inertiaConstant_FWD;
	inertiaConstant_RWD = cfg.inertiaConstant_RWD;
	inertiaConstant_AWD = cfg.inertiaConstant_AWD;

	// Horsepower
	horsepowerMax = cfg.horsepowerMax;
	hpLineWidth = cfg.hpLineWidth;
	hpLineType = cfg.hpLineType;
	hpAxisVisible = cfg.hpAxisVisible;

	// Torque
	torqueMax = cfg.torqueMax;
	torqueLineWidth = cfg.torqueLineWidth;
	torqueLineType = cfg.torqueLineType;
	torqueAxisVisible = cfg.torqueAxisVisible;

	// RPM/Speed to Time chart
	showGrid_rpmSpeedTime = cfg.showGrid_rpmSpeedTime;
	timeAxisVisible = cfg.timeAxisVisible;
	timeAxisMax = cfg.timeAxisMax;

	// RPM (series line)
	rpmLineWidth = cfg.rpmLineWidth;
	rpmLineType = cfg.rpmLineType;
	rpmYAxisVisible = cfg.rpmYAxisVisible;

	// Speed (series line)
	speedLineWidth = cfg.speedLineWidth;
	speedLineType = cfg.speedLineType;
	speedAxisVisible = cfg.speedAxisVisible;

	// Dyno hardware
	loadCell_brake_front_calibration = cfg.loadCell_brake_front_calibration;
	loadCell_brake_rear_calibration = cfg.loadCell_brake_rear_calibration;

	// COM Port settings
	autoPortScan = cfg.autoPortScan;
	autoStartPolling = cfg.autoStartPolling;
	pollingInterval = cfg.pollingInterval;
	idString = cfg.idString;
	comReadBufferSize = cfg.comReadBufferSize;
	incomingEventQueueLimit = cfg.incomingEventQueueLimit;
	tickMultiplier = cfg.tickMultiplier;
	timeDeltaByteLength = cfg.timeDeltaByteLength;

	baudRate = cfg.baudRate;
	parity = cfg.parity;
	wordLength = cfg.wordLength;
	stopBits = cfg.stopBits;
	flowControl = cfg.flowControl;

	smoothingWindowSize = cfg.smoothingWindowSize;

	// Authorization options
	autoAuthorize = cfg.autoAuthorize;

	return *this;
	}

// Return true if the configuration has been loaded
bool DynoConfig::isLoaded()
	{
	return(timeLoaded.m_dt > 0);
	}

void DynoConfig::Serialize(CArchive& ar)
	{
	POSITION pos;
	int colourMapCount;
	int keyValue;
	COLORREF colour;

	// Saving versus loading data
	if (ar.IsStoring())
		{
		// Version
		ar << cfgVersionNumber;
		
		// Dyno-hardware specific
		ar << dynoShaftDiameter;
		ar << dynoToEngineRPMMultiplier;

		// Dates
		ar << createdDate;
		ar << lastUpdatedDate;

		// Colours
		colourMapCount = colourRefsMap.GetCount();
		ar << colourMapCount;		// Save the count first
		
		// Point to the first element in the map (order is arbitrary)
		pos = colourRefsMap.GetStartPosition();

		// Roll through the items in the colour map and archive them
		while(pos != NULL)
			{
			colourRefsMap.GetNextAssoc(pos, keyValue, colour);		// Retrieve the key and colour
			ar << keyValue << colour;
			}

		// Graph and chart settings
		ar << showCircularMeterValueBorders;
		ar << units;
		ar << legend;

		// Speedometer
		ar << speedometerMax;
		ar << speedometerRedline;

	
		// Tachometer
		ar << rpmMax;
		ar << tachometerRedline;

		// Volt Meter
		ar << showVoltMeter;

		//AFR Value
		ar << AFRValue;

		// Main chart (either 'HP/Torque to RPM chart' or 'RPM/Speed to Time chart') 
		ar << autoScrollChart;

		// HP/Torque to RPM chart
		ar << showGrid_hpTorqueRpm;
		ar << rpmXAxisVisible;

		// RPM/Speed to Time chart
		ar << showGrid_rpmSpeedTime;
		ar << timeAxisVisible;
		ar << timeAxisMax;

		// Horsepower
		ar << horsepowerMax;
		ar << hpLineWidth;
		ar << hpLineType;
		ar << hpAxisVisible;

		// Torque
		ar << torqueMax;
		ar << torqueLineWidth;
		ar << torqueLineType;
		ar << torqueAxisVisible;
		ar << inertiaConstant_FWD;
		ar << inertiaConstant_RWD;
		ar << inertiaConstant_AWD;

		// RPM (Series line)
		ar << rpmLineWidth;
		ar << rpmLineType;
		ar << rpmYAxisVisible;

		// Speed (Series line)
		ar << speedLineWidth;
		ar << speedLineType;
		ar << speedAxisVisible;

		// Dyno hardware
		ar << loadCell_brake_front_calibration;
		ar << loadCell_brake_rear_calibration;

		// COM Port settings
		ar << autoPortScan;
		ar << autoStartPolling;
		ar << pollingInterval;
		ar << idString;
		ar << comReadBufferSize;
		ar << incomingEventQueueLimit;
		ar << tickMultiplier;
		ar << timeDeltaByteLength;

		ar << baudRate;
		ar << parity;
		ar << wordLength;
		ar << stopBits;
		ar << flowControl;

		ar << smoothingWindowSize;

		// Authorization options
		ar << autoAuthorize;
		}
	else
		{
		// Version
		ar >> cfgVersionNumber;

		// Dyno-hardware specific
		ar >> dynoShaftDiameter;
		ar >> dynoToEngineRPMMultiplier;

		// Dates
		ar >> createdDate;
		ar >> lastUpdatedDate;

		// Colours
		ar >> colourMapCount;		// Save the count first

		// Roll through the items in the archive and build the colour map
		for(int index=0; index < colourMapCount; index++)
			{
			ar >> keyValue;			// Retrieve the key value
			ar >> colour;			// Retrieve the colour code

			colourRefsMap.SetAt(keyValue, colour);	// Insert the element
			}

		// Graph and chart settings
		ar >> showCircularMeterValueBorders;
		ar >> units;
		ar >> legend;

		// Speedometer
		ar >> speedometerMax;
		ar >> speedometerRedline;
				
		// Tachometer
		ar >> rpmMax;
		ar >> tachometerRedline;


		//Voltmeter
		ar >> showVoltMeter;

		ar >> AFRValue;

		// Main chart (either 'HP/Torque to RPM chart' or 'RPM/Speed to Time chart') 
		ar >> autoScrollChart;

		// HP/Torque to RPM chart
		ar >> showGrid_hpTorqueRpm;
		ar >> rpmXAxisVisible;

		// RPM/Speed to Time chart
		ar >> showGrid_rpmSpeedTime;
		ar >> timeAxisVisible;
		ar >> timeAxisMax;

		// Horsepower
		ar >> horsepowerMax;
		ar >> hpLineWidth;
		ar >> hpLineType;
		ar >> hpAxisVisible;

		// Torque
		ar >> torqueMax;
		ar >> torqueLineWidth;
		ar >> torqueLineType;
		ar >> torqueAxisVisible;
		ar >> inertiaConstant_FWD;
		ar >> inertiaConstant_RWD;
		ar >> inertiaConstant_AWD;

		// RPM (Series line)
		ar >> rpmLineWidth;
		ar >> rpmLineType;
		ar >> rpmYAxisVisible;

		// Speed (Series line)
		ar >> speedLineWidth;
		ar >> speedLineType;
		ar >> speedAxisVisible;

		// Dyno hardware
		ar >> loadCell_brake_front_calibration;
		ar >> loadCell_brake_rear_calibration;

		// COM Port settings
		ar >> autoPortScan;
		ar >> autoStartPolling;
		ar >> pollingInterval;
		ar >> idString;
		ar >> comReadBufferSize;
		ar >> incomingEventQueueLimit;
		ar >> tickMultiplier;
		ar >> timeDeltaByteLength;

		ar >> baudRate;
		ar >> parity;
		ar >> wordLength;
		ar >> stopBits;
		ar >> flowControl;

		ar >> smoothingWindowSize;

		// Authorization options
		ar >> autoAuthorize;
		}
	}

// Fill the class with the default values
void DynoConfig::resetDefaults()
	{
	// Dyno-hardware specific
	dynoShaftDiameter = 32.0;				// 32" diameter shaft (used to calculate speed in MPH)
	dynoToEngineRPMMultiplier = 10.0;		// 10x ratio between the Dyno RPM and the Engine RPM 

	// Set up the default colours here
	colourRefsMap.SetAt(Constants.colour_target_Background, RGB(50, 10, 0));					// Reddish-black
	colourRefsMap.SetAt(Constants.colour_target_Labels, RGB(237, 7, 230));						// Chris' purple
	colourRefsMap.SetAt(Constants.colour_target_Graph_names, RGB(250, 0, 0));					// Very red
	colourRefsMap.SetAt(Constants.colour_target_Meter_FaceBorder, RGB(0, 255, 255));
	colourRefsMap.SetAt(Constants.colour_target_Meter_Tickmark, RGB(0, 155, 155));
	colourRefsMap.SetAt(Constants.colour_target_Meter_valueText, RGB(0, 225, 15));
	colourRefsMap.SetAt(Constants.colour_target_Meter_valueBorderHighlite, RGB(0, 255, 0));		// Full green
	colourRefsMap.SetAt(Constants.colour_target_Meter_valueBorderShadow, RGB(20, 200, 20));		// Greenish shadow
	colourRefsMap.SetAt(Constants.colour_target_SpdNeedleNormal, RGB(0, 255, 10));				// Mostly green
	colourRefsMap.SetAt(Constants.colour_target_SpdNeedleHigh, RGB(250, 10, 10));
	colourRefsMap.SetAt(Constants.colour_target_TachNeedleNormal, RGB(0, 255, 10));				// Mostly green
	colourRefsMap.SetAt(Constants.colour_target_TachNeedleHigh, RGB(250, 10, 10));
	colourRefsMap.SetAt(Constants.colour_target_RichLeanFill, RGB(10, 255, 5));
	colourRefsMap.SetAt(Constants.colour_target_htrChart_border, RGB(20, 200, 20));				// Greenish shadow
	colourRefsMap.SetAt(Constants.colour_target_htrChart_background, RGB(50, 10, 0));			// Reddish-black
	colourRefsMap.SetAt(Constants.colour_target_htrChart_grid, RGB(255, 250, 255));
	colourRefsMap.SetAt(Constants.colour_target_htrChart_torqueAxisLabels, RGB(230, 7, 223));	// Close to Chris' purple
	colourRefsMap.SetAt(Constants.colour_target_htrChart_hpAxisLabels, RGB(230, 7, 223));
	colourRefsMap.SetAt(Constants.colour_target_htrChart_rpmAxisLabels, RGB(230, 7, 223));
	colourRefsMap.SetAt(Constants.colour_target_htrChart_torqueLine, RGB(20, 200, 20));
	colourRefsMap.SetAt(Constants.colour_target_htrChart_hpLine, RGB(200, 200, 200));
	colourRefsMap.SetAt(Constants.colour_target_htrChart_rpmLine, RGB(125, 250, 200));
	colourRefsMap.SetAt(Constants.colour_target_htrChart_speedLine, RGB(10, 200, 1750));


	// General meter options
	showCircularMeterValueBorders = true;
	units = Constants.Units_imperial;				// Default to imperial units
	legend = -1;									// Default to no legend

	// Speedometer
	speedometerMax = Constants.def_speedometer_max;
	speedometerRedline = Constants.def_speedometer_redline;

	// Tachometer
	rpmMax = Constants.def_tachometer_max;
	tachometerRedline = Constants.def_tachometer_redline;

	// Volt Meter
	showVoltMeter = false;

	// AFR Value
	AFRValue = 14.7;			// 14.7 is the value for gasoline

	// Main chart (either 'HP/Torque to RPM chart' or 'RPM/Speed to Time chart') 
	autoScrollChart = true;

	// HP/Torque to RPM chart
	showGrid_hpTorqueRpm = true;
	rpmXAxisVisible = true;
	
	// RPM/Speed to Time chart
	showGrid_rpmSpeedTime = true;
	timeAxisVisible = true;
	timeAxisMax = Constants.def_timeAxisMax;

	// Horsepower
	horsepowerMax = 500;
	hpLineWidth = 3;
	hpLineType = PS_SOLID;
	hpAxisVisible = true;

	// Torque
	torqueMax = 500;
	torqueLineWidth = 1;
	torqueLineType = PS_SOLID;
	torqueAxisVisible = true;
	inertiaConstant_FWD = 88.0;		// Front drum only - will change based on hardware
	inertiaConstant_RWD = 88.0;		// Rear drum only - will change based on hardware
	inertiaConstant_AWD = 184.0;	// Both drums + gear boxes and drive shaft - will change based on hardware

	// RPM (series line)
	rpmLineWidth = 1;
	rpmLineType = PS_SOLID;
	rpmYAxisVisible = true;

	// Speed (series line)
	speedLineWidth = 3;
	speedLineType = PS_SOLID;
	speedAxisVisible = true;

	// Dyno hardware
	loadCell_brake_front_calibration = 1.0;
	loadCell_brake_rear_calibration = 1.0;

	// COM Port settings
	autoPortScan = true;
	autoStartPolling = true;
	pollingInterval = 10;				// Default polling interval is 10ms
	idString.Empty();
	comReadBufferSize = 1500;
	incomingEventQueueLimit = 10000;
	tickMultiplier = 0.0625;			// Assume 62.5 nanoseconds
	timeDeltaByteLength = 4;			// 32-bit time delta is 4 bytes

	baudRate = 57600;
	parity = 0;
	wordLength = 8;
	stopBits = 1;
	flowControl = 0;

	smoothingWindowSize = 5;		// Default to 5 points for smoothing

	// Authorization options
	autoAuthorize = true;
	}


