
// ChildFrm.cpp : implementation of the CChildFrame class
//

#include "pch.h"
#include "framework.h"
#include "DynoMFCXP32.h"

#include "ChildFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
END_MESSAGE_MAP()

// CChildFrame construction/destruction

CChildFrame::CChildFrame() noexcept
{
	// TODO: add member initialization code here
}

CChildFrame::~CChildFrame()
{
}


BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
	{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
	}

// CChildFrame diagnostics

#ifdef _DEBUG
void CChildFrame::AssertValid() const
	{
	CMDIChildWnd::AssertValid();
	}

void CChildFrame::Dump(CDumpContext& dc) const
	{
	CMDIChildWnd::Dump(dc);
	}
#endif //_DEBUG

// CChildFrame message handlers

// Called when the MDI Child Frame is activated
void CChildFrame::ActivateFrame(int nCmdShow)
	{
	// Maximize the child window on startup
	nCmdShow = SW_SHOWMAXIMIZED;

	CMDIChildWnd::ActivateFrame(nCmdShow);
	}
