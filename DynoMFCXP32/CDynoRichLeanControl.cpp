#include "pch.h"
#include "CDynoRichLeanControl.h"
#include "MemDC.h"

CDynoRichLeanControl::CDynoRichLeanControl()
{

	
}

CDynoRichLeanControl::~CDynoRichLeanControl()
{

	if ((m_pBitmapOldBackground) &&
		(m_bitmapBackground.GetSafeHandle()) &&
		(m_dcBackground.GetSafeHdc()))
		m_dcBackground.SelectObject(m_pBitmapOldBackground);


}

BEGIN_MESSAGE_MAP(CDynoRichLeanControl, CStatic)
	//{{AFX_MSG_MAP(CCircularMeterCtrl)
	ON_WM_PAINT()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CDynoRichLeanControl::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// Find out how big we are
	GetClientRect(&m_rectCtrl);

	// make a memory dc
	COMemDC memDC(&dc, &m_rectCtrl);

	// Set up a memory DC for the background stuff if one isn't being used
	if ((m_dcBackground.GetSafeHdc() == NULL) || (m_bitmapBackground.m_hObject == NULL))
	{
		m_dcBackground.CreateCompatibleDC(&dc);
		m_bitmapBackground.CreateCompatibleBitmap(&dc, m_rectCtrl.Width(), m_rectCtrl.Height());
		m_pBitmapOldBackground = m_dcBackground.SelectObject(&m_bitmapBackground);

		// Fill this bitmap with the background.

		// Note: This requires some serious drawing and calculating, 
		// therefore it is drawn "once" to a bitmap and 
		// the bitmap is stored and blt'd when needed.
		DrawBackground(&m_dcBackground, m_rectCtrl);
	}

	memDC.BitBlt(0, 0, m_rectCtrl.Width(), m_rectCtrl.Height(), &m_dcBackground, 0, 0, SRCCOPY);



}

void CDynoRichLeanControl::OnSize(UINT nType, int cx, int cy)
{
	CStatic::OnSize(nType, cx, cy);

	ReconstructControl();
}

void CDynoRichLeanControl::DrawBackground(CDC* pDC, CRect& rect)
{
	int nAngleDeg, nRef;
	int nHeight;
	int nHalfPoints;
	int nStartAngleDeg, nEndAngleDeg;
	int nTickDeg;
	int nAngleIncrementDeg;


	double dTemp, dAngleRad, dX, dY;
	double dRadPerDeg;
	TEXTMETRIC tm;


	CString strTemp;

	CBrush brushFill, * pBrushOld;
	CFont* pFontOld;
	CPen penDraw, * pPenOld;

	//Fill the background
	brushFill.DeleteObject();
	brushFill.CreateSolidBrush(m_windowColour);			// Black
	pBrushOld = pDC->SelectObject(&brushFill);

	penDraw.DeleteObject();
	penDraw.CreatePen(PS_SOLID, 2, m_windowColour);
	pPenOld = pDC->SelectObject(&penDraw);
	pDC->Rectangle(rect);

	m_rectValue.left = rect.left;
	m_rectValue.top = rect.top;
	m_rectValue.right = rect.right;
	m_rectValue.bottom = rect.bottom;

	m_fontValue.DeleteObject();
	nHeight = m_rectValue.Height() * 0.65;
	m_fontValue.CreateFont(nHeight, 0, 0, 0, FW_EXTRABOLD,
		FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("Ariel"));

	pFontOld = pDC->SelectObject(&m_fontValue);
	pDC->GetTextMetrics(&tm);
	m_nValueFontHeight = tm.tmHeight;



	// Determine the location for the value within the rectangle based on the font height
	m_nValueBaseline = m_rectValue.bottom - m_nValueFontHeight / 2;
	m_nValueCenter = m_rectValue.left + m_rectValue.Width() / 2;

	

	if (m_dCurrentValue > 0)
	{
	
		brushFill.DeleteObject();
		brushFill.CreateSolidBrush(m_fillColour);
		if (m_strName == "RICH")
		{
			m_rectValue.left = m_rectCtrl.right - m_rectCtrl.right * (m_dCurrentValue / 100);
			m_rectValue.right = m_rectCtrl.right;
		}
		else
		{
			m_rectValue.left = m_rectCtrl.left;
			m_rectValue.right = m_rectCtrl.right - m_rectCtrl.right * ((100-m_dCurrentValue) / 100);

		}

		m_rectValue.top = m_rectCtrl.top;		
		m_rectValue.bottom = m_rectCtrl.bottom;
		pDC->FillRect(m_rectValue, &brushFill);

	}

	// Use the font to draw some text with the specified text and background colours
	// Set the colors (based on system colors)
	pDC->SetTextColor(m_graphNameColour);
	pDC->SetBkMode(TRANSPARENT);				// Use the same as the regular background


	// Draw the name of the meter below the face
	pDC->SetTextAlign(TA_CENTER | TA_BASELINE);
	pDC->TextOut(m_nValueCenter, m_nValueBaseline, m_strName);

	

	//m_rectValue.OffsetRect(0, 10);
	
}




void CDynoRichLeanControl::SetRange(double dMin, double dMax)
{
	m_dMaxValue = dMax;
	m_dMinValue = dMin;
	ReconstructControl();
}

void CDynoRichLeanControl::SetRichLeanValue(double fval)
{
	m_dCurrentValue = fval;
	ReconstructControl();
}



void CDynoRichLeanControl::SetName(CString& strMeterName)
{
	m_strName = strMeterName;
	ReconstructControl();
}

void CDynoRichLeanControl::SetWindowColour(COLORREF newColour)
{
	m_windowColour = newColour;
	ReconstructControl();
}

void CDynoRichLeanControl::SetFillColour(COLORREF newColour)
{
	m_fillColour = newColour;
	ReconstructControl();
}

void CDynoRichLeanControl::SetGraphNameColour(COLORREF newColour)
{
	m_graphNameColour = newColour;
	ReconstructControl();
}

void CDynoRichLeanControl::SetValueColour(COLORREF newTextColour, COLORREF newBackGroundColour)
{
	m_valueTextColour = newTextColour;
	m_valueBackgroundColour = newBackGroundColour;

	ReconstructControl();
}

void CDynoRichLeanControl::SetValueBorderColours(COLORREF highlight, COLORREF shadow)
{
	m_valueBorderHighlite = highlight;
	m_valueBorderShadow = shadow;

	ReconstructControl();
}

void CDynoRichLeanControl::ReconstructControl()
{
	// If we've got a stored background - remove it!
	if ((m_pBitmapOldBackground) &&
		(m_bitmapBackground.GetSafeHandle()) &&
		(m_dcBackground.GetSafeHdc()))
	{
		m_dcBackground.SelectObject(m_pBitmapOldBackground);
		m_dcBackground.DeleteDC();
		m_bitmapBackground.DeleteObject();
	}

	Invalidate();
}


