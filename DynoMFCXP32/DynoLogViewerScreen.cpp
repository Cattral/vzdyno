// DynoLogViewerScreen.cpp : implementation file
//

#include "pch.h"
#include "DynoMFCXP32.h"
#include "DynoLogViewerScreen.h"
#include "afxdialogex.h"


// DynoLogViewerScreen dialog

IMPLEMENT_DYNAMIC(DynoLogViewerScreen, CDialogEx)

DynoLogViewerScreen::DynoLogViewerScreen(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_LOG_VIEWER, pParent)
{

}

DynoLogViewerScreen::~DynoLogViewerScreen()
{
}

void DynoLogViewerScreen::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOG_VIEWER_LIST, m_list);
	}


BEGIN_MESSAGE_MAP(DynoLogViewerScreen, CDialogEx)
END_MESSAGE_MAP()


// DynoLogViewerScreen message handlers
