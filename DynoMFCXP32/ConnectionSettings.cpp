// ConnectionSettings.cpp : implementation file
//

#include "pch.h"
#include "DynoMFCXP32.h"
#include "ConnectionSettings.h"
#include "afxdialogex.h"


// ConnectionSettings dialog

IMPLEMENT_DYNAMIC(ConnectionSettings, CDialogEx)

ConnectionSettings::ConnectionSettings(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CONNECTION_SETTINGS, pParent)
{

}

ConnectionSettings::~ConnectionSettings()
{
}

void ConnectionSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CON_SETTINGS_AUTO_COM_SCAN, m_autoPortScan);
	DDX_Control(pDX, IDC_CON_SETTINGS_ID_STRING, m_idString);
	DDX_Control(pDX, IDC_CON_SETTINGS_TEST_BUTTON, m_testButton);
	DDX_Control(pDX, IDC_CON_SETTINGS_PORTLIST, m_portList);
	DDX_Control(pDX, IDC_CON_SETTINGS_POL_INT, m_pollInterval);
	}


BEGIN_MESSAGE_MAP(ConnectionSettings, CDialogEx)
	ON_BN_CLICKED(IDC_CON_SETTINGS_AUTO_COM_SCAN, &ConnectionSettings::OnBnClickedConSettingsAutoComScan)
	ON_EN_SETFOCUS(IDC_CON_SETTINGS_POL_INT, &ConnectionSettings::OnEnSetfocusConSettingsPolInt)
	ON_EN_KILLFOCUS(IDC_CON_SETTINGS_POL_INT, &ConnectionSettings::OnEnKillfocusConSettingsPolInt)
	ON_EN_SETFOCUS(IDC_CON_SETTINGS_ID_STRING, &ConnectionSettings::OnEnSetfocusConSettingsIdString)
	ON_EN_KILLFOCUS(IDC_CON_SETTINGS_ID_STRING, &ConnectionSettings::OnEnKillfocusConSettingsIdString)
	ON_BN_CLICKED(IDC_CON_SETTINGS_TEST_BUTTON, &ConnectionSettings::OnBnClickedConSettingsTestButton)
	ON_EN_CHANGE(IDC_CON_SETTINGS_ID_STRING, &ConnectionSettings::OnEnChangeConSettingsIdString)
END_MESSAGE_MAP()


// ConnectionSettings message handlers


BOOL ConnectionSettings::OnInitDialog()
	{
	CDialogEx::OnInitDialog();

	// Initialization

	// Update the controls
	updateAllControls();
	setModified(false);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
	}

// Update all of the controls
void ConnectionSettings::updateAllControls()
	{
	CString tempString;

	// Set the auto port scan check mark
	m_autoPortScan.SetCheck(localCfg.autoPortScan ? 1 : 0);

	// Populate the ID string
	m_idString.SetWindowText(localCfg.idString);

	// Polling interval
	tempString.Format(_T("%d"), localCfg.pollingInterval);
	m_pollInterval.SetWindowTextW(tempString);

	
	// Copy all of the names into the COM Port list box
	m_portList.ResetContent();

	// Roll through the ports and populate the list
	for (const auto& port : globalPorts->portAndNames)
		{
		tempString.Format(_T("COM%u <%s>\n"), port.first, port.second.c_str());
		m_portList.AddString(tempString);
		}

	// Position it on the first item in the list
	m_portList.SetSel(-1, FALSE);
	}

// Update the local configuration with the value
void ConnectionSettings::OnBnClickedConSettingsAutoComScan()
	{
	localCfg.autoPortScan = m_autoPortScan.GetCheck() != 0;
	}

void ConnectionSettings::OnEnSetfocusConSettingsPolInt()
	{
	m_pollInterval.GetWindowTextW(holdString);
	}

void ConnectionSettings::OnEnKillfocusConSettingsPolInt()
	{
	CString newValue;

	m_pollInterval.GetWindowTextW(newValue);		// Get the current value

	// If it's not the same as it was when we entered the dialog then deal with it
	if (newValue.Compare(holdString))
		{
		// Convert it to an integer but it has to be at least 1ms
		localCfg.pollingInterval = _wtoi(newValue.GetBuffer());

		if (localCfg.pollingInterval < 1)
			{
			m_pollInterval.SetWindowText(_T("1"));
			localCfg.pollingInterval = 1;
			}

		setModified(true);
		}
	}

void ConnectionSettings::OnEnSetfocusConSettingsIdString()
	{
	m_idString.GetWindowTextW(holdString);
	}

void ConnectionSettings::OnEnKillfocusConSettingsIdString()
	{
	CString newValue;

	m_idString.GetWindowTextW(newValue);		// Get the current value

	// If it's not the same as it was when we entered the dialog then deal with it
	if (newValue.Compare(holdString))
		{
		localCfg.idString = newValue;

		setModified(true);
		}
	}

// As the ID string text changes, update the availability of the test button
void ConnectionSettings::OnEnChangeConSettingsIdString()
	{
	m_testButton.EnableWindow((m_idString.GetWindowTextLengthW() > 0) && (globalPorts->portAndNames.size() > 0));
	}

// The test button will check to see if the current string can be found in one of the scanned COM ports
void ConnectionSettings::OnBnClickedConSettingsTestButton()
	{
	CString searchString, tempString;

	// Get the search value from the screen
	m_idString.GetWindowText(searchString);

	// Remove all of the selections
	m_portList.SetSel(-1, FALSE);

	// Roll through all of the strings in the list box and highlight the ones where it matches.
	// We're searching the text in the list box, rather than the original structure that was
	// used to create it, in case it has been sorted.
	for (int index = m_portList.GetCount(); index; index--)
		{
		// Get the text from the list box
		m_portList.GetText(index - 1, tempString);

		// Compare the search string to the text in the list box
		if (tempString.Find(searchString) > -1)
			m_portList.SetSel(index - 1);		// Select it
		}

	// After it's been run, turn off the test button (for now)
	m_testButton.EnableWindow(FALSE);
	}

// Set the modified flag
void ConnectionSettings::setModified(bool modFlag)
	{
	modified = modFlag;

	/*
	// Modify the controls depending on the modified status
	if (modified)
		{


		}
	else
		{


		}
	*/

	}


