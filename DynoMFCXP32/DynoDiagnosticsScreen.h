#pragma once

#include "ListCtrlEx.h"
#include "DynoMFCXP32.h"
#include "VZDynoDefines.h"


// DynoDiagnosticsScreen dialog

// A holder for information about each pin in the different lists
struct pinRecordInfo
	{
	DWORD_PTR dataAddress;
	int pinNumber;
	};


class DynoDiagnosticsScreen : public CDialogEx
{
	DECLARE_DYNAMIC(DynoDiagnosticsScreen)

public:
	DynoDiagnosticsScreen(CWnd* pParent = nullptr);   // standard constructor
	virtual ~DynoDiagnosticsScreen();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIAGNOSTICS };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedDiagnosticsResetProcessorButton();
	afx_msg void OnBnClickedDiagnosticsReadSensorButton();
	afx_msg void OnBnClickedDiagnosticsReadallSensorButton();

	// Pass-in variables
	ComPort* globalPorts;
	int openPortIndex = -1;

	// Class-wide
	sensorValues currentSensorValues;
	char *lastKnownDynoState;
	bool streamingSensorData;

	char lastProcessorValue[DYNO_NUM_PROCESSORS][3];			// [Address][State][Last CMD]
	char lastMotorValue[DYNO_NUM_MOTORS][6];					// [Address][Token][Command][LOW/HIGH]
	char lastOtherDeviceValue[DYNO_NUM_OTHER_DEVICES][6];		// [][Token]
	char lastProcedureValue[DYNO_NUM_PROCEDURES][6];			// [Procedure #][Parameter]



protected:

	void populateProcessorList();
	void updateProcessorValues();
	void populateSensorList();
	void updateSensorValues();
	void populateMotorList();
	void updateMotorValues();
	void populateOtherDeviceList();
	void updateOtherDeviceValues();
	void populateProcedureTestList();
	void updateProcedureTestStatusValues();

	void updateStatus(CString text, bool displayTime = true);
	bool processReceived(char* data, int length);
	bool checkSensorValueSanity(uint16_t value, int token = -1);
	void updateControls();

	float  convertAnalogToTemperature(unsigned int analogReadValue);


	// Private variables
	int m_readPortTimer = 0;					// Timer for reading from COM port
	bool continuousSet = false;
	bool poll_waitingForRead = false;

	pinRecordInfo pinInfo[70];					// Maintain data for each pin

public:

	CButton m_resetProcessorButton;
	CButton m_readButton;
	CButton m_continousSensorRead;


	CListCtrlEx m_otherDevice_list;
	CListCtrlEx m_sensorList;
	CListCtrlEx m_processorList;
	CListCtrlEx m_motorList;
	CListCtrlEx m_procedureTestingList;

	CEdit m_status;
	CButton m_sensorsReadAll;
	CButton m_motorsSetLow;
	CButton m_motorsSetHigh;
	CButton m_otherSetLow;
	CButton m_otherSetHigh;
	CButton m_testProcedureButton;
	CEdit m_testProcedureParameter;

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnLvnItemchangedDiagnosticsOtherDeviceList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLvnItemchangedDiagnosticsProcessorList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLvnItemchangedDiagnosticsMotorList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedDiagnosticsSetLowMotorsButton();
	afx_msg void OnBnClickedDiagnosticsSetHighMotorsButton();
	afx_msg void OnBnClickedDiagnosticsSetLowOtherButton();
	afx_msg void OnBnClickedDiagnosticsOtherPwmButton();
	afx_msg void OnBnClickedDiagnosticsSetHighOtherButton();
	afx_msg void OnLvnItemchangedDiagnosticsSensorList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLvnItemchangedDiagnosticsProcedureTesting(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedDiagnosticsTestProcedureButton();
	afx_msg void OnBnClickedDiagnosticsContinuousSensorCheck();
	CButton m_readStreamButton;
	afx_msg void OnBnClickedDiagnosticsReadstream();
	// Read all of the characters from the COM
	CButton m_drainInputButton;
	// Attempt to flush the COM buffers
	CButton m_flushIOButton;
	// Reset the COM port
	CButton m_resetPortButton;
	afx_msg void OnBnClickedDiagnosticsDraininputButton();
	afx_msg void OnBnClickedDiagnosticsFlushioButton();
	afx_msg void OnBnClickedDiagnosticsResetPortButton();
	CButton m_getStateButton;
	afx_msg void OnBnClickedDiagnosticsGetStateButton();
};
