// DynoComPortDialog.cpp : implementation file
//

#include "pch.h"
#include "DynoMFCXP32.h"
#include "DynoComPortDialog.h"
#include "afxdialogex.h"


// DynoComPortDialog dialog

IMPLEMENT_DYNAMIC(DynoComPortDialog, CDialogEx)

DynoComPortDialog::DynoComPortDialog(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DynoComPortDialog, pParent)
{

}

DynoComPortDialog::~DynoComPortDialog()
{
}

void DynoComPortDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DCP_COM_DROPLIST, m_comList);
	DDX_Control(pDX, IDC_DCP_OPEN_PORT_BUTTON, m_openPortButton);
	DDX_Control(pDX, IDC_DCP_CLOSE_PORT_BUTTON, m_closePortButton);
	DDX_Control(pDX, IDC_DCP_CLEAR_BUTTON, m_clearResultsButton);
	DDX_Control(pDX, IDC_DCP_RESPONSE, m_results);
	DDX_Control(pDX, IDC_DCP_SEND_BUTTON, m_sendButton);
	DDX_Control(pDX, IDC_DCP_BUTTON_COM_REFRESH, m_refreshButton);
	DDX_Control(pDX, IDC_DCP_SAVE_BUTTON, m_saveResultsButton);
	DDX_Control(pDX, IDC_DCP_BAUDRATE, m_baudRate);
	DDX_Control(pDX, IDC_DCP_PARITY, m_portSettings);
	DDX_Control(pDX, IDC_DCP_OPEN_PORT_LIST, m_openPortList);
	DDX_Control(pDX, IDC_DCP_CLOSE_ALL, m_closeAll);
	DDX_Control(pDX, IDC_DCP_TEXT_TO_SEND, m_sendText);
	DDX_Control(pDX, IDC_DCP_RECEIVED, m_received);
	}


BEGIN_MESSAGE_MAP(DynoComPortDialog, CDialogEx)
	ON_BN_CLICKED(IDC_DCP_BUTTON_COM_REFRESH, &DynoComPortDialog::OnBnClickedDcpButtonComRefresh)
	ON_BN_CLICKED(IDC_DCP_CLEAR_BUTTON, &DynoComPortDialog::OnBnClickedDcpClearButton)
	ON_BN_CLICKED(IDC_DCP_OPEN_PORT_BUTTON, &DynoComPortDialog::OnBnClickedDcpOpenPortButton)
	ON_BN_CLICKED(IDC_DCP_CLOSE_PORT_BUTTON, &DynoComPortDialog::OnBnClickedDcpClosePortButton)
	ON_CBN_SELCHANGE(IDC_DCP_COM_DROPLIST, &DynoComPortDialog::OnCbnSelchangeDcpComDroplist)
	ON_BN_CLICKED(IDC_DCP_CLOSE_ALL, &DynoComPortDialog::OnBnClickedDcpCloseAll)
	ON_BN_CLICKED(IDC_DCP_SEND_BUTTON, &DynoComPortDialog::OnBnClickedDcpSendButton)
	ON_EN_CHANGE(IDC_DCP_TEXT_TO_SEND, &DynoComPortDialog::OnEnChangeDcpTextToSend)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_DCP_BAUDRATE, &DynoComPortDialog::OnCbnSelchangeDcpBaudrate)
	ON_CBN_SELCHANGE(IDC_DCP_PARITY, &DynoComPortDialog::OnCbnSelchangeDcpParity)
END_MESSAGE_MAP()


// DynoComPortDialog message handlers


BOOL DynoComPortDialog::OnInitDialog()
	{
	CDialogEx::OnInitDialog();

	// Initialize the controls with the COM port settings
	initializeDefaultValues();

	// Fill in the COM drop list
	populateComList();
	updateControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
	}

// Initialize the combo boxes with the acceptable values
void DynoComPortDialog::initializeDefaultValues()
	{
	// Start with all of the available baud rates
	m_baudRate.AddString(_T("2 million"));
	m_baudRate.SetItemData(0, 2000000);
	m_baudRate.AddString(_T("1 million"));
	m_baudRate.SetItemData(1, 1000000);
	m_baudRate.AddString(_T("500k"));
	m_baudRate.SetItemData(2, 500000);
	m_baudRate.AddString(_T("250k"));
	m_baudRate.SetItemData(3, 250000);
	m_baudRate.AddString(_T("115200"));
	m_baudRate.SetItemData(4, 115200);
	m_baudRate.AddString(_T("57600"));
	m_baudRate.SetItemData(5, 57600);
	m_baudRate.AddString(_T("38400"));
	m_baudRate.SetItemData(6, 38400);
	m_baudRate.AddString(_T("28800"));
	m_baudRate.SetItemData(7, 28800);
	m_baudRate.AddString(_T("19200"));
	m_baudRate.SetItemData(8, 19200);
	m_baudRate.AddString(_T("14400"));
	m_baudRate.SetItemData(9, 14400);
	m_baudRate.AddString(_T("9600"));		// Default
	m_baudRate.SetItemData(10, 9600);
	m_baudRate.AddString(_T("4800"));
	m_baudRate.SetItemData(11, 4800);
	m_baudRate.AddString(_T("2400"));
	m_baudRate.SetItemData(12, 2400);
	m_baudRate.AddString(_T("1200"));
	m_baudRate.SetItemData(13, 1200);
	m_baudRate.AddString(_T("600"));
	m_baudRate.SetItemData(14, 600);
	m_baudRate.AddString(_T("300"));
	m_baudRate.SetItemData(15, 300);

	// Select the baud rate based on the current configuration
	for (int index = 0; index < m_baudRate.GetCount(); index++)
		{
		if (m_baudRate.GetItemData(index) == localCfg.baudRate)
			{
			m_baudRate.SetCurSel(index);
			break;
			}
		}

	// The Adruino has a predefined set of valid communications parameters, as follows:
	m_portSettings.AddString(_T("5 bits, No parity, 1 stop bit"));
	m_portSettings.AddString(_T("6 bits, No parity, 1 stop bit"));
	m_portSettings.AddString(_T("7 bits, No parity, 1 stop bit"));
	m_portSettings.AddString(_T("8 bits, No parity, 1 stop bit"));		// Default
	m_portSettings.AddString(_T("5 bits, No parity, 2 stop bits"));
	m_portSettings.AddString(_T("6 bits, No parity, 2 stop bits"));
	m_portSettings.AddString(_T("7 bits, No parity, 2 stop bits"));
	m_portSettings.AddString(_T("8 bits, No parity, 2 stop bits"));
	m_portSettings.AddString(_T("5 bits, Even parity, 1 stop bit"));
	m_portSettings.AddString(_T("6 bits, Even parity, 1 stop bit"));
	m_portSettings.AddString(_T("7 bits, Even parity, 1 stop bit"));
	m_portSettings.AddString(_T("8 bits, Even parity, 1 stop bit"));
	m_portSettings.AddString(_T("5 bits, Even parity, 2 stop bits"));
	m_portSettings.AddString(_T("6 bits, Even parity, 2 stop bits"));
	m_portSettings.AddString(_T("7 bits, Even parity, 2 stop bits"));
	m_portSettings.AddString(_T("8 bits, Even parity, 2 stop bits"));
	m_portSettings.AddString(_T("5 bits, Odd parity, 1 stop bit"));
	m_portSettings.AddString(_T("6 bits, Odd parity, 1 stop bit"));
	m_portSettings.AddString(_T("7 bits, Odd parity, 1 stop bit"));
	m_portSettings.AddString(_T("8 bits, Odd parity, 1 stop bit"));
	m_portSettings.AddString(_T("5 bits, Odd parity, 2 stop bits"));
	m_portSettings.AddString(_T("6 bits, Odd parity, 2 stop bits"));
	m_portSettings.AddString(_T("7 bits, Odd parity, 2 stop bits"));
	m_portSettings.AddString(_T("8 bits, Odd parity, 2 stop bits"));

	m_portSettings.SetCurSel(3);	// Default to 8, N, 1
	}

void DynoComPortDialog::OnBnClickedDcpButtonComRefresh()
	{
	CString message;


	// Attempt to enumerate the ports
	bool enumResult = globalPorts->enumerateComPorts();

	if (enumResult)
		{
		message.Format(_T("Discovered %d port%s"), globalPorts->portAndNames.size(), globalPorts->portAndNames.size() == 1 ? "" : "s");
		log(message);

		message.Format(_T("Enumeration time: %I64u ms"), globalPorts->getEnumerationTime());
		}
	else
		{
		message.Format(_T("COM Port enumeration failed: %u"), globalPorts->getEnumerationError());
		}


	// Show the result in the log
	log(message);

	// Update the COM drop list and the other controls
	populateComList();
	updateControls();
	}


// Copy all of the names into the COM Port drop list
void DynoComPortDialog::populateComList()
	{
	CWaitCursor wait;

	m_comList.ResetContent();

	// If there is nothing in the list then put an appropriate message there
	if (globalPorts->portAndNames.size() < 1)
		{
		m_comList.AddString(_T("No COM Ports"));
		m_comList.SetCurSel(0);
		m_comList.EnableWindow(false);
		
		return;
		}

	// Roll through the ports and populate the list
	CString tempString;
	int idFoundIndex = 0;

	for (const auto& port : globalPorts->portAndNames)
		{
		#pragma warning(suppress: 26489)
		tempString.Format(_T("COM%u <%s>\n"), port.first, port.second.c_str());
		m_comList.AddString(tempString);

		// Remember this index if the ID String was found
		if (localCfg.idString.GetLength() && (tempString.Find(localCfg.idString) > -1))
			idFoundIndex = m_comList.GetCount()-1;
		}

	// Try to position on one that holds the ID string, if available.
	// Otherwise, just position on the first one, which was set at the beginning.
	m_comList.SetCurSel(idFoundIndex);
	OnCbnSelchangeDcpComDroplist();
	}

void DynoComPortDialog::OnBnClickedDcpClearButton()
	{
	if (MessageBox(_T("Clearing log. Are you sure?"), _T("Confirmation of log reset"), MB_YESNO) != IDYES)
		return;

	// Clear the log window
	m_results.SetWindowText(_T(""));

	updateControls();
	}

void DynoComPortDialog::OnBnClickedDcpOpenPortButton()
	{
	// Attempt to open the port that is currently selected.
	// TODO: Later, allow for the selection of other options (word length, etc)
	int newPortIndex = globalPorts->Open(currentlySelectedPort, localCfg.baudRate);

	// If the port failed then display and log the message before returning
	if (newPortIndex == -1)
		{
		// Get the error message
		CString errorMessage = globalPorts->lastErrorString;
		
		// Add it to the log then display it on the screen
		log(errorMessage);
		MessageBox(errorMessage, _T("Error opening COM Port"), MB_ICONERROR);

		// Exit
		return;
		}

	// Once a port has been open and configured, add it to the open list
	CString tempString;
	int listPosition;

	tempString.Format(_T("COM%u <%s>\n"), (globalPorts->portAndNames.at(m_comList.GetCurSel())).first, (globalPorts->portAndNames.at(m_comList.GetCurSel())).second.c_str());
	listPosition = m_openPortList.InsertString(newPortIndex, tempString);
	m_openPortList.SetItemData(listPosition, newPortIndex);

	
	// Position on the new port in the open list
	m_openPortList.SetCurSel(listPosition);

	// Update the log
	tempString.Format(_T("Opened: %s"), tempString);
	log(tempString);

	// With at least one port OPEN, start the timer if it hasn't already been done
	if (m_readPortTimer == 0)
		{
		m_readPortTimer = SetTimer(1, 100, NULL);		// 100ms timer for COM polling

		// If there is an error starting the timer then log and display it
		if (m_readPortTimer == 0)
			{
			CString errorMessage = GetLastErrorAsString();

			errorMessage.Format(_T("Error: No timer available for COM port polling\r\n--> %s"), errorMessage);

			// Add it to the log then display it on the screen
			log(errorMessage);
			MessageBox(errorMessage, _T("COM Port error"), MB_ICONERROR);
			}
		}


	updateControls();
	}

// Close the selected port and remove it from the list.
// NOTE: No specific call is made to kill the timer. If it notices that
// there are no open COM ports then it will kill itself.
void DynoComPortDialog::OnBnClickedDcpClosePortButton()
	{
	CString text;
	int listIndex = m_openPortList.GetCurSel();

	// Find the COM port and close it
	UINT portIndex = m_openPortList.GetItemData(listIndex);
	globalPorts->Close(portIndex);

	// Remember which port was closed
	m_openPortList.GetLBText(listIndex, text);

	// Remove the item from the open COM list
	m_openPortList.DeleteString(listIndex);

	// If there are any more open ports then select the first one
	if (m_openPortList.GetCount() > 0)
		m_openPortList.SetCurSel(0);

	// Update the log
	text.Format(_T("Closed: %s"), text);
	log(text);

	updateControls();
	}

// Append the specified text to the log
void DynoComPortDialog::log(CString text)
	{
	int length = m_results.GetWindowTextLengthW();

	m_results.SetSel(length, length);	// Put the selection at the end of the text
	m_results.ReplaceSel(text + "\r\n");
	}

// Append the specified text to the log
void DynoComPortDialog::updateReceived(CString text)
	{
	int length = m_received.GetWindowTextLengthW();

	if (length == 30000)
		m_received.SetWindowTextW(_T(""));
	else
		m_received.SetSel(length, length);	// Put the selection at the end of the text


	m_received.ReplaceSel(text + "\r\n");
	}


// Update the controls on the dialog
void DynoComPortDialog::updateControls()
	{
	bool anyOpen = m_openPortList.GetCount() > 0;
	bool portOpen = currentlySelectedPort == 0 ? false : globalPorts->isOpen(currentlySelectedPort);
	int resultLogLength = m_results.GetWindowTextLengthW();

	// Enable / disable controls based on whether the port is open or closed
	m_comList.EnableWindow(globalPorts->portAndNames.size() > 0);
	m_openPortList.EnableWindow(anyOpen);
	m_sendText.EnableWindow(portOpen);
	m_openPortButton.EnableWindow(globalPorts->portsEnumerated() && (m_openPortList.GetCount() < globalPorts->maxPortCount) && !portOpen);
	m_closePortButton.EnableWindow(anyOpen);
	m_closeAll.EnableWindow(anyOpen);

	// Specifically turn off the Send button if the text is disabled
	if (portOpen)
		OnEnChangeDcpTextToSend();			// If the port is open then rely on the text-check function
	else
		m_sendButton.EnableWindow(false);	// Otherwise, turn it off

	// Results box
	m_clearResultsButton.EnableWindow(resultLogLength > 0);
	m_saveResultsButton.EnableWindow(resultLogLength > 0);
	}

// When the COM List selection changes, update the selected port
void DynoComPortDialog::OnCbnSelchangeDcpComDroplist()
	{
	currentlySelectedPort = (globalPorts->portAndNames.at(m_comList.GetCurSel())).first;

	updateControls();
	}

// Close all of the open COM ports
void DynoComPortDialog::OnBnClickedDcpCloseAll()
	{
	// Close all of the ports in the open list
	while (m_openPortList.GetCount() > 0)
		{
		// Keep selecting the first item in the list and the calling the close function
		m_openPortList.SetCurSel(0);
		OnBnClickedDcpClosePortButton();
		}
	}

// Send the text that is in the control
void DynoComPortDialog::OnBnClickedDcpSendButton()
	{
	// Send the data to the COM port
	
	CString text;
	int listIndex = m_openPortList.GetCurSel();

	// Get the text
	m_sendText.GetWindowText(text);

	// Find the COM port and send the text
	UINT portIndex = m_openPortList.GetItemData(listIndex);
	int length = globalPorts->Send(portIndex, text);

	if (length < text.GetLength())
		{
		log(globalPorts->lastErrorString);
		}
	}

// Enable the send button if there is text in the control
void DynoComPortDialog::OnEnChangeDcpTextToSend()
	{
	m_sendButton.EnableWindow(m_sendText.GetWindowTextLength() > 0);
	}

// On this interval, check each of the COM ports for incoming data
void DynoComPortDialog::OnTimer(UINT_PTR nIDEvent)
	{
	bool portsOpen = m_openPortList.GetCount() > 0;

	// If there are no open COM ports then kill the timer
	if (!portsOpen)
		{
		KillTimer(m_readPortTimer);
		m_readPortTimer = 0;
		return;
		}

	// Temporarily stop the timer
	KillTimer(m_readPortTimer);

	// Read from each of the open COM ports
	char buffer[256];
	for (int portIndex = 0; portIndex < globalPorts->maxPortCount; portIndex++)
		{
		memset(buffer, 0, 256);
		DWORD bytesRead = globalPorts->Read(portIndex, buffer, 256);

		// If anything was read then display it
		if (bytesRead > 0)
			{
			updateReceived(CString(buffer));
			}
		}

	// Restart the timer
	// FOR NOW: Assume the time is working and skip error check
	m_readPortTimer = SetTimer(1, 100, NULL);		// 100ms timer for COM polling

	CDialogEx::OnTimer(nIDEvent);
	}


void DynoComPortDialog::OnDestroy()
	{
	CDialogEx::OnDestroy();

	// Kill the timer if it's running
	if (m_readPortTimer > 0)
		KillTimer(m_readPortTimer);
	}

// Baud Rate selection changed
void DynoComPortDialog::OnCbnSelchangeDcpBaudrate()
	{
	localCfg.baudRate = m_baudRate.GetItemData(m_baudRate.GetCurSel());
	// TODO: Add your control notification handler code here
	}

// COM Parameters selection changed
void DynoComPortDialog::OnCbnSelchangeDcpParity()
	{
	// FOR NOW: If the other parameters are changed then ignore the change
	m_portSettings.SetCurSel(3);	// Default to 8, N, 1
	}
