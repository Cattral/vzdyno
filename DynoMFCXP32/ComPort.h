#pragma once

#include "pch.h"

// Serial port header files
#include "SerialPort.h"
#include "enumser.h"


class ComPort
	{
	public:
		ComPort();
		~ComPort();

		// Port and name storage
		// NOTE: Make this protected later.
//		CEnumerateSerial::CPortsArray ports;
		CEnumerateSerial::CPortAndNamesArray portAndNames;
//		CEnumerateSerial::CNamesArray names;

		static const int maxPortCount = 4;

		// A list four ports is sufficient for the Arduino
		CSerialPort2 openPorts[maxPortCount];
		UINT openList[maxPortCount] = { 0, 0, 0, 0 };
		CString lastErrorString;

		// Functions
		int Open(int portNumber, DWORD baudRate, int forcedPosition = -1);			// Open the COM Port
		void Close(int portIndex = -1);												// Close the COM port
		DWORD getInputBufferCount(int portIndex);
		bool enumerateComPorts(int method = 0);										// Enumerate the COM Ports

		DWORD Send(int portIndex, char* data, int length, int postWriteDelayMs = 100);

		DWORD Send(int portIndex, CString data);
		DWORD Read(int portIndex, char *data, DWORD bytesToRead);

		// Diagnostic and troubleshooting
		void Reset(int portIndex);												// Reset the COM port
		void ClearBuffers(int portIndex, COMMPROP& before, COMMPROP& after);	// Clear the IO buffers


		// Get and Set methods
		DWORD getEnumerationError()
			{
			return enumerationError;
			}

		ULONGLONG getEnumerationTime()
			{
			return enumerationTime;
			}

		bool portsEnumerated()
			{
			return enumeratedPorts;
			}

		int nextPortIndex();					// Return the next available port index
		bool isOpen(UINT checkPort);			// Return true if the specified port is open


	protected:

		bool enumeratedPorts = false;
		ULONGLONG enumerationTime = 0;
		DWORD enumerationError;
		DWORD systemWideBaud = 0;				// NOTE: This should not be necessary but needed for troubleshooting COM error
	};


/*

[System.IO.Ports.SerialPort]::getportnames()

$port= new-Object System.IO.Ports.SerialPort COM5,9600,None,8,one
$port.open()
$port.WriteLine("Hello world")
$port.ReadLine()
$port.Close()



*/
