#pragma once
// VZDynoDefines.h
// Substitutions for values that cannot be stored in the global Constants structure.
// Ex: Case statement labels.

// Tokens for data transmission and import.
//
// UPPERCASE values are Arduino --> GUI.

//	@ = time stamp
//	A = Time delta - front main sensor
//	B = Time delta - front offset sensor
//	C = Time delta - rear main sensor
//	D = Time delta - rear offset sensor
//	E = Software emergency stop button pressed
//	F = Power failure (Possible hard e-stop)
//	G = Vibration
//	H = Horsepower
//	I = Front gearbox temperature
//	J = Rear gearbox temperature
//	K = Front bearing temperature
//	L = Rear bearing temperature
//	M = Room temperature
//	N = Barometric pressure
//	O = Humidity
//	P = Front drum RPM
//	Q = Rear drum RPM
//	R = Engine RPM
//	S = Speed
//	T = Torque
//	U = Sound
//	V = Voltage
//	W = Carbon Monoxide
//	X = Terminates a list of time delta values
//	Y = Front brake tension
//	Z = Rear brake tension
//	0 = AFR sensor
//	1 = Locking pin position (LR)
//	2 = Locking pin position (LF)
//	3 = Locking pin position (RF)
//	4 = Locking pin position (RR)
//	5 = Base position
//	6 = MAINS detect
//	7 = Base proximity front
//	8 = Base proximity rear
//	9 = Front brake encoder
//	# = Rear brake encoder
//	$ = Front encoder main
//	% = Front encoder offset
//	^ = Rear encoder main
//	& = Rear encoder offset
//	* = Pendant select
//	( = Pendant execute
//	! = Pendant e-stop
//	) = Current state

// Time delta used to create Events


#define TOKEN_timestamp '@'
#define TOKEN_timeDelta_mf 'A'
#define TOKEN_timeDelta_of 'B'
#define TOKEN_timeDelta_mr 'C'
#define TOKEN_timeDelta_or 'D'
#define TOKEN_eStop 'E'
#define TOKEN_powerFailure 'F'				// Not the same as MAINS detect because it is sent when "Power has failed"
#define TOKEN_vibration 'G'
#define TOKEN_horsepower 'H'
#define TOKEN_tmp_gearbox_front 'I'
#define TOKEN_tmp_gearbox_rear 'J'
#define TOKEN_tmp_bearing_front 'K'
#define TOKEN_tmp_bearing_rear 'L'
#define TOKEN_tmp_room 'M'
#define TOKEN_pressure 'N'
#define TOKEN_humidity 'O'
#define TOKEN_rpm_drum_front 'P'
#define TOKEN_rpm_drum_rear 'Q'
#define TOKEN_rpm_engine 'R'
#define TOKEN_speed 'S'
#define TOKEN_torque 'T'
#define TOKEN_sound 'U'
#define TOKEN_voltage 'V'
#define TOKEN_CO 'W'
#define TOKEN_endDrumEncoder 'X'
#define TOKEN_tension_brake_front 'Y'
#define TOKEN_tension_brake_rear 'Z'
#define TOKEN_afr '0'
#define TOKEN_pos_lockpin_LR '1'
#define TOKEN_pos_lockpin_LF '2'
#define TOKEN_pos_lockpin_RF '3'
#define TOKEN_pos_lockpin_RR '4'
#define TOKEN_base_position '5'
#define TOKEN_mains_detect '6'
#define TOKEN_basemotor_prox_front '7'
#define TOKEN_basemotor_prox_rear '8'
#define TOKEN_brake_front_encoder '9'
#define TOKEN_brake_rear_encoder '#'
#define TOKEN_encoder_front_main '$'
#define TOKEN_encoder_front_offset '%'
#define TOKEN_encoder_rear_main '^'
#define TOKEN_encoder_rear_offset '&'
#define TOKEN_pendant_select '*'
#define TOKEN_pendant_exec '('
#define TOKEN_pendant_estop '!'
#define TOKEN_current_state ')'
#define TOKEN_procedure_result '='


// lowercase values are GUI --> Arduino.

// a = Return most recent sensor value readings
// b = Force new sensor readings and return them


#define TOKEN_changeDynoState '~'			// Tell the Dyno to change states (most likely startup)
#define TOKEN_getDynoState '`'				// Get the Dyno state
#define TOKEN_rebootArduino '|'				// Send the soft-reboot command
#define TOKEN_getLastSensorValues 'a'
#define TOKEN_refreshSensorValues 'b'
#define TOKEN_start 'c'
#define TOKEN_setBase 'd'					// "d0" = Forward contactor, "d1" = reverse contactor
#define TOKEN_setBrake_front 'e'			// "e0" = On/off, "e1" = direction
#define TOKEN_setBrake_rear 'f'
#define TOKEN_setSolenoid_energize_front 'g'
#define TOKEN_setSolenoid_energize_rear 'h'
#define TOKEN_setSolenoid_hold_front 'i'
#define TOKEN_setSolenoid_hold_rear 'j'
#define TOKEN_setLP_LR 'k'
#define TOKEN_setLP_LF 'l'
#define TOKEN_setLP_RF 'm'
#define TOKEN_setLP_RR 'n'
#define TOKEN_setFans 'o'
#define TOKEN_setLED 'p'
#define TOKEN_setAWDClutch_front 'q'
#define TOKEN_setAWDClutch_rear 'r'
#define TOKEN_setGearboxCooling_front 's'
#define TOKEN_setGearboxCooling_rear 't'
#define TOKEN_setPowerConnect 'u'
#define TOKEN_sensorPollingStream_begin 'x'
#define TOKEN_sensorPollingStream_end 'y'



//	"z" indicates a procedure, and the two-digit code "z00" represents the code.
//	00 = Home base, 01XX = Move base to XX
//	02 = Engage brakes, 03 = Release brakes,  
//	04 = Engage front brake, 05 = Release front brake,  
//	06 = Engage rear brake, 07 = Release rear brake,  
#define TOKEN_executeProcedure 'z'
#define TOKEN_procedure_HomeBase						0
#define TOKEN_procedure_MoveBaseToX						1
#define TOKEN_procedure_brakes_calibrate_front			2
#define TOKEN_procedure_brakes_calibrate_rear			3
#define TOKEN_procedure_EngageBrakes					4
#define TOKEN_procedure_ReleaseBrakes					5
#define TOKEN_procedure_EngageFrontBrake				6
#define TOKEN_procedure_ReleaseFrontBrake				7
#define TOKEN_procedure_EngageRearBrake					8
#define TOKEN_procedure_ReleaseRearBrake				9
#define TOKEN_procedure_EngageAWDClutches				10
#define TOKEN_procedure_ReleaseAWDClutches				11

// Results from executing a procedure
#define RESULT_executeProc_unknown						1
#define RESULT_executeProc_running						2
#define RESULT_executeProc_run							3
#define RESULT_executeProc_passed						4
#define RESULT_executeProc_failed						5


// Specificity commands for motors and other devices
#define TOKEN_CMD_unknown								-1
#define TOKEN_CMD_motorOnOff							1
#define TOKEN_CMD_motorDirection						2
#define TOKEN_CMD_base_forwardContactor					3
#define TOKEN_CMD_base_reverseContactor					4

//#define TOKEN_CMD_base_home							5
//#define TOKEN_CMD_base_move_to_X						6

//#define TOKEN_CMD_brakes_calibrate_front				7
//#define TOKEN_CMD_brakes_calibrate_rear				8

//#define TOKEN_CMD_brakes_engage_all					9
//#define TOKEN_CMD_brakes_release_all					10
//#define TOKEN_CMD_brakes_engage_front					11
//#define TOKEN_CMD_brakes_release_front				12
//#define TOKEN_CMD_brakes_engage_rear					13
//#define TOKEN_CMD_brakes_release_rear					14

#define TOKEN_CMD_signal_LOW							98
#define TOKEN_CMD_signal_HIGH							99

#define TOKEN_Full_String _T("@ACDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopq")


// Arduino-specific
// Although these are only of importance in the control system, it doesn't hurt
// to have them in the main GUI. Moreover, it may help for logging, later.

// Current state of the system
#define DYNO_STATE_UNKNOWN					-1
#define DYNO_STATE_INIT						1			// Starting up the Arduino; control system unsure of GUI's status
#define DYNO_STATE_IDLE						2			// Waiting for commands from GUI
#define DYNO_STATE_STARTUP					3			// Prepare for vehicle
#define DYNO_STATE_ADJUST_BASE				4
#define DYNO_STATE_ADJUST_BRAKES			5
#define DYNO_STATE_ADJUST_LOCKING_PINS		6
#define DYNO_STATE_ASSESSING				7			// Dyno running, not committed to which drum is spinning and in which direction (GUI only)
#define DYNO_STATE_RUNNING					8
#define DYNO_STATE_FINISHED					9			//
#define DYNO_STATE_ESTOP_PRESSED			10
#define DYNO_STATE_POWER_FAIL				11			// Power has been cut, either through loss of power or hard e-stop press

// Give the GUI some text for each state
#ifdef DYNO_GUI

#define DYNO_STATE_TEXT_UNKNOWN					_T("Unknown state")
#define DYNO_STATE_TEXT_INIT					_T("Initialization")
#define DYNO_STATE_TEXT_IDLE					_T("Idle")
#define DYNO_STATE_TEXT_STARTUP					_T("Startup procedure")
#define DYNO_STATE_TEXT_ADJUST_BASE				_T("Adjusting base")
#define DYNO_STATE_TEXT_ADJUST_BRAKES			_T("Adjusting brakes")
#define DYNO_STATE_TEXT_ADJUST_LOCKING_PINS		_T("Adjusting locking pins")
#define DYNO_STATE_TEXT_ASSESSING				_T("Assessing")
#define DYNO_STATE_TEXT_RUNNING					_T("Running")
#define DYNO_STATE_TEXT_FINISHED				_T("Finished")
#define DYNO_STATE_TEXT_ESTOP_PRESSED			_T("E-stop pressed")
#define DYNO_STATE_TEXT_POWER_FAIL				_T("Power failure")


#endif



#define DYNO_ERROR_NONE						0			// Indicates that there is no known error
#define DYNO_ERROR_PROCESSING_COMMANDS		1
#define DYNO_ERROR_SENDING_SENSOR_DATA		2
#define DYNO_ERROR_INVALID_BRAKE_INDEX		3
#define DYNO_ERROR_LOAD_CELL_NOT_RESPONDING 4

// Drum state
#define DRUM_STATE_UNKNOWN					-1
#define DRUM_STATE_STOPPED					1
#define DRUM_STATE_FORWARD					2
#define DRUM_STATE_REVERSE					3

// Base position
#define BASE_UNKNOWN						(uint16_t)-1
#define BASE_HOME							(uint16_t)0
#define BASE_MAX							(uint16_t)1023		// (*) Temporary

// Brakes
#define BRAKE_FRONT							1
#define BRAKE_REAR							2

#define BRAKE_ACTION_RELEASE				3
#define BRAKE_ACTION_ENGAGE					4
#define BRAKE_ACTION_ESTOP					5

// NOTE: This voltage specifiers will have to be double-checked during initial calibration
#define BRAKE_SOLENOID_POWER_ON				HIGH
#define BRAKE_SOLENOID_POWER_OFF			LOW
#define BRAKE_POWER_ON						LOW
#define BRAKE_POWER_OFF						HIGH
#define BRAKE_DIRECTION_ENGAGE				LOW
#define BRAKE_DIRECTION_RELEASE				HIGH
#define AWD_POWER_ON						HIGH
#define AWD_POWER_OFF						LOW
#define LOCKPIN_POWER_ON					HIGH
#define LOCKPIN_POWER_OFF					LOW
#define BASEMOTOR_POWER_ON					HIGH
#define BASEMOTOR_POWER_OFF					LOW

#define MAINS_POWER_ON						LOW
#define MAINS_POWER_OFF						HIGH

#define LED_ON								HIGH
#define LED_OFF								LOW



// Result from polling serial port
#define RESULT_POLL_SERIAL_OK				0
#define RESULT_POLL_SERIAL_NODATA			1
#define RESULT_POLL_SERIAL_ERROR_RX			2
#define RESULT_POLL_SERIAL_ERROR_TX			3
#define RESULT_POLL_SERIAL_ERROR_COMM		4

// Sensors
#define SENSOR_VALUE_UNKNOWN				UINT16_MAX
#define SENSOR_BRAKE_TENSION_UNKNOWN		UINT32_MAX

// Analog sensor pins
#define PIN_SENSOR_A_WEATHER_TEMP			A0
#define PIN_SENSOR_A_WEATHER_PRESSURE		A1
#define PIN_SENSOR_A_WEATHER_HUMIDITY		A2
#define PIN_A_3								A3		// Placeholder
#define PIN_A_4								A4		// Placeholder
#define PIN_SENSOR_A_GEARBOX_FRONT_TEMP		A5
#define PIN_SENSOR_A_GEARBOX_REAR_TEMP		A6
#define PIN_SENSOR_A_BEARING_FRONT_TEMP		A7
#define PIN_SENSOR_A_BEARING_REAR_TEMP		A8
#define PIN_SENSOR_A_LOCKPIN_LR_POS			A9
#define PIN_SENSOR_A_LOCKPIN_LF_POS			A10
#define PIN_SENSOR_A_LOCKPIN_RF_POS			A11
#define PIN_SENSOR_A_LOCKPIN_RR_POS			A12
#define PIN_SENSOR_A_RPM					A13
#define PIN_SENSOR_A_AFR					A14

// When compiling this in a non-Arduino environment, give A0 - A14 some values.
// This way, functions in the GUI that want to know things about the pin
// will not have any trouble compiling.
// I've used the MEGA-specific definitions so they should be compatible
// with the other code that is out there, but it's conditional on finding
// the DYNO_GUI define anyway.
// IMPORTANT: If in the future, the GUI is aware of models other than the
// MEGA, then these defines may have to change.
#ifdef DYNO_GUI

#define A0	54
#define A1	55
#define A2	56
#define A3	57
#define A4	58
#define A5	59
#define A6	60
#define A7	61
#define A8	62
#define A9	63	
#define A10	64	
#define A11	65	
#define A12	66	
#define A13	67	
#define A14	68
#define A15	69

#endif

// Digital sensor pins
#define PIN_SENSOR_D_ODB_II_1					0
#define PIN_SENSOR_D_ODB_II_2					1
#define PIN_BUTTON_D_PENDANT_SELECT				2
#define PIN_BUTTON_D_PENDANT_EXECUTE			3
#define PIN_ACTION_D_FANS_VFD					4
#define PIN_D_5									5	// Placeholder

#define PIN_ACTION_D_LOCKPIN_LR_POWER			6
#define PIN_ACTION_D_LOCKPIN_LR_DIR				7
#define PIN_ACTION_D_LOCKPIN_LF_POWER			8
#define PIN_ACTION_D_LOCKPIN_LF_DIR				9
#define PIN_ACTION_D_LOCKPIN_RF_POWER			10
#define PIN_ACTION_D_LOCKPIN_RF_DIR				11
#define PIN_ACTION_D_LOCKPIN_RR_POWER			12
#define PIN_ACTION_D_ONBOARD_LED				13
#define PIN_ACTION_D_LOCKPIN_RR_DIR				14

#define PIN_D_15								15	// Placeholder
#define PIN_D_16								16	// Placeholder
#define PIN_D_17								17	// Placeholder
#define PIN_SENSOR_D_ENCODER_FRONT_OFFSET		18
#define PIN_SENSOR_D_ENCODER_REAR_OFFSET		19
#define PIN_D_20								20	// Placeholder
#define PIN_D_21								21

#define PIN_SENSOR_D_BASEMOTOR_POS				22
#define PIN_ACTION_D_AWD_CLUTCH_FRONT			23
#define PIN_ACTION_D_AWD_CLUTCH_REAR			24
#define PIN_BUTTON_D_PENDANT_STOP				25
#define PIN_SENSOR_D_MAINS_DETECT				26
#define PIN_ACTION_D_GEARBOX_FRONT_COOLING		27
#define PIN_ACTION_D_GEARBOX_REAR_COOLING		28
#define PIN_D_29								29	// Placeholder
#define PIN_D_30								30	// Placeholder

#define PIN_SENSOR_D_BRAKE_FRONT_TENSION_DOUT	31
#define PIN_SENSOR_D_BRAKE_FRONT_MOTOR_ENCODER	32
#define PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER	33
#define PIN_ACTION_D_BRAKE_FRONT_MOTOR_POWER	34
#define PIN_ACTION_D_BRAKE_FRONT_MOTOR_DIR		35
#define PIN_ACTION_D_BRAKE_FRONT_SOLENOID_ENG	36
#define PIN_ACTION_D_BRAKE_FRONT_SOLENOID_HOLD	37
#define PIN_ACTION_D_BRAKE_REAR_MOTOR_POWER		38
#define PIN_ACTION_D_BRAKE_REAR_MOTOR_DIR		39
#define PIN_ACTION_D_BRAKE_REAR_SOLENOID_ENG	40
#define PIN_ACTION_D_BRAKE_REAR_SOLENOID_HOLD	41

#define PIN_ACTION_D_MAINS_DISCONNECT			42
#define PIN_ACTION_D_BASEMOTOR_FORWARD			43
#define PIN_ACTION_D_BASEMOTOR_REVERSE			44
#define PIN_D_45								45	// Placeholder (PWM)
#define PIN_D_46								46	// Placeholder (PWM)
#define PIN_SENSOR_D_BRAKE_FRONT_TENSION_SCK	47

#define PIN_SENSOR_D_ENCODER_FRONT_MAIN			48
#define PIN_SENSOR_D_ENCODER_REAR_MAIN			49

#define PIN_SENSOR_D_BRAKE_REAR_TENSION_DOUT	50
#define PIN_SENSOR_D_BASEMOTOR_PROX_FRONT		51
#define PIN_SENSOR_D_BASEMOTOR_PROX_REAR		52
#define PIN_SENSOR_D_BRAKE_REAR_TENSION_SCK		53

// Hardware specific values and limitations
#define DYNO_NUM_PROCESSORS						1
#define DYNO_NUM_SENSORS						28
#define DYNO_NUM_MOTORS							15
#define DYNO_NUM_OTHER_DEVICES					11
#define DYNO_NUM_PROCEDURES						10


// Custom data structures

// sensorValues stores all of the non-interrupt-based sensor values,
// even between dyno runs. The uint16_t data type is used for ease
// of communication with the GUI.
// Previously, the uint32_t was used, but it was unnecessarily long
// for reading 10-bit value sensors. If need be, one of more of the fields
// can be changed to accommodate a larger value.
struct sensorValues
	{
	// Analog pins
	uint16_t weather_temperature = SENSOR_VALUE_UNKNOWN;        // Room temperature
	uint16_t weather_pressure = SENSOR_VALUE_UNKNOWN;           // Barometric pressure
	uint16_t weather_humidity = SENSOR_VALUE_UNKNOWN;           // Humidity
	uint16_t gearbox_front_temp = SENSOR_VALUE_UNKNOWN;         // Gearbox thermistor reading
	uint16_t gearbox_rear_temp = SENSOR_VALUE_UNKNOWN;
	uint16_t bearing_front_temp = SENSOR_VALUE_UNKNOWN;         // Bearing thermistor reading
	uint16_t bearing_rear_temp = SENSOR_VALUE_UNKNOWN;
	uint16_t lockpin_LR_pos = SENSOR_VALUE_UNKNOWN;             // Locking pin encoder
	uint16_t lockpin_LF_pos = SENSOR_VALUE_UNKNOWN;
	uint16_t lockpin_RF_pos = SENSOR_VALUE_UNKNOWN;
	uint16_t lockpin_RR_pos = SENSOR_VALUE_UNKNOWN;
	uint16_t RPM = SENSOR_VALUE_UNKNOWN;                        // RPM: <This may be moved to a digital interrupt pin>
	uint16_t AFR = SENSOR_VALUE_UNKNOWN;                        // AFR sensor

	// Digital pins
	uint16_t base_position = BASE_UNKNOWN;                      // Last known base position
	uint16_t mains_detect = SENSOR_VALUE_UNKNOWN;               // Indicates that MAINS has power 
	uint16_t basemotor_prox_front = SENSOR_VALUE_UNKNOWN;       // Indicates base is fully extended
	uint16_t basemotor_prox_rear = SENSOR_VALUE_UNKNOWN;        // Indicates base is fully retracted
	uint16_t brake_front_encoder = SENSOR_VALUE_UNKNOWN;		// Brake encoder
	uint16_t brake_rear_encoder = SENSOR_VALUE_UNKNOWN;			// 
	uint16_t encoder_front_main = SENSOR_VALUE_UNKNOWN;			// Hall effect sensors on encoder wheels
	uint16_t encoder_front_offset = SENSOR_VALUE_UNKNOWN;		//
	uint16_t encoder_rear_main = SENSOR_VALUE_UNKNOWN;			//
	uint16_t encoder_rear_offset = SENSOR_VALUE_UNKNOWN;		//
	uint16_t pendant_select = SENSOR_VALUE_UNKNOWN;				// Pendant buttons
	uint16_t pendant_exec = SENSOR_VALUE_UNKNOWN;				//
	uint16_t pendant_estop = SENSOR_VALUE_UNKNOWN;				//

	// Brakes are digital but not raw-pin reads
	uint32_t brake_front_tension = SENSOR_BRAKE_TENSION_UNKNOWN;        // Brake load cell reading
	uint32_t brake_rear_tension = SENSOR_BRAKE_TENSION_UNKNOWN;

	// Return the type of pin
	// This isn't the most elegant function but it's easy and works cross-platform .
	// Returns: 0 if not found, 1 if Analog, 2 if digital
	int getPinType(int pin)
		{
		if ((pin >= A0) && (pin <= A15))
			return 1;

		if ((pin >= 0) && (pin <= 53))
			return 2;

		return 0;
		}

	// Return true if the pin is digital
	bool isDigitalPin(int pin)
		{
		return getPinType(pin) == 2;
		}

	// Return true if the pin in analog
	bool isAnalogPin(int pin)
		{
		return getPinType(pin) == 1;
		}
	};
