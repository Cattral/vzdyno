#pragma once
#include <afxwin.h>
class CDynoRichLeanControl : public CStatic
    {

    // Construction
    public:CDynoRichLeanControl();

    protected:
        int m_nBottomCX;
        int m_nBottomCY;
        int m_nBottomRadius;
        int m_nTopRadius;
        int m_nValueFontHeight;
        int m_nValueBaseline;
        int m_nValueCenter;
        double m_dMaxValue;
        double m_dMinValue;
        double m_dCurrentValue=0;

     
          CString m_strName;
          CString m_currGraphName;



          CFont m_fontValue;

         
          CRect m_rectCtrl;
          CRect m_rectValue;
          CRgn m_rgnBoundary;
         
         
          COLORREF m_windowColour;
          COLORREF m_graphNameColour;
          COLORREF m_faceBorderColour;
          COLORREF m_valueTextColour;
          COLORREF m_valueBackgroundColour;
          COLORREF m_valueBorderHighlite;
          COLORREF m_fillColour;
          COLORREF m_valueBorderShadow;
          CDC m_dcBackground;

          CBitmap* m_pBitmapOldBackground;
          CBitmap m_bitmapBackground;

            public: void SetName(CString& strName);
                  void  SetRange(double dMin, double dMax);
                  void SetRichLeanValue(double fval);

          // Colours

          void SetWindowColour(COLORREF newColour);
          void SetFillColour(COLORREF newColour);
          void SetGraphNameColour(COLORREF newColour);
          //void SetFaceBorderColour(COLORREF newColour);
          void SetValueColour(COLORREF newTextColour, COLORREF newBackGroundColour = RGB(0, 0, 0));
          void SetValueBorderColours(COLORREF highlight, COLORREF shadow);


   


    protected: void DrawBackground(CDC* pDC, CRect& rect);


               void ReconstructControl();
               afx_msg void OnPaint();
               afx_msg void OnSize(UINT nType, int cx, int cy);
               DECLARE_MESSAGE_MAP()

    
    public: virtual ~CDynoRichLeanControl();





};

