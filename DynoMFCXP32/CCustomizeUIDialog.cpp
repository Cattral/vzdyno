// CCustomizeUIDialog.cpp : implementation file
//

#include "pch.h"
#include "DynoMFCXP32.h"
#include "CCustomizeUIDialog.h"
#include "afxdialogex.h"


// CCustomizeUIDialog dialog

IMPLEMENT_DYNAMIC(CCustomizeUIDialog, CDialogEx)

CCustomizeUIDialog::CCustomizeUIDialog(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DynoCustomizeUIDialog, pParent)
{

}

CCustomizeUIDialog::~CCustomizeUIDialog()
{
}

void CCustomizeUIDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CUSTOM_UI_COLOURS, m_colourTargets);
	DDX_Control(pDX, IDC_CUSTOM_UI_COLOUR_BUTTON, m_colourSelectionButton);
	DDX_Control(pDX, IDC_CUSTOM_UI_FILENAME, m_filename);
	DDX_Control(pDX, IDC_CUSTOM_UI_BROWSE_BUTTON, m_browseButton);
	DDX_Control(pDX, IDC_CUSTOM_UI_RESET, m_resetButton);
	DDX_Control(pDX, IDC_CUSTOM_UI_DEFAULTS, m_defaultsButton);
	DDX_Control(pDX, IDC_CUSTOM_UI_LOADBUTTON, m_loadButton);
	DDX_Control(pDX, IDC_CUSTOM_UI_SAVEBUTTON, m_saveButton);
	DDX_Control(pDX, IDC_CUSTOM_UI_SPEED_MAX, m_speedMax);
	DDX_Control(pDX, IDC_CUSTOM_UI_RPM_MAX, m_rpmMax);
	DDX_Control(pDX, IDC_CUSTOM_UI_METRIC, m_unitSelection);
	DDX_Control(pDX, IDCANCEL, m_cancelButton);
	DDX_Control(pDX, IDC_CUSTOM_UI_SPEED_REDLINE, m_speed_redLine);
	DDX_Control(pDX, IDC_CUSTOM_UI_RPM_REDLINE, m_rpm_redLine);
	DDX_Control(pDX, IDC_CUSTOM_UI_TORQUE_MAX, m_torqueMax);
	DDX_Control(pDX, IDC_CUSTOM_UI_HP_MAX, m_hpMax);
	DDX_Control(pDX, IDC_CUSTOM_UI_TORQUE_PEN, m_torqueLineType);
	DDX_Control(pDX, IDC_CUSTOM_UI_HP_PEN, m_hpLineType);
	DDX_Control(pDX, IDC_CUSTOM_UI_Fuel_Type, m_fuelType);
	DDX_Control(pDX, IDC_CUSTOM_UI_SHOW_TORQUE_AXIS, m_torqueShowAxis);
	DDX_Control(pDX, IDC_CUSTOM_UI_SHOW_HP_AXIS, m_hpShowAxis);
	DDX_Control(pDX, IDC_CUSTOM_UI_SHOW_GRIDLINES, m_showGridlines);
	DDX_Control(pDX, IDC_CUSTOM_UI_INERTIA_CONST_TEXT, m_inertiaConstText);
	DDX_Control(pDX, IDC_CUSTOM_UI_INERTIA_CONST_VALUE, m_inertiaConstValue);
	}


BEGIN_MESSAGE_MAP(CCustomizeUIDialog, CDialogEx)
	ON_LBN_SELCANCEL(IDC_CUSTOM_UI_COLOURS, &CCustomizeUIDialog::OnLbnSelcancelCustomUiColours)
	ON_LBN_SELCHANGE(IDC_CUSTOM_UI_COLOURS, &CCustomizeUIDialog::OnLbnSelchangeCustomUiColours)
	ON_BN_CLICKED(IDC_CUSTOM_UI_COLOUR_BUTTON, &CCustomizeUIDialog::OnBnClickedCustomUiColourButton)
	ON_BN_CLICKED(IDOK, &CCustomizeUIDialog::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CUSTOM_UI_RESET, &CCustomizeUIDialog::OnBnClickedCustomUiReset)
	ON_BN_CLICKED(IDC_CUSTOM_UI_DEFAULTS, &CCustomizeUIDialog::OnBnClickedCustomUiDefaults)
	ON_EN_SETFOCUS(IDC_CUSTOM_UI_SPEED_MAX, &CCustomizeUIDialog::OnEnSetfocusCustomUiSpeedMax)
	ON_EN_KILLFOCUS(IDC_CUSTOM_UI_SPEED_MAX, &CCustomizeUIDialog::OnEnKillfocusCustomUiSpeedMax)
	ON_EN_SETFOCUS(IDC_CUSTOM_UI_SPEED_REDLINE, &CCustomizeUIDialog::OnEnSetfocusCustomUiSpeedRedline)
	ON_EN_KILLFOCUS(IDC_CUSTOM_UI_SPEED_REDLINE, &CCustomizeUIDialog::OnEnKillfocusCustomUiSpeedRedline)
	ON_EN_SETFOCUS(IDC_CUSTOM_UI_RPM_MAX, &CCustomizeUIDialog::OnEnSetfocusCustomUiRpmMax)
	ON_EN_KILLFOCUS(IDC_CUSTOM_UI_RPM_MAX, &CCustomizeUIDialog::OnEnKillfocusCustomUiRpmMax)
	ON_EN_SETFOCUS(IDC_CUSTOM_UI_RPM_REDLINE, &CCustomizeUIDialog::OnEnSetfocusCustomUiRpmRedline)
	ON_EN_KILLFOCUS(IDC_CUSTOM_UI_RPM_REDLINE, &CCustomizeUIDialog::OnEnKillfocusCustomUiRpmRedline)
	ON_BN_CLICKED(IDC_CUSTOM_UI_METRIC, &CCustomizeUIDialog::OnBnClickedCustomUiMetric)
	ON_BN_CLICKED(IDC_CUSTOM_UI_IMPERIAL, &CCustomizeUIDialog::OnBnClickedCustomUiImperial)
	ON_EN_SETFOCUS(IDC_CUSTOM_UI_TORQUE_MAX, &CCustomizeUIDialog::OnEnSetfocusCustomUiTorqueMax)
	ON_EN_KILLFOCUS(IDC_CUSTOM_UI_TORQUE_MAX, &CCustomizeUIDialog::OnEnKillfocusCustomUiTorqueMax)
	ON_EN_SETFOCUS(IDC_CUSTOM_UI_HP_MAX, &CCustomizeUIDialog::OnEnSetfocusCustomUiHpMax)
	ON_BN_CLICKED(IDC_CUSTOM_UI_SHOW_TORQUE_AXIS, &CCustomizeUIDialog::OnBnClickedCustomUiShowTorqueAxis)
	ON_BN_CLICKED(IDC_CUSTOM_UI_SHOW_HP_AXIS, &CCustomizeUIDialog::OnBnClickedCustomUiShowHpAxis)
	ON_CBN_SELCHANGE(IDC_CUSTOM_UI_TORQUE_PEN, &CCustomizeUIDialog::OnCbnSelchangeCustomUiTorquePen)
	ON_CBN_SELCHANGE(IDC_CUSTOM_UI_HP_PEN, &CCustomizeUIDialog::OnCbnSelchangeCustomUiHpPen)
	ON_EN_KILLFOCUS(IDC_CUSTOM_UI_HP_MAX, &CCustomizeUIDialog::OnEnKillfocusCustomUiHpMax)
	ON_BN_CLICKED(IDC_CUSTOM_UI_SHOW_GRIDLINES, &CCustomizeUIDialog::OnBnClickedCustomUiShowGridlines)
	ON_WM_RBUTTONDBLCLK()
	ON_EN_SETFOCUS(IDC_CUSTOM_UI_INERTIA_CONST_VALUE, &CCustomizeUIDialog::OnEnSetfocusCustomUiInertiaConstValue)
	ON_EN_KILLFOCUS(IDC_CUSTOM_UI_INERTIA_CONST_VALUE, &CCustomizeUIDialog::OnEnKillfocusCustomUiInertiaConstValue)
	ON_BN_CLICKED(IDC_CUSTOM_UI_SAVEBUTTON, &CCustomizeUIDialog::OnBnClickedCustomUiSavebutton)
	ON_CBN_SELCHANGE(IDC_CUSTOM_UI_Fuel_Type, &CCustomizeUIDialog::OnCbnSelchangeCustomUiFuelType)
END_MESSAGE_MAP()


// CCustomizeUIDialog message handlers


BOOL CCustomizeUIDialog::OnInitDialog()
	{
	CDialogEx::OnInitDialog();

	// Before doing anything, make a backup of the config so that we can restore it without exiting
	backupCfg = localCfg;


	// Set up the MFC Color Button
	m_colourSelectionButton.EnableOtherButton(_T("Other"), FALSE);

	// Initialize the pen style selection drop lists
	m_torqueLineType.AddString(Constants.String_PenType_solid.c_str());
	m_torqueLineType.AddString(Constants.String_PenType_wide.c_str());
	m_torqueLineType.AddString(Constants.String_PenType_extraWide.c_str());
	m_torqueLineType.AddString(Constants.String_PenType_dash.c_str());
	m_torqueLineType.AddString(Constants.String_PenType_dot.c_str());

	m_torqueLineType.SetItemData(0, PS_SOLID);
	m_torqueLineType.SetItemData(1, PS_SOLID);		// Wide lines are always Solid
	m_torqueLineType.SetItemData(2, PS_SOLID);		// Extra wide lines are always Solid
	m_torqueLineType.SetItemData(3, PS_DASH);
	m_torqueLineType.SetItemData(4, PS_DOT);

	m_hpLineType.AddString(Constants.String_PenType_solid.c_str());
	m_hpLineType.AddString(Constants.String_PenType_wide.c_str());
	m_hpLineType.AddString(Constants.String_PenType_extraWide.c_str());
	m_hpLineType.AddString(Constants.String_PenType_dash.c_str());
	m_hpLineType.AddString(Constants.String_PenType_dot.c_str());

	m_hpLineType.SetItemData(0, PS_SOLID);
	m_hpLineType.SetItemData(1, PS_SOLID);		
	m_hpLineType.SetItemData(2, PS_SOLID);		
	m_hpLineType.SetItemData(3, PS_DASH);
	m_hpLineType.SetItemData(4, PS_DOT);



	int index = m_fuelType.AddString(_T("Gasoline"));
	m_fuelType.SetItemData(index, 1470);
	index = m_fuelType.AddString(_T("Diesel"));
	m_fuelType.SetItemData(index, 1450);
	index = m_fuelType.AddString(_T("Methanol"));
	m_fuelType.SetItemData(index, 647);
	



	//m_fuelType.SetItemData(0, PS_SOLID);

	// Update the controls
	updateAllControls();
	setModified(false);


	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
	}

// Update all of the controls
void CCustomizeUIDialog::updateAllControls()
	{
	// The calling function is responsible for populating colour target array and
	for (int i = 0; i < Constants.colour_target_count; i++)
		{
		CString nextName;
		COLORREF colourValue;

		if (!colourNamesMap->Lookup(i, nextName))
			continue;

		if (!localCfg.colourRefsMap.Lookup(i, colourValue))
			continue;

		int listPosition = m_colourTargets.AddString(nextName);

		m_colourTargets.SetItemData
		(listPosition, static_cast<DWORD_PTR>(colourValue));
		}


	// Set the selection to the first one in the list
	if (m_colourTargets.GetCount() > 0)
		{
		m_colourTargets.SetCurSel(0);
		OnLbnSelchangeCustomUiColours();
		}

	// Set the selection check. For now, assume that it's metric unless it's imperial.
	// Later, there will be options for both and "undefined" will also be a state.
	CheckRadioButton(IDC_CUSTOM_UI_METRIC, IDC_CUSTOM_UI_IMPERIAL,
		localCfg.units == Constants.Units_imperial ? IDC_CUSTOM_UI_IMPERIAL : IDC_CUSTOM_UI_METRIC);

	// Select the proper line types based on the style and width, if applicable
	m_torqueLineType.SetCurSel(localCfg.torqueLineType == PS_DASH ? 3 : localCfg.torqueLineType == PS_DOT ? 4 : localCfg.torqueLineType - PS_SOLID + localCfg.torqueLineWidth - 1);
	m_hpLineType.SetCurSel(localCfg.hpLineType == PS_DASH ? 3 : localCfg.hpLineType == PS_DOT ? 4 : localCfg.hpLineType - PS_SOLID + localCfg.hpLineWidth - 1);


	CString tempString;

	// Speedometer
	tempString.Format(_T("%d"), localCfg.speedometerMax);
	m_speedMax.SetWindowTextW(tempString);
	tempString.Format(_T("%d"), localCfg.speedometerRedline);
	m_speed_redLine.SetWindowTextW(tempString);

	// Tachometer
	tempString.Format(_T("%d"), localCfg.rpmMax);
	m_rpmMax.SetWindowTextW(tempString);
	tempString.Format(_T("%d"), localCfg.tachometerRedline);
	m_rpm_redLine.SetWindowTextW(tempString);

	// Torque
	tempString.Format(_T("%d"), static_cast<int>(localCfg.torqueMax));
	m_torqueMax.SetWindowTextW(tempString);
	m_torqueShowAxis.SetCheck(localCfg.torqueAxisVisible ? 1 : 0);

	// Horsepower
	tempString.Format(_T("%d"), static_cast<int>(localCfg.horsepowerMax));
	m_hpMax.SetWindowTextW(tempString);
	m_hpShowAxis.SetCheck(localCfg.hpAxisVisible ? 1 : 0);

	// Grid lines
	m_showGridlines.SetCheck(localCfg.showGrid_hpTorqueRpm);

	// Inertia constant
	tempString.Format(_T("%f"), localCfg.inertiaConstant_AWD);
	m_inertiaConstValue.SetWindowTextW(tempString);


	
	m_fuelType.SetCurSel(localCfg.AFRValue == 14.7 ? 0 : localCfg.AFRValue == 14.5 ? 1 : localCfg.AFRValue ==6.47 ? 2 : 0);


	}

// Set the modified flag
void CCustomizeUIDialog::setModified(bool modFlag)
	{
	modified = modFlag;

	/*
	// Modify the controls depending on the modified status
	if (modified)
		{
		

		}
	else
		{


		}
	*/

	m_cancelButton.EnableWindow(modified);
	m_resetButton.EnableWindow(modified);
	}

void CCustomizeUIDialog::OnLbnSelcancelCustomUiColours()
	{
	// TODO: Add your control notification handler code here
	}

// The target has changed
void CCustomizeUIDialog::OnLbnSelchangeCustomUiColours()
	{
	COLORREF curColour;
	int index = m_colourTargets.GetCurSel();

	if (index < 0)
		return;

	// Pull the COLORREF from the stored item data
	curColour = static_cast<COLORREF>(m_colourTargets.GetItemData(index));

	// Set the colour picker to the correct colour
	m_colourSelectionButton.SetColor(curColour);
	}

// When the colour changes, update the value in the list, but not the map
void CCustomizeUIDialog::OnBnClickedCustomUiColourButton()
	{
	COLORREF newColour;
	int listPosition = m_colourTargets.GetCurSel();

	if (listPosition < 0)
		return;

	// Pull the colour from the button
	newColour = m_colourSelectionButton.GetColor();

	// Keep track of the new colour in the list
	m_colourTargets.SetItemData(listPosition, static_cast<DWORD_PTR>(newColour));
	setModified(true);
	}

// The Keep Settings button was pressed
void CCustomizeUIDialog::OnBnClickedOk()
	{
	// NOTE: This function is not responsible for saving themes at this point.

	COLORREF curColour;

	// Copy the colours out of the list control back to the outer map.
	for (int i = 0; i < Constants.colour_target_count; i++)
		{
		CString nextName;

		// Pull the COLORREF from the stored item data
		curColour = static_cast<COLORREF>(m_colourTargets.GetItemData(i));

		localCfg.colourRefsMap.SetAt(i, curColour);
		}

	// Any changes to the local configuration file will be handled by the calling function

	CDialogEx::OnOK();
	}


// Undo the changes since coming into the dialog
void CCustomizeUIDialog::OnBnClickedCustomUiReset()
	{
	localCfg = backupCfg;

	// Recreate the contents of the list box
	m_colourTargets.ResetContent();
	updateAllControls();
	OnLbnSelchangeCustomUiColours();

	setModified(false);
	}

// Load all of the default values, include the colours
void CCustomizeUIDialog::OnBnClickedCustomUiDefaults()
	{
	localCfg.resetDefaults();

	// Recreate the contents of the list box
	m_colourTargets.ResetContent();
	updateAllControls();
	OnLbnSelchangeCustomUiColours();

	setModified(true);
	}

// When the max speed gets focus, keep the value so that we can compare it later
void CCustomizeUIDialog::OnEnSetfocusCustomUiSpeedMax()
	{
	m_speedMax.GetWindowTextW(holdString);
	}

void CCustomizeUIDialog::OnEnKillfocusCustomUiSpeedMax()
	{
	CString newValue;

	m_speedMax.GetWindowTextW(newValue);		// Get the current value

	// If it's not the same as it was when we entered the dialog then deal with it
	if (newValue.Compare(holdString))		
		{
		localCfg.speedometerMax = _wtoi(newValue.GetBuffer());

		setModified(true);
		}
	}

void CCustomizeUIDialog::OnEnSetfocusCustomUiSpeedRedline()
	{
	m_speed_redLine.GetWindowTextW(holdString);
	}

void CCustomizeUIDialog::OnEnKillfocusCustomUiSpeedRedline()
	{
	CString newValue;

	m_speed_redLine.GetWindowTextW(newValue);		// Get the current value

	// If it's not the same as it was when we entered the dialog then deal with it
	if (newValue.Compare(holdString))
		{
		localCfg.speedometerRedline = _wtoi(newValue.GetBuffer());

		setModified(true);
		}
	}

void CCustomizeUIDialog::OnEnSetfocusCustomUiRpmMax()
	{
	m_rpmMax.GetWindowTextW(holdString);
	}

void CCustomizeUIDialog::OnEnKillfocusCustomUiRpmMax()
	{
	CString newValue;

	m_rpmMax.GetWindowTextW(newValue);		// Get the current value

	// If it's not the same as it was when we entered the dialog then deal with it
	if (newValue.Compare(holdString))
		{
		localCfg.rpmMax = _wtoi(newValue.GetBuffer());

		setModified(true);
		}
	}

void CCustomizeUIDialog::OnEnSetfocusCustomUiRpmRedline()
	{
	m_rpm_redLine.GetWindowTextW(holdString);
	}

void CCustomizeUIDialog::OnEnKillfocusCustomUiRpmRedline()
	{
	CString newValue;

	m_rpm_redLine.GetWindowTextW(newValue);		// Get the current value

	// If it's not the same as it was when we entered the dialog then deal with it
	if (newValue.Compare(holdString))
		{
		localCfg.tachometerRedline = _wtoi(newValue.GetBuffer());

		setModified(true);
		}
	}

// Metric selected
void CCustomizeUIDialog::OnBnClickedCustomUiMetric()
	{
	// Change the units if necessary
	if (localCfg.units != Constants.Units_metric)
		{
		localCfg.units = Constants.Units_metric;
		setModified(true);
		}
	}

// Imperial measure selected
void CCustomizeUIDialog::OnBnClickedCustomUiImperial()
	{
	// Change the units if necessary
	if (localCfg.units != Constants.Units_imperial)
		{
		localCfg.units = Constants.Units_imperial;
		setModified(true);
		}
	}

void CCustomizeUIDialog::OnEnSetfocusCustomUiTorqueMax()
	{
	m_torqueMax.GetWindowTextW(holdString);
	}

void CCustomizeUIDialog::OnEnKillfocusCustomUiTorqueMax()
	{
	CString newValue;

	m_torqueMax.GetWindowTextW(newValue);		// Get the current value

	// If it's not the same as it was when we entered the dialog then deal with it
	if (newValue.Compare(holdString))
		{
		localCfg.torqueMax = _wtoi(newValue.GetBuffer());

		setModified(true);
		}
	}

void CCustomizeUIDialog::OnEnSetfocusCustomUiHpMax()
	{
	m_hpMax.GetWindowTextW(holdString);
	}

void CCustomizeUIDialog::OnEnKillfocusCustomUiHpMax()
	{
	CString newValue;

	m_hpMax.GetWindowTextW(newValue);		// Get the current value

	// If it's not the same as it was when we entered the dialog then deal with it
	if (newValue.Compare(holdString))
		{
		localCfg.horsepowerMax = _wtoi(newValue.GetBuffer());

		setModified(true);
		}
	}

// Torque axis visibility
void CCustomizeUIDialog::OnBnClickedCustomUiShowTorqueAxis()
	{
	// Toggle the visibility
	localCfg.torqueAxisVisible = !localCfg.torqueAxisVisible;

	setModified(true);
	}

// Horsepower axis visibility
void CCustomizeUIDialog::OnBnClickedCustomUiShowHpAxis()
	{
	// Toggle the visibility
	localCfg.hpAxisVisible = !localCfg.hpAxisVisible;

	setModified(true);
	}

// Toggle grid visibility
void CCustomizeUIDialog::OnBnClickedCustomUiShowGridlines()
	{
	// Toggle the visibility
	localCfg.showGrid_hpTorqueRpm = !localCfg.showGrid_hpTorqueRpm;

	setModified(true);	
	}


void CCustomizeUIDialog::OnCbnSelchangeCustomUiTorquePen()
	{
	// Depending on the selection, both the line type and width need to be modified
	switch (m_torqueLineType.GetCurSel())
		{
			case 0:		// Solid
				localCfg.torqueLineType = PS_SOLID;
				localCfg.torqueLineWidth = 1;
				break;

			case 1:		// Wide
				localCfg.torqueLineType = PS_SOLID;
				localCfg.torqueLineWidth = 2;
				break;

			case 2:		// Extra wide
				localCfg.torqueLineType = PS_SOLID;
				localCfg.torqueLineWidth = 3;
				break;

			case 3:		// Dash
				localCfg.torqueLineType = PS_DASH;
				localCfg.torqueLineWidth = 1;
				break;

			case 4:		// Dot
				localCfg.torqueLineType = PS_DOT;
				localCfg.torqueLineWidth = 1;
				break;

			default:	// Default to PS_SOLID
				localCfg.torqueLineType = PS_SOLID;
				localCfg.torqueLineWidth = 1;
		}

	setModified(true);
	}



void CCustomizeUIDialog::OnCbnSelchangeCustomUiHpPen()
	{
	// Depending on the selection, both the line type and width need to be modified
	switch (m_hpLineType.GetCurSel())
		{
			case 0:		// Solid
				localCfg.hpLineType = PS_SOLID;
				localCfg.hpLineWidth = 1;
				break;

			case 1:		// Wide
				localCfg.hpLineType = PS_SOLID;
				localCfg.hpLineWidth = 2;
				break;

			case 2:		// Extra wide
				localCfg.hpLineType = PS_SOLID;
				localCfg.hpLineWidth = 3;
				break;

			case 3:		// Dash
				localCfg.hpLineType = PS_DASH;
				localCfg.hpLineWidth = 1;
				break;

			case 4:		// Dot
				localCfg.hpLineType = PS_DOT;
				localCfg.hpLineWidth = 1;
				break;

			default:	// Default to PS_SOLID
				localCfg.hpLineType = PS_SOLID;
				localCfg.hpLineWidth = 1;
		}

	setModified(true);
	}



// Hidden option display on double-right-click
void CCustomizeUIDialog::OnRButtonDblClk(UINT nFlags, CPoint point)
	{
	// Bring up the inertial text and edit box
	m_inertiaConstText.ShowWindow(SW_NORMAL);
	m_inertiaConstValue.ShowWindow(SW_NORMAL);

	CDialogEx::OnRButtonDblClk(nFlags, point);
	}

void CCustomizeUIDialog::OnEnSetfocusCustomUiInertiaConstValue()
	{
	m_inertiaConstValue.GetWindowTextW(holdString);
	}


void CCustomizeUIDialog::OnEnKillfocusCustomUiInertiaConstValue()
	{
	CString newValue;

	m_inertiaConstValue.GetWindowTextW(newValue);		// Get the current value

	// If it's not the same as it was when we entered the dialog then deal with it
	if (newValue.Compare(holdString))
		{
		localCfg.inertiaConstant_AWD = _wtof(newValue.GetBuffer());

		setModified(true);
		}
	}


void CCustomizeUIDialog::OnBnClickedCustomUiSavebutton()
{
	// TODO: Add your control notification handler code here
}


void CCustomizeUIDialog::OnCbnSelchangeCustomUiFuelType()
{
	int idx = m_fuelType.GetCurSel();
	if (idx >= 0)
	{
		double d= m_fuelType.GetItemData(idx);
		localCfg.AFRValue = double(d/100);
	}
	else
		localCfg.AFRValue = m_fuelType.GetItemData(0)/100;

	setModified(true);
}
