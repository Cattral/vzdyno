#include "pch.h"

#include "DynoMFCXP32.h"
#include "VZDynoDefines.h"
#include "DynoFileDetails.h"
#include "CMainForm.h"

#include "DynoImportDataScreen.h"


IMPLEMENT_SERIAL(DynoFileDetails, CObject, VERSIONABLE_SCHEMA | 1)


// Erase all of the details
void DynoFileDetails::Reset(DynoConfig* config, std::shared_ptr<char[]> newData, int newLength)
	{
	// If there was no new datafile specific then just empty the shared pointer
	if (newData == nullptr)
		{
		dataStream.reset();
		dataLength = 0;
		}
	else
		{
		dataStream = newData;
		dataLength = newLength;
		}

	// Point to the configuration data for the current form
	currentFormConfig = config;

	// Reset the remaining variables
	filepath.Empty();
	eventList.RemoveAll();
	imported = false;
	sumTimeDeltas = 0.0;

	// Time and date values
	startTime = { 0 };
	endTime = { 0 };
	saveTime = { 0 };
	}

// Parse the data and build the structure.
// Assume that "Reset" has already been called if necessary, and just overwrite existing data.
// Because this file builds events according to a timestamp that can be selected individually
// or as part of a range at any time, this function simply processes the entire file.
bool DynoFileDetails::ParseData(CProgressCtrl * progressBar)
	{
	// If there is no data then return false
	if (dataStream == nullptr)
		return false;

	MSG msg;

	CString dataPrefixes;
	CString currentValue;
	CString exportName;

	// Automatic time distribution
	SYSTEMTIME currentTime, lastTimeStamp;
	POSITION lastTimestampedEventTail = nullptr;
	int eventCountAtLastTimestamp = -1;
	int rpmEventsSinceTimestamp = 0;
	sumTimeDeltas = 0.0;


	char* data = &dataStream[0];
	bool updateProgress = (progressBar != nullptr);
	bool timeInitialized = false;


	// If there are time deltas in the stream then they have to be handled by the COM processing
	// function in CMainForm. That requires the following structure for communication.
	CMainForm::backgroundComProcessing bkgProcessing;
	bkgProcessing.init(&eventList, currentFormConfig);

	uint32_t timeDeltaValue;
	std::shared_ptr<char[]> tempTDArray;

	tempTDArray.reset(new char[dataLength]);	// Match the size of the incoming data (superset, so it's overkill but safe)
	bkgProcessing.dataStream = tempTDArray;

	// Build the prefix list
	dataPrefixes = TOKEN_Full_String;

	// The import screen
	DynoImportDataScreen dids;



	// Parse the data and look for the tokens. Don't assume that it starts or ends correctly because
	// it could be mid-stream.
	// TODO: We'll be able to pick up mid-stream and continue from last time when more data is available.

	bool forceStopPlayback = false;		// FOR NOW: Leaving the flag in case we want to control this later

	// Initialize the progress bar
	if (updateProgress)
		progressBar->SetRange32(0, dataLength);

	int index = 0;
	int startPos = -1, endPos;

	while ((index < dataLength) && !forceStopPlayback)
		{
		// Update the progress bar
		if(updateProgress)
			progressBar->SetPos(index);

		// Windows message pump while we're in this loop
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			}

		// Check to see if the playback has been stopped (assume paused)
		if (forceStopPlayback)
			break;

		wchar_t byte = data[index];

		// Start by skipping over data that doesn't start with a sentinel
		if ((index < dataLength - 1) && (dataPrefixes.Find(byte) < 0))
			{
			index++;
			continue;
			}

		// Now that we have the start sentinel, keep reading digits until the next sentinel or end is reached
		int position = dataPrefixes.Find(byte);

		// If it is a sentinel then it is either the start or end of a reading
		if ((position > -1) || (index == dataLength - 1))
			{
			// If there is already a start position then assume that this is the start of the next block
			if (startPos > -1)
				{
				endPos = index - 1;

				// If we had reached the end then be sure to pick up the last character
				if (position < 0)
					{
					endPos++;
					}

				// Extract the value from between the sentinels
				currentValue = CString(data + startPos + 1, endPos - startPos);
				currentValue.Trim();

				// Get the current system time (FOR NOW, even though it's arbitrary, really)
				GetLocalTime(&currentTime);

				// Deal with the value accordingly
				switch (data[startPos])
					{
					int tempInt;

					case TOKEN_timestamp:		// Timestamp (assumed to be a Unix time_t integer)

						tempInt = _wtoi(currentValue);
						eventList.AddTail(DynoEvent(DynoEvent::EventType::Timestamp, tempInt, 0, currentTime, 0.0));

						// If the start time hasn't been set yet then assume this is it
						if (!timeInitialized)
							{
							UnixTimeToSystemTime(tempInt, &startTime);
							lastTimeStamp = startTime;
							lastTimestampedEventTail = bkgProcessing.incomingEventList->GetTailPosition();
							eventCountAtLastTimestamp = bkgProcessing.incomingEventList->GetCount();
							rpmEventsSinceTimestamp = 0;

							timeInitialized = true;
							}
						else
							{
							// Just keep updating the end time every time we find a new one
							UnixTimeToSystemTime(tempInt, &endTime);

							// Start by finding the number of seconds between timestamps
							double differenceInSeconds = SystemTimeDifference(lastTimeStamp, endTime);

							// If there is no difference in time then make it 1 second per RPM Event
							if (!differenceInSeconds)
								differenceInSeconds = (double)rpmEventsSinceTimestamp;

							// Next, split it up so that it is evenly distributed between RPM events
							differenceInSeconds /= (double)rpmEventsSinceTimestamp;

							// From the last time stamp until the current one, update all of the values with a relative timestamp.
							// We know the position of the tail at the last update, so we'll start there.

							// First, we have to figure out what the distribution should be.
							int eventsToUpdate = bkgProcessing.incomingEventList->GetCount() - eventCountAtLastTimestamp;

							POSITION updatePosition = lastTimestampedEventTail ? lastTimestampedEventTail : bkgProcessing.incomingEventList->GetHeadPosition();
							SYSTEMTIME advancingTime = endTime;

							for(int updateTimeIndex = 0; updateTimeIndex < eventsToUpdate; updateTimeIndex++)
								{
								DynoEvent* tempEvent = &bkgProcessing.incomingEventList->GetNext(updatePosition);

								// Check to see what type of Event it is
								if (tempEvent->type == DynoEvent::RPM_drum)
									{
									// If it's an RPM event then increment the time before updating it
									AddSecondsToSystemTime(&advancingTime, &advancingTime, differenceInSeconds);
									tempEvent->timestamp = advancingTime;
									tempEvent->absTime = SystemTimeDifference(startTime, tempEvent->timestamp);

									// NOTE: The smoothing is being forced because importing drum RPM is normally done using time_delta,
									// which is smoothed at the time. Because this is essentially being hardcoded into the GUI, the
									// smoothing is assumed to have been pre-import.
									// In the future, this is something that can be changed.
									tempEvent->smoothedParameter_ema = tempEvent->originalParameter;
									tempEvent->finalSmoothed = true;
									}
								else if (tempEvent->type == DynoEvent::Timestamp)
									{
									// Don't do anything because it will modify the event timestamp
									;
									}
								else
									{
									// For non-RPM Events that aren't Timestamps, use the most recent time (as likely set by previous RPM Event)
									tempEvent->timestamp = advancingTime;
									}
								}

							// Mark this as the most recent timestamp and keep going
							lastTimeStamp = endTime;
							lastTimestampedEventTail = bkgProcessing.incomingEventList->GetTailPosition();
							eventCountAtLastTimestamp = bkgProcessing.incomingEventList->GetCount();
							rpmEventsSinceTimestamp = 0;
							}

						break;

					case TOKEN_speed:			// Speed reading						
						eventList.AddTail(DynoEvent(DynoEvent::EventType::Speed, _wtof(currentValue), currentFormConfig->units, currentTime, 0.0));
						break;

					case TOKEN_rpm_engine:		// Engine tachometer reading
						eventList.AddTail(DynoEvent(DynoEvent::EventType::RPM_engine, _wtof(currentValue), 0, currentTime, 0.0));
						rpmEventsSinceTimestamp++;
						break;

					case TOKEN_rpm_drum_front:		// Drum tachometer reading
						eventList.AddTail(DynoEvent(DynoEvent::EventType::RPM_drum, _wtof(currentValue), 0, currentTime, 0.0));
						rpmEventsSinceTimestamp++;
						break;

					case TOKEN_horsepower:		// Horsepower reading
						eventList.AddTail(DynoEvent(DynoEvent::EventType::Horsepower, _wtof(currentValue), 0, currentTime, 0.0));
						break;

					case TOKEN_torque:			// Torque reading
						eventList.AddTail(DynoEvent(DynoEvent::EventType::Torque, _wtof(currentValue), 0, currentTime, 0.0));
						break;
						
					case TOKEN_voltage:			// Voltage reading
						eventList.AddTail(DynoEvent(DynoEvent::EventType::Voltage, _wtof(currentValue), 0, currentTime, 0.0));
						break;

					case TOKEN_sound:			// Sound reading
//						eventList.AddTail(DynoEvent(DynoEvent::EventType::Sound, _wtof(currentValue), 0, currentTime, 0.0));

						// Use the COM processing function to process the time deltas and append the Events.
						// Before calling it, point to the single-reading alternative data input stream.
						// It is created by copying the integer (cast as uint16_t) into the pre-established fixed buffer.
						timeDeltaValue = _wtoi(currentValue);
						tempTDArray[bkgProcessing.altDataBufferLength] = TOKEN_sound;
						memcpy((void*)&tempTDArray[bkgProcessing.altDataBufferLength + 1], (void*)&timeDeltaValue, currentFormConfig->timeDeltaByteLength);
						bkgProcessing.altDataBufferLength += currentFormConfig->timeDeltaByteLength + 1;


						break;

					case TOKEN_vibration:	// Vibration reading
						eventList.AddTail(DynoEvent(DynoEvent::EventType::Vibration, _wtof(currentValue), 0, currentTime, 0.0));
						break;

					case TOKEN_tmp_room:	// Temperature reading
						eventList.AddTail(DynoEvent(DynoEvent::EventType::Temperature, _wtof(currentValue), 0, currentTime, 0.0));
						break;

					case TOKEN_humidity:	// Humidity reading
						eventList.AddTail(DynoEvent(DynoEvent::EventType::Humidity, _wtof(currentValue), 0, currentTime, 0.0));
						break;

					case TOKEN_pressure:	// Pressure reading
						eventList.AddTail(DynoEvent(DynoEvent::EventType::Pressure, _wtof(currentValue), 0, currentTime, 0.0));
						break;

					case TOKEN_CO:	// Carbon monoxide reading
						eventList.AddTail(DynoEvent(DynoEvent::EventType::CO, _wtof(currentValue), 0, currentTime, 0.0));
						break;

					case TOKEN_timeDelta_mf:		// Time delta reading

						// Use the COM processing function to process the time deltas and append the Events.
						// Before calling it, point to the single-reading alternative data input stream.
						// It is created by copying the integer (cast as uint16_t) into the pre-established fixed buffer.
						timeDeltaValue = _wtoi(currentValue);
						tempTDArray[bkgProcessing.altDataBufferLength] = TOKEN_timeDelta_mf;
						memcpy((void*)&tempTDArray[bkgProcessing.altDataBufferLength+1], (void*)&timeDeltaValue, currentFormConfig->timeDeltaByteLength);
						bkgProcessing.altDataBufferLength += currentFormConfig->timeDeltaByteLength+1;
						rpmEventsSinceTimestamp++;

						break;

					case TOKEN_endDrumEncoder:		// Transmit all of the time delta values up that are currently in the buffer

						// If there is no data in the alternate stream then don't call the processing function.
						if (!bkgProcessing.altDataBufferLength)
							{
							MessageBox(NULL, _T("Import file contains a time delta end sentinel 'X' but there are no values for time delta."
								" Double-check that the tick counters are prefixed with a 'D'."), _T("Error importing data"), MB_ICONINFORMATION);

							break;
							}

/*						bkgProcessing.psplines_N = 0;
						bkgProcessing.psplines_M = 50;
						bkgProcessing.psplines_RHO = 10;

						CMainForm::backgroundPollCOMPort(static_cast<LPVOID>(&bkgProcessing));

						exportName.Format(_T("Dyno_export-(N%d, M%d, Rho%d).csv"), bkgProcessing.psplines_N, bkgProcessing.psplines_M, bkgProcessing.psplines_RHO);
						exportData(exportName);
*/

						// TODO: The location of this is a hack, for now, during development.
						// The import screen will be moved and modified later.

						dids.N = 23;
						dids.M = 58;
						dids.Rho = 6;

						if (dids.DoModal() == IDOK)
							{
							bkgProcessing.psplines_N = dids.N > 0 ? dids.N : 23;
							bkgProcessing.psplines_M = dids.M > 0 && dids.M < 501 ? dids.M : 58;
							bkgProcessing.psplines_RHO = dids.Rho > -6 && dids.Rho < 13 ? dids.Rho : 3;

							bkgProcessing.clearRunVars();
							bkgProcessing.incomingEventList->RemoveAll();

							SYSTEMTIME startTestTime, endTestTime;
							GetLocalTime(&startTestTime);

							CMainForm::backgroundPollCOMPort(static_cast<LPVOID>(&bkgProcessing));
							GetLocalTime(&endTestTime);

//							exportName.Format(_T("C:\\Users\\cattr\\Documents\\Work Related\\Dyno\\Application\\Testing\\Dyno_export-(Error_A %000000.5lf, Error_R %000000.5lf,Time %00000.1lfs, N%d, M%d, Rho%d).csv"),
//								bkgProcessing.totalAbsError, bkgProcessing.totalRegError, SystemTimeDifference(startTestTime, endTestTime), bkgProcessing.psplines_N, bkgProcessing.psplines_M, bkgProcessing.psplines_RHO);

							exportName.Format(_T("Dyno_export-(Error_A %000000.5lf, Error_R %000000.5lf,Time %00000.1lfs, N%d, M%d, Rho%d).csv"),
								bkgProcessing.totalAbsError, bkgProcessing.totalRegError, SystemTimeDifference(startTestTime, endTestTime), bkgProcessing.psplines_N, bkgProcessing.psplines_M, bkgProcessing.psplines_RHO);


							exportData(exportName);
							}




/*
						int N, M, RHO;

						for (N = 5; N < 29; N += 2)
							{
							CString exportName;
							SYSTEMTIME startTestTime, endTestTime;

							for (M = (N < 50 ? 50 : N+5); M < 91; M++)
								{
								for (RHO = 0; RHO < 11; RHO++)
									{
									bkgProcessing.psplines_N = N;
									bkgProcessing.psplines_M = M;
									bkgProcessing.psplines_RHO = RHO;

									bkgProcessing.incomingEventList->RemoveAll();
									bkgProcessing.clearRunVars();

									GetLocalTime(&startTestTime);

									CMainForm::backgroundPollCOMPort(static_cast<LPVOID>(&bkgProcessing));
									GetLocalTime(&endTestTime);

									// Output all of the data to a CSV
									exportName.Format(_T("C:\\Users\\cattr\\Documents\\Work Related\\Dyno\\Application\\Testing\\Dyno_export-(Error_A %000000.5lf, Error_R %000000.5lf,Time %00000.1lfs, N%d, M%d, Rho%d).csv"),
										bkgProcessing.totalAbsError, bkgProcessing.totalRegError, SystemTimeDifference(startTestTime, endTestTime), bkgProcessing.psplines_N, bkgProcessing.psplines_M, bkgProcessing.psplines_RHO);

									exportData(exportName);
									}
								}
							}
*/

						// Add the list of time deltas to the imported sum
						sumTimeDeltas += bkgProcessing.totalTime;
						
						// After the time delta stream is processed, reset it
						bkgProcessing.altDataBufferLength = 0;

						break;

					default:

						// If the event is not in the list then put in a placeholder regardless
						eventList.AddTail(DynoEvent(DynoEvent::EventType::NotSet, _wtof(currentValue), 0, currentTime, 0.0));
						break;
					}

				// Clear out the values and continue
				startPos = endPos = -1;
				}
			else
				{
				// Mark the start position
				startPos = index++;
				}

			// Head back to the start of the loop
			continue;
			}

		index++;

		}

	// Mark it as "Imported"
	imported = true;

	// Return after the input has been processed
	return true;
	}

// Load a prerecorded session.
// The calling function is responsible for opening and closing the file.
bool DynoFileDetails::loadSession(CFile &loadFile)
	{
	bool result = true;

	// Load using serialization
	CArchive ar(&loadFile, CArchive::load);		// Create the archive
	Serialize(ar);								// Serialize it
	ar.Close();									// Close the archive

	return result;
	}

// Save the current session.
// The calling function is responsible for opening a valid file and then closing it.
bool DynoFileDetails::saveSession(CFile &saveFile)
	{
	bool result = true;

	// Get the current time and store it in the Session file
	GetLocalTime(&saveTime);

	// Save the file title and path
	filepath = saveFile.GetFilePath();
	fileTitle = saveFile.GetFileTitle();


	CArchive ar(&saveFile, CArchive::store);	// Create the archive		
	Serialize(ar);								// Serialize it
	ar.Close();									// Close the archive

	return result;
	}

// Export the Events to a CSV file.
// TODO: Later, there might be some customization of what is being exported.
bool DynoFileDetails::exportData(CString filename)
	{
	CStdioFile csvFile;

	if (csvFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
		DynoEvent* tempEvent;
		POSITION pos = eventList.GetHeadPosition();

		csvFile.WriteString(_T("Type,Abs_time,accel_o,accel_s,param_o,param_s,param_e,td_o,td_s,td_e\n"));

		while (pos)
			{
			CString record;

			tempEvent = &eventList.GetNext(pos);

			record.Format(_T("%d,%000000.10lf,%000000.10lf,%000000.10lf,"
				"%000000.10lf,%000000.10lf,%000000.10lf,"
				"%000000.10lf,%000000.10lf,%000000.10lf\n"),
				tempEvent->type, tempEvent->absTime, tempEvent->originalAcceleration, tempEvent->smoothedAcceleration,
				tempEvent->originalParameter, tempEvent->smoothedParameter_sma, tempEvent->smoothedParameter_ema,
				tempEvent->originalTimeDelta, tempEvent->smoothedTimeDelta_sma, tempEvent->smoothedTimeDelta_ema);

			csvFile.WriteString(record);
			}

		csvFile.Close();		// Close the recording file
		}
	else
		{
		CString error = GetLastErrorAsString();

		MessageBox(NULL, error, _T("Export Session error"), MB_ICONERROR);
		return false;
		}

	return true;
	}

// Return the timestamp of the first Event
SYSTEMTIME DynoFileDetails::getFirstEventTime()
	{
	SYSTEMTIME startTime = { 0 };

	if (eventList.GetCount() > 0)
		{
		startTime = eventList.GetHead().timestamp;
		}

	return startTime;
	}

// Return the timestamp of the first Event
SYSTEMTIME DynoFileDetails::getFirstRPMEventTime()
	{
	SYSTEMTIME firstRPMTime = { 0 };
	DynoEvent* tempEvent;

	POSITION pos = eventList.GetHeadPosition();

	// Roll through the list until we hit the first RPM Event
	while (pos)
		{
		tempEvent = &eventList.GetNext(pos);

		// The first RPM_drum Event we come across, return the timestamp
		if (tempEvent->type == DynoEvent::EventType::RPM_drum)
			{
			firstRPMTime = tempEvent->timestamp;
			break;
			}
		}

	return firstRPMTime;
	}



// Return the timestamp of the last Event
SYSTEMTIME DynoFileDetails::getLastEventTime()
	{
	SYSTEMTIME endTime = { 0 };

	if (eventList.GetCount() > 0)
		{
		endTime = eventList.GetTail().timestamp;
		}

	return endTime;
	}

// Serialize the Session details
void DynoFileDetails::Serialize(CArchive& ar)
	{
	// Saving versus loading data
	if (ar.IsStoring())
		{
		// Save the configuration file that was used when the Session was created
		currentFormConfig->Serialize(ar);

		// Filename info
		ar << filepath;
		ar << fileTitle;

		// Misc
		ar << imported;

		// Time variables

		ar << startTime.wDay;
		ar << startTime.wDayOfWeek;
		ar << startTime.wHour;
		ar << startTime.wMilliseconds;
		ar << startTime.wMinute;
		ar << startTime.wMonth;
		ar << startTime.wSecond;
		ar << startTime.wYear;

		ar << endTime.wDay;
		ar << endTime.wDayOfWeek;
		ar << endTime.wHour;
		ar << endTime.wMilliseconds;
		ar << endTime.wMinute;
		ar << endTime.wMonth;
		ar << endTime.wSecond;
		ar << endTime.wYear;

		ar << saveTime.wDay;
		ar << saveTime.wDayOfWeek;
		ar << saveTime.wHour;
		ar << saveTime.wMilliseconds;
		ar << saveTime.wMinute;
		ar << saveTime.wMonth;
		ar << saveTime.wSecond;
		ar << saveTime.wYear;

		// Save the number of events
		int eventCount = eventList.GetCount();
		ar << eventCount;

		// Point to the first Event in the list
		POSITION pos = eventList.GetHeadPosition();

		// Roll through the events and save them
		for (int index = 0; index < eventCount; index++)
			{
			eventList.GetNext(pos).Serialize(ar);
			}
		}
	else
		{
		// Save the configuration file that was used when the Session was created
		currentFormConfig->Serialize(ar);

		// Filename info
		ar >> filepath;
		ar >> fileTitle;

		// Misc
		ar >> imported;

		// Time variables

		ar >> startTime.wDay;
		ar >> startTime.wDayOfWeek;
		ar >> startTime.wHour;
		ar >> startTime.wMilliseconds;
		ar >> startTime.wMinute;
		ar >> startTime.wMonth;
		ar >> startTime.wSecond;
		ar >> startTime.wYear;

		ar >> endTime.wDay;
		ar >> endTime.wDayOfWeek;
		ar >> endTime.wHour;
		ar >> endTime.wMilliseconds;
		ar >> endTime.wMinute;
		ar >> endTime.wMonth;
		ar >> endTime.wSecond;
		ar >> endTime.wYear;

		ar >> saveTime.wDay;
		ar >> saveTime.wDayOfWeek;
		ar >> saveTime.wHour;
		ar >> saveTime.wMilliseconds;
		ar >> saveTime.wMinute;
		ar >> saveTime.wMonth;
		ar >> saveTime.wSecond;
		ar >> saveTime.wYear;

		// Save the number of events
		int eventCount;
		ar >> eventCount;

		// Roll through the Events and add them
		for (int index = 0; index < eventCount; index++)
			{
			DynoEvent event;

			event.Serialize(ar);							// Load the next one
			eventList.AddTail(event);						// Add it to the end of the Event list

			if(event.type == DynoEvent::EventType::RPM_drum)
				sumTimeDeltas += event.originalTimeDelta;	// Build the absolute time
			}
		}
	}


