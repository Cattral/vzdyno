
// DynoMFCXP32View.h : interface of the CDynoMFCXP32View class
//

#pragma once


class CDynoMFCXP32View : public CView
{
protected: // create from serialization only
	CDynoMFCXP32View() noexcept;
	DECLARE_DYNCREATE(CDynoMFCXP32View)

// Attributes
public:
	CDynoMFCXP32Doc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CDynoMFCXP32View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	};

#ifndef _DEBUG  // debug version in DynoMFCXP32View.cpp
inline CDynoMFCXP32Doc* CDynoMFCXP32View::GetDocument() const
   { return reinterpret_cast<CDynoMFCXP32Doc*>(m_pDocument); }
#endif

