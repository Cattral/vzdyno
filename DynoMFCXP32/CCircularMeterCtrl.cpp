#include "pch.h"
#include "CCircularMeterCtrl.h"

#include "MemDC.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCircularMeterCtrl

CCircularMeterCtrl::CCircularMeterCtrl()
	{

	m_dCurrentValue = 0.0;
	m_dMaxValue = 360.0;
	m_dMinValue = 0.0;

	m_nScaleDecimals = 1;
	m_nValueDecimals = 3;

	m_colorNeedle = RGB(0, 255, 255);

	}

CCircularMeterCtrl::~CCircularMeterCtrl()
	{
	
	if ((m_pBitmapOldBackground) &&
		(m_bitmapBackground.GetSafeHandle()) &&
		(m_dcBackground.GetSafeHdc()))
		m_dcBackground.SelectObject(m_pBitmapOldBackground);

	
	}


BEGIN_MESSAGE_MAP(CCircularMeterCtrl, CStatic)
	//{{AFX_MSG_MAP(CCircularMeterCtrl)
	ON_WM_PAINT()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CCircularMeterCtrl message handlers

void CCircularMeterCtrl::OnPaint()
	{
	CPaintDC dc(this); // device context for painting

	
	// Find out how big we are
	GetClientRect(&m_rectCtrl);

	// make a memory dc
	COMemDC memDC(&dc, &m_rectCtrl);

	// Set up a memory DC for the background stuff if one isn't being used
	if ((m_dcBackground.GetSafeHdc() == NULL) || (m_bitmapBackground.m_hObject == NULL))
		{
		m_dcBackground.CreateCompatibleDC(&dc);
		m_bitmapBackground.CreateCompatibleBitmap(&dc, m_rectCtrl.Width(), m_rectCtrl.Height());
		m_pBitmapOldBackground = m_dcBackground.SelectObject(&m_bitmapBackground);

		// Fill this bitmap with the background.

		// Note: This requires some serious drawing and calculating, 
		// therefore it is drawn "once" to a bitmap and 
		// the bitmap is stored and blt'd when needed.
		DrawMeterBackground(&m_dcBackground, m_rectCtrl);
		}

	// Drop in the background
	memDC.BitBlt(0, 0, m_rectCtrl.Width(), m_rectCtrl.Height(), &m_dcBackground, 0, 0, SRCCOPY);

	// Add the needle to the background
	DrawNeedle(&memDC);

	// Add the value to the background
	DrawValue(&memDC);	
	}

void CCircularMeterCtrl::DrawValue(CDC* pDC)
	{
	CFont* pFontOld;
	CString strTemp;

	// Pick up the font.
	// Note: the font was determined in the drawing of the background
	pFontOld = pDC->SelectObject(&m_fontValue);

	// Set the colors based on the stored colour values
	pDC->SetTextColor(m_valueTextColour);
	pDC->SetBkColor(m_valueBackgroundColour);


	// Draw the text in the recessed rectangle
	pDC->SetTextAlign(TA_CENTER | TA_BASELINE);
	strTemp.Format(_T("%.*f"), m_nValueDecimals, m_dCurrentValue);
	pDC->TextOut(m_nValueCenter, m_nValueBaseline, strTemp);

	// Restore the color and the font
	pDC->SetBkColor(m_windowColour);
	pDC->SelectObject(pFontOld);
	}

void CCircularMeterCtrl::UpdateNeedle(double dValue)
	{
	m_dCurrentValue = dValue;
	Invalidate();
	}

void CCircularMeterCtrl::DrawNeedle(CDC* pDC)
	{
	CPoint pointNeedle[3];
	CPen penDraw, * pPenOld;
	CBrush brushFill, * pBrushOld;
	double dAngleRad;
	double adjustedValue = m_dCurrentValue;		// Grab the current value for scaling, if necessary
	double dTemp;
	int nResult;

	// This function draws a triangular needle.
	// The base of the needle is a horizontal line that runs through the center point of the arcs
	// that make up the face of the meter.
	// The tip of the needle is at an angle that is calculated based on the current value and the scale.

	// The needle is constructed as a 3-point polygon (i.e. triangle). The triangle is drawn to the 
	// screen based on a clipping region that is derived from the meter face.  See "DrawMeterBackground".

	// Calculate the first and last points of the needle.
	// NOTE: The size is hard coded to (-2, -2) - (2, 2) for now. It can be made dynamic later.
	pointNeedle[0].x = m_nBottomCX - 2;
	pointNeedle[0].y = m_nBottomCY - 2;
	pointNeedle[2].x = m_nBottomCX + 2;
	pointNeedle[2].y = m_nBottomCY + 2;


	// If there is a scaler on the value (e.g. 1000 RPM = 1, ..., 8000 RPM = 8) then adjust it
	if (m_needleScaler)
		adjustedValue *= m_needleScaler;

	// Calculate the angle for the tip of the needle
	dAngleRad = (adjustedValue - m_dMinValue) * (m_dRightAngleRad - m_dLeftAngleRad) /
		(m_dMaxValue - m_dMinValue) + m_dLeftAngleRad;

	// If the angle is beyond the meter, draw the needle at the limit (as if it is "pegged")
	dAngleRad = max(dAngleRad, m_dRightAngleRad);
	dAngleRad = min(dAngleRad, m_dLeftAngleRad);

	// Calculate the X position of the tip
	dTemp = m_nBottomCX + m_nTopRadius * cos(dAngleRad);
	pointNeedle[1].x = ROUND(dTemp);

	// Calculate the Y position of the tip
	dTemp = m_nBottomCY - m_nTopRadius * sin(dAngleRad);
	pointNeedle[1].y = ROUND(dTemp);

	// Select the clipping region based on the meter
	pDC->SelectClipRgn(&m_rgnBoundary);

	// Create a pen and brush based on the needle color
	brushFill.CreateSolidBrush(m_colorNeedle);
	penDraw.CreatePen(PS_SOLID, 1, m_colorNeedle);

	// Select the pen and brush
	pPenOld = pDC->SelectObject(&penDraw);
	pBrushOld = pDC->SelectObject(&brushFill);

	// Draw the needle
	pDC->Polygon(pointNeedle, 3);

	// Restore the clipping region
	nResult = pDC->SelectClipRgn(NULL);

	// Restore the pen and brush
	pDC->SelectObject(pPenOld);
	pDC->SelectObject(pBrushOld);	
	}

void CCircularMeterCtrl::DrawMeterBackground(CDC* pDC, CRect& rect)
	{
	int nAngleDeg, nRef;
	int nHeight;
	int nHalfPoints;
	int nStartAngleDeg, nEndAngleDeg;
	int nTickDeg;
	int nAngleIncrementDeg;

	double dTemp, dAngleRad, dX, dY;
	double dRadPerDeg;


	CString strTemp;

	CBrush brushFill, * pBrushOld;
	CFont* pFontOld;
	CPen penDraw, * pPenOld;
	TEXTMETRIC tm;
	CPoint tickLabelPoint[9];


	nRef = 0;

	nHalfPoints = nRef;   // hold onto this for later use

/*


	// create a region based on the meter face
	// (to be used later in clipping the needle)
	m_rgnBoundary.DeleteObject();
	m_rgnBoundary.CreatePolygonRgn(m_pointBoundary, nRef, ALTERNATE);

*/

	// Fill the background
	brushFill.DeleteObject();
	brushFill.CreateSolidBrush(m_windowColour);			// Black
	pBrushOld = pDC->SelectObject(&brushFill);

	penDraw.DeleteObject();
	penDraw.CreatePen(PS_SOLID, 2, m_windowColour);
	pPenOld = pDC->SelectObject(&penDraw);
	pDC->Rectangle(rect);

	// Create the outside of the meter
	penDraw.DeleteObject();
	penDraw.CreatePen(PS_SOLID, 1, m_faceBorderColour);
	pDC->SelectObject(&penDraw);

	// Bring the meter outline down and to the right
	m_rectFace.left = rect.left + rect.Width() / 6;
	m_rectFace.top = rect.top;
	m_rectFace.right = rect.right - rect.Width() / 6;
	m_rectFace.bottom = rect.bottom;

	// At this point, we're going to enforce that it is a circle, rather than a non-circular ellipse
	int whDifference = m_rectFace.Width() - m_rectFace.Height();

	if (whDifference < 0)
		{
		// If it's narrower than it is tall then adjust the height.
		// Because the bottom border is fixed, just squish it downward by adjusting the top.
		m_rectFace.top -= whDifference;
		}
	else if (whDifference > 0)
		{
		// If it's taller than it is wide then adjust the width. This should be done to
		// best keep the box centered. If the value is odd then shift one more to the right
		if (whDifference % 2)
			{
			m_rectFace.right--;
			whDifference--;
			}

		// Pinch it equally on both sides
		m_rectFace.right -= whDifference/2;
		m_rectFace.left += whDifference / 2;
		}

	// Determine the center point of the radii for the meter face
	m_nBottomCX = (m_rectFace.left + m_rectFace.right) / 2;
	m_nBottomCY = (m_rectFace.top + m_rectFace.bottom) / 2;

	// Determine the radii for the top of the meter and the bottom of the meter
	m_nTopRadius = static_cast<int>((static_cast<double>(m_rectFace.Height()) *.5));
	m_nBottomRadius = static_cast<int>((static_cast<double>(m_rectFace.Height()) * .1));

	// After the rectangle for the face has been finalized, locate the one for the value
	m_rectValue.left = m_rectFace.left + (LONG)(m_rectFace.Width() / 5);
	m_rectValue.right = m_rectFace.right - (LONG)(m_rectFace.Width() / 5);
	m_rectValue.bottom = m_rectFace.bottom -(LONG)(m_rectFace.Width() *.15);
	m_rectValue.top = m_rectFace.top +(LONG)(m_rectFace.Width() * .7);

	// Calculate the radians per degree.
	// This helps me lay things out in degrees, which are more intuitive than radians.
	dRadPerDeg = 4.0 * atan(1.0) / 180.0;

	// Set the left and right limits for the meter face
	nStartAngleDeg = 0;
	nEndAngleDeg = 180;
	nTickDeg = 15;

	// This is the density of points along the arcs
	nAngleIncrementDeg = 5;

	// Convert these to radians for computer (rather than human) use
	m_dLeftAngleRad = nEndAngleDeg * dRadPerDeg;
	m_dRightAngleRad = nStartAngleDeg * dRadPerDeg;


	// Draw the meter face; for now, just an ellipse in the new rectangle
	pDC->Ellipse(m_rectFace);

	// Draw the tiny dot in the center of the meter dial
	// FOR NOW: It is a fixed size of (-2, -2) - (2, 2) but this can be dynamic later
	pDC->Ellipse(m_nBottomCX - 2, m_nBottomCY - 2, m_nBottomCX + 2, m_nBottomCY + 2);

	// Restore the brush
	pDC->SelectObject(pBrushOld);

	penDraw.DeleteObject();
	penDraw.CreatePen(PS_SOLID, 1, m_tickMarkColour);
	pDC->SelectObject(&penDraw);

	// Draw the tick marks.
	// Make the tick marks extend down 20% of the face
	dTemp = m_nTopRadius - 0.2 * (m_nTopRadius - m_nBottomRadius);

	// Draw the tick marks
	for (nAngleDeg = 180; nAngleDeg >= nStartAngleDeg; nAngleDeg -= nTickDeg)
		{
		dAngleRad = nAngleDeg * dRadPerDeg;

		// move to the top of the tick mark
		dX = m_nBottomCX + m_nTopRadius * cos(dAngleRad);
		dY = m_nBottomCY - m_nTopRadius * sin(dAngleRad);
		pDC->MoveTo(ROUND(dX), ROUND(dY));

		// move to the bottom of the tick mark
		dX = m_nBottomCX + dTemp * cos(dAngleRad);
		dY = m_nBottomCY - dTemp * sin(dAngleRad);
		pDC->LineTo(ROUND(dX), ROUND(dY));
		}

	/*
	// draw the left side tick marks
	for (nAngleDeg = 90 + nTickDeg; nAngleDeg < nEndAngleDeg; nAngleDeg += nTickDeg)
		{
		dAngleRad = nAngleDeg * dRadPerDeg;

		// move to the top of the tick mark
		dX = m_nBottomCX + m_nTopRadius * cos(dAngleRad);
		dY = m_nBottomCY - m_nTopRadius * sin(dAngleRad);
		pDC->MoveTo(ROUND(dX), ROUND(dY));

		// move to the bottom of the tick mark
		dX = m_nBottomCX + dTemp * cos(dAngleRad);
		dY = m_nBottomCY - dTemp * sin(dAngleRad);
		pDC->LineTo(ROUND(dX), ROUND(dY));
		}
*/
	// now we're done with the pen!
	pDC->SelectObject(pPenOld);

	if (drawNumberBorder)
		{
		// Draw the recessed rectangle for the numerical value.
		LONG vbLeft, vbRight, vbTop, vbBottom;
		vbLeft = m_rectValue.left; //  +(LONG)(m_rectValue.Width() / 3.5);
		vbRight = m_rectValue.right; //  -(LONG)(m_rectValue.Width() / 3.5);
		vbTop = m_rectValue.top;
		vbBottom = m_rectValue.bottom;

		// Draw the left and top sides with the shadow
		penDraw.DeleteObject();
		penDraw.CreatePen(PS_SOLID, 2, m_valueBorderShadow);
		pPenOld = pDC->SelectObject(&penDraw);
		pDC->MoveTo(vbLeft, vbBottom);
		pDC->LineTo(vbLeft, vbTop);
		pDC->LineTo(vbRight, vbTop);
		pDC->SelectObject(pPenOld);

		// Draw the right and bottom sides with the highlight
		penDraw.DeleteObject();
		penDraw.CreatePen(PS_SOLID, 2, m_valueBorderHighlite);
		pPenOld = pDC->SelectObject(&penDraw);
		pDC->LineTo(vbRight, vbBottom);
		pDC->LineTo(vbLeft, vbBottom);
		pDC->SelectObject(pPenOld);
		}

	
	// NOTE: Right now, the font is the same for both the title and the value
	
	// Determine the font size
	nHeight = m_rectValue.Height() * 85 / 100;

	m_fontValue.DeleteObject();
	m_fontValue.CreateFont(nHeight, 0, 0, 0, FW_EXTRABOLD,
		FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("Ariel"));

	// Now select this font and calculate the actual height to see what Windows gave us
	pFontOld = pDC->SelectObject(&m_fontValue);
	pDC->GetTextMetrics(&tm);
	m_nValueFontHeight = tm.tmHeight;


	// Determine the location for the value within the rectangle based on the font height
	m_nValueBaseline = m_rectValue.bottom - m_nValueFontHeight / 4;
	m_nValueCenter = m_rectValue.left + m_rectValue.Width() / 2;

	// Use the font to draw some text with the specified text and background colours
	// Set the colors (based on system colors)
	pDC->SetTextColor(m_graphNameColour);
	pDC->SetBkColor(m_windowColour);				// Use the same as the regular background

	// Draw the name of the meter below the face
	pDC->SetTextAlign(TA_CENTER | TA_BASELINE);
	pDC->TextOut(m_nValueCenter, m_rectValue.top - m_nValueFontHeight / 4, m_strMeterName);

	
	// Work around the meter face and put the values for the tick marks
	pDC->SetTextAlign(TA_LEFT | TA_BASELINE);
	pDC->SetTextColor(m_tickLabelColour);


	LONG _x = rect.left, _y = rect.bottom - m_nValueFontHeight * 25 / 100;

	// Initialize the list of coordinates to (-1,-1) to indicate that they aren't being used
	for (auto &point : tickLabelPoint)
		point.x = point.y = -1;


	//m_nValueCenter
	int arrLength = sizeof(tickValues) / sizeof(int);
	int middle_val = (arrLength-1)/2;

	// FOR NOW: Manually put values at the Left, Top, and Right Axis positions.
	// When it's time to put the proper values in, just fill in the coordinates.
	tickLabelPoint[0].x = m_rectFace.left - (LONG)(m_nValueFontHeight / 1.5);
	tickLabelPoint[0].y = m_nBottomCY;
	if(tickValues[middle_val]>=100)
		tickLabelPoint[4].x = m_nBottomCX - (LONG)(m_nValueFontHeight / 1.5);		// This should change, depending on how many characters there are in the label
	else
		tickLabelPoint[4].x = m_nBottomCX - (LONG)(m_nValueFontHeight / 3);		// This should change, depending on how many characters there are in the label

	tickLabelPoint[4].y = m_rectFace.top - (LONG)(m_nValueFontHeight / 4);
	tickLabelPoint[8].x = m_rectFace.right + (LONG)(m_nValueFontHeight / 4);
	tickLabelPoint[8].y = m_nBottomCY;


	int numberIndex = 0;

	// Roll through the list of values
	for (auto i : tickValues)
		{
		// If the point is valid then display the corresponding value
		if (tickLabelPoint[numberIndex].x > -1)
			{
			strTemp.Format(_T("%d"), i);
			pDC->TextOut(tickLabelPoint[numberIndex].x, tickLabelPoint[numberIndex].y, strTemp);

			// NOTE: If we ever want to have the precision in the labels then we'll need this.
			// The logic above would best be modified, as well.
			// strTemp.Format(_T("%.*lf"), m_nScaleDecimals, m_dMinValue);
			}

		numberIndex++;
		}

	// Restore the font and background color
	pDC->SelectObject(pFontOld);
	pDC->SetBkColor(m_windowColour);
	}


void CCircularMeterCtrl::OnSize(UINT nType, int cx, int cy)
	{
	CStatic::OnSize(nType, cx, cy);

	ReconstructControl();
	}

void CCircularMeterCtrl::ReconstructControl()
	{
	// If we've got a stored background - remove it!
	if ((m_pBitmapOldBackground) &&
		(m_bitmapBackground.GetSafeHandle()) &&
		(m_dcBackground.GetSafeHdc()))
		{
		m_dcBackground.SelectObject(m_pBitmapOldBackground);
		m_dcBackground.DeleteDC();
		m_bitmapBackground.DeleteObject();
		}

	Invalidate();
	}

void CCircularMeterCtrl::SetRange(double dMin, double dMax)
	{
	m_dMaxValue = dMax;
	m_dMinValue = dMin;
	ReconstructControl();
	}

void CCircularMeterCtrl::SetScaleDecimals(int nDecimals)
	{
	m_nScaleDecimals = nDecimals;
	ReconstructControl();
	}

void CCircularMeterCtrl::SetValueDecimals(int nDecimals)
	{
	m_nValueDecimals = nDecimals;
	ReconstructControl();
	}

void CCircularMeterCtrl::SetName(CString& strMeterName)
	{
	m_strMeterName = strMeterName;
	ReconstructControl();
	}

void CCircularMeterCtrl::SetWindowColour(COLORREF newColour)
	{
	m_windowColour = newColour;
	ReconstructControl();
	}

void CCircularMeterCtrl::SetGraphNameColour(COLORREF newColour)
	{
	m_graphNameColour = newColour;
	ReconstructControl();
	}

void CCircularMeterCtrl::SetNeedleColour(COLORREF newColour)
	{
	m_colorNeedle = newColour;
	ReconstructControl();
	}

void CCircularMeterCtrl::SetTickMarkColour(COLORREF newColour)
	{
	m_tickMarkColour = newColour;
	ReconstructControl();
	}

void CCircularMeterCtrl::SetTickLabelColour(COLORREF newColour)
	{
	m_tickLabelColour = newColour;
	ReconstructControl();
	}

void CCircularMeterCtrl::SetFaceBorderColour(COLORREF newColour)
	{
	m_faceBorderColour = newColour;
	ReconstructControl();
	}

// Set the new colours for text lettering and background
void CCircularMeterCtrl::SetValueColour(COLORREF newTextColour, COLORREF newBackGroundColour)
	{
	m_valueTextColour = newTextColour;
	m_valueBackgroundColour = newBackGroundColour;

	ReconstructControl();
	}

void CCircularMeterCtrl::SetValueBorderColours(COLORREF highlight, COLORREF shadow)
	{
	m_valueBorderHighlite = highlight;
	m_valueBorderShadow = shadow;

	ReconstructControl();
	}

// Replace the entire set of tick values (which are actually labels)
void CCircularMeterCtrl::SetTickValues(int* newValues)
	{
	// Roll through the values and update them.
	// Assume the the incoming values have the proper number.
	int index = 0;
	for (auto& oldValue : tickValues)
		{
		oldValue = newValues[index++];
		}
	}


