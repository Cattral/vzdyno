// VehicleLookupScreen.cpp : implementation file
//

#include "pch.h"
#include "DynoMFCXP32.h"
#include "VehicleLookupScreen.h"
#include "afxdialogex.h"


// VehicleLookupScreen dialog

IMPLEMENT_DYNAMIC(VehicleLookupScreen, CDialogEx)

VehicleLookupScreen::VehicleLookupScreen(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_VEHICLE_LOOKUP_SCREEN, pParent)
{

}

VehicleLookupScreen::~VehicleLookupScreen()
{
}

void VehicleLookupScreen::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_VEHICLE_LOOKUP_VIN, m_vin);
	DDX_Control(pDX, IDC_VEHICLE_LOOKUP_MAKE, m_make);
	DDX_Control(pDX, IDC_VEHICLE_LOOKUP_MODEL, m_model);
	DDX_Control(pDX, IDC_VEHICLE_LOOKUP_YEAR, m_year);
	}


BEGIN_MESSAGE_MAP(VehicleLookupScreen, CDialogEx)
	ON_CBN_SELCHANGE(IDC_VEHICLE_LOOKUP_MAKE, &VehicleLookupScreen::OnCbnSelchangeVehicleLookupMake)
END_MESSAGE_MAP()


// VehicleLookupScreen message handlers


BOOL VehicleLookupScreen::OnInitDialog()
	{
	CDialogEx::OnInitDialog();

	// FOR NOW: Add the list of makes manually.
	// Later, this will be done by retrieving the list from the server.
	int element;
	element = m_make.AddString(_T("-Select make-"));

	element = m_make.AddString(_T("Acura"));
	element = m_make.AddString(_T("Alfa Romeo"));
	element = m_make.AddString(_T("Audi"));
	element = m_make.AddString(_T("BMW"));
	element = m_make.AddString(_T("Bentley"));
	element = m_make.AddString(_T("Buick"));
	element = m_make.AddString(_T("Cadillac"));
	element = m_make.AddString(_T("Chevrolet"));
	element = m_make.AddString(_T("Chrysler"));
	element = m_make.AddString(_T("Dodge"));
	element = m_make.AddString(_T("Fiat"));
	element = m_make.AddString(_T("Ford"));
	element = m_make.AddString(_T("GMC"));
	element = m_make.AddString(_T("Genesis"));
	element = m_make.AddString(_T("Honda"));
	element = m_make.AddString(_T("Hyundai"));
	element = m_make.AddString(_T("Infiniti"));
	element = m_make.AddString(_T("Jaguar"));
	element = m_make.AddString(_T("Jeep"));
	element = m_make.AddString(_T("Kia"));
	element = m_make.AddString(_T("Land Rover"));
	element = m_make.AddString(_T("Lexus"));
	element = m_make.AddString(_T("Lincoln"));
	element = m_make.AddString(_T("Lotus"));
	element = m_make.AddString(_T("Maserati"));
	element = m_make.AddString(_T("Mazda"));
	element = m_make.AddString(_T("Mercedes-Benz"));
	element = m_make.AddString(_T("Mercury"));
	element = m_make.AddString(_T("Mini"));
	element = m_make.AddString(_T("Mitsubishi"));
	element = m_make.AddString(_T("Nissan"));
	element = m_make.AddString(_T("Polestar"));
	element = m_make.AddString(_T("Pontiac"));
	element = m_make.AddString(_T("Porsche"));
	element = m_make.AddString(_T("Ram"));
	element = m_make.AddString(_T("Rivian"));
	element = m_make.AddString(_T("Rolls-Royce"));
	element = m_make.AddString(_T("Saab"));
	element = m_make.AddString(_T("Saturn"));
	element = m_make.AddString(_T("Scion"));
	element = m_make.AddString(_T("Smart"));
	element = m_make.AddString(_T("Subaru"));
	element = m_make.AddString(_T("Suzuki"));
	element = m_make.AddString(_T("Tesla"));
	element = m_make.AddString(_T("Toyota"));
	element = m_make.AddString(_T("Volkswagen"));
	element = m_make.AddString(_T("Volvo"));

	//m_make.SetItemData(element, (DWORD_PTR)element);

	// The model and year are disabled to start with
	m_model.EnableWindow(false);
	m_year.EnableWindow(false);

	m_make.SetCurSel(0);


	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
	}


void VehicleLookupScreen::OnCbnSelchangeVehicleLookupMake()
	{
	// TODO: Add your control notification handler code here

	m_model.ResetContent();
	m_year.ResetContent();

	// If the make hasn't been selected then don't populate the make and model
	if (!m_make.GetCurSel())
		{
		m_model.EnableWindow(false);
		m_year.EnableWindow(false);

		return;
		}

	// The models will be populated from the database
	m_model.EnableWindow(true);
	m_model.AddString(_T("-Select model-"));

	// FOR NOW: Just put in a full list of years.
	// These will come from the database later, based on make and model.
	m_year.EnableWindow(true);
	m_year.AddString(_T("-Select year-"));
	for (int i = 2023; i > 1949; i--)
		{
		CString tempString;

		tempString.Format(_T("%d"), i);
		m_year.AddString(tempString);
		}

	m_model.SetCurSel(0);
	m_year.SetCurSel(0);

	}
