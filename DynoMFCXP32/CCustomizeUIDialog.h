#pragma once

#include "DynoConfig.h"

// CCustomizeUIDialog dialog

class CCustomizeUIDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CCustomizeUIDialog)

public:
	CCustomizeUIDialog(CWnd* pParent = nullptr);   // standard constructor
	virtual ~CCustomizeUIDialog();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DynoCustomizeUIDialog };
#endif


	// Extra variables added by Rob
	CMap<int, int, CString, CString> *colourNamesMap;
	DynoConfig localCfg, backupCfg;

	// Return the value of the modified flag
	bool isModified()
		{
		return modified;
		}


protected:

	CString holdString;	// Temp string used to see if the value has changed
	bool modified;		// Keep track of whether anything has been changed

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	void updateAllControls();
	void setModified(bool modFlag);


	DECLARE_MESSAGE_MAP()
public:
	CListBox m_colourTargets;
	CMFCColorButton m_colourSelectionButton;
	CEdit m_filename;
	CButton m_browseButton;
	virtual BOOL OnInitDialog();
	afx_msg void OnLbnSelcancelCustomUiColours();
	afx_msg void OnLbnSelchangeCustomUiColours();
	afx_msg void OnBnClickedCustomUiColourButton();
	afx_msg void OnBnClickedOk();
	CButton m_resetButton;
	CButton m_defaultsButton;
	CButton m_loadButton;
	CButton m_saveButton;
	afx_msg void OnBnClickedCustomUiReset();
	CEdit m_maxEdit;
	CEdit m_speedMax;
	CEdit m_rpmMax;
	CButton m_unitSelection;
	afx_msg void OnBnClickedCustomUiDefaults();
	CButton m_cancelButton;
	afx_msg void OnEnSetfocusCustomUiSpeedMax();
	afx_msg void OnEnKillfocusCustomUiSpeedMax();
	CEdit m_speed_redLine;
	CEdit m_rpm_redLine;
	afx_msg void OnEnSetfocusCustomUiSpeedRedline();
	afx_msg void OnEnKillfocusCustomUiSpeedRedline();
	afx_msg void OnEnSetfocusCustomUiRpmMax();
	afx_msg void OnEnKillfocusCustomUiRpmMax();
	afx_msg void OnEnSetfocusCustomUiRpmRedline();
	afx_msg void OnEnKillfocusCustomUiRpmRedline();
	afx_msg void OnBnClickedCustomUiMetric();
	afx_msg void OnBnClickedCustomUiImperial();
	CEdit m_torqueMax;
	CEdit m_hpMax;
	CComboBox m_torqueLineType;
	CComboBox m_hpLineType;
	CComboBox m_fuelType;
	CButton m_torqueShowAxis;
	CButton m_hpShowAxis;
	afx_msg void OnEnSetfocusCustomUiTorqueMax();
	afx_msg void OnEnKillfocusCustomUiTorqueMax();
	afx_msg void OnEnSetfocusCustomUiHpMax();
	afx_msg void OnBnClickedCustomUiShowTorqueAxis();
	afx_msg void OnBnClickedCustomUiShowHpAxis();
	afx_msg void OnCbnSelchangeCustomUiTorquePen();
	afx_msg void OnCbnSelchangeCustomUiHpPen();
	afx_msg void OnEnKillfocusCustomUiHpMax();

	CButton m_showGridlines;
	afx_msg void OnBnClickedCustomUiShowGridlines();
	CStatic m_inertiaConstText;
	CEdit m_inertiaConstValue;
	afx_msg void OnRButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnEnSetfocusCustomUiInertiaConstValue();
	afx_msg void OnEnKillfocusCustomUiInertiaConstValue();
	afx_msg void OnBnClickedCustomUiSavebutton();
	afx_msg void OnCbnSelchangeCustomUiFuelType();
};
