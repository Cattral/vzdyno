#include "pch.h"
#include "ComPort.h"

#include "DynoMFCXP32.h"

DWORD COM_writeSize = 0;

// Default constructor
ComPort::ComPort()
	{
	// The default constructor should begin by enumerating the ports

	}


ComPort::~ComPort()
	{
	}

// Open the specific COM port.
// If -1 is passed in for the port number then open the first available port in the list.
// Return the index of the port that was opened, or -1 on failure.

// FOR NOW: Assume No parity, 8 word length, 1 stop bit, and no flow control
int ComPort::Open(int portNumber, DWORD baudRate, int forcedPosition)
	{
	int openPort = -1;

	// First, make sure that there is a place to keep the COM handle
		int portPosition;
	
		if (forcedPosition == -1)
			portPosition = nextPortIndex();
		else
			portPosition = forcedPosition;

	// If there are no indexes available then return an error
	if (portPosition == -1)
		{
		lastErrorString = _T("No free COM handle indexes.");
		return -1;
		}

	// If the ports haven't been enumerated then return an error
	if (!portsEnumerated())
		{
		lastErrorString = _T("COM ports not enumerated.");
		return -1;
		}


	// If the port number was not specified then use the first free one
	if (portNumber == -1)
		{


		}

	COMMCONFIG config;
	CSerialPort2::GetDefaultConfig(portNumber, config);

	CSerialPort2& port = openPorts[portPosition];
	if (!port.Open(portNumber, baudRate, CSerialPort2::Parity::NoParity, 8, CSerialPort2::StopBits::OneStopBit, CSerialPort2::FlowControl::XonXoffFlowControl, TRUE))
		{
		CString errorMessage = GetLastErrorAsString();

		// Prepare the error message
		lastErrorString.Format(_T("Port: COM%u <%s>\r\nError: %s"), (portAndNames.at(portPosition)).first, (portAndNames.at(portPosition).second.c_str()), errorMessage);

		// Return unsuccessful
		return -1;
		}

	HANDLE hPort = port.Detach();
	port.Attach(hPort);

	DWORD dwModemStatus;
	port.GetModemStatus(dwModemStatus);

	DCB dcb;
	port.GetState(dcb);

	dcb.BaudRate = baudRate;
	port.SetState(dcb);

	systemWideBaud = baudRate;		// Needed for troubleshooting

	DWORD dwErrors = 0;
	port.ClearError(dwErrors);

	port.SetBreak();
	port.ClearBreak();

	COMSTAT stat{};
	port.GetStatus(stat);

	COMMTIMEOUTS timeouts{};
	port.GetTimeouts(timeouts);

	port.Setup(1024, 1024);

	port.GetConfig(config);

	config.dcb.BaudRate = baudRate;
	port.SetConfig(config);

	port.Set0WriteTimeout();
	port.Set0ReadTimeout();

	openList[portPosition] = portNumber;

	return portPosition;
	}


// Close the COM port.
// If -1 is passed in then close all of the open COM ports
void ComPort::Close(int portIndex)
	{
	// Roll through all of the port indexes
	for (int indexes = 0; indexes < maxPortCount; indexes++)
		{
		// If all of the ports are being closed or we found the match then close it
		if ((portIndex == -1) || (indexes == portIndex))
			{
			CSerialPort2& port = openPorts[indexes];

			// Close the port
			COMMPROP properties{};
			port.GetProperties(properties);

			// Clear the buffers
			port.ClearWriteBuffer();
			port.ClearReadBuffer();

			// Flush the buffer
			port.Flush();
			port.CancelIo();

			// Close the port
			port.Close();

			// Set the port specification to unused
			openList[indexes] = 0;
			}
		}
	}

// Return the number of characters that are waiting in the input queue
DWORD ComPort::getInputBufferCount(int portIndex)
	{
	// Grab the right port to work with
	CSerialPort2& port = openPorts[portIndex];

	DWORD bw;
	BOOL gotBytes = port.BytesWaiting(bw);

	return gotBytes ? bw : 0;
	}

// Reset the COM port.
// NOTE: This function should NOT be necessary in the future.
// A strange communication problem is causing the Send() function to fail,
// but it works when you close the port and re-open it. For now,
// this is what the Reset() function will do.
void ComPort::Reset(int portIndex)
	{
	int portNumber = openList[portIndex];

	CSerialPort2& port = openPorts[portNumber];

	// Close it
	Close(portIndex);

	// Re-open it
	(void)Open(portNumber, systemWideBaud, portIndex);
	}

// Terminate the IO operations and clear the buffers.
// NOTE: This is a diagnostic and troubleshooting function that
// should not be necessary while running in production.
void ComPort::ClearBuffers(int portIndex, COMMPROP &properties_entry, COMMPROP &properties_return)
	{
	int portNumber = openList[portIndex];

	CSerialPort2& port = openPorts[portIndex];

	port.GetProperties(properties_entry);

	// Abort and clear out the buffers
	port.TerminateOutstandingWrites();
	port.Purge(PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
	port.SetMask(EV_TXEMPTY);

	// Clear the buffers
	port.ClearWriteBuffer();
	port.ClearReadBuffer();

	// Flush the buffer
	port.Flush();
	port.CancelIo();

	port.GetProperties(properties_return);
	}


// Enumerate the COM ports using the specified method.
// NOTE: At this time, there is only one method of enumeration so the parameter is not used.
bool ComPort::enumerateComPorts(int method)
	{
	ULONGLONG nStartTick = GetTickCount();

	// Build the internal list, overwriting what is already there.
	bool result = CEnumerateSerial::UsingSetupAPI1(portAndNames);

	// Record the enumeration time
	if (result)
		{
		enumerationTime = GetTickCount() - nStartTick;
		enumerationError = ERROR_SUCCESS;
		enumeratedPorts = true;
		}
	else
		{
		enumerationTime = 0;
		enumerationError = GetLastError();
		enumeratedPorts = false;
		}

// Close down COM
//	CoUninitialize();

	return result;
	}

// Return the next available port index, or -1 if nothing is available
int ComPort::nextPortIndex()
	{
	for (auto index = 0; index < maxPortCount; index++)
		{
		if (openList[index] == 0)
			return(index);
		}

	// If it goes through the full list then there's no spot available
	return -1;
	}

bool ComPort::isOpen(UINT checkPort)
	{
	for (auto index = 0; index < maxPortCount; index++)
		{
		if (openList[index] == checkPort)
			return(true);
		}

	// If it goes through the full list then it isn't open
	return false;
	}

#pragma warning(suppress: 26461)
VOID WINAPI CompletionRoutine(_In_ DWORD dwErrorCode, _In_ DWORD dwNumberOfBytesTransfered, _Inout_ LPOVERLAPPED lpOverlapped) noexcept
	{
	UNREFERENCED_PARAMETER(dwErrorCode);
	UNREFERENCED_PARAMETER(dwNumberOfBytesTransfered);
	UNREFERENCED_PARAMETER(lpOverlapped);

	COM_writeSize = dwNumberOfBytesTransfered;

	}

// Send the specific data to the specific port index.
// Optionally, there is a post-write delay (postWriteDelayMs) so that the calling function
// can expect that a response will be available, if applicable.
DWORD ComPort::Send(int portIndex, char *data, int length, int postWriteDelayMs)
	{
	HANDLE hEvent = nullptr;
	DWORD size = 0;

	DWORD lastError;
	CString errorMessage;

	// Grab the right port to work with
	CSerialPort2& port = openPorts[portIndex];

	COMSTAT stat{};
	OVERLAPPED m_osWrite{};
	DWORD dwErrors = 0;

	hEvent = CreateEvent(nullptr, TRUE, FALSE, nullptr);

	#pragma warning(suppress: 26477)
	ATLASSERT(hEvent != nullptr);
	m_osWrite.hEvent = hEvent;

	DWORD x_lastError = GetLastError();
	CString x_errorMessage = GetLastErrorAsString();

	// NOTE:
	// When I come back to this, check here: https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-writefile
	//
	// The last thing that I noticed before stopping the exploration into the COM error is that when the Arduino
	// has a full transmit buffer (white test LED), we can't send anything. Maybe the receive buffer on the Arduino
	// side is full and in a "transmit" loop?
	// If so, then things need to be cleared by the Arudino, and it figure this out by itself. If the transmit is full
	// then it needs to just stop sending and maybe even clear its own buffers to whatever extent possible.

	char temp__data[10000];
	memset(temp__data, 0, 10000);

	DWORD temp__length = length > 100 ? (DWORD)100 : (DWORD)length;
	memcpy(temp__data, data, temp__length);

	DWORD y_lastError = GetLastError();
	CString y_errorMessage = GetLastErrorAsString();

	BOOL writeStatus = port.Write(data, length, m_osWrite);

	DWORD z_lastError = GetLastError();
	CString z_errorMessage = GetLastErrorAsString();

	// Write the data
//	BOOL writeStatus = port.Write(data, length, m_osWrite);

	//If the write didn't complete immediately then wait for it
	if (!writeStatus)
		{
		lastError = GetLastError();
		errorMessage = GetLastErrorAsString();

		// Prepare the error message
		lastErrorString.Format(_T("COM write error: %s"), errorMessage);


		// If it failed for a reason other than a pending write, return
		if (z_lastError != ERROR_IO_PENDING)
			{
			lastError = GetLastError();
			errorMessage = GetLastErrorAsString();

			// Prepare the error message
			lastErrorString.Format(_T("COM write error: %s"), errorMessage);

			// Free the handle
			CloseHandle(m_osWrite.hEvent);

			// About and clear out the buffers
			port.TerminateOutstandingWrites();
			port.Purge(PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
			port.SetMask(EV_TXEMPTY);


			COMMPROP properties{};
			port.GetProperties(properties);

			// Clear the buffers
			port.ClearWriteBuffer();
			port.ClearReadBuffer();

			// Flush the buffer
			port.Flush();
			port.CancelIo();

			Sleep(2000);

			// Close the port
			DCB dcb;
			port.GetState(dcb);
			port.Close();
			
			openList[portIndex] = port.Open(portIndex, dcb.BaudRate);


			return 0;
			}

		BOOL waitStatus = WaitForSingleObject(m_osWrite.hEvent, 3000);

		DWORD za_lastError = GetLastError();
		CString za_errorMessage = GetLastErrorAsString();

		if (waitStatus != WAIT_OBJECT_0)
			{
			// If it can't write then forget about it
			lastError = GetLastError();
			errorMessage = GetLastErrorAsString();

			// Prepare the error message
			lastErrorString.Format(_T("COM write error: %s"), errorMessage);


			// Free the handle
			CloseHandle(m_osWrite.hEvent);

			// About and clear out the buffers
			port.TerminateOutstandingWrites();
			port.Purge(PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
			port.SetMask(EV_TXEMPTY);

			COMMPROP properties{};
			port.GetProperties(properties);

			// Clear the buffers
			port.ClearWriteBuffer();
			port.ClearReadBuffer();

			// Flush the buffer
			port.Flush();
			port.CancelIo();

			Sleep(100);
			
			// Close the port
//			DCB dcb;
//			port.GetState(dcb);
//			port.Close();
//			openList[portIndex] = port.Open(portIndex, dcb.BaudRate);


			return -1;
			}

		// The result should be waiting now
		writeStatus = port.GetOverlappedResult(m_osWrite, size, FALSE);

		if (!writeStatus)
			{
			// Serious error - exit

			lastError = GetLastError();
			errorMessage = GetLastErrorAsString();

			// Prepare the error message
			lastErrorString.Format(_T("COM write error: %s"), errorMessage);


			// Free the handle
			CloseHandle(m_osWrite.hEvent);

			// About and clear out the buffers
			port.TerminateOutstandingWrites();
			port.Purge(PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
			port.SetMask(EV_TXEMPTY);

			return 0;
			}

		}


		COMMPROP properties{};
		port.GetProperties(properties);


	// Free the handle
	CloseHandle(m_osWrite.hEvent);

	// Before returning, delay the processing if required
	Sleep(postWriteDelayMs);

	return size;


/*

		if (lastError == ERROR_IO_PENDING)
			{
			// Try one more time to get the overlapped event

			writeStatus = port.GetOverlappedResult(m_osWrite, size, FALSE);

			if (!writeStatus)
				{
				lastError = GetLastError();
				errorMessage = GetLastErrorAsString();

				// Free the handle
				CloseHandle(m_osWrite.hEvent);

				// About and clear out the buffers
				port.TerminateOutstandingWrites();
				port.Purge(PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
				port.SetMask(EV_TXEMPTY);

				return 0;
				}
			}

		}
*/

/*

	if (!status)
		{
		// There was a timeout
		lastError = GetLastError();
		errorMessage = GetLastErrorAsString();

		// Prepare the error message
		lastErrorString.Format(_T("COM write error: %s"), errorMessage);


		if (lastError == ERROR_IO_PENDING)
			{
			status = port.GetOverlappedResult(m_osWrite, size, TRUE);

			}
		else
			{


			}
		}

*/

	/*
	status = WaitForSingleObject(m_osWrite.hEvent, 3000);

	if (status != WAIT_OBJECT_0)
		{
		int a = 5;

		lastError = GetLastError();
		errorMessage = GetLastErrorAsString();

		if (lastError == ERROR_IO_PENDING)
			{
			status = WaitForSingleObject(m_osWrite.hEvent, 5000);

			if (status != WAIT_OBJECT_0)
				{
				lastError = GetLastError();
				errorMessage = GetLastErrorAsString();

				// NOTE: This is extreme, but for now, clear all of the buffers and let it return

				DWORD bw;
				BOOL gotBytes = port.BytesWaiting(bw);

				port.TerminateOutstandingReads();
				port.TerminateOutstandingWrites();

				// Clear the buffers
				port.ClearWriteBuffer();
				port.ClearReadBuffer();

				// Flush the buffer
				port.Flush();
				port.CancelIo();

				DWORD dwMask = 0;
				port.GetMask(dwMask);

				port.SetMask(EV_TXEMPTY);

				// Free the handle
				CloseHandle(m_osWrite.hEvent);

				port.Purge(PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

				return 0;
				}

			}



		}
	*/

/*
	if (!status)
		{
		// There was a timeout
		lastError = GetLastError();
		errorMessage = GetLastErrorAsString();

		// Prepare the error message
		lastErrorString.Format(_T("COM write error: %s"), errorMessage);


		if (lastError == ERROR_IO_PENDING)
			{
			
			status = WaitForSingleObject(m_osWrite.hEvent, 3000);

			if (status != WAIT_OBJECT_0)
				{
				// There was a timeout
				lastError = GetLastError();
				errorMessage = GetLastErrorAsString();

				// NOTE: This is extreme, but for now, clear all of the buffers and let it return

				DWORD bw;
				BOOL gotBytes = port.BytesWaiting(bw);

				port.TerminateOutstandingReads();
				port.TerminateOutstandingWrites();

				// Clear the buffers
				port.ClearWriteBuffer();
				port.ClearReadBuffer();

				// Flush the buffer
				port.Flush();
				port.CancelIo();

				DWORD dwMask = 0;
				port.GetMask(dwMask);

				port.SetMask(EV_TXEMPTY);

				// Free the handle
				//CloseHandle(m_osWrite.hEvent);

				port.Purge(PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

				return 0;
				}

			size = 0;
			status = port.GetOverlappedResult(m_osWrite, size, FALSE);

			if (!status)
				{
				lastError = GetLastError();
				errorMessage = GetLastErrorAsString();

				// Prepare the error message
				lastErrorString.Format(_T("COM write error: %s"), errorMessage);

				if (lastError == ERROR_IO_PENDING)
					{
					// Just no sending the data...
					int a = 1;
					}
				}
			}
		else
			{
			// An error has occurred
			return 0;
			}
		}

	port.SetMask(EV_TXEMPTY);

	// Free the handle
	CloseHandle(m_osWrite.hEvent);

	return size;
*/
	}

// Send the specific data to the specific port index
DWORD ComPort::Send(int portIndex, CString data)
	{
	int len = data.GetLength();
	char buffer[500];
	memset(buffer, 0, 500);

	// Convert the wide character string (LPWSTR) into a char* and call the other Send()
	size_t convertedCount = wcstombs(buffer, data.GetBuffer(), 100);

	return(Send(portIndex, buffer, convertedCount));
	}

// Read from the specified COM port.
// Return the amount of data that was read.
// The data buffer needs to be 
DWORD ComPort::Read(int portIndex, char *data, DWORD bytesToRead)
	{
	DWORD toRead = 0, didRead = 0;

	// If we try to read from a port that isn't open then just return
	if (openList[portIndex] == 0)
		return 0;

	// Grab the right port to work with
	CSerialPort2& port = openPorts[portIndex];

	COMSTAT stat{};
	OVERLAPPED m_osRead{};
	DWORD dwErrors = 0;

	m_osRead.hEvent = CreateEvent(nullptr, TRUE, FALSE, nullptr);

	port.ClearError(dwErrors);
	port.GetStatus(stat);

	// If there is nothing in the input queue then return
	if (!stat.cbInQue)
		return 0;

//	DWORD mask = EV_RXCHAR;

//	port.WaitEvent(mask, m_osRead);

	BOOL status = port.Read(data, bytesToRead, m_osRead, &didRead);

	if (!status)
		{
		if (GetLastError() == ERROR_IO_PENDING)
			{
			port.GetOverlappedResult(m_osRead, didRead, TRUE);
			}
		else
			{
			// An error has occurred
			return 0;
			}
		}

	port.GetStatus(stat);

	if (stat.cbInQue)
		{
		// Need to keep reading
		int a = stat.cbInQue;

		}


	// Free the handle
	CloseHandle(m_osRead.hEvent);

//	port.Purge(PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);


	return didRead;
	}

