// DynoImportDataScreen.cpp : implementation file
//

#include "pch.h"
#include "DynoMFCXP32.h"
#include "DynoImportDataScreen.h"
#include "afxdialogex.h"


// DynoImportDataScreen dialog

IMPLEMENT_DYNAMIC(DynoImportDataScreen, CDialogEx)

DynoImportDataScreen::DynoImportDataScreen(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_ImportDataScreen, pParent)
{

}

DynoImportDataScreen::~DynoImportDataScreen()
{
}

void DynoImportDataScreen::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMPORT_N, m_N);
	DDX_Control(pDX, IDC_IMPORT_M, m_M);
	DDX_Control(pDX, IDC_IMPORT_RHO, m_Rho);
	}


BEGIN_MESSAGE_MAP(DynoImportDataScreen, CDialogEx)
END_MESSAGE_MAP()


// DynoImportDataScreen message handlers


BOOL DynoImportDataScreen::OnInitDialog()
	{
	CDialogEx::OnInitDialog();

	CString tempString;

	// Display the preset values
	tempString.Format(_T("%d"), N);
	m_N.SetWindowTextW(tempString);

	tempString.Format(_T("%d"), M);
	m_M.SetWindowTextW(tempString);

	tempString.Format(_T("%d"), Rho);
	m_Rho.SetWindowTextW(tempString);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
	}


void DynoImportDataScreen::OnOK()
	{
	CString tempString;

	// Copy the values into the variables
	m_N.GetWindowText(tempString);
	N = _wtoi(tempString);

	m_M.GetWindowText(tempString);
	M = _wtoi(tempString);

	m_Rho.GetWindowText(tempString);
	Rho = _wtoi(tempString);

	CDialogEx::OnOK();
	}
