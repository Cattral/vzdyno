// CModelessReplayDialog.cpp : implementation file
//

#include "pch.h"

#include "DynoMFCXP32.h"

#include "CModelessReplayDialog.h"
#include "afxdialogex.h"


// CModelessReplayDialog dialog

IMPLEMENT_DYNAMIC(CModelessReplayDialog, CDialogEx)

CModelessReplayDialog::CModelessReplayDialog(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MODELESS_REPLAY_DIALOG, pParent)
{
	m_parent = pParent;		// Keep track of the parent window
}

CModelessReplayDialog::~CModelessReplayDialog()
{
}

void CModelessReplayDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_REPLAY_CONTROL_TEXT, m_text);
	DDX_Control(pDX, IDC_REPLAY_CONTROL_SLIDER, m_slider);
	DDX_Control(pDX, IDC_REPLAY_CONTROL_PLAY_BUTTON, m_playButton);
	DDX_Control(pDX, IDC_REPLAY_CONTROL_CLOSE_BUTTON, m_closeFileButton);
	DDX_Control(pDX, IDC_REPLAY_CONTROL_SPEED_SELECTOR, m_replaySpeed);
	DDX_Control(pDX, IDC_REPLAY_CONTROL_LOOP, m_loopButton);
	DDX_Control(pDX, IDC_REPLAY_CONTROL_PROGRESS, m_progress);
	}


BEGIN_MESSAGE_MAP(CModelessReplayDialog, CDialogEx)
	ON_BN_CLICKED(IDC_REPLAY_CONTROL_PLAY_BUTTON, &CModelessReplayDialog::OnBnClickedReplayControlPlayButton)
	ON_BN_CLICKED(IDC_REPLAY_CONTROL_CLOSE_BUTTON, &CModelessReplayDialog::OnBnClickedReplayControlCloseButton)
	ON_CBN_SELCHANGE(IDC_REPLAY_CONTROL_SPEED_SELECTOR, &CModelessReplayDialog::OnCbnSelchangeReplayControlSpeedSelector)
	ON_BN_CLICKED(IDC_REPLAY_CONTROL_LOOP, &CModelessReplayDialog::OnBnClickedReplayControlLoop)
	ON_WM_HSCROLL()
END_MESSAGE_MAP()


// CModelessReplayDialog message handlers


void CModelessReplayDialog::PostNcDestroy()
	{
	// TODO: Add your specialized code here and/or call the base class

	CDialogEx::PostNcDestroy();
	delete this;
	}

void CModelessReplayDialog::OnCancel()
	{
	// Don't let the user cancel out of the Dialog, for now
	
	// Most likely, this will just hide the window


//	CDialogEx::OnCancel();
	}

BOOL CModelessReplayDialog::OnInitDialog()
	{
	CDialogEx::OnInitDialog();

	// Put the choices into the speed selection drop list
	m_replaySpeed.SetItemData(m_replaySpeed.AddString(Constants.String_ReplaySpeed_Fast.c_str()), Constants.replaySpeed_ms_fast);
	m_replaySpeed.SetItemData(m_replaySpeed.AddString(Constants.String_ReplaySpeed_Medium.c_str()), Constants.replaySpeed_ms_medium);
	m_replaySpeed.SetItemData(m_replaySpeed.AddString(Constants.String_ReplaySpeed_Slow.c_str()), Constants.replaySpeed_ms_slow);
	m_replaySpeed.SetItemData(m_replaySpeed.AddString(Constants.String_ReplaySpeed_Realtime.c_str()), Constants.replaySpeed_ms_realTime);

	m_replaySpeed.SetCurSel(0);
	OnCbnSelchangeReplayControlSpeedSelector();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
	}

// When the Play Button is pressed, send a message to the MainForm
void CModelessReplayDialog::OnBnClickedReplayControlPlayButton()
	{
	// Get the position of the track bar and send it back as a "start" position for replay
	m_parent->PostMessage(WM_COMMAND, IDC_REPLAY_CONTROL_PLAY_BUTTON, 0);
	}

// When the Close File Button is pressed, send a message to the MainForm
void CModelessReplayDialog::OnBnClickedReplayControlCloseButton()
	{
	m_parent->PostMessage(WM_COMMAND, IDC_REPLAY_CONTROL_CLOSE_BUTTON, 0);
	}

// Specify the size of the file
void CModelessReplayDialog::SetFileSize(int fileLength)
	{
	filesize = fileLength;		// Store the new size

	// Set the range for the slider and the tick frequency
	m_slider.SetTicFreq(static_cast<int>(filesize / 10));
	m_slider.SetRange(0, filesize, TRUE);

	// If we're setting a new file size then it's safe to assume that we can reset
	SetStatus(Constants.PlaybackStatus_Not_started);
	}

// Set the current file position and return the old one
int CModelessReplayDialog::SetFilePos(int filePos)
	{
	int oldPos = currentPostion;		// Remember the original position

	currentPostion = filePos;			// Set the new position
	m_slider.SetPos(currentPostion);	// Adjust the slider control

	return oldPos;
	}

// Set the text in the control
void CModelessReplayDialog::SetText(CString text)
	{
	m_text.SetWindowTextW(text);
	}

// Set the playback status, ignoring what it is now
void CModelessReplayDialog::SetStatus(int status)
	{
	playbackStatus = status;

	// Depending on the status, take the appropriate action
	if (status == Constants.PlaybackStatus_Not_started)
		{
		// If it's not started then set the button to Play and move the slider to the far left
		playing = false;
		m_playButton.SetWindowTextW(_T("&Play"));
		m_playButton.EnableWindow(TRUE);
		m_closeFileButton.EnableWindow(TRUE);
		m_slider.ShowWindow(SW_SHOW);
		m_progress.ShowWindow(SW_HIDE);
		(void)SetFilePos(0);
		}
	else if (status == Constants.PlaybackStatus_File_Parsing)
		{
		playing = false;

		m_playButton.SetWindowTextW(_T("&Play"));
		m_playButton.EnableWindow(FALSE);
		m_closeFileButton.EnableWindow(FALSE);
		m_slider.ShowWindow(SW_HIDE);
		m_progress.ShowWindow(SW_SHOW);
		}
	else if (status == Constants.PlaybackStatus_Playing)
		{
		playing = true;

		m_playButton.SetWindowTextW(_T("&Pause"));
		m_playButton.EnableWindow(TRUE);
		m_closeFileButton.EnableWindow(FALSE);
		m_slider.ShowWindow(SW_SHOW);
		m_progress.ShowWindow(SW_HIDE);
		}
	else if (status == Constants.PlaybackStatus_Paused)
		{
		playing = false;
		m_playButton.SetWindowTextW(_T("&Resume"));
		m_playButton.EnableWindow(TRUE);
		m_closeFileButton.EnableWindow(TRUE);
		m_slider.ShowWindow(SW_SHOW);
		m_progress.ShowWindow(SW_HIDE);
		}
	else if (status == Constants.PlaybackStatus_Ended)
		{
		playing = false;
		m_playButton.SetWindowTextW(_T("&Restart"));
		m_playButton.EnableWindow(TRUE);
		m_closeFileButton.EnableWindow(TRUE);
		m_slider.ShowWindow(SW_SHOW);
		m_progress.ShowWindow(SW_HIDE);
		}
	else
		{
		// Catch it if the status is invalid
		playbackStatus = -1;
		MessageBox(_T("Internal error: Invalid playback status"), _T("Error"), MB_ICONERROR);
		}
	}

void CModelessReplayDialog::OnCbnSelchangeReplayControlSpeedSelector()
	{
	// When the selector changes, update the local variable with the associated data
	speedSelection = m_replaySpeed.GetItemData(m_replaySpeed.GetCurSel());
	}

void CModelessReplayDialog::OnBnClickedReplayControlLoop()
	{
	loop = m_loopButton.GetCheck() > 0;
	}

// When the slider is moved, this message is called.
// Reposition the file index as it is moved.
void CModelessReplayDialog::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
	{
	CSliderCtrl* pSlider = reinterpret_cast<CSliderCtrl*>(pScrollBar);

	// If the track bar for file position was moved then deal with it
	if (pSlider == &m_slider)
		{
		CString displayText;

		// If the file had played to the end and the track bar is moved then
		// change the status to "Paused" to that the "Restart" does not take it to position 0.
		if (playbackStatus == Constants.PlaybackStatus_Ended)
			SetStatus(Constants.PlaybackStatus_Paused);

		// Adjust the position of the file
		SetFilePos(pSlider->GetPos());

		// Display the Event number
		if (pSlider->GetPos() == pSlider->GetRangeMax())
			{
			displayText.Format(_T("Positioned on final event, #%d"), pSlider->GetPos() + 1);
			SetStatus(Constants.PlaybackStatus_Ended);
			}
		else
			displayText.Format(_T("Play starting at event #%u"), pSlider->GetPos() +1);

		m_text.SetWindowTextW(displayText);
		}

	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
	}
