
// DynoMFCXP32.h : main header file for the DynoMFCXP32 application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols

#include <winbase.h>
#include <winnt.h>
#include <time.h>

#define DYNO_GUI 1			// Indicates to conditional compile statements that we're in the GUI code


#include "ComPort.h"
#include "DynoConfig.h"



// Global function declarations
CString GetLastErrorAsString();
void UnixTimeToFileTime(time_t t, LPFILETIME pft);
void UnixTimeToSystemTime(time_t t, LPSYSTEMTIME pst);
double SystemTimeDifference(SYSTEMTIME time_1, SYSTEMTIME time_2);
void AddSecondsToSystemTime(SYSTEMTIME* timeIn, SYSTEMTIME* timeOut, double tfSeconds);


// Constants

static struct 
	{
	// Titles and label strings
	const std::wstring String_not_connected = _T("Not connected");
	const std::wstring String_pollingActive = _T("Polling");
	const std::wstring String_pollingPaused = _T("Polling paused");
	const std::wstring String_menu_connect = _T("&Connect");
	const std::wstring String_menu_openPort = _T("&Open port");
	const std::wstring String_companyName = _T("VZ Performance");
	const std::wstring String_horsepower_v = _T("H\nO\nR\nS\nE\nP\nO\nW\nE\nR");
	const std::wstring String_torque_v = _T("T\nO\nR\nQ\nU\nE");
	const std::wstring String_afr_v = _T("A\nF\nR");
	const std::wstring String_rpm_h = _T("RPM");
	const std::wstring String_rpm_v = _T("\n\nR\nP\nM");
	const std::wstring String_time_h = _T("TIME");
	const std::wstring String_speed_v = _T("\n\nS\nP\nE\nE\nD");
	const std::wstring String_rich = _T("RICH");
	const std::wstring String_lean = _T("LEAN");
	const std::wstring String_smallLeanRich = _T("L\nE\nA\nN\n\nR\nI\nC\nH");

	const std::wstring String_hp_label = _T("HP =");
	const std::wstring String_tq_label = _T("TQ =");
	const std::wstring String_afr_label = _T("AFR =");
	const std::wstring String_tqarea_label = _T("TQ\nAREA  =");
	const std::wstring String_accel_label = _T("ACCEL =");

	// Timer event IDs
	const int timer_initial = 1;
	const int timer_polling = 2;

	// Colours
	enum ColourTarget {
		colour_target_Background, colour_target_Labels, colour_target_Graph_names,
		colour_target_Meter_FaceBorder, colour_target_Meter_Tickmark, colour_target_Meter_valueText,
		colour_target_Meter_valueBorderHighlite, colour_target_Meter_valueBorderShadow,
		colour_target_SpdNeedleNormal, colour_target_SpdNeedleHigh, colour_target_TachNeedleNormal,
		colour_target_TachNeedleHigh, colour_target_RichLeanFill,
		colour_target_htrChart_border,
		colour_target_htrChart_background, colour_target_htrChart_grid,
		colour_target_htrChart_torqueAxisLabels, colour_target_htrChart_hpAxisLabels,
		colour_target_htrChart_rpmAxisLabels,
		colour_target_htrChart_torqueLine, colour_target_htrChart_hpLine, 
		colour_target_htrChart_rpmLine, colour_target_htrChart_speedLine,
		colour_target_count			// The count always has to be the final one
		};

	// UI-related strings
	
	// Colours
	const std::wstring String_Background = _T("Background");
	const std::wstring String_Labels = _T("Labels");
	const std::wstring String_Graph_names = _T("Graph names");
	const std::wstring String_Meter_FaceBorder = _T("Meter face borders");
	const std::wstring String_Meter_Tickmark = _T("Meter tick marks");
	const std::wstring String_Meter_valueText = _T("Meter value text");
	const std::wstring String_Meter_valueHighlight  = _T("Meter value border highlight");
	const std::wstring String_Meter_valueBorderShadow = _T("Meter value border shadow");	
	const std::wstring String_SpdNeedleNormal = _T("Speedometer needle normal");
	const std::wstring String_SpdNeedleHigh = _T("Speedometer needle high");
	const std::wstring String_TachNeedleNormal = _T("Tachometer needle normal");
	const std::wstring String_TachNeedleHigh = _T("Tachometer needle high");
	const std::wstring String_RichLeanFill = _T("Rich/Lean fill");
	const std::wstring String_THPChart_border = _T("Main chart border");
	const std::wstring String_THPChart_background = _T("Main chart background");
	const std::wstring String_THPChart_grid = _T("Main chart grid");
	const std::wstring String_THPChart_torqueLabels = _T("Main chart left axis labels");
	const std::wstring String_THPChart_hpLabels = _T("Main chart right axis labels");
	const std::wstring String_THPChart_rpmLabels = _T("Main chart bottom axis labels");
	const std::wstring String_THPChart_torqueLine = _T("Main chart torque line");
	const std::wstring String_THPChart_hpLine = _T("Main chart horsepower line");
	const std::wstring String_THPChart_rpmLine = _T("Main chart RPM line");
	const std::wstring String_THPChart_speedLine = _T("Main chart speed line");

	// Pen types
	const std::wstring String_PenType_solid = _T("Solid");
	const std::wstring String_PenType_wide = _T("Wide");
	const std::wstring String_PenType_extraWide = _T("Extra wide");
	const std::wstring String_PenType_dash = _T("Dash");
	const std::wstring String_PenType_dot = _T("Dot");

	// Date and time formats
	const std::wstring String_DateFormat = _T("%A, %B %d, %Y");
	const std::wstring String_TimeDateFormat_24 = _T("%A, %B %d, %Y at %H:%M:%S");
	const std::wstring String_TimeDateFormat_12 = _T("%A, %B %d, %Y at %I:%M:%S %p");

	// Replay speed selector
	const std::wstring String_ReplaySpeed_Fast = _T("Fast forward");
	const int replaySpeed_ms_fast = 50;
	const std::wstring String_ReplaySpeed_Medium = _T("Medium speed");
	const int replaySpeed_ms_medium = 150;
	const std::wstring String_ReplaySpeed_Slow = _T("Slow motion");
	const int replaySpeed_ms_slow = 300;
	const std::wstring String_ReplaySpeed_Realtime = _T("Real-time replay");
	const int replaySpeed_ms_realTime = 0;

	// Miscellaneous system strings
	const std::wstring String_filename_config = _T("VZDyno.cfg");

	const std::wstring String_datafile_ext = _T("VZD");
	const std::wstring String_datafile_wc_ext = _T("*.VZD");
	const std::wstring String_datafile_filter = _T("VZDyno data file (*.VZD)|*.VZD|All files (*.*)|*.*||");

	const std::wstring String_importfile_ext = _T("TXT");
	const std::wstring String_importfile_wc_ext = _T("*.TXT");
	const std::wstring String_importfile_filter = _T("Text file for import (*.TXT)|*.TXT|All files (*.*)|*.*||");


	// List of processors
	const std::wstring String_processor_arduino = _T("Arduino Mega 2560");

	// List of sensors
	const std::wstring String_sensor_weather_temperature = _T("Weather station - room temperature");
	const std::wstring String_sensor_weather_pressure = _T("Weather station - pressure");
	const std::wstring String_sensor_weather_humidity = _T("Weather station - humidity");
	const std::wstring String_sensor_brake_front_tension = _T("Brake - front - tension");
	const std::wstring String_sensor_brake_rear_tension = _T("Brake - rear - tension");
	const std::wstring String_sensor_brake_front_encoder = _T("Brake - front - encoder");
	const std::wstring String_sensor_brake_rear_encoder = _T("Brake - rear - encoder");
	const std::wstring String_sensor_gearbox_front_temp = _T("Gearbox - front - temperature");
	const std::wstring String_sensor_gearbox_rear_temp = _T("Gearbox - rear - temperature");
	const std::wstring String_sensor_bearing_front_temp = _T("Bearing - front - temperature");
	const std::wstring String_sensor_bearing_rear_temp = _T("Bearing - rear - temperature");
	const std::wstring String_sensor_lockpin_LR_pos = _T("Locking pin - Left/Rear - position");
	const std::wstring String_sensor_lockpin_LF_pos = _T("Locking pin - Left/Front - position");
	const std::wstring String_sensor_lockpin_RF_pos = _T("Locking pin - Right/Front - position");
	const std::wstring String_sensor_lockpin_RR_pos = _T("Locking pin - Right/Rear - position");
	const std::wstring String_sensor_basemotor_front_prox = _T("Base - prox - front");
	const std::wstring String_sensor_basemotor_rear_prox = _T("Base - prox - rear");
	const std::wstring String_sensor_encoder_front_main = _T("Encoder - front - main");
	const std::wstring String_sensor_encoder_front_offset = _T("Encoder - front - offset");
	const std::wstring String_sensor_encoder_rear_main = _T("Encoder - rear - main");
	const std::wstring String_sensor_encoder_rear_offset = _T("Encoder - rear - offset");
	const std::wstring String_sensor_RPM = _T("RPM");
	const std::wstring String_sensor_AFR = _T("AFR");
	const std::wstring String_sensor_base_position = _T("Base - position");
	const std::wstring String_sensor_mains_detect = _T("MAINS - power detect");
	const std::wstring String_sensor_pendant_select = _T("Pendant - select button");
	const std::wstring String_sensor_pendant_exec = _T("Pendant - execute button");
	const std::wstring String_sensor_pendant_estop = _T("Pendant - e-stop button");

	// List of motors
	const std::wstring String_motor_base = _T("Base");
	const std::wstring String_motor_brake_front_o = _T("Brake - front - on/off");
	const std::wstring String_motor_brake_front_d = _T("Brake - front - direction");
	const std::wstring String_motor_brake_rear_o = _T("Brake - rear - on/off");
	const std::wstring String_motor_brake_rear_d = _T("Brake - rear - direction");
	const std::wstring String_motor_lockpin_LRO = _T("Locking pin - Left/Rear - on/off");
	const std::wstring String_motor_lockpin_LRD = _T("Locking pin - Left/Rear - direction");
	const std::wstring String_motor_lockpin_LFO = _T("Locking pin - Left/Front - on/off");
	const std::wstring String_motor_lockpin_LFD = _T("Locking pin - Left/Front - direction");
	const std::wstring String_motor_lockpin_RFO = _T("Locking pin - Right/Front - on/off");
	const std::wstring String_motor_lockpin_RFD = _T("Locking pin - Right/Front - direction");
	const std::wstring String_motor_lockpin_RRO = _T("Locking pin - Right/Rear - on/off");
	const std::wstring String_motor_lockpin_RRD = _T("Locking pin - Right/Rear - direction");

	// List of other devices
	const std::wstring String_other_device_mains_disconnect = _T("MAINS - Power on/off");
	const std::wstring String_other_device_vfd_fans = _T("VFD - Main fans");
	const std::wstring String_other_device_AWD_clutch_front = _T("AWD clutch - front");
	const std::wstring String_other_device_AWD_clutch_rear = _T("AWD clutch - rear");
	const std::wstring String_other_device_gearbox_front_cooling = _T("Gearbox - front - cooling");
	const std::wstring String_other_device_gearbox_rear_cooling = _T("Gearbox - rear - cooling");
	const std::wstring String_other_device_brake_front_solenoid_energize = _T("Brake - front - solenoid - energize");
	const std::wstring String_other_device_brake_front_solenoid_hold = _T("Brake - front - solenoid - hold");
	const std::wstring String_other_device_brake_rear_solenoid_energize = _T("Brake - rear - solenoid - energize");
	const std::wstring String_other_device_brake_rear_solenoid_hold = _T("Brake - rear - solenoid - hold");
	const std::wstring String_other_device_base_contactor_forward = _T("Base - contactor - forward");
	const std::wstring String_other_device_base_contactor_reverse = _T("Base - contactor - reverse");

	const std::wstring String_other_device_LED_on_board = _T("LED - On board");


	// List of procedures (or subsystems)
	const std::wstring String_procedure_base_home = _T("Move base to home position");
	const std::wstring String_procedure_base_move_to_X = _T("Move base to specific position");
	const std::wstring String_procedure_brakes_calibrate_front = _T("Calibrate front brake");
	const std::wstring String_procedure_brakes_calibrate_rear = _T("Calibrate rear brake");
	const std::wstring String_procedure_brakes_engage_all = _T("Engage brakes");
	const std::wstring String_procedure_brakes_release_all = _T("Release brakes");
	const std::wstring String_procedure_brakes_engage_front = _T("Engage front brake");
	const std::wstring String_procedure_brakes_release_front = _T("Release front brake");
	const std::wstring String_procedure_brakes_engage_rear = _T("Engage rear brake");
	const std::wstring String_procedure_brakes_release_rear = _T("Release rear brake");

	// Miscellaneous system values

	// UI Defaults
	const int def_speedometer_max = 200;
	const int def_speedometer_redline = 160;

	const int def_tachometer_max = 8000;
	const int def_tachometer_redline = 7000;

	const double def_timeAxisMax = 40000.0;

	// Definition for conversion:
	// "One foot pound is the work done by a force of one pound acting through a distance of one foot,
	// in the direction of the force. It equates to 1.355 817 948 331 4004 joules."
	const double torqueConversion = 0.737562149277266;

	// Converting to MPH from RPM requires the RPM * diameter of the dyno shaft * this constant.
	// The constant is: PI * 63360 (inches per mile) * 60 (minutes in an hour).
	const double rpmToMph = 0.002974993;
	
	const double milesToKm = 1.609344;
	const double horsepowerConstant = 5252.0;

	// Standard PI to a good enough precision
	const double PI = 3.14159265358979323846;
	const double RADS_6 = 1.0471975511966;			// (2 x PI) / 6 for calculating our velocity

	// Units
	enum Units {Units_undefined, Units_metric, Units_imperial, Units_both};

	// Fuel Type
	enum FuelType {Diesel, Gasoline, Methanol };

	// Playback status codes
	enum PlaybackStatus {PlaybackStatus_File_Parsing, PlaybackStatus_Not_started,
		PlaybackStatus_Playing, PlaybackStatus_Paused, PlaybackStatus_Ended};

	// Types of charts available
	enum MainChartType { Chart_t_hp_vs_rpm, Chart_rpm_spd_vs_time };

	} Constants;

// CDynoMFCXP32App:
// See DynoMFCXP32.cpp for the implementation of this class
//

class CDynoMFCXP32App : public CWinApp
{
public:
	CDynoMFCXP32App() noexcept;

	// Keep track of a COM port group
	ComPort currentComPorts;
	DynoConfig CurrentDynoConfiguration;

	bool loadDynoConfig(DynoConfig &config);
	bool saveDynoConfig(bool updateSavedTime = false);
	void resetDynoConfig();

	double counter();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
	afx_msg void OnAppAbout();
	afx_msg void OnMeterButton();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnFileComPort();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	};

extern CDynoMFCXP32App theApp;
