#pragma once

#include "DynoConfig.h"



// ConnectionSettings dialog

class ConnectionSettings : public CDialogEx
{
	DECLARE_DYNAMIC(ConnectionSettings)

public:
	ConnectionSettings(CWnd* pParent = nullptr);   // standard constructor
	virtual ~ConnectionSettings();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CONNECTION_SETTINGS };
#endif

	// Pass-in variables
	ComPort* globalPorts;



	// Extra variables added by Rob
	DynoConfig localCfg, backupCfg;

	// Return the value of the modified flag
	bool isModified()
		{
		return modified;
		}


protected:
	
	
	CString holdString;	// Temp string used to see if the value has changed
	bool modified;		// Keep track of whether anything has been changed

	void setModified(bool modFlag);
	void updateAllControls();

	
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CButton m_autoPortScan;
	CEdit m_idString;
	CButton m_testButton;
	afx_msg void OnBnClickedConSettingsAutoComScan();
	afx_msg void OnEnSetfocusConSettingsPolInt();
	afx_msg void OnEnKillfocusConSettingsPolInt();
	afx_msg void OnEnSetfocusConSettingsIdString();
	afx_msg void OnEnKillfocusConSettingsIdString();
	afx_msg void OnBnClickedConSettingsTestButton();
	virtual BOOL OnInitDialog();
	afx_msg void OnEnChangeConSettingsIdString();
	CListBox m_portList;
	CEdit m_pollInterval;
	};
