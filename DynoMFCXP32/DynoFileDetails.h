#pragma once
#include <afx.h>

#include <afxtempl.h>
#include <memory>

#include "DynoEvent.h"


// This class is used to store data about a Dyno VZD file.
// It also contains the functionality to process a data stream.
class DynoFileDetails : public CObject
	{
	public:
		
		// External variables
		DynoConfig* currentFormConfig;		// The configuration for the current form

		// Dynamically allocated storage for input / output file data
		std::shared_ptr<char[]> dataStream;
		double sumTimeDeltas;
		int dataLength;
		bool imported;

		CString filepath;
		CString fileTitle;

		// Variables
		SYSTEMTIME startTime, endTime, saveTime;

		// Maintain a CList of the events in the file
		CList<DynoEvent, DynoEvent&> eventList;


		// Functions
		void Reset(DynoConfig* config, std::shared_ptr<char[]> newData = nullptr, int newLength = 0);		// Clear out the details
		bool ParseData(CProgressCtrl *progressBar = nullptr);
		bool loadSession(CFile &loadFile);																	// Load a pre-recorded session
		bool saveSession(CFile &saveFile);																	// Save the current session
		bool exportData(CString filename);																	// Export current data to a CSV file

		// Information about events
		SYSTEMTIME getFirstEventTime();
		SYSTEMTIME getFirstRPMEventTime();
		SYSTEMTIME getLastEventTime();
		
		// Serialization
		void Serialize(CArchive& ar);
		DECLARE_SERIAL(DynoFileDetails);


	};

