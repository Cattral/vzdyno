#pragma once
// VZDynoDefines.h
// Substitutions for values that cannot be stored in the global Constants structure.
// Ex: Case statement labels.

// Tokens for data transmission and import.
//	@ = time stamp
//	D = Time delta used to create Events
//	X = Terminates a list of time delta values
//	S = Speed
//	R = Engine RPM
//	H = Horsepower
//	T = Torque
//	V = Voltage
//	A = Sound
//	M = Vibration
//	F = Temperature
//	W = Humidity
//	P = Barometric pressure
//	C = Carbon Monoxide

#define TOKEN_timestamp '@'
#define TOKEN_timeDelta 'D'
#define TOKEN_endDrumEncoder 'X'
#define TOKEN_speed 'S'
#define TOKEN_rpm_engine 'R'
#define TOKEN_rpm_drum 'U'
#define TOKEN_horsepower 'H'
#define TOKEN_torque 'T'
#define TOKEN_voltage 'V'
#define TOKEN_sound 'A'
#define TOKEN_vibration 'M'
#define TOKEN_temperature 'F'
#define TOKEN_humidity 'W'
#define TOKEN_pressure 'P'
#define TOKEN_CO 'C'

#define TOKEN_Full_String _T("@DXSRUHTVAMFWPC")


// Ardunio-specific
// Although these are only of importance in the control system, it doesn't hurt
// to have them in the main GUI. Moreover, it may help for logging, later.

// Analog sensor pins
#define PIN_SENSOR_A_WEATHER_TEMP			A0
#define PIN_SENSOR_A_WEATHER_PRESSURE		A1
#define PIN_SENSOR_A_WEATHER_HUMIDITY		A2
#define PIN_SENSOR_A_BRAKE_1_POS			A3
#define PIN_SENSOR_A_BRAKE_2_POS			A4
#define PIN_SENSOR_A_GEARBOX_1_TEMP			A5
#define PIN_SENSOR_A_GEARBOX_2_TEMP			A6
#define PIN_SENSOR_A_BEARING_1_TEMP			A7
#define PIN_SENSOR_A_BEARING_2_TEMP			A8
#define PIN_SENSOR_A_LOCKPIN_1_POS			A9
#define PIN_SENSOR_A_LOCKPIN_2_POS			A10
#define PIN_SENSOR_A_LOCKPIN_3_POS			A11
#define PIN_SENSOR_A_LOCKPIN_4_POS			A12
#define PIN_SENSOR_A_RPM					A13
#define PIN_SENSOR_A_AFR					A14


// Digital sensor pins
#define PIN_SENSOR_D_ODB_II_1				0
#define PIN_SENSOR_D_ODB_II_2				1
#define PIN_SENSOR_D_BASEMOTOR_PROX_1		2
#define PIN_SENSOR_D_BASEMOTOR_PROX_2		3
#define PIN_ACTION_D_FANS_VFD				4
// Placeholder								5

#define PIN_ACTION_D_LOCKPIN_1_POWER		6
#define PIN_ACTION_D_LOCKPIN_1_DIR			7
#define PIN_ACTION_D_LOCKPIN_2_POWER		8
#define PIN_ACTION_D_LOCKPIN_2_DIR			9
#define PIN_ACTION_D_LOCKPIN_3_POWER		10
#define PIN_ACTION_D_LOCKPIN_3_DIR			11
#define PIN_ACTION_D_LOCKPIN_4_POWER		12
#define PIN_ACTION_D_ONBOARD_LED			13
#define PIN_ACTION_D_LOCKPIN_4_DIR			14

// Placeholder								15
// Placeholder								16
// Placeholder								17
#define PIN_SENSOR_D_ENCODER_2_OFFSET		18
#define PIN_SENSOR_D_ENCODER_1_OFFSET		19
#define PIN_SENSOR_D_BASEMOTOR_POS			20
#define PIN_BUTTON_D_PENDANT_STOP			21

// Placeholder								22
#define PIN_ACTION_D_AWD_CLUTCH_1			23
#define PIN_ACTION_D_AWD_CLUTCH_2			24
// Placeholder								25
#define PIN_SENSOR_D_MAINS_DETECT			26
#define PIN_ACTION_D_GEARBOX_1_COOLING		27
#define PIN_ACTION_D_GEARBOX_2_COOLING		28
// Placeholder								29
// Placeholder								30
// Placeholder								31

#define PIN_SENSOR_D_BRAKE_1_MOTOR_ENCODER	32
#define PIN_SENSOR_D_BRAKE_2_MOTOR_ENCODER	33
#define PIN_ACTION_D_BRAKE_1_MOTOR_POWER	34
#define PIN_ACTION_D_BRAKE_1_MOTOR_DIR		35
#define PIN_ACTION_D_BRAKE_1_SOLENOID_ENG	36
#define PIN_ACTION_D_BRAKE_1_SOLENOID_HOLD	37
#define PIN_ACTION_D_BRAKE_2_MOTOR_POWER	38
#define PIN_ACTION_D_BRAKE_2_MOTOR_DIR		39
#define PIN_ACTION_D_BRAKE_2_SOLENOID_ENG	40
#define PIN_ACTION_D_BRAKE_2_SOLENOID_HOLD	41

#define PIN_ACTION_D_MAINS_DISCONNECT		42
#define PIN_ACTION_D_BASEMOTOR_FORWARD		43
#define PIN_ACTION_D_BASEMOTOR_REVERSE		44
// Placeholder								45
// Placeholder								46
// Placeholder								47

#define PIN_SENSOR_D_ENCODER_2_MAIN			48
#define PIN_SENSOR_D_ENCODER_1_MAIN			49

// Placeholder								50
#define PIN_BUTTON_D_PENDANT_SELECT			51
#define PIN_BUTTON_D_PENDANT_EXECUTE		52
// Placeholder								53


