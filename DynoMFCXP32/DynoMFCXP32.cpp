
// DynoMFCXP32.cpp : Defines the class behaviors for the application.
//

#include "pch.h"


#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include "framework.h"
#include "afxwinappex.h"
#include "afxdialogex.h"
#include "DynoMFCXP32.h"
#include "MainFrm.h"
#include "CMainForm.h"
#include "DynoComPortDialog.h"

#include "linalg.h"
#include "interpolation.h"

using namespace alglib;


// Splash window
#include "SplashWindow.h"

// Serial port header files
#include "ComPort.h"


#include "ChildFrm.h"
#include "DynoMFCXP32Doc.h"
#include "DynoMFCXP32View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// Global variables





// CDynoMFCXP32App

BEGIN_MESSAGE_MAP(CDynoMFCXP32App, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CDynoMFCXP32App::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
	ON_COMMAND(ID_METER, &CDynoMFCXP32App::OnMeterButton)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinApp::OnFilePrintSetup)
	ON_COMMAND(ID_FILE_COM_PORT, &CDynoMFCXP32App::OnFileComPort)
END_MESSAGE_MAP()




// CDynoMFCXP32App construction

CDynoMFCXP32App::CDynoMFCXP32App() noexcept
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_ALL_ASPECTS;
#ifdef _MANAGED
	// If the application is built using Common Language Runtime support (/clr):
	//     1) This additional setting is needed for Restart Manager support to work properly.
	//     2) In your project, you must add a reference to System.Windows.Forms in order to build.
	System::Windows::Forms::Application::SetUnhandledExceptionMode(System::Windows::Forms::UnhandledExceptionMode::ThrowException);
#endif

	// TODO: replace application ID string below with unique ID string; recommended
	// format for string is CompanyName.ProductName.SubProduct.VersionInformation
	SetAppID(_T("VFperformance.DynoMFCXP32.WinXP.Dev"));

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

// The one and only CDynoMFCXP32App object

CDynoMFCXP32App theApp;


// CDynoMFCXP32App initialization

BOOL CDynoMFCXP32App::InitInstance()
{
	
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);

	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();
	EnableTaskbarInteraction(FALSE);

	// Bring up the splash screen
	CSplashWindow::ShowSplashScreen(NULL, _T("Starting application."));
	Sleep(500);

	CSplashWindow::ShowSplashScreen(NULL, _T("Starting application.."));
	Sleep(500);

	// Load the system configuration file
	loadDynoConfig(CurrentDynoConfiguration);

	CSplashWindow::ShowSplashScreen(NULL, _T("Starting application..."));
	Sleep(1000);

	CSplashWindow::ShowSplashScreen(NULL, _T("Starting application...."));
	Sleep(1000);


	CSplashWindow::ShowSplashScreen(NULL, _T(""));
	Sleep(1000);

	// Bring it up and then remove it once complete
	CSplashWindow::ShowSplashScreen(NULL, NULL, 1000);


	// AfxInitRichEdit2() is required to use RichEdit control
	// AfxInitRichEdit2();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need

	// Change the registry key under which our settings are stored
	SetRegistryKey(_T("VZperformance"));
	LoadStdProfileSettings(4);  // Load standard INI file options (including MRU)


	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views

/*
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_DynoMFCXP32TYPE,
		RUNTIME_CLASS(CDynoMFCXP32Doc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CDynoMFCXP32View));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);
*/

	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_DynoMFCXP32TYPE,
		RUNTIME_CLASS(CDynoMFCXP32Doc),
		RUNTIME_CLASS(CChildFrame),			// custom MDI child frame
		RUNTIME_CLASS(CMainForm));			// Our main form view
	if (!pDocTemplate)
		return FALSE;

	AddDocTemplate(pDocTemplate);


	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
	{
		delete pMainFrame;
		return FALSE;
	}
	m_pMainWnd = pMainFrame;


	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line. Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The main window has been initialized, so show and update it
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	return TRUE;
}

int CDynoMFCXP32App::ExitInstance()
{
	//TODO: handle additional resources you may have added
	AfxOleTerm(FALSE);

	return CWinApp::ExitInstance();
}

// CDynoMFCXP32App message handlers


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg() noexcept;

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() noexcept : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// App command to run the dialog
void CDynoMFCXP32App::OnAppAbout()
	{

	//CAboutDlg aboutDlg;
	//aboutDlg.DoModal();

	// Bring up the Splash Screen for 10 seconds instead of the standard About box
	CSplashWindow::ShowSplashScreen(NULL, _T("About VZDyno"), 10000);
	}

// Load all of the default values into the current configuration
void CDynoMFCXP32App::resetDynoConfig()
	{
	// Grab the current system time
	time_t currentTime;
	time(&currentTime);		

	// If the current creation time is valid then preserve it, otherwise use the current time
	if (!CurrentDynoConfiguration.isLoaded())
		{
		CurrentDynoConfiguration.createdDate = COleDateTime(currentTime);
		}

	// Store the current time as the last-updated
	CurrentDynoConfiguration.lastUpdatedDate = COleDateTime(currentTime);

	// Load the default values
	CurrentDynoConfiguration.resetDefaults();
	CurrentDynoConfiguration.updatedSinceSave = true;
	}

// Open and load the configuration file
bool CDynoMFCXP32App::loadDynoConfig(DynoConfig& config)
	{
	CFile file;
	BOOL opened = FALSE;

	CFileStatus status;

	// Clear the current file path
	config.currentFilePath.Empty();

	// Check to see if the configuration file exists
	if (CFile::GetStatus(Constants.String_filename_config.c_str(), status))
		{
		// Try to open the configuration file read only.
		opened = file.Open(Constants.String_filename_config.c_str(), CFile::modeRead);

		// If it opened the load the contents
		if (opened)
			{
			// Remember where it was loaded from
			config.currentFilePath = file.GetFilePath();

			try
				{
				// Load using serialization
				CArchive ar(&file, CArchive::load);			// Create the archive
				config.Serialize(ar);						// Serialize it
				ar.Close();									// Close the archive
				}
			catch (CArchiveException * e)
				{
				// If there is an error reading the archive then the scheme is probably wrong.
				// Just close the file, reset everything, and re-create it.
				file.Close();

				// Log error in e

				// If it didn't exist then load the defaults and create the file
				resetDynoConfig();

				opened = saveDynoConfig();

				// We can't easily display a dialog because the opening screen is probably up.
				// Just play a beep and log it. This should be a rare occurrence.
				if (opened)
					{
					MessageBeep(MB_ICONEXCLAMATION);
					//MessageBox(NULL, _T("Configuration file corrupt. Successfully recreated with default settings."), _T("Configuration error"), MB_ICONEXCLAMATION);
					}
				else
					{
					MessageBeep(MB_ICONERROR);
					//MessageBox(NULL, _T("Configuration file corrupt. Error creating new file."), _T("Configuration error"), MB_ICONERROR);
					}

				return(opened);
				}

			file.Close();								// Close the file

			// Temporarily hold on to the time it was loaded
			time_t currentTime;
			time(&currentTime);

			config.timeLoaded = COleDateTime(currentTime);
			}
		else
			{
			// Log the error

			// Display the error in the status bar


			// Load the default values

			}


		}
	else
		{
		// If it didn't exist then load the defaults and create the file
		resetDynoConfig();

		opened = saveDynoConfig();
		}

	// Return whether the file opened or not, either originally or after being created
	return(opened == TRUE);
	}

// Save the configuration file that is currently active.
// FOR NOW: Since the file is pretty simple, don't bother creating a backup.
bool CDynoMFCXP32App::saveDynoConfig(bool updateSavedTime)
	{
	CFile file;
	bool saveResult = false;

	// Don't worry about whether there is an old file there or not. Just create a new one
	// and overwrite it if it exists.
	saveResult = file.Open(Constants.String_filename_config.c_str(), CFile::modeCreate | CFile::modeWrite);

	// If it opened the save the contents
	if (saveResult)
		{
		// If we want to update the time it was last updated then do it now
		if (updateSavedTime)
			{
			time_t currentTime;
			time(&currentTime);

			CurrentDynoConfiguration.lastUpdatedDate = COleDateTime(currentTime);
			}

		CArchive ar(&file, CArchive::store);		// Create the archive		
		CurrentDynoConfiguration.Serialize(ar);		// Serialize it
		ar.Close();									// Close the archive
		file.Close();								// Close the file
		}

	return(saveResult);
	}


// CDynoMFCXP32App message handlers


// App command to run the dialog
void CDynoMFCXP32App::OnMeterButton()
	{
/*
	time_t currentTime;

	time(&currentTime);

	CString timeString;

	timeString.Format(_T("Time: %d"), currentTime);
	MessageBox(NULL, timeString, _T("Time"), MB_ICONINFORMATION);
*/

/* Performance test 
	alglib::real_2d_array a, b, c;
	int n = 2000;
	int i, j;
	double timeneeded, flops;

	// Initialize arrays
	a.setlength(n, n);
	b.setlength(n, n);
	c.setlength(n, n);
	for (i = 0; i < n; i++)
		for (j = 0; j < n; j++)
			{
			a[i][j] = alglib::randomreal() - 0.5;
			b[i][j] = alglib::randomreal() - 0.5;
			c[i][j] = 0.0;
			}

	// Set global threading settings (applied to all ALGLIB functions);
	// default is to perform serial computations, unless parallel execution
	// is activated. Parallel execution tries to utilize all cores; this
	// behavior can be changed with alglib::setnworkers() call.
	alglib::setglobalthreading(alglib::parallel);

	// Perform matrix-matrix product.
	flops = 2 * pow((double)n, (double)3);
	timeneeded = counter();
	alglib::rmatrixgemm(
		n, n, n,
		1.0,
		a, 0, 0, 0,
		b, 0, 0, 1,
		0.0,
		c, 0, 0);
	timeneeded = counter() - timeneeded;

		CString tempString;

	// Evaluate performance
	tempString.Format(_T("Performance is %.1f GFLOPS"), (double)(1.0E-9 * flops / timeneeded));
		
	MessageBox(NULL, printString, _T("ALGLIB Performance"), MB_ICONINFORMATION);

*/

//
// In this example we demonstrate penalized spline fitting of noisy data
//
// We have:
// * x - abscissas
// * y - vector of experimental data, straight line with small noise
//
//	real_1d_array x = "[0.00,0.10,0.20,0.30,0.40,0.50,0.60,0.70,0.80,0.90]";
//	real_1d_array y = "[0.10,0.00,0.30,0.40,0.30,0.40,0.62,0.68,0.75,0.95]";
	real_1d_array x = "[1.0,2.0,3.0]";
	real_1d_array y = "[20.0,12.0,8.0]";

	ae_int_t info;
	double v;
	spline1dinterpolant s;
	spline1dfitreport rep;
	double rho;

	CString printString, tempString;

	//
	// Fit with VERY small amount of smoothing (rho = -5.0)
	// and large number of basis functions (M=50).
	//
	// With such small regularization penalized spline almost fully reproduces function values
	//
	rho = -5.0;
	spline1dfitpenalized(x, y, 50, rho, info, s, rep);

//	printf("%d\n", int(info)); // EXPECTED: 1
	tempString.Format(_T("%d\n"), int(info));
	printString += tempString;

	v = spline1dcalc(s, 0.0);
//	printf("%.1f\n", double(v)); // EXPECTED: 0.10
	tempString.Format(_T("%.1f\n"), double(v));
	printString += tempString;
	v = spline1dcalc(s, 2.0);
	//	printf("%.1f\n", double(v)); // EXPECTED: 0.10
	tempString.Format(_T("%.1f\n"), double(v));
	printString += tempString;
	v = spline1dcalc(s, 3.0);
	//	printf("%.1f\n", double(v)); // EXPECTED: 0.10
	tempString.Format(_T("%.1f\n"), double(v));
	printString += tempString;



	//
	// Fit with VERY large amount of smoothing (rho = 10.0)
	// and large number of basis functions (M=50).
	//
	// With such regularization our spline should become close to the straight line fit.
	// We will compare its value in x=1.0 with results obtained from such fit.
	//
	rho = +10.0;
	spline1dfitpenalized(x, y, 50, rho, info, s, rep);
//	printf("%d\n", int(info)); // EXPECTED: 1
	tempString.Format(_T("%d\n"), int(info));
	printString += tempString;


	v = spline1dcalc(s, 1.0);
//	printf("%.2f\n", double(v)); // EXPECTED: 0.969
	tempString.Format(_T("%.2f\n"), double(v));
	printString += tempString;

	//
	// In real life applications you may need some moderate degree of fitting,
	// so we try to fit once more with rho=3.0.
	//
	rho = +3.0;
	spline1dfitpenalized(x, y, 50, rho, info, s, rep);
//	printf("%d\n", int(info)); // EXPECTED: 1
	tempString.Format(_T("%d\n"), int(info));
	printString += tempString;


	MessageBox(NULL, printString, _T("ALGLIB PSpline test"), MB_ICONINFORMATION);
	}

double  CDynoMFCXP32App::counter()
	{
	return 0.001 * GetTickCount();
	}



void CDynoMFCXP32App::OnFileComPort()
	{
	DynoComPortDialog dcp;

	// Pass in COM port group
	dcp.globalPorts = &currentComPorts;


	INT_PTR result = dcp.DoModal();
	}

BOOL CDynoMFCXP32App::PreTranslateMessage(MSG* pMsg)
	{
	// TODO: Add your specialized code here and/or call the base class

	if (CSplashWindow::PreTranslateAppMessage(pMsg))
		return TRUE;

	return CWinApp::PreTranslateMessage(pMsg);
	}


// Global functions

//Returns the last Win32 error, in string format. Returns an empty string if there is no error.
CString GetLastErrorAsString()
	{
	// Get the error message, if any
	DWORD errorMessageID = ::GetLastError();
	
	if (errorMessageID == 0)
		return CString();		// No error message has been recorded

	LPSTR messageBuffer = nullptr;
	size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_MAX_WIDTH_MASK,
		NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

	CString errorString(messageBuffer);
	

	//Free the buffer.
	LocalFree(messageBuffer);

	return errorString;
	}

// Convert a Unix time_t into a Windows FILETIME
void UnixTimeToFileTime(time_t t, LPFILETIME pft)
	{
	// Note that LONGLONG is a 64-bit value
	LONGLONG ll;

	ll = Int32x32To64(t, 10000000) + 116444736000000000;
	pft->dwLowDateTime = (DWORD)ll;
	pft->dwHighDateTime = ll >> 32;
	}

// Convert a Unix time_t into a Windows SYSTEMTIME
void UnixTimeToSystemTime(time_t t, LPSYSTEMTIME pst)
	{
	FILETIME ft;

	UnixTimeToFileTime(t, &ft);
	FileTimeToSystemTime(&ft, pst);
	}


// Return the number of seconds difference between the two SYSTEMTIME parameters.
// NOTE: It's highly inefficient, but it isn't called very often. If this
// changes then it can be optimized.
double SystemTimeDifference(SYSTEMTIME time_1, SYSTEMTIME time_2)
	{
	// Calculate the difference between the two times
	FILETIME ft_1, ft_2;
	CFileTimeSpan span;

	// Convert the SYSTEMTIME variables to FILETIME to use the built-in CFileTimeSpan
	if (!SystemTimeToFileTime(&time_1, &ft_1) || !SystemTimeToFileTime(&time_2, &ft_2))
		{
		// TODO: Log this type of error
		return(0.0);
		}

	// Find the difference between them
	span = CFileTime(ft_2) - CFileTime(ft_1);

	// Convert it to fractional seconds and return it

	return((double)(span.GetTimeSpan()/CFileTime::Second));
	}

const double clfSecondsPer100ns = 100. * 1.E-9;

// Add the specified number of seconds to the first SYSTEMTIME and assign it to the second one
void AddSecondsToSystemTime(SYSTEMTIME* timeIn, SYSTEMTIME* timeOut, double tfSeconds)
	{
	union
		{
		ULARGE_INTEGER li;
		FILETIME       ft;
		};

	// Convert timeIn to FILETIME
	SystemTimeToFileTime(timeIn, &ft);

	// Add in the seconds
	li.QuadPart += (ULONGLONG)(tfSeconds / clfSecondsPer100ns);

	// Convert back to SYSTEMTIME
	FileTimeToSystemTime(&ft, timeOut);
	}


