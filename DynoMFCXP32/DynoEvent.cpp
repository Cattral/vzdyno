#include "pch.h"
#include "DynoEvent.h"

#include "DynoMFCXP32.h"

IMPLEMENT_SERIAL(DynoEvent, CObject, VERSIONABLE_SCHEMA | 1)

DynoEvent::DynoEvent()
	{
	timestamp = { 0 };
	type = EventType::NotSet;
	units = Constants.Units_undefined;
	finalSmoothed = false;
	GetLocalTime(&sysTime);
	}

DynoEvent::DynoEvent(int eventType, double eventParameter, int unitType, SYSTEMTIME eventTime, double timeDelta, DynoEvent* previousDrumEvent)
	{
	timestamp = eventTime;
	type = eventType;
	units = unitType;
	originalParameter = eventParameter;
	originalTimeDelta = timeDelta;

	// The ABS time always starts at 0
	absTime = 0.0;

	// Assume that the record was created before smoothing or it doesn't use it
	smoothedAcceleration = 0.0;
	smoothedParameter_sma = 0.0;
	smoothedParameter_ema = 0.0;
	smoothedTimeDelta_sma = 0.0;
	smoothedTimeDelta_ema = 0.0;

	// Other initialization
	originalAcceleration = 0.0;

	// For certain types of events, smoothing does not exist
	if (type == EventType::Timestamp)
		{
		smoothedParameter_sma = smoothedParameter_ema = originalParameter;
		finalSmoothed = true;
		}
	else
		{
		finalSmoothed = false;
		}

	// Timestamp the original creation time
	GetLocalTime(&sysTime);

	// Internal use only
	previousRPM_drum = previousDrumEvent;
	nextRPM_drum = nullptr;
	RPM_drum_index = 0;
	}

DynoEvent::~DynoEvent()
	{
	type = EventType::NotSet;
	}

// Assignment/copy constructor
DynoEvent& DynoEvent::operator=(const DynoEvent& event)
	{

	// Start by checking for self-assignment
	if (&event == this)
		return *this;

	// Copy all of the variables
	sysTime = event.sysTime;
	timestamp = event.timestamp;
	type = event.type;
	units = event.units;
	absTime = event.absTime;
	originalAcceleration = event.originalAcceleration;
	smoothedAcceleration = event.smoothedAcceleration;
	originalParameter = event.originalParameter;
	smoothedParameter_sma = event.smoothedParameter_sma;
	smoothedParameter_ema = event.smoothedParameter_ema;
	originalTimeDelta = event.originalTimeDelta;
	smoothedTimeDelta_sma = event.smoothedTimeDelta_sma;
	smoothedTimeDelta_ema = event.smoothedTimeDelta_ema;
	finalSmoothed = event.finalSmoothed;

	// Used internally and not serialized
	previousRPM_drum = event.previousRPM_drum;
	nextRPM_drum = event.nextRPM_drum;
	RPM_drum_index = event.RPM_drum_index;

	return *this;
	}

// Return a nice string that represents the Event
CString DynoEvent::GetString()
	{
	CString eventString;

	// Create the string based on the type
	if (type == EventType::NotSet)
		eventString = _T("<Invalid type>");
	else if (type == EventType::Timestamp)
		{
		COleDateTime eventTime(static_cast<__time32_t>(originalParameter));
		eventString.Format(_T("Timestamp: %s"), eventTime.Format(Constants.String_TimeDateFormat_24.c_str()));
		}
	else if (type == EventType::Speed)
		{
		eventString.Format(_T("Speedometer: %000.1lf %s [Smoothed: S %000.1lf E %000.1lf]"), originalParameter, UnitString(), smoothedParameter_sma, smoothedParameter_ema);
		}
	else if (type == EventType::RPM_drum)
		{
		eventString.Format(_T("Drum tachometer: %000.0lf %s [Smoothed: S %000.0lf E %000.0lf Absolute time: %000000.6lf]"), originalParameter, UnitString(), smoothedParameter_sma, smoothedParameter_ema, absTime);
		}
	else if (type == EventType::RPM_engine)
		{
		eventString.Format(_T("Engine tachometer: %000.0lf %s [Absolute time: %000000.6lf]"), originalParameter, UnitString(), absTime);
		}
	else if (type == EventType::Torque)
		{
		eventString.Format(_T("Torque: %000.0lf %s [Smoothed: S %000.0lf E %000.0lf]"), originalParameter, UnitString(), smoothedParameter_sma, smoothedParameter_ema);
		}
	else if (type == EventType::Horsepower)
		{
		eventString.Format(_T("Horsepower: %000.0lf %s [Smoothed: S %000.0lf E %000.0lf]"), originalParameter, UnitString(), smoothedParameter_sma, smoothedParameter_ema);
		}	
	else if (type == EventType::Voltage)
		{
		eventString.Format(_T("Voltage: %00.1lf %s"), originalParameter, UnitString());
		}
	else if (type == EventType::Sound)
		{
		eventString.Format(_T("Sound: %000.0lf %s"), originalParameter, UnitString());
		}
	else if (type == EventType::Vibration)
		{
		eventString.Format(_T("Vibration: %000.0lf %s"), originalParameter, UnitString());
		}
	else if (type == EventType::Temperature)
		{
		eventString.Format(_T("Temperature: %00.1lf %s"), originalParameter, UnitString());
		}
	else if (type == EventType::Humidity)
		{
		eventString.Format(_T("Humidity: %00.1lf %s"), originalParameter, UnitString());
		}
	else if (type == EventType::Pressure)
		{
		eventString.Format(_T("Pressure: %00.1lf %s"), originalParameter, UnitString());
		}
	else if (type == EventType::CO)
		{
		eventString.Format(_T("CO: %00.1lf %s"), originalParameter, UnitString());
		}
	else
		{
		eventString.Format(_T("Event not recognized: %lf <%s> [Smoothed: S %lf E %lf]"), originalParameter, UnitString(), smoothedParameter_sma, smoothedParameter_ema);
		}

	return eventString;
	}

// Return a nice string that represents the Event
CString DynoEvent::UnitString()
	{
	CString description;

	if (type == EventType::Speed)
		{
		if (units == Constants.Units_metric)
			{
			description = _T("km/h");
			}
		else if (units == Constants.Units_imperial)
			{
			description = _T("mph");
			}
		}
	else if ((type == EventType::RPM_drum) || (type == EventType::RPM_engine))
		{
		description = _T("RPM");
		}
	else if (type == EventType::Torque)
		{
		if (units == Constants.Units_metric)
			{
			description = _T("Nm");
			}
		else if (units == Constants.Units_imperial)
			{
			description = _T("ft-lbs");
			}
		}
	else if (type == EventType::Horsepower)
		{
		description = _T("hp");
		}
	else if (type == EventType::Timestamp)
		{
		description = _T("");
		}
	else if (type == EventType::Voltage)
		{
		description = _T("V");
		}
	else if (type == EventType::Sound)
		{
		description = _T("");
		}
	else if (type == EventType::Vibration)
		{
		description = _T("");
		}
	else if (type == EventType::Temperature)
		{
		if (units == Constants.Units_metric)
			{
			description = _T("C");
			}
		else if (units == Constants.Units_imperial)
			{
			description = _T("F");
			}
		}
	else if (type == EventType::Humidity)
		{
		description = _T("");
		}
	else if (type == EventType::CO)
		{
		description = _T("ppm");
		}

	// FOR NOW: Leave the undefined and the "both" settings blank

		return description;
	}

void DynoEvent::Serialize(CArchive& ar)
	{

	// Saving versus loading data
	if (ar.IsStoring())
		{
		// Dates
		ar << sysTime.wDay;
		ar << sysTime.wDayOfWeek;
		ar << sysTime.wHour;
		ar << sysTime.wMilliseconds;
		ar << sysTime.wMinute;
		ar << sysTime.wMonth;
		ar << sysTime.wSecond;
		ar << sysTime.wYear;

		ar << timestamp.wDay;
		ar << timestamp.wDayOfWeek;
		ar << timestamp.wHour;
		ar << timestamp.wMilliseconds;
		ar << timestamp.wMinute;
		ar << timestamp.wMonth;
		ar << timestamp.wSecond;
		ar << timestamp.wYear;

		// Event details
		ar << type;
		ar << units;
		ar << absTime;
		ar << originalAcceleration;
		ar << smoothedAcceleration;
		ar << originalParameter;
		ar << smoothedParameter_sma;
		ar << smoothedParameter_ema;
		ar << originalTimeDelta;
		ar << smoothedTimeDelta_sma;
		ar << smoothedTimeDelta_ema;
		ar << finalSmoothed;
		}
	else
		{
		// Dates
		ar >> sysTime.wDay;
		ar >> sysTime.wDayOfWeek;
		ar >> sysTime.wHour;
		ar >> sysTime.wMilliseconds;
		ar >> sysTime.wMinute;
		ar >> sysTime.wMonth;
		ar >> sysTime.wSecond;
		ar >> sysTime.wYear;

		ar >> timestamp.wDay;
		ar >> timestamp.wDayOfWeek;
		ar >> timestamp.wHour;
		ar >> timestamp.wMilliseconds;
		ar >> timestamp.wMinute;
		ar >> timestamp.wMonth;
		ar >> timestamp.wSecond;
		ar >> timestamp.wYear;

		// Event details
		ar >> type;
		ar >> units;
		ar >> absTime;
		ar >> originalAcceleration;
		ar >> smoothedAcceleration;
		ar >> originalParameter;
		ar >> smoothedParameter_sma;
		ar >> smoothedParameter_ema;
		ar >> originalTimeDelta;
		ar >> smoothedTimeDelta_sma;
		ar >> smoothedTimeDelta_ema;
		ar >> finalSmoothed;
		}
	}
