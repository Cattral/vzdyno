#pragma once

#include "3DMeterCtrl.h"
#include "CCircularMeterCtrl.h"
#include "XColorStatic.h"
#include "CDynoRichLeanControl.h"

#include "CModelessReplayDialog.h"
#include "DynoFileDetails.h"
 
#include "DynoEvent.h"

// Chart-specific headers
#include "ChartCtrl/ChartCtrl.h"
#include "ChartCtrl/ChartLineSerie.h"
#include "ChartCtrl/ChartLabel.h"

#include "ChartCtrl/ChartLineSerie.h"
#include "ChartCtrl/ChartPointsSerie.h"
#include "ChartCtrl/ChartSurfaceSerie.h"
#include "ChartCtrl/ChartGrid.h"
#include "ChartCtrl/ChartBarSerie.h"

#include "ChartCtrl/ChartAxisLabel.h"
#include "ChartCtrl/ChartStandardAxis.h"
#include "ChartCtrl/ChartDateTimeAxis.h"

#include "ChartCtrl/ChartCrossHairCursor.h"
#include "ChartCtrl/ChartDragLineCursor.h"


// CMainForm form view

class CMainForm : public CFormView
{
	DECLARE_DYNCREATE(CMainForm)

protected:
	CMainForm();           // protected constructor used by dynamic creation
	virtual ~CMainForm();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MAINFORMVIEW };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	// Used by the background COM Port polling task.
	// This also takes care of smoothing. Events will be free to leave here
	// once they have fallen out of range (i.e. are older than the processTarget value).
	// If multiple COM ports are being polled then there will be an instance of this
	// structure for each one of them.
	struct backgroundComProcessing
		{
		CWinThread* comThread;								// Background threat pointer
		DynoConfig* sessionConfig;							// A pointer to the running session configuration

		// Control
		bool running;										// True if there is a process running
		bool forceExit;										// Set to true to cause the process to halt

		// COM Port data source
		ComPort* activePorts;
		int portIndex;										// The index of the COM that this is monitoring

		// Alternative data source
		std::shared_ptr<char[]> dataStream;					// Pointer to a buffer used as an alternative data source
		int altDataBufferLength;							// The length of the alternative buffer

		// Internal and parameters
		int haveSmoothed;									// The number of finalized Events, some of which are not ready to graph yet
		int readyToGraph;									// The number of finalized Events waiting to be pulled out to graph
		int RPM_drum_total;									// The total number of RPM Drum Events (used for smoothing)
		long millsSinceLastRead;							// The number of milliseconds since the last data was read
		double lastRadsPerSecond_o;							// Keep track of the last original RadsPerSecond value
		double lastRadsPerSecond_ss;						// Keep track of the last SMA smoothed RadsPerSecond value
		double lastRadsPerSecond_se;						// Keep track of the last SME smoothed RadsPerSecond value

		double totalTime;									// The sum of the time
		double tdSum[3];									// The time delta sums for the current, T-1, and T-2 time deltas
		double tdValues[3];									// The time delta values for the current, T-1, and T-2 time deltas

		// P-Splines parameters
		int psplines_N;
		int psplines_M;
		int psplines_RHO;


		CList<DynoEvent, DynoEvent&>* incomingEventList;	// Pointer to a CList to store incoming Events (both raw and smoothed)
		POSITION lastDisplayedPos;
		POSITION lastGraphable;
		double totalAbsError;
		double totalRegError;

		void clearRunVars()
			{
			haveSmoothed = 0;
			readyToGraph = 0;
			RPM_drum_total = 0;
			millsSinceLastRead = -1;
			lastDisplayedPos = nullptr;
			lastGraphable = nullptr;
			totalAbsError = 0.0;
			totalRegError = 0.0;

			lastRadsPerSecond_o = 0.0;			// This will be initialized with the first time delta reading
			lastRadsPerSecond_ss = 0.0;
			lastRadsPerSecond_se = 0.0;

			// In the Lagrangian polynomial tdSum are the X variables, and tdValues are the Y
			totalTime = 0.0;
			tdSum[0] = tdSum[1] = tdSum[2] = 0.0;
			tdValues[0] = tdValues[1] = tdValues[2] = 0.0;
			}

		void init(CList<DynoEvent, DynoEvent&>* eventList, DynoConfig* cfg, ComPort* ports = nullptr, int portNum = -1)
			{			
			comThread = nullptr;
			running = true;
			sessionConfig = cfg;
			forceExit = false;
			activePorts = ports;
			portIndex = portNum;

			// P-Splines parameters
			psplines_M = 0;
			psplines_N = 0;
			psplines_RHO = 0;

			// Clear the variables that get modified during the run
			clearRunVars();

			// Assume that it isn't an alternative stream. This needs to be set
			// manually by the calling function.
			altDataBufferLength = 0;
			dataStream = nullptr;

			incomingEventList = eventList;
			incomingEventList->RemoveAll();
			}

		} bkgComProcessMessage;		// Only 1 of these for now, but if we read multiple COMs then we need an array of them

	static UINT backgroundPollCOMPort(LPVOID data);
	static void calculateEventValues(CList <DynoEvent, DynoEvent&>* internalEventList, POSITION pos, backgroundComProcessing* bkgData);


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	CBrush m_brush;						// Brush for background colour

	// UI Options
	CMap<int, int, CString, CString> colourNamesMap;
	DynoConfig* currentFormConfig;
	
	// If there is a file that has been loaded, use it's original configuration if available
	DynoConfig loadedFileConfig;

	// Main chart variables
	int chartShown = Constants.Chart_t_hp_vs_rpm;		// The torque and HP versus RPM is the default chart
		
	double chart_lastRadsPerSecond;
	double chart_currentRpm = 0.0;
	double chart_maxRpm = 0.0;
	double chart_timeZero;

	// UI Functions
	COLORREF getMappedColour(int mapIndex);
	void DrawGraphLabels();
	void DrawCharts();
	void RedrawEverything();
	void buildTickValueArray(int *defaultSpeedValues, int startVal, int maxVal);
	void parseSessionFile();
	void displayEvent(DynoEvent& event);
	void playLoadedSession(int startingEventIndex = 0);
	void updateWindowTitle();

	// Internal processing
	void processIncomingData();

	// Control update functions
	void UpdateSpeedometer(double speed);
	void UpdateTachometer(double rpm);
	void UpdateRichLean(CString gname, double fill);
	void UpdateOutputValues(CString gname, CString value);
	void UpdateVoltMeter(double volts);
	void UpdateMainChart(DynoEvent& event);


	// Titles and names on the screen
	CXColorStatic m_bigname;
	HANDLE titleFontHandle = nullptr;	// Maintain the font handle
	CXColorStatic m_leftOfTopChart;		// Default is "Torque"
	CXColorStatic m_rightOfTopChart;	// Default is "Horsepower"
	CXColorStatic m_bottomOfUpperChart;	// Default is "RPM"
	CXColorStatic m_leftOfLowerChart;	// Default is "AFR"
	CXColorStatic m_hpLabel;	// Default is "HP"
	
	CXColorStatic m_tqLabel;	// Default is "TQ"
	CXColorStatic m_afrLabel;	// Default is "AFR"
	CXColorStatic m_tqareaLabel;	// Default is "TQ AREA"
	CXColorStatic m_accelLabel;	// Default is "ACCEL"

	//CXColorStatic m_richGraph;
	//CXColorStatic m_leanGraph;
	CXColorStatic m_smallLeanRich;


	CXColorStatic m_hpLabel_value;
	CXColorStatic m_tqLabel_value;
	CXColorStatic m_afrLabel_value;
	CXColorStatic m_tqareaLabel_value;
	CXColorStatic m_accelLabel_value;


	CStatic m_richLeanBorder;
	CStatic m_richLeanDivider;
	CStatic m_RightVerticalDivider;
	CStatic m_RightHorizonatalDivider;



	// Files
	DynoFileDetails loadedFileDetails;	// Details about the Session file that was loaded for playback
	DynoFileDetails recordFileDetails;	// Details about the Session that is being recorded
	CFile dataInputFile;				// Used for playback
	CFile dataOutputFile;				// Recording file
	
	// Dynamically allocated storage for input / output file data
	std::shared_ptr<char[]> dataStream;



	// File flags
	bool streamingInputFile = false;			// Indicates that a data stream is currently being played back
	bool recording = false;						// Indicates that the current data stream is being recorded
	bool forceStopPlayback = false;				// If this flag is set then force the playback to stop
	bool forceStopProcessing = false;			// If this flag is set then force the processing to stop

	CModelessReplayDialog *m_remoteControl;		// Session Replay remote control dialog		


	// Communications settings
	bool autoEnumerate = true;
	int openPortIndex = -1;			// TODO: This will later expand to support multiple ports in this window / document

	// Point back to the parent application
	CDynoMFCXP32App *DynoApp;


	void updateComMenu();
	CMenu* findMenu(CString menuName, CMenu* root);
	static void log(CString& text);



	DECLARE_MESSAGE_MAP()
public:
	C3DMeterCtrl m_3DMeterCtrl;
	CCircularMeterCtrl m_tachometer;
	CCircularMeterCtrl m_speedometerCtrl;
	CDynoRichLeanControl m_richGraph;
	CDynoRichLeanControl m_leanGraph;

	CChartCtrl m_mainChart;
	CChartCtrl m_afrChart;
	CChartStandardAxis* pBottomAxis;
	CChartStandardAxis* pLeftAxis;
	CChartStandardAxis* pRightAxis;
	CChartLineSerie* pSeries_torqueAtRpm;
	CChartLineSerie* pSeries_hpAtRpm;
	CChartLineSerie* pSeries_drumRpmAtTime;
	CChartLineSerie* pSeries_engineRpmAtTime;
	CChartLineSerie* pSeries_speedAtTime;


	CChartStandardAxis* afrBottomAxis;
	CChartStandardAxis* afrLeftAxis;
	CChartStandardAxis* afrRightAxis;

	CChartLineSerie* pSeries_AfrAtTime;
	CChartLineSerie* pSeries_LeanAtTime;
	CChartLineSerie* pSeries_AfrStatic;


	int m_pollTimer;



	virtual void OnInitialUpdate();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnConnectSettings();
	afx_msg void OnConnectTestcomports();
	afx_msg void OnConnectAutoconnect();
//	afx_msg void OnStartpollingDefaultport();
	afx_msg void OnAuthorizationAutoauthorize();
	afx_msg void OnRButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnConnectIdentifyports();
//	afx_msg void OnUpdateStartpollingDefaultport(CCmdUI* pCmdUI);
	afx_msg void OnAuthorizationManualauthorize();
	afx_msg void OnAuthorizationContactsupport();
	afx_msg void OnAuthorizationLivechat();
	afx_msg void OnAuthorizationSettings();
	afx_msg void OnConnectPauseAllPolling();
	afx_msg void OnUpdateConnectPauseallpolling(CCmdUI* pCmdUI);
	afx_msg void OnMenuClickRange(UINT nID);
	afx_msg void OnStnDblclickSpeedometer();
	afx_msg void OnStnDblclickTachometer();
	afx_msg void OnViewMetervalueborders();
	afx_msg void OnUpdateViewMetervalueborders(CCmdUI* pCmdUI);
	afx_msg void OnViewCustomizeinterface();
	afx_msg void OnUpdateViewCustomizeinterface(CCmdUI* pCmdUI);
	afx_msg void OnFileOpen();
	afx_msg void OnFileClose();
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileClose(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAuthorizationAutoauthorize(CCmdUI* pCmdUI);
	afx_msg void OnUpdateConnectAutoconnect(CCmdUI* pCmdUI);
	afx_msg void OnFileReplay();
	afx_msg void OnUpdateFileReplay(CCmdUI* pCmdUI);
	afx_msg void OnFileExamineSession();
	afx_msg void OnUpdateFileExamineSession(CCmdUI* pCmdUI);
	afx_msg void OnBnClickedReplayControlPlayButton();
	afx_msg void OnBnClickedReplayControlCloseButton();
	afx_msg void OnViewReset();
	afx_msg void OnFileSave();
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnFileSaveAs();
	afx_msg void OnUpdateFileSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnFileImport();
	afx_msg void OnUpdateFileImport(CCmdUI* pCmdUI);
	afx_msg void OnFileExport();
	afx_msg void OnUpdateFileExport(CCmdUI* pCmdUI);
	afx_msg void OnGraphTorqueandhorsepower();
	afx_msg void OnUpdateGraphTorqueandhorsepower(CCmdUI* pCmdUI);
	afx_msg void OnGraphRpmandspeed();
	afx_msg void OnUpdateGraphRpmandspeed(CCmdUI* pCmdUI);
	afx_msg void OnViewAutoscrollgraph();
	afx_msg void OnUpdateViewAutoscrollgraph(CCmdUI* pCmdUI);
	afx_msg void OnAuthorizationAdministrativeoptions();
	afx_msg void OnOptionsMasterConfig();
	afx_msg void OnLegendTop();
	afx_msg void OnUpdateLegendTop(CCmdUI* pCmdUI);
	afx_msg void OnLegendBottom();
	afx_msg void OnUpdateLegendBottom(CCmdUI* pCmdUI);
	afx_msg void OnLegendNone();
	afx_msg void OnUpdateLegendNone(CCmdUI* pCmdUI);
	afx_msg void OnVehicleLookup();
	afx_msg void OnConnectDiagnostics();
	afx_msg void OnViewJumptoorigin();
	afx_msg void OnHelpLogviewer();
	afx_msg void OnConnectStartpolling();
	afx_msg void OnUpdateConnectStartpolling(CCmdUI* pCmdUI);
	afx_msg void OnUpdateConnectDiagnostics(CCmdUI* pCmdUI);
	afx_msg void OnViewShowvoltmeter();
	afx_msg void OnUpdateViewShowvoltmeter(CCmdUI* pCmdUI);
};


