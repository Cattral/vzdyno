#pragma once


// CModelessReplayDialog dialog

class CModelessReplayDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CModelessReplayDialog)

public:
	CModelessReplayDialog(CWnd* pParent = nullptr);   // standard constructor
	virtual ~CModelessReplayDialog();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MODELESS_REPLAY_DIALOG };
#endif


	CWnd* m_parent;
	bool playing = false;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual void PostNcDestroy();
	virtual void OnCancel();

	// Internal variables
	CStatic m_text;
	CSliderCtrl m_slider;
	CButton m_playButton;
	CButton m_closeFileButton;
	CComboBox m_replaySpeed;
	CButton m_loopButton;

	int currentPostion = 0;
	int filesize = 0;
	int playbackStatus;
	int speedSelection;
	bool loop = false;


public:

	CProgressCtrl m_progress;

	// External UI control functions
	void SetFileSize(int fileLength);		// Specify the size of the file
	int SetFilePos(int filePos);			// Set the current file position
	void SetStatus(int status);				// Set the playback status
	void SetText(CString text);				// Set the text in the control

	// Get methods
	int GetPosition()
		{
		return currentPostion;
		}

	int GetStatus()
		{
		return playbackStatus;
		}

	int GetFileSize()
		{
		return filesize;
		}

	int GetSpeedSelection()
		{
		return speedSelection;
		}

	bool playLoop()
		{
		return loop;
		}

	afx_msg void OnBnClickedReplayControlPlayButton();
	afx_msg void OnBnClickedReplayControlCloseButton();
	afx_msg void OnCbnSelchangeReplayControlSpeedSelector();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedReplayControlLoop();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	};
