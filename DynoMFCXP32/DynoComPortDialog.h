#pragma once

#include <vector>

#include "enumser.h"
#include "ComPort.h"
#include "DynoConfig.h"


// DynoComPortDialog dialog

class DynoComPortDialog : public CDialogEx
{
	DECLARE_DYNAMIC(DynoComPortDialog)

public:
	DynoComPortDialog(CWnd* pParent = nullptr);   // standard constructor
	virtual ~DynoComPortDialog();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DynoComPortDialog };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	
	// Pass-in variables
	ComPort *globalPorts;
	DynoConfig localCfg;

	// State variables
	UINT currentlySelectedPort = 0;


	// Controls
	CComboBox m_comList;
	
	// Events
	afx_msg void OnBnClickedDcpButtonComRefresh();
	virtual BOOL OnInitDialog();


private:

	void populateComList();			// Fill in the droplist
	void updateControls();

	void log(CString text);
	void updateReceived(CString text);


	void initializeDefaultValues();

	// Private variables
	int m_readPortTimer = 0;		// Timer for reading from COM port




public:
	afx_msg void OnBnClickedDcpClearButton();
	afx_msg void OnBnClickedDcpOpenPortButton();
	CButton m_openPortButton;
	CButton m_closePortButton;
	CButton m_clearResultsButton;
	CEdit m_results;
	afx_msg void OnBnClickedDcpClosePortButton();
	CButton m_sendButton;
	CButton m_refreshButton;
	CButton m_saveResultsButton;
	CComboBox m_baudRate;
	CComboBox m_portSettings;
	CComboBox m_openPortList;
	afx_msg void OnCbnSelchangeDcpComDroplist();
	CButton m_closeAll;
	afx_msg void OnBnClickedDcpCloseAll();
	afx_msg void OnBnClickedDcpSendButton();
	CEdit m_sendText;
	afx_msg void OnEnChangeDcpTextToSend();
	CEdit m_received;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnCbnSelchangeDcpBaudrate();
	afx_msg void OnCbnSelchangeDcpParity();
	};
