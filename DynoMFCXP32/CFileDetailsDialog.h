#pragma once

#include "DynoFileDetails.h"


// CFileDetailsDialog dialog

class CFileDetailsDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CFileDetailsDialog)

public:
	CFileDetailsDialog(CWnd* pParent = nullptr);   // standard constructor
	virtual ~CFileDetailsDialog();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FILE_DETAILS_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:

	// Underlying file details
	DynoFileDetails* fileDetails = nullptr;
	
	// Statistics
	double minRPM_value = DBL_MAX;
	double maxRPM_value = 0.0;
	double minSpeed_value = DBL_MAX;
	double maxSpeed_value = 0.0;
	double minTorque_value = DBL_MAX;
	double maxTorque_value = 0.0;;
	double minHP_value = DBL_MAX;
	double maxHP_value = 0.0;

	int minRPM_index = -1;
	int maxRPM_index = -1;
	int minSpeed_index = -1;
	int maxSpeed_index = -1;
	int minTorque_index = -1;
	int maxTorque_index = -1;
	int minHP_index = -1;
	int maxHP_index = -1;

	int firstRPMDecrease_index = -1;

	// UI controls
	CStatic m_numEventsText;
	CStatic m_fileLengthText;
	CEdit m_startTime;
	CEdit m_endTime;
	CListBox m_eventList;
	virtual BOOL OnInitDialog();
	afx_msg void OnLbnDblclkFileDetailsEventList();
	CStatic m_statistics;
	CEdit m_rangeFrom;
	CEdit m_rangeTo;
	afx_msg void OnBnClickedFileDetailsDeleteButton();
	afx_msg void OnStnClickedFileDetailsEventStatistics();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	};
