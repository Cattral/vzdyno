// CFileDetailsDialog.cpp : implementation file
//

#include "pch.h"
#include "DynoMFCXP32.h"
#include "CFileDetailsDialog.h"
#include "afxdialogex.h"


// CFileDetailsDialog dialog

IMPLEMENT_DYNAMIC(CFileDetailsDialog, CDialogEx)

CFileDetailsDialog::CFileDetailsDialog(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_FILE_DETAILS_DIALOG, pParent)
{

}

CFileDetailsDialog::~CFileDetailsDialog()
{
}

void CFileDetailsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FILE_DETAILS_NUM_EVENTS, m_numEventsText);
	DDX_Control(pDX, IDC_FILE_DETAILS_LENGTH, m_fileLengthText);
	DDX_Control(pDX, IDC_FILE_DETAILS_START_TIME, m_startTime);
	DDX_Control(pDX, IDC_FILE_DETAILS_END_TIME, m_endTime);
	DDX_Control(pDX, IDC_FILE_DETAILS_EVENT_LIST, m_eventList);
	DDX_Control(pDX, IDC_FILE_DETAILS_EVENT_STATISTICS, m_statistics);
	DDX_Control(pDX, IDC_FILE_DETAILS_RANGE_FROM, m_rangeFrom);
	DDX_Control(pDX, IDC_FILE_DETAILS_RANGE_TO, m_rangeTo);
	}


BEGIN_MESSAGE_MAP(CFileDetailsDialog, CDialogEx)
	ON_LBN_DBLCLK(IDC_FILE_DETAILS_EVENT_LIST, &CFileDetailsDialog::OnLbnDblclkFileDetailsEventList)
	ON_BN_CLICKED(IDC_FILE_DETAILS_DELETE_BUTTON, &CFileDetailsDialog::OnBnClickedFileDetailsDeleteButton)
	ON_STN_CLICKED(IDC_FILE_DETAILS_EVENT_STATISTICS, &CFileDetailsDialog::OnStnClickedFileDetailsEventStatistics)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CFileDetailsDialog message handlers


BOOL CFileDetailsDialog::OnInitDialog()
	{
	CDialogEx::OnInitDialog();

	// If the file details structure has been initialized then populate the data on the screen
	if (fileDetails == nullptr)
		return TRUE;

	CString tempString;
	int insertPosition;

	// Display the number of events
	tempString.Format(_T("Number of events: %d"), fileDetails->eventList.GetCount());
	m_numEventsText.SetWindowTextW(tempString);

	// Display the start and end time for both Session and the RPM Events
	COleDateTime startSessionTime(fileDetails->startTime);

	if (startSessionTime.GetStatus())
		startSessionTime = fileDetails->getFirstEventTime();
	
	COleDateTime endSessionTime(fileDetails->endTime);

	if (endSessionTime.GetStatus())
		endSessionTime = fileDetails->getLastEventTime();

	COleDateTimeSpan rpmSpan(0, 0, 0, (int)fileDetails->sumTimeDeltas);
	COleDateTime firstRPMTime(fileDetails->getFirstRPMEventTime());
	COleDateTime lastRPMTime;
	CString sessionDuration;

	// If the start time is valid then display it
	if (!firstRPMTime.GetStatus())
		{
		m_startTime.SetWindowText(firstRPMTime.Format(Constants.String_TimeDateFormat_24.c_str()));
		lastRPMTime = firstRPMTime + rpmSpan;
		}
	else
		{
		m_startTime.SetWindowTextW(_T("No starting RPM time found"));
		}

	// If the end time is valid then display it
	if (!lastRPMTime.GetStatus())
		{
		m_endTime.SetWindowText(lastRPMTime.Format(Constants.String_TimeDateFormat_24.c_str()));
		}
	else
		m_endTime.SetWindowTextW(_T("No ending RPM time found"));

	// Display the sum of time deltas (as total RPM running time)
	tempString.Format(_T("%ld min, %ld sec"), rpmSpan.GetMinutes(), rpmSpan.GetSeconds());
	m_fileLengthText.SetWindowTextW(tempString);


	// If both start and end times for the Session are valid then calculate and display the length
	if (fileDetails->startTime.wMonth && fileDetails->endTime.wMonth)
		{
		COleDateTimeSpan timeDifference = endSessionTime - startSessionTime;

		// Calculate and display the length of time
		sessionDuration.Format(_T("%ld min, %ld sec"), timeDifference.GetMinutes(), timeDifference.GetSeconds());
		}
	else
		sessionDuration = _T("Unknown");

	// Add all of the events
	POSITION pos = fileDetails->eventList.GetHeadPosition();
	POSITION lastPos;
	DynoEvent* curEvent;

	// As we roll through the list, populate the control and calculate the statistics
	for (int index = 0; index < fileDetails->eventList.GetCount(); index++)
		{
		// Point to the current event
		curEvent = &fileDetails->eventList.GetAt(pos);

		// FOR NOW: Rely on the EMA parameter; this might be selectable later, if it makes a difference.
		// Create the string based on the type
			if (curEvent->type == DynoEvent::EventType::Speed)
				{
				// Check against the minimum
				if (minSpeed_value > curEvent->smoothedParameter_ema)
					{
					minSpeed_value = curEvent->smoothedParameter_ema;
					minSpeed_index = index;
					}

				// Check against the maximum
				if (maxSpeed_value < curEvent->smoothedParameter_ema)
					{
					maxSpeed_value = curEvent->smoothedParameter_ema;
					maxSpeed_index = index;
					}
				}
			else if (curEvent->type == DynoEvent::EventType::RPM_drum)
				{
				// Check against the minimum; but don't set anything at zero or less
				if ((minRPM_value > curEvent->smoothedParameter_ema) && (curEvent->smoothedParameter_ema > 0.0))
					{
					minRPM_value = curEvent->smoothedParameter_ema;
					minRPM_index = index;
					}

				// Check against the maximum
				if (maxRPM_value < curEvent->smoothedParameter_ema)
					{
					maxRPM_value = curEvent->smoothedParameter_ema;
					maxRPM_index = index;
					}

				// FOR NOW: Set a marker at the first position where the RPM decreases
				if (curEvent->smoothedParameter_ema < maxRPM_value)
					{
					if(firstRPMDecrease_index == -1.0)
						firstRPMDecrease_index = index;
					}
				}
			else if (curEvent->type == DynoEvent::EventType::Torque)
				{
				// Check against the minimum
				if (minTorque_value > curEvent->smoothedParameter_ema)
					{
					minTorque_value = curEvent->smoothedParameter_ema;
					minTorque_index = index;
					}

				// Check against the maximum
				if (maxTorque_value < curEvent->smoothedParameter_ema)
					{
					maxTorque_value = curEvent->smoothedParameter_ema;
					maxTorque_index = index;
					}
				}
			else if (curEvent->type == DynoEvent::EventType::Horsepower)
				{
				// Check against the minimum
				if (minHP_value > curEvent->smoothedParameter_ema)
					{
					minHP_value = curEvent->smoothedParameter_ema;
					minHP_index = index;
					}

				// Check against the maximum
				if (maxHP_value < curEvent->smoothedParameter_ema)
					{
					maxHP_value = curEvent->smoothedParameter_ema;
					maxHP_index = index;
					}
				}

		lastPos = pos;
		tempString.Format(_T("#%d: %s"), index + 1, fileDetails->eventList.GetNext(pos).GetString());
		insertPosition = m_eventList.AddString(tempString);

		// With each element in the list, store the POSITION of the Event in the CList for quick access
		m_eventList.SetItemData(insertPosition, reinterpret_cast<DWORD_PTR>(lastPos));
		}

	// Build and show the statistics string
	tempString.Format(_T("Session start: %s\n"
		"Session end: %s\n"
		"Session duration: %s\n"
		"RPM Range: %00000.1lf (#%d) to %00000.1lf (#%d)\n"
		"First RPM decrease at event #%d\n"
		"Torque Range: %00000.1lf (#%d) to %00000.1lf (#%d)\n"
		"Horsepower Range: %00000.1lf (#%d) to %00000.1lf (#%d)\n"),
		fileDetails->startTime.wMonth ? startSessionTime.Format(Constants.String_TimeDateFormat_24.c_str()) : _T("No start time found"),
		fileDetails->endTime.wMonth ? endSessionTime.Format(Constants.String_TimeDateFormat_24.c_str()) : _T("No end time found"),
		sessionDuration,
		minRPM_value, minRPM_index, maxRPM_value, maxRPM_index,
		firstRPMDecrease_index,
		minTorque_value, minTorque_index, maxTorque_value, maxTorque_index,
		minHP_value, minHP_index, maxHP_value, maxHP_index
	);

	m_statistics.SetWindowTextW(tempString);
		

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
	}

// Double clicking on an Event
void CFileDetailsDialog::OnLbnDblclkFileDetailsEventList()
	{
	// FOR NOW: Just display as much information as we have about the Event
	CString titleBarText;
	int eventNumber = m_eventList.GetCurSel();

	if (eventNumber < 0)
		return;

	DynoEvent *selectedEvent = &fileDetails->eventList.GetAt(reinterpret_cast<POSITION>(m_eventList.GetItemData(eventNumber)));

	COleDateTime creationDate(selectedEvent->creationTime());
	COleDateTime timestamp(selectedEvent->timestamp);

	CString text;

	text.Format(_T("%s\n\nCreated: %s\nEvent timestamp: %s\nFinalized: %s\Units: %s\n"
		"Original parameter: %00000000.10lf\nSmoothed parameter-S: %00000000.10lf\nSmoothed parameter-E: %00000000.10lf\n"
		"Original time delta: %00000000.10lf\nSmoothed time delta-S: %00000000.10lf\nSmoothed time delta-E: %00000000.10lf\n"
		"Acceleration: %00000000.10lf [S %00000000.10lf]\n"
	),
		selectedEvent->GetString(),
		creationDate.Format(Constants.String_TimeDateFormat_24.c_str()),
		timestamp.Format(Constants.String_TimeDateFormat_24.c_str()),
		selectedEvent->finalSmoothed ? _T("Yes\n") : _T("No\n"),
		selectedEvent->UnitString(),
		selectedEvent->originalParameter, selectedEvent->smoothedParameter_sma, selectedEvent->smoothedParameter_ema,
		selectedEvent->originalTimeDelta, selectedEvent->smoothedTimeDelta_sma, selectedEvent->smoothedTimeDelta_ema),
		selectedEvent->originalAcceleration, selectedEvent->smoothedAcceleration;

	titleBarText.Format(_T("Event #%d information"), eventNumber + 1);
	MessageBox(text, titleBarText, MB_ICONINFORMATION);
	}

// Delete the specified range of items
void CFileDetailsDialog::OnBnClickedFileDetailsDeleteButton()
	{
	// TODO: Put the error checking code in, or condense it to a single edit box

	CString tempString;
	int from, to;

	m_rangeFrom.GetWindowTextW(tempString);
	from = _wtoi(tempString.GetBuffer()) - 1;

	m_rangeTo.GetWindowTextW(tempString);
	to = _wtoi(tempString.GetBuffer()) - 1;

	if (to < from)
		return;

	// Delete the range of events
	POSITION pos = fileDetails->eventList.FindIndex(from);
	POSITION endPos = fileDetails->eventList.FindIndex(to);
	POSITION lastPos;

	while (pos)
		{
		lastPos = pos;

		// Figure out which was is next before deleting this one
		fileDetails->eventList.GetNext(pos);

		// Delete the current Event
		fileDetails->eventList.RemoveAt(lastPos);
		m_eventList.DeleteString(from);


		// If we just deleted the last one in the range then stop
		if (lastPos == endPos)
			break;
		}


	MessageBox(_T("TODO: Statistics are now wrong"), _T("Don't forget..."), MB_ICONINFORMATION);
	}


void CFileDetailsDialog::OnStnClickedFileDetailsEventStatistics()
	{
	// TODO: Add your control notification handler code here
	}


void CFileDetailsDialog::OnTimer(UINT_PTR nIDEvent)
	{
	// TODO: Add your message handler code here and/or call default

	CDialogEx::OnTimer(nIDEvent);
	}
