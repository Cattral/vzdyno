// pch.h: This is a precompiled header file.
// Files listed below are compiled only once, improving build performance for future builds.
// This also affects IntelliSense performance, including code completion and many code browsing features.
// However, files listed here are ALL re-compiled if any one of them is updated between builds.
// Do not add files here that you will be updating frequently as this negates the performance advantage.

#ifndef PCH_H
#define PCH_H

// add headers that you want to pre-compile here
#include "framework.h"
#include <afxext.h>
#include <afxcontrolbars.h>
#include <setupapi.h>



//Pull in Standard Windows support
#include <Windows.h>

//Pull in ATL support
#include <atlbase.h>
#include <atlstr.h>

//Other includes
#include <tchar.h>
#include <setupapi.h>
#include <winspool.h>
#include <Wbemcli.h>
#include <comdef.h>
#include <stdio.h>

// Multimedia
#include <MMSystem.h>


//Pull in STL support
#include <vector>
#include <string>
#include <utility>


//Out of the box lets exclude support for CEnumerateSerial::UsingComDB on the Windows SDK 7.1 or earlier since msports.h
//is only available with the Windows SDK 8 or later.
#include <ntverp.h>
#if VER_PRODUCTBUILD <= 7600
#define NO_CENUMERATESERIAL_USING_COMDB
#endif //#if VER_PRODUCTBUILD <= 7600

#ifndef NO_CENUMERATESERIAL_USING_COMDB
#include <msports.h>
#endif //#ifndef NO_CENUMERATESERIAL_USING_COMDB


#endif //PCH_H
