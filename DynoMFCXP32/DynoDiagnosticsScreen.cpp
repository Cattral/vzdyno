// DynoDiagnosticsScreen.cpp : implementation file
//

#include "pch.h"
#include "DynoMFCXP32.h"
#include "DynoDiagnosticsScreen.h"
#include "afxdialogex.h"

#include "VZDynoDefines.h"


// DynoDiagnosticsScreen dialog

IMPLEMENT_DYNAMIC(DynoDiagnosticsScreen, CDialogEx)

DynoDiagnosticsScreen::DynoDiagnosticsScreen(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIAGNOSTICS, pParent)
{

}

DynoDiagnosticsScreen::~DynoDiagnosticsScreen()
{
}

void DynoDiagnosticsScreen::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DIAGNOSTICS_RESET_PROCESSOR_BUTTON, m_resetProcessorButton);
	DDX_Control(pDX, IDC_DIAGNOSTICS_READ_SENSOR_BUTTON, m_readButton);
	DDX_Control(pDX, IDC_DIAGNOSTICS_OTHER_DEVICE_LIST, m_otherDevice_list);
	DDX_Control(pDX, IDC_DIAGNOSTICS_SENSOR_LIST, m_sensorList);
	DDX_Control(pDX, IDC_DIAGNOSTICS_PROCESSOR_LIST, m_processorList);
	DDX_Control(pDX, IDC_DIAGNOSTICS_MOTOR_LIST, m_motorList);
	DDX_Control(pDX, IDC_DIAGNOSTICS_CONTINUOUS_SENSOR_CHECK, m_continousSensorRead);
	DDX_Control(pDX, IDC_DIAGNOSTICS_STATUS_BAR, m_status);
	DDX_Control(pDX, IDC_DIAGNOSTICS_READALL_SENSOR_BUTTON, m_sensorsReadAll);
	DDX_Control(pDX, IDC_DIAGNOSTICS_SET_LOW_MOTORS_BUTTON, m_motorsSetLow);
	DDX_Control(pDX, IDC_DIAGNOSTICS_SET_HIGH_MOTORS_BUTTON, m_motorsSetHigh);
	DDX_Control(pDX, IDC_DIAGNOSTICS_SET_LOW_OTHER_BUTTON, m_otherSetLow);
	DDX_Control(pDX, IDC_DIAGNOSTICS_SET_HIGH_OTHER_BUTTON, m_otherSetHigh);
	DDX_Control(pDX, IDC_DIAGNOSTICS_PROCEDURE_TESTING, m_procedureTestingList);
	DDX_Control(pDX, IDC_DIAGNOSTICS_TEST_PROCEDURE_BUTTON, m_testProcedureButton);
	DDX_Control(pDX, IDC_DIAGNOSTICS_TEST_PROCEDURE_PARAMETER, m_testProcedureParameter);
	DDX_Control(pDX, IDC_DIAGNOSTICS_READSTREAM, m_readStreamButton);
	DDX_Control(pDX, IDC_DIAGNOSTICS_DRAININPUT_BUTTON, m_drainInputButton);
	DDX_Control(pDX, IDC_DIAGNOSTICS_FLUSHIO_BUTTON, m_flushIOButton);
	DDX_Control(pDX, IDC_DIAGNOSTICS_RESET_PORT_BUTTON, m_resetPortButton);
	DDX_Control(pDX, IDC_DIAGNOSTICS_GET_STATE_BUTTON2, m_getStateButton);
	}


BEGIN_MESSAGE_MAP(DynoDiagnosticsScreen, CDialogEx)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_RESET_PROCESSOR_BUTTON, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsResetProcessorButton)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_READ_SENSOR_BUTTON, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsReadSensorButton)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_READALL_SENSOR_BUTTON, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsReadallSensorButton)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_DIAGNOSTICS_OTHER_DEVICE_LIST, &DynoDiagnosticsScreen::OnLvnItemchangedDiagnosticsOtherDeviceList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_DIAGNOSTICS_PROCESSOR_LIST, &DynoDiagnosticsScreen::OnLvnItemchangedDiagnosticsProcessorList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_DIAGNOSTICS_MOTOR_LIST, &DynoDiagnosticsScreen::OnLvnItemchangedDiagnosticsMotorList)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_SET_LOW_MOTORS_BUTTON, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsSetLowMotorsButton)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_SET_HIGH_MOTORS_BUTTON, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsSetHighMotorsButton)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_SET_LOW_OTHER_BUTTON, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsSetLowOtherButton)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_OTHER_PWM_BUTTON, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsOtherPwmButton)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_SET_HIGH_OTHER_BUTTON, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsSetHighOtherButton)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_DIAGNOSTICS_SENSOR_LIST, &DynoDiagnosticsScreen::OnLvnItemchangedDiagnosticsSensorList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_DIAGNOSTICS_PROCEDURE_TESTING, &DynoDiagnosticsScreen::OnLvnItemchangedDiagnosticsProcedureTesting)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_TEST_PROCEDURE_BUTTON, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsTestProcedureButton)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_CONTINUOUS_SENSOR_CHECK, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsContinuousSensorCheck)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_READSTREAM, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsReadstream)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_DRAININPUT_BUTTON, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsDraininputButton)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_FLUSHIO_BUTTON, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsFlushioButton)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_RESET_PORT_BUTTON, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsResetPortButton)
	ON_BN_CLICKED(IDC_DIAGNOSTICS_GET_STATE_BUTTON2, &DynoDiagnosticsScreen::OnBnClickedDiagnosticsGetStateButton)
END_MESSAGE_MAP()


// DynoDiagnosticsScreen message handlers


BOOL DynoDiagnosticsScreen::OnInitDialog()
	{
	CDialogEx::OnInitDialog();

	// Initialization
	lastProcessorValue[0][0] = TOKEN_CMD_unknown;
	memset((void*)lastMotorValue, TOKEN_CMD_unknown, DYNO_NUM_MOTORS*6);
	memset((void*)lastOtherDeviceValue, TOKEN_CMD_unknown, DYNO_NUM_OTHER_DEVICES*6);
	memset((void*)lastProcedureValue, RESULT_executeProc_unknown, DYNO_NUM_PROCEDURES * 6);

	// Set up the full row select style
	m_processorList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_sensorList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_motorList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_otherDevice_list.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	m_procedureTestingList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	// Change Back colour and Text colour when not in focus 
	m_processorList.SetUnFocusColors(::GetSysColor(COLOR_HIGHLIGHT), ::GetSysColor(COLOR_HIGHLIGHTTEXT));
	m_processorList.SetShowSelectedItemUnFocus(true);
	m_sensorList.SetUnFocusColors(::GetSysColor(COLOR_HIGHLIGHT), ::GetSysColor(COLOR_HIGHLIGHTTEXT));
	m_sensorList.SetShowSelectedItemUnFocus(true);
	m_motorList.SetUnFocusColors(::GetSysColor(COLOR_HIGHLIGHT), ::GetSysColor(COLOR_HIGHLIGHTTEXT));
	m_motorList.SetShowSelectedItemUnFocus(true);
	m_otherDevice_list.SetUnFocusColors(::GetSysColor(COLOR_HIGHLIGHT), ::GetSysColor(COLOR_HIGHLIGHTTEXT));
	m_otherDevice_list.SetShowSelectedItemUnFocus(true);

	// Set up all of the columns
	m_processorList.InsertColumn(0, _T("Processor"), LVCFMT_LEFT, 150);
	m_processorList.InsertColumn(1, _T("State"), LVCFMT_CENTER, 100);
	m_processorList.InsertColumn(2, _T("Last CMD"), LVCFMT_CENTER, 84);
	m_sensorList.InsertColumn(0, _T("Sensor"), LVCFMT_LEFT, 250);
	m_sensorList.InsertColumn(1, _T("Value"), LVCFMT_CENTER, 84);
	m_motorList.InsertColumn(0, _T("Motor"), LVCFMT_LEFT, 250);
	m_motorList.InsertColumn(1, _T("Last CMD"), LVCFMT_CENTER, 84);
	m_otherDevice_list.InsertColumn(0, _T("Device name"), LVCFMT_LEFT, 250);
	m_otherDevice_list.InsertColumn(1, _T("Last CMD"), LVCFMT_CENTER, 84);
	m_procedureTestingList.InsertColumn(0, _T("Procedure / subsystem"), LVCFMT_LEFT, 250);
	m_procedureTestingList.InsertColumn(1, _T("Status"), LVCFMT_CENTER, 84);


	// The entries in the list boxes will rarely change because it means that there is a hardware change in the Dyno.
	// As such, they will be hard coded here. 
	populateProcessorList();
	populateSensorList();
	populateMotorList();
	populateOtherDeviceList();
	populateProcedureTestList();

	updateStatus(_T("No commands issued and no data received"), false);

	// Set the button states
	updateControls();


	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
	}

// Set the button states
void DynoDiagnosticsScreen::updateControls()
	{
	// The reboot needs a line to be selected	
	m_resetProcessorButton.EnableWindow((m_processorList.GetItemCount() > 0) && (m_processorList.GetSelectedCount() > 0));

	// The read-single needs a line to be selected
	m_readButton.EnableWindow((m_sensorList.GetItemCount() > 0) && (m_sensorList.GetSelectedCount() > 0));

	// The Read buttons for sensors are always valid unless the list is empty 
	m_sensorsReadAll.EnableWindow(m_sensorList.GetItemCount() > 0);
	m_continousSensorRead.EnableWindow(m_sensorList.GetItemCount() > 0);

	// As long as a motor is selected, we can send it commands	
	m_motorsSetLow.EnableWindow((m_motorList.GetItemCount() > 0) && (m_motorList.GetSelectedCount() > 0));
	m_motorsSetHigh.EnableWindow((m_motorList.GetItemCount() > 0) && (m_motorList.GetSelectedCount() > 0));;
	m_otherSetLow.EnableWindow((m_otherDevice_list.GetItemCount() > 0) && (m_otherDevice_list.GetSelectedCount() > 0));;
	m_otherSetHigh.EnableWindow((m_otherDevice_list.GetItemCount() > 0) && (m_otherDevice_list.GetSelectedCount() > 0));;
	}


// Populate the processor list
void DynoDiagnosticsScreen::populateProcessorList()
	{
	int nItem = m_processorList.InsertItem(0, Constants.String_processor_arduino.c_str());
	m_processorList.SetItemData(nItem, (DWORD_PTR)&lastProcessorValue[nItem]);
	lastProcessorValue[nItem][1] = DYNO_STATE_UNKNOWN;
	lastProcessorValue[nItem][2] = RESULT_executeProc_unknown;

	m_processorList.SelectItem(0, true);

	// Keep a pointer to the Dyno's state so that it's easy to change and display
	lastKnownDynoState = &lastProcessorValue[nItem][1];

	// Update the other columns
	updateProcessorValues();
	}

	// Update the list of processor values
void DynoDiagnosticsScreen::updateProcessorValues()
	{
	m_processorList.SetRedraw(FALSE);

	int processorCount = m_processorList.GetItemCount();

	// Roll through the list of items
	for (int index = 0; index < processorCount; index++)
		{
		CString valueString;
		char* valuePtr;

		valuePtr = (char*)m_processorList.GetItemData(index);

		// Determine and update the state
		switch (valuePtr[1])
			{
			case DYNO_STATE_UNKNOWN:
				valueString = DYNO_STATE_TEXT_UNKNOWN;
				break;

			case DYNO_STATE_INIT:
				valueString = DYNO_STATE_TEXT_INIT;
				break;

			case DYNO_STATE_IDLE:
				valueString = DYNO_STATE_TEXT_IDLE;
				break;

			case DYNO_STATE_STARTUP:
				valueString = DYNO_STATE_TEXT_STARTUP;
				break;

			case DYNO_STATE_ADJUST_BASE:
				valueString = DYNO_STATE_TEXT_ADJUST_BASE;
				break;

			case DYNO_STATE_ADJUST_BRAKES:
				valueString = DYNO_STATE_TEXT_ADJUST_BRAKES;
				break;

			case DYNO_STATE_ADJUST_LOCKING_PINS:
				valueString = DYNO_STATE_TEXT_ADJUST_LOCKING_PINS;
				break;

			case DYNO_STATE_ASSESSING:
				valueString = DYNO_STATE_TEXT_ASSESSING;
				break;

			case DYNO_STATE_RUNNING:
				valueString = DYNO_STATE_TEXT_RUNNING;
				break;

			case DYNO_STATE_FINISHED:
				valueString = DYNO_STATE_TEXT_FINISHED;
				break;

			case DYNO_STATE_ESTOP_PRESSED:
				valueString = DYNO_STATE_TEXT_ESTOP_PRESSED;
				break;

			case DYNO_STATE_POWER_FAIL:
				valueString = DYNO_STATE_TEXT_POWER_FAIL;
				break;

			default:
				valueString = "Error";
			}

		// Update the value in the appropriate row and column
		m_processorList.SetItemText(index, 1, valueString);

		// Next, print the result of the last command that was run
		switch (valuePtr[2])
			{
			case RESULT_executeProc_unknown:
				valueString = "Unknown";
				break;

			case RESULT_executeProc_running:
				valueString = "Running";
				break;

			case RESULT_executeProc_run:
				valueString = "Run";
				break;

			case RESULT_executeProc_passed:
				valueString = "Passed";
				break;

			case RESULT_executeProc_failed:
				valueString = "Failed";
				break;

			default:
				valueString = "Error";
			}

		// Update the value in the appropriate row and column
		m_processorList.SetItemText(index, 2, valueString);
		}

	m_processorList.SetRedraw(TRUE);
	}

// Populate the sensor list
void DynoDiagnosticsScreen::populateSensorList()
	{
	m_sensorList.SetRedraw(FALSE);

	int nItem = m_sensorList.InsertItem(0, Constants.String_sensor_weather_temperature.c_str());
	pinInfo[PIN_SENSOR_A_WEATHER_TEMP].pinNumber = PIN_SENSOR_A_WEATHER_TEMP;
	pinInfo[PIN_SENSOR_A_WEATHER_TEMP].dataAddress = (DWORD_PTR)&currentSensorValues.weather_temperature;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_WEATHER_TEMP]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_weather_pressure.c_str());
	pinInfo[PIN_SENSOR_A_WEATHER_PRESSURE].pinNumber = PIN_SENSOR_A_WEATHER_PRESSURE;
	pinInfo[PIN_SENSOR_A_WEATHER_PRESSURE].dataAddress = (DWORD_PTR)&currentSensorValues.weather_pressure;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_WEATHER_PRESSURE]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_weather_humidity.c_str());
	pinInfo[PIN_SENSOR_A_WEATHER_HUMIDITY].pinNumber = PIN_SENSOR_A_WEATHER_HUMIDITY;
	pinInfo[PIN_SENSOR_A_WEATHER_HUMIDITY].dataAddress = (DWORD_PTR)&currentSensorValues.weather_humidity;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_WEATHER_HUMIDITY]);

	// For simplicity, for the brake tension, I'm using the index of the DOUT digital pin.
	// Each load cell uses two pins, but they are not being read directly.
	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_brake_front_tension.c_str());
	pinInfo[PIN_SENSOR_D_BRAKE_FRONT_TENSION_DOUT].pinNumber = PIN_SENSOR_D_BRAKE_FRONT_TENSION_DOUT;
	pinInfo[PIN_SENSOR_D_BRAKE_FRONT_TENSION_DOUT].dataAddress = (DWORD_PTR)&currentSensorValues.brake_front_tension;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_D_BRAKE_FRONT_TENSION_DOUT]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_brake_rear_tension.c_str());
	pinInfo[PIN_SENSOR_D_BRAKE_REAR_TENSION_DOUT].pinNumber = PIN_SENSOR_D_BRAKE_REAR_TENSION_DOUT;
	pinInfo[PIN_SENSOR_D_BRAKE_REAR_TENSION_DOUT].dataAddress = (DWORD_PTR)&currentSensorValues.brake_rear_tension;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_D_BRAKE_REAR_TENSION_DOUT]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_brake_front_encoder.c_str());
	pinInfo[PIN_SENSOR_D_BRAKE_FRONT_MOTOR_ENCODER].pinNumber = PIN_SENSOR_D_BRAKE_FRONT_MOTOR_ENCODER;
	pinInfo[PIN_SENSOR_D_BRAKE_FRONT_MOTOR_ENCODER].dataAddress = (DWORD_PTR)&currentSensorValues.brake_front_encoder;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_D_BRAKE_FRONT_MOTOR_ENCODER]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_brake_rear_encoder.c_str());
	pinInfo[PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER].pinNumber = PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER;
	pinInfo[PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER].dataAddress = (DWORD_PTR)&currentSensorValues.brake_rear_encoder;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_D_BRAKE_REAR_MOTOR_ENCODER]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_gearbox_front_temp.c_str());
	pinInfo[PIN_SENSOR_A_GEARBOX_FRONT_TEMP].pinNumber = PIN_SENSOR_A_GEARBOX_FRONT_TEMP;
	pinInfo[PIN_SENSOR_A_GEARBOX_FRONT_TEMP].dataAddress = (DWORD_PTR)&currentSensorValues.gearbox_front_temp;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_GEARBOX_FRONT_TEMP]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_gearbox_rear_temp.c_str());
	pinInfo[PIN_SENSOR_A_GEARBOX_REAR_TEMP].pinNumber = PIN_SENSOR_A_GEARBOX_REAR_TEMP;
	pinInfo[PIN_SENSOR_A_GEARBOX_REAR_TEMP].dataAddress = (DWORD_PTR)&currentSensorValues.gearbox_rear_temp;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_GEARBOX_REAR_TEMP]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_bearing_front_temp.c_str());
	pinInfo[PIN_SENSOR_A_BEARING_FRONT_TEMP].pinNumber = PIN_SENSOR_A_BEARING_FRONT_TEMP;
	pinInfo[PIN_SENSOR_A_BEARING_FRONT_TEMP].dataAddress = (DWORD_PTR)&currentSensorValues.bearing_front_temp;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_BEARING_FRONT_TEMP]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_bearing_rear_temp.c_str());
	pinInfo[PIN_SENSOR_A_BEARING_REAR_TEMP].pinNumber = PIN_SENSOR_A_BEARING_REAR_TEMP;
	pinInfo[PIN_SENSOR_A_BEARING_REAR_TEMP].dataAddress = (DWORD_PTR)&currentSensorValues.bearing_rear_temp;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_BEARING_REAR_TEMP]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_lockpin_LR_pos.c_str());
	pinInfo[PIN_SENSOR_A_LOCKPIN_LR_POS].pinNumber = PIN_SENSOR_A_LOCKPIN_LR_POS;
	pinInfo[PIN_SENSOR_A_LOCKPIN_LR_POS].dataAddress = (DWORD_PTR)&currentSensorValues.lockpin_LR_pos;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_LOCKPIN_LR_POS]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_lockpin_LF_pos.c_str());
	pinInfo[PIN_SENSOR_A_LOCKPIN_LF_POS].pinNumber = PIN_SENSOR_A_LOCKPIN_LF_POS;
	pinInfo[PIN_SENSOR_A_LOCKPIN_LF_POS].dataAddress = (DWORD_PTR)&currentSensorValues.lockpin_LF_pos;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_LOCKPIN_LF_POS]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_lockpin_RF_pos.c_str());
	pinInfo[PIN_SENSOR_A_LOCKPIN_RF_POS].pinNumber = PIN_SENSOR_A_LOCKPIN_RF_POS;
	pinInfo[PIN_SENSOR_A_LOCKPIN_RF_POS].dataAddress = (DWORD_PTR)&currentSensorValues.lockpin_RF_pos;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_LOCKPIN_RF_POS]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_lockpin_RR_pos.c_str());
	pinInfo[PIN_SENSOR_A_LOCKPIN_RR_POS].pinNumber = PIN_SENSOR_A_LOCKPIN_RR_POS;
	pinInfo[PIN_SENSOR_A_LOCKPIN_RR_POS].dataAddress = (DWORD_PTR)&currentSensorValues.lockpin_RR_pos;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_LOCKPIN_RR_POS]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_basemotor_front_prox.c_str());
	pinInfo[PIN_SENSOR_D_BASEMOTOR_PROX_FRONT].pinNumber = PIN_SENSOR_D_BASEMOTOR_PROX_FRONT;
	pinInfo[PIN_SENSOR_D_BASEMOTOR_PROX_FRONT].dataAddress = (DWORD_PTR)&currentSensorValues.basemotor_prox_front;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_D_BASEMOTOR_PROX_FRONT]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_basemotor_rear_prox.c_str());
	pinInfo[PIN_SENSOR_D_BASEMOTOR_PROX_REAR].pinNumber = PIN_SENSOR_D_BASEMOTOR_PROX_REAR;
	pinInfo[PIN_SENSOR_D_BASEMOTOR_PROX_REAR].dataAddress = (DWORD_PTR)&currentSensorValues.basemotor_prox_rear;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_D_BASEMOTOR_PROX_REAR]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_encoder_front_main.c_str());
	pinInfo[PIN_SENSOR_D_ENCODER_FRONT_MAIN].pinNumber = PIN_SENSOR_D_ENCODER_FRONT_MAIN;
	pinInfo[PIN_SENSOR_D_ENCODER_FRONT_MAIN].dataAddress = (DWORD_PTR)&currentSensorValues.encoder_front_main;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_D_ENCODER_FRONT_MAIN]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_encoder_front_offset.c_str());
	pinInfo[PIN_SENSOR_D_ENCODER_FRONT_OFFSET].pinNumber = PIN_SENSOR_D_ENCODER_FRONT_OFFSET;
	pinInfo[PIN_SENSOR_D_ENCODER_FRONT_OFFSET].dataAddress = (DWORD_PTR)&currentSensorValues.encoder_front_offset;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_D_ENCODER_FRONT_OFFSET]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_encoder_rear_main.c_str());
	pinInfo[PIN_SENSOR_D_ENCODER_REAR_MAIN].pinNumber = PIN_SENSOR_D_ENCODER_REAR_MAIN;
	pinInfo[PIN_SENSOR_D_ENCODER_REAR_MAIN].dataAddress = (DWORD_PTR)&currentSensorValues.encoder_rear_main;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_D_ENCODER_REAR_MAIN]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_encoder_rear_offset.c_str());
	pinInfo[PIN_SENSOR_D_ENCODER_REAR_OFFSET].pinNumber = PIN_SENSOR_D_ENCODER_REAR_OFFSET;
	pinInfo[PIN_SENSOR_D_ENCODER_REAR_OFFSET].dataAddress = (DWORD_PTR)&currentSensorValues.encoder_rear_offset;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_D_ENCODER_REAR_OFFSET]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_RPM.c_str());
	pinInfo[PIN_SENSOR_A_RPM].pinNumber = PIN_SENSOR_A_RPM;
	pinInfo[PIN_SENSOR_A_RPM].dataAddress = (DWORD_PTR)&currentSensorValues.RPM;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_RPM]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_AFR.c_str());
	pinInfo[PIN_SENSOR_A_AFR].pinNumber = PIN_SENSOR_A_AFR;
	pinInfo[PIN_SENSOR_A_AFR].dataAddress = (DWORD_PTR)&currentSensorValues.AFR;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_A_AFR]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_base_position.c_str());
	pinInfo[PIN_SENSOR_D_BASEMOTOR_POS].pinNumber = PIN_SENSOR_D_BASEMOTOR_POS;
	pinInfo[PIN_SENSOR_D_BASEMOTOR_POS].dataAddress = (DWORD_PTR)&currentSensorValues.base_position;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_D_BASEMOTOR_POS]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_mains_detect.c_str());
	pinInfo[PIN_SENSOR_D_MAINS_DETECT].pinNumber = PIN_SENSOR_D_MAINS_DETECT;
	pinInfo[PIN_SENSOR_D_MAINS_DETECT].dataAddress = (DWORD_PTR)&currentSensorValues.mains_detect;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_SENSOR_D_MAINS_DETECT]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_pendant_select.c_str());
	pinInfo[PIN_BUTTON_D_PENDANT_SELECT].pinNumber = PIN_BUTTON_D_PENDANT_SELECT;
	pinInfo[PIN_BUTTON_D_PENDANT_SELECT].dataAddress = (DWORD_PTR)&currentSensorValues.pendant_select;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_BUTTON_D_PENDANT_SELECT]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_pendant_exec.c_str());
	pinInfo[PIN_BUTTON_D_PENDANT_EXECUTE].pinNumber = PIN_BUTTON_D_PENDANT_EXECUTE;
	pinInfo[PIN_BUTTON_D_PENDANT_EXECUTE].dataAddress = (DWORD_PTR)&currentSensorValues.pendant_exec;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_BUTTON_D_PENDANT_EXECUTE]);

	nItem = m_sensorList.InsertItem(++nItem, Constants.String_sensor_pendant_estop.c_str());
	pinInfo[PIN_BUTTON_D_PENDANT_STOP].pinNumber = PIN_BUTTON_D_PENDANT_STOP;
	pinInfo[PIN_BUTTON_D_PENDANT_STOP].dataAddress = (DWORD_PTR)&currentSensorValues.pendant_estop;
	m_sensorList.SetItemData(nItem, (DWORD_PTR)&pinInfo[PIN_BUTTON_D_PENDANT_STOP]);

	// Update value column
	updateSensorValues();
	}

// Update the list of sensor values
void DynoDiagnosticsScreen::updateSensorValues()
	{
	m_sensorList.SetRedraw(FALSE);

	int sensorCount = m_sensorList.GetItemCount();

	// Roll through the list of items
	for (int index = 0; index < sensorCount; index++)
		{
		CString valueString;
		pinRecordInfo* pinInfoPtr;
		uint16_t* valuePtr;

		pinInfoPtr = (pinRecordInfo*)m_sensorList.GetItemData(index);
		valuePtr = (uint16_t*)pinInfoPtr->dataAddress;

		if (*valuePtr == SENSOR_VALUE_UNKNOWN)
			valueString = "?";
		else
			{
			// TODO: IF there is any data modification that needs to happen before it is displayed,
			// such as converting a value into a temperature, then this is where it goes.
			if (index == 0)
				{
				float temp = convertAnalogToTemperature((INT16)*valuePtr);
				valueString.Format(_T("%i (%3.2f C)"), (INT16)*valuePtr, temp);
				}
			else if (currentSensorValues.isDigitalPin(pinInfoPtr->pinNumber))
				{
				valueString.Format(_T("%s (%i)"), *valuePtr == 1 ? _T("High") : _T("Low"), (INT16)*valuePtr);
				}
			else
				{
				valueString.Format(_T("%i"), (INT16)*valuePtr);
				}

			// NOTE: Also, we can intelligently update, where only changed values are updated,
			// but only both if this causes strange flickering.
			}

		// Update the value in the appropriate row and column
		m_sensorList.SetItemText(index, 1, valueString);
		}

	m_sensorList.SetRedraw(TRUE);
	}

// Populate the motor list
void DynoDiagnosticsScreen::populateMotorList()
	{
	int nItem = m_motorList.InsertItem(0, Constants.String_motor_base.c_str());
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	// TODO: LOGIC NEEDS TO BE CHECKED/MODIFIED FOR BASE MOTOR

	nItem = m_motorList.InsertItem(++nItem, Constants.String_other_device_base_contactor_forward.c_str());
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);
	lastMotorValue[nItem][1] = TOKEN_setBase;
	lastMotorValue[nItem][2] = TOKEN_CMD_base_forwardContactor;

	nItem = m_motorList.InsertItem(++nItem, Constants.String_other_device_base_contactor_reverse.c_str());
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);
	lastMotorValue[nItem][1] = TOKEN_setBase;
	lastMotorValue[nItem][2] = TOKEN_CMD_base_reverseContactor;

	nItem = m_motorList.InsertItem(++nItem, Constants.String_motor_brake_front_o.c_str());
	lastMotorValue[nItem][1] = TOKEN_setBrake_front;
	lastMotorValue[nItem][2] = TOKEN_CMD_motorOnOff;		// On/off
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	nItem = m_motorList.InsertItem(++nItem, Constants.String_motor_brake_front_d.c_str());
	lastMotorValue[nItem][1] = TOKEN_setBrake_front;
	lastMotorValue[nItem][2] = TOKEN_CMD_motorDirection;	// Direction
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	nItem = m_motorList.InsertItem(++nItem, Constants.String_motor_brake_rear_o.c_str());
	lastMotorValue[nItem][1] = TOKEN_setBrake_rear;
	lastMotorValue[nItem][2] = TOKEN_CMD_motorOnOff;		// On/off
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	nItem = m_motorList.InsertItem(++nItem, Constants.String_motor_brake_rear_d.c_str());
	lastMotorValue[nItem][1] = TOKEN_setBrake_rear;
	lastMotorValue[nItem][2] = TOKEN_CMD_motorDirection;	// Direction
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	nItem = m_motorList.InsertItem(++nItem, Constants.String_motor_lockpin_LRO.c_str());
	lastMotorValue[nItem][1] = TOKEN_setLP_LR;
	lastMotorValue[nItem][2] = TOKEN_CMD_motorOnOff;		// On/off
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	nItem = m_motorList.InsertItem(++nItem, Constants.String_motor_lockpin_LRD.c_str());
	lastMotorValue[nItem][1] = TOKEN_setLP_LR;
	lastMotorValue[nItem][2] = TOKEN_CMD_motorDirection;	// Direction
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	nItem = m_motorList.InsertItem(++nItem, Constants.String_motor_lockpin_LFO.c_str());
	lastMotorValue[nItem][1] = TOKEN_setLP_LF;
	lastMotorValue[nItem][2] = TOKEN_CMD_motorOnOff;		// On/off
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	nItem = m_motorList.InsertItem(++nItem, Constants.String_motor_lockpin_LFD.c_str());
	lastMotorValue[nItem][1] = TOKEN_setLP_LF;
	lastMotorValue[nItem][2] = TOKEN_CMD_motorDirection;	// Direction
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	nItem = m_motorList.InsertItem(++nItem, Constants.String_motor_lockpin_RFO.c_str());
	lastMotorValue[nItem][1] = TOKEN_setLP_RF;
	lastMotorValue[nItem][2] = TOKEN_CMD_motorOnOff;		// On/off
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	nItem = m_motorList.InsertItem(++nItem, Constants.String_motor_lockpin_RFD.c_str());
	lastMotorValue[nItem][1] = TOKEN_setLP_RF;
	lastMotorValue[nItem][2] = TOKEN_CMD_motorDirection;	// Direction
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	nItem = m_motorList.InsertItem(++nItem, Constants.String_motor_lockpin_RRO.c_str());
	lastMotorValue[nItem][1] = TOKEN_setLP_RR;
	lastMotorValue[nItem][2] = TOKEN_CMD_motorOnOff;		// On/off
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	nItem = m_motorList.InsertItem(++nItem, Constants.String_motor_lockpin_RRD.c_str());
	lastMotorValue[nItem][1] = TOKEN_setLP_RR;
	lastMotorValue[nItem][2] = TOKEN_CMD_motorDirection;	// Direction
	m_motorList.SetItemData(nItem, (DWORD_PTR)&lastMotorValue[nItem]);

	// Update value column
	updateMotorValues();
	}

// Update the list of motor values
void DynoDiagnosticsScreen::updateMotorValues()
	{
	m_motorList.SetRedraw(FALSE);

	int motorCount = m_motorList.GetItemCount();

	// Roll through the list of items
	for (int index = 0; index < motorCount; index++)
		{
		CString valueString;
		char* valuePtr;

		valuePtr = (char*)m_motorList.GetItemData(index);

		switch (valuePtr[0])
			{
			case TOKEN_CMD_unknown:
				valueString = "?";
				break;

			case TOKEN_CMD_signal_LOW:
				valueString = "Low";
				break;

			case TOKEN_CMD_signal_HIGH:
				valueString = "High";
				break;

			default:
				valueString = "Error";
			}

		// Update the value in the appropriate row and column
		m_motorList.SetItemText(index, 1, valueString);
		}

	m_motorList.SetRedraw(TRUE);
	}

// Populate the other device list
void DynoDiagnosticsScreen::populateOtherDeviceList()
	{
	int nItem = m_otherDevice_list.InsertItem(0, Constants.String_other_device_mains_disconnect.c_str());
	m_otherDevice_list.SetItemData(nItem, (DWORD_PTR)&lastOtherDeviceValue[nItem]);
	lastOtherDeviceValue[nItem][1] = TOKEN_setPowerConnect;

	nItem = m_otherDevice_list.InsertItem(++nItem, Constants.String_other_device_vfd_fans.c_str());
	m_otherDevice_list.SetItemData(nItem, (DWORD_PTR)&lastOtherDeviceValue[nItem]);
	// TODO: The fans PWM needs to be added

	nItem = m_otherDevice_list.InsertItem(++nItem, Constants.String_other_device_AWD_clutch_front.c_str());
	m_otherDevice_list.SetItemData(nItem, (DWORD_PTR)&lastOtherDeviceValue[nItem]);
	lastOtherDeviceValue[nItem][1] = TOKEN_setAWDClutch_front;

	nItem = m_otherDevice_list.InsertItem(++nItem, Constants.String_other_device_AWD_clutch_rear.c_str());
	m_otherDevice_list.SetItemData(nItem, (DWORD_PTR)&lastOtherDeviceValue[nItem]);
	lastOtherDeviceValue[nItem][1] = TOKEN_setAWDClutch_rear;

	nItem = m_otherDevice_list.InsertItem(++nItem, Constants.String_other_device_gearbox_front_cooling.c_str());
	m_otherDevice_list.SetItemData(nItem, (DWORD_PTR)&lastOtherDeviceValue[nItem]);
	lastOtherDeviceValue[nItem][1] = TOKEN_setGearboxCooling_front;

	nItem = m_otherDevice_list.InsertItem(++nItem, Constants.String_other_device_gearbox_rear_cooling.c_str());
	m_otherDevice_list.SetItemData(nItem, (DWORD_PTR)&lastOtherDeviceValue[nItem]);
	lastOtherDeviceValue[nItem][1] = TOKEN_setGearboxCooling_rear;

	nItem = m_otherDevice_list.InsertItem(++nItem, Constants.String_other_device_brake_front_solenoid_energize.c_str());
	m_otherDevice_list.SetItemData(nItem, (DWORD_PTR)&lastOtherDeviceValue[nItem]);
	lastOtherDeviceValue[nItem][1] = TOKEN_setSolenoid_energize_front;

	nItem = m_otherDevice_list.InsertItem(++nItem, Constants.String_other_device_brake_front_solenoid_hold.c_str());
	m_otherDevice_list.SetItemData(nItem, (DWORD_PTR)&lastOtherDeviceValue[nItem]);
	lastOtherDeviceValue[nItem][1] = TOKEN_setSolenoid_hold_front;

	nItem = m_otherDevice_list.InsertItem(++nItem, Constants.String_other_device_brake_rear_solenoid_energize.c_str());
	m_otherDevice_list.SetItemData(nItem, (DWORD_PTR)&lastOtherDeviceValue[nItem]);
	lastOtherDeviceValue[nItem][1] = TOKEN_setSolenoid_energize_rear;

	nItem = m_otherDevice_list.InsertItem(++nItem, Constants.String_other_device_brake_rear_solenoid_hold.c_str());
	m_otherDevice_list.SetItemData(nItem, (DWORD_PTR)&lastOtherDeviceValue[nItem]);
	lastOtherDeviceValue[nItem][1] = TOKEN_setSolenoid_hold_rear;

	nItem = m_otherDevice_list.InsertItem(++nItem, Constants.String_other_device_LED_on_board.c_str());
	m_otherDevice_list.SetItemData(nItem, (DWORD_PTR)&lastOtherDeviceValue[nItem]);
	lastOtherDeviceValue[nItem][1] = TOKEN_setLED;

	// Update value column
	updateOtherDeviceValues();
	}

// Update the list of other device values
void DynoDiagnosticsScreen::updateOtherDeviceValues()
	{
	int otherCount = m_otherDevice_list.GetItemCount();

	// Roll through the list of items
	for (int index = 0; index < otherCount; index++)
		{
		CString valueString;
		char* valuePtr;

		valuePtr = (char*)m_otherDevice_list.GetItemData(index);

		switch (*valuePtr)
			{
			case TOKEN_CMD_unknown:
				valueString = "?";
				break;

			case TOKEN_CMD_signal_LOW:
				valueString = "Low";
				break;

			case TOKEN_CMD_signal_HIGH:
				valueString = "High";
				break;

			default:
				valueString = "Error";
			}

		// Update the value in the appropriate row and column
		m_otherDevice_list.SetItemText(index, 1, valueString);
		}
	}

void DynoDiagnosticsScreen::populateProcedureTestList()
	{
	int nItem = m_procedureTestingList.InsertItem(0, Constants.String_procedure_base_home.c_str());
	m_procedureTestingList.SetItemData(nItem, (DWORD_PTR)&lastProcedureValue[nItem]);
	lastProcedureValue[nItem][1] = TOKEN_executeProcedure;
	lastProcedureValue[nItem][2] = TOKEN_procedure_HomeBase;

	nItem = m_procedureTestingList.InsertItem(++nItem, Constants.String_procedure_base_move_to_X.c_str());
	m_procedureTestingList.SetItemData(nItem, (DWORD_PTR)&lastProcedureValue[nItem]);
	lastProcedureValue[nItem][1] = TOKEN_executeProcedure;
	lastProcedureValue[nItem][2] = TOKEN_procedure_MoveBaseToX;
	lastProcedureValue[nItem][3] = 00;								// TODO: Put the value for the base position here

	nItem = m_procedureTestingList.InsertItem(++nItem, Constants.String_procedure_brakes_calibrate_front.c_str());
	m_procedureTestingList.SetItemData(nItem, (DWORD_PTR)&lastProcedureValue[nItem]);
	lastProcedureValue[nItem][1] = TOKEN_executeProcedure;
	lastProcedureValue[nItem][2] = TOKEN_procedure_brakes_calibrate_front;

	nItem = m_procedureTestingList.InsertItem(++nItem, Constants.String_procedure_brakes_calibrate_rear.c_str());
	m_procedureTestingList.SetItemData(nItem, (DWORD_PTR)&lastProcedureValue[nItem]);
	lastProcedureValue[nItem][1] = TOKEN_executeProcedure;
	lastProcedureValue[nItem][2] = TOKEN_procedure_brakes_calibrate_rear;

	nItem = m_procedureTestingList.InsertItem(++nItem, Constants.String_procedure_brakes_engage_all.c_str());
	m_procedureTestingList.SetItemData(nItem, (DWORD_PTR)&lastProcedureValue[nItem]);
	lastProcedureValue[nItem][1] = TOKEN_executeProcedure;
	lastProcedureValue[nItem][2] = TOKEN_procedure_EngageBrakes;

	nItem = m_procedureTestingList.InsertItem(++nItem, Constants.String_procedure_brakes_release_all.c_str());
	m_procedureTestingList.SetItemData(nItem, (DWORD_PTR)&lastProcedureValue[nItem]);
	lastProcedureValue[nItem][1] = TOKEN_executeProcedure;
	lastProcedureValue[nItem][2] = TOKEN_procedure_ReleaseBrakes;

	nItem = m_procedureTestingList.InsertItem(++nItem, Constants.String_procedure_brakes_engage_front.c_str());
	m_procedureTestingList.SetItemData(nItem, (DWORD_PTR)&lastProcedureValue[nItem]);
	lastProcedureValue[nItem][1] = TOKEN_executeProcedure;
	lastProcedureValue[nItem][2] = TOKEN_procedure_EngageFrontBrake;

	nItem = m_procedureTestingList.InsertItem(++nItem, Constants.String_procedure_brakes_release_front.c_str());
	m_procedureTestingList.SetItemData(nItem, (DWORD_PTR)&lastProcedureValue[nItem]);
	lastProcedureValue[nItem][1] = TOKEN_executeProcedure;
	lastProcedureValue[nItem][2] = TOKEN_procedure_ReleaseFrontBrake;

	nItem = m_procedureTestingList.InsertItem(++nItem, Constants.String_procedure_brakes_engage_rear.c_str());
	m_procedureTestingList.SetItemData(nItem, (DWORD_PTR)&lastProcedureValue[nItem]);
	lastProcedureValue[nItem][1] = TOKEN_executeProcedure;
	lastProcedureValue[nItem][2] = TOKEN_procedure_EngageRearBrake;

	nItem = m_procedureTestingList.InsertItem(++nItem, Constants.String_procedure_brakes_release_rear.c_str());
	m_procedureTestingList.SetItemData(nItem, (DWORD_PTR)&lastProcedureValue[nItem]);
	lastProcedureValue[nItem][1] = TOKEN_executeProcedure;
	lastProcedureValue[nItem][2] = TOKEN_procedure_ReleaseRearBrake;

	// Update value column
	updateProcedureTestStatusValues();
	}


void DynoDiagnosticsScreen::updateProcedureTestStatusValues()
	{
	int procedureCount = m_procedureTestingList.GetItemCount();

	// Roll through the list of items
	for (int index = 0; index < procedureCount; index++)
		{
		CString valueString;
		char* valuePtr;

		valuePtr = (char*)m_procedureTestingList.GetItemData(index);

		switch (*valuePtr)
			{
			case RESULT_executeProc_unknown:
				valueString = "Unknown";
				break;

			case RESULT_executeProc_running:
				valueString = "Running";
				break;

			case RESULT_executeProc_run:
				valueString = "Run";
				break;

			case RESULT_executeProc_passed:
				valueString = "Passed";
				break;

			case RESULT_executeProc_failed:
				valueString = "Failed";
				break;

			default:
				valueString = "Error";
			}

		// Update the value in the appropriate row and column
		m_procedureTestingList.SetItemText(index, 1, valueString);
		}
	}

// Display the specified status on the screen, optionally with the time
void DynoDiagnosticsScreen::updateStatus(CString text, bool displayTime)
	{
	m_status.SetWindowTextW(text + (displayTime ? " (time)" : ""));
	}

void DynoDiagnosticsScreen::OnBnClickedDiagnosticsResetProcessorButton()
	{
	POSITION curRecord = m_processorList.GetFirstSelectedItemPosition();

	// Allocated extra buffer space to rule out possible pointer issues from COM bug
	char sendData[100];
	memset(sendData, 0, 100);

	if (curRecord)
		{
		int nItem = m_processorList.GetNextItem(-1, LVNI_SELECTED);

		if (nItem > -1)
			{
			char* valuePtr;

			valuePtr = (char*)m_processorList.GetItemData(nItem);

			// Send the RESET command to the selected processor
			sendData[0] = TOKEN_rebootArduino;
			sendData[1] = 0;
			sendData[2] = 0;
			sendData[3] = 0;
			sendData[4] = 0;

			int length = globalPorts->Send(openPortIndex, sendData, 1);

			if (length < 1)
				{
				// TODO: Log error

				m_status.SetWindowTextW(globalPorts->lastErrorString);
				}
			else
				{
				// Set it to "running"
				valuePtr[2] = RESULT_executeProc_running;
				}
			}
		}

	// Update value column
	updateProcessorValues();
	}


// Query the Arduino for all of the data on the sensors and display it
void DynoDiagnosticsScreen::OnBnClickedDiagnosticsReadSensorButton()
	{
	// TODO: Functionality for reading a single sensor isn't implemented yet



	}

void DynoDiagnosticsScreen::OnBnClickedDiagnosticsReadallSensorButton()
	{
	CString msg (TOKEN_getLastSensorValues);
	MSG uiMessage;

	// Turn off the buttons until the read is complete
	m_readButton.EnableWindow(false);
	m_sensorsReadAll.EnableWindow(false);
	m_readStreamButton.EnableWindow(false);

	do
		{
		// If we're waiting on a read to happen then don't keep spamming requests
		if (!poll_waitingForRead)
			{
			int stillNeedToRead = globalPorts->getInputBufferCount(openPortIndex);

			// Double-check to see that there is no data in the incoming buffer before sending more
			if (stillNeedToRead > 0)
				{
				poll_waitingForRead = true;
				continue;
				}

			// TODO: Check to see if there is anything outgoing (or flush it)

			int length = globalPorts->Send(openPortIndex, msg);

			if (length < msg.GetLength())
				{
				// TODO: Log error

				m_status.SetWindowTextW(globalPorts->lastErrorString);

				// Try to clear the error
				//globalPorts->


				// If the COM port is open then close it
				globalPorts->Reset(openPortIndex);

				// If it's a serious error then half the auto-reading if it's going
				m_continousSensorRead.SetCheck(0);
				continuousSet = false;
				break;
				}
			else
				{
				// Set the semaphore
				poll_waitingForRead = true;
				}
			}

		// After leaving first block, check if a read is needed. If it isn't, use an else because it can be
		// set inside the conditional block.
		if (poll_waitingForRead)
			{
			// With at least one port OPEN, start the timer if it hasn't already been done
			if (m_readPortTimer == 0)
				{
				m_readPortTimer = SetTimer(1, 100, NULL);		// 100ms timer for COM polling

				// If there is an error starting the timer then log and display it
				if (m_readPortTimer == 0)
					{
					CString errorMessage = GetLastErrorAsString();

					errorMessage.Format(_T("Error: No timer available for COM port polling\r\n--> %s"), errorMessage);

					// Add it to the log then display it on the screen
					m_status.SetWindowTextW(errorMessage);
					MessageBox(errorMessage, _T("COM Port error"), MB_ICONERROR);
					}
				}

			int a = 1;		// Poll should be false by now, from the time
			}

		// Windows message pump while we're in this loop
		while (PeekMessage(&uiMessage, NULL, 0, 0, PM_REMOVE))
			{
			TranslateMessage(&uiMessage);
			DispatchMessage(&uiMessage);
			}

		} while (continuousSet || poll_waitingForRead);

		// Kill the timer if it's running
		if (m_readPortTimer > 0)
			{
			KillTimer(m_readPortTimer);
			m_readPortTimer = 0;
			}

		poll_waitingForRead = false;

		m_readButton.EnableWindow(true);
		m_sensorsReadAll.EnableWindow(true);
		m_readStreamButton.EnableWindow(true);
	}

// Send a command that causes the Arduino to just continuously send sensor polling data until told to stop
void DynoDiagnosticsScreen::OnBnClickedDiagnosticsReadstream()
	{
	CString msg;
	MSG uiMessage;
	int stillNeedToRead = 0;

	// Check to see if it's already streaming.
	// If so, send the stop command and change the state.
	if (streamingSensorData)
		{
		msg = TOKEN_sensorPollingStream_end;
		m_readButton.EnableWindow(true);
		m_sensorsReadAll.EnableWindow(true);

		// Send the stop command
		int length = globalPorts->Send(openPortIndex, msg);

		if (length < msg.GetLength())
			{
			// TODO: Log error

			m_status.SetWindowTextW(globalPorts->lastErrorString);
			}

		// Kill the timer if it's running
		if (m_readPortTimer > 0)
			{
			KillTimer(m_readPortTimer);
			m_readPortTimer = 0;
			}

		// Stop the stream checking 
		streamingSensorData = false;

		// Take a short break after sending the stop command so that the input buffer empties
		Sleep(250);

		// Check to see if there is any data that has to be read
		while(stillNeedToRead = globalPorts->getInputBufferCount(openPortIndex))
			{
			// Create a status message
			msg.Format(_T("Input buffer size: %u"), stillNeedToRead);
			m_status.SetWindowTextW(msg);

			// Set the semaphore
			poll_waitingForRead = true;

			// With at least one port OPEN, start the timer if it hasn't already been done
			if (m_readPortTimer == 0)
				{
				m_readPortTimer = SetTimer(1, 100, NULL);		// 100ms timer for COM polling

				// If there is an error starting the timer then log and display it
				if (m_readPortTimer == 0)
					{
					CString errorMessage = GetLastErrorAsString();

					errorMessage.Format(_T("Error: No timer available for COM port polling\r\n--> %s"), errorMessage);

					// Add it to the log then display it on the screen
					m_status.SetWindowTextW(errorMessage);
					MessageBox(errorMessage, _T("COM Port error"), MB_ICONERROR);
					}
				}

			// Windows message pump while we're in this loop
			while (PeekMessage(&uiMessage, NULL, 0, 0, PM_REMOVE))
				{
				TranslateMessage(&uiMessage);
				DispatchMessage(&uiMessage);
				}
			}

		// Kill the timer once the loop is complete
		if (m_readPortTimer > 0)
			{
			KillTimer(m_readPortTimer);
			m_readPortTimer = 0;
			}

		// Clear the internals in the processing function so that there isn't remnants of a broken transmission
		(void)processReceived(nullptr, -1);


		// Change the button right before exiting
		m_readStreamButton.SetWindowTextW(_T("Read stream"));
		m_readStreamButton.EnableWindow(true);

		return;
		}

	// Set up the stream-begin command
	msg = TOKEN_sensorPollingStream_begin;

	// Turn off the buttons until the read is complete
	m_readButton.EnableWindow(false);
	m_sensorsReadAll.EnableWindow(false);
	m_readStreamButton.EnableWindow(false);

	// Change the read stream button and then re-enable it
	m_readStreamButton.SetWindowTextW(_T("Stop stream"));
	m_readStreamButton.EnableWindow(true);

	// Start by sending the command
	int length = globalPorts->Send(openPortIndex, msg);

	if (length < msg.GetLength())
		{
		// TODO: Log error

		m_status.SetWindowTextW(globalPorts->lastErrorString);
		return;
		}

	// With the streaming command set, start waiting for a read
	poll_waitingForRead = true;
	streamingSensorData = true;

	// With at least one port OPEN, start the timer if it hasn't already been done
	if (m_readPortTimer == 0)
		{
		m_readPortTimer = SetTimer(1, 100, NULL);		// 100ms timer for COM polling

		// If there is an error starting the timer then log and display it
		if (m_readPortTimer == 0)
			{
			CString errorMessage = GetLastErrorAsString();

			errorMessage.Format(_T("Error: No timer available for COM port polling\r\n--> %s"), errorMessage);

			// Add it to the log then display it on the screen
			m_status.SetWindowTextW(errorMessage);
			MessageBox(errorMessage, _T("COM Port error"), MB_ICONERROR);
			}
		}

	// The following loop is all that is required because the data is streaming and we are
	// relying on the timer to process the incoming data.
	do
		{
		// Update the status line
		msg.Format(_T("Input buffer size: %u"), stillNeedToRead);
		m_status.SetWindowTextW(msg);


		// Windows message pump while we're in this loop
		while (PeekMessage(&uiMessage, NULL, 0, 0, PM_REMOVE))
			{
			TranslateMessage(&uiMessage);
			DispatchMessage(&uiMessage);
			}

		} while (streamingSensorData);

	}


// On this interval, check the open COM port for incoming data
void DynoDiagnosticsScreen::OnTimer(UINT_PTR nIDEvent)
	{
	DWORD bytesRead, bytesWaiting;
	char buffer[100001];
	bool processOk;

	// Temporarily stop the timer
	KillTimer(m_readPortTimer);

	// Read from the open COM port
	do
		{
		memset(buffer, 0, 100000);

		// Read the data and if there isn't any the exit the loop
		if(!(bytesRead = globalPorts->Read(openPortIndex, buffer, 100000)))
			break;

		// Process the incoming data, and if it appear that the stream was cut off then
		// keep reading.
		processOk = processReceived(buffer, bytesRead);
		Sleep(50);
		bytesWaiting = globalPorts->getInputBufferCount(openPortIndex);

		} while (!processOk || bytesWaiting);

	poll_waitingForRead = false;


	// Restart the timer
	// FOR NOW: Assume the time is working and skip error check
	m_readPortTimer = SetTimer(1, 100, NULL);		// 100ms timer for COM polling

	CDialogEx::OnTimer(nIDEvent);
	}

// Process the data that was received by the COM port read.
// In this diagnostic mode, we're not using an Event system. Rather, each
// value will be chosen and updated manually in the list. It's not the
// most efficient in terms of coding, but it's accurate.
//
// Return: true if there is nothing left in the unprocessed data. This indicates
// to the calling function that data to be read is still pending.
// NOTE: This is not perfect because there could be an even break in the data,
// yet more is pending (if broken on a token). So, TODO is probably that
// the length being sent by the calling function is compared, when possible
// to what it is expecting to see.
bool DynoDiagnosticsScreen::processReceived(char *data, int length)
	{
	CString message;
	int pos;

	// Maintain a small array between function calls for unprocessed data.
	// This will be what is left over at the end of the data, but was not
	// processed because it was short and assumed to be cut off.
	// The next call to this function will append to it and continue as
	// if it had been part of the same stream.
	static char unprocessedData[100];
	static int unprocessedLength = -1;
	static int currentToken;

	// If the function is called with a NULL data pointer then it is being
	// asked to reset the internals and assume that there is no broken message.
	if (data == nullptr)
		{
		unprocessedLength = -1;
		memset(unprocessedData, 0, 100);

		return true;
		}


	m_status.SetWindowTextW(_T("Processing data..."));

	// Create a union to convert the data
	union
		{
		char raw[sizeof(uint16_t)];
		uint16_t value;
		} dataParameter;

	// Roll through the incoming data and set the values accordingly
	for (pos = 0; pos < length; pos += 1 + (int)sizeof(uint16_t))
		{
		// If there is any unprocessed data remaining from the previous run then take care of it now
		if (unprocessedLength > -1)
			{
			// Quick sanity check on the length
			if (unprocessedLength > (int)sizeof(uint16_t))
				{
				MessageBox(_T("Data processing error"), _T("Unprocessed data"));
				return false;
				}

			// Start by copying the old data into the parameter
			memcpy((void*)dataParameter.raw, unprocessedData, unprocessedLength);

			// Make sure that we have a token and a full parameter before continuing
			if (length < ((int)sizeof(uint16_t)) - unprocessedLength)
				{
				// If the data is still short of a full parameter then keep it all for the next time

				// Bring in the next part of the parameter
				memcpy((void*)(unprocessedData + unprocessedLength), data, length);
				unprocessedLength += length;

				// TODO: Log this

				break;
				}

			// Bring in the parameter
			memcpy((void*)(dataParameter.raw + unprocessedLength), data, ((int)sizeof(uint16_t)) - unprocessedLength);

			// Rewind the position pointer so that the next time through the loop, it will be set
			// at the beginning of the next token
			pos -= (unprocessedLength+1);

			// Reset the unprocessed data
			unprocessedLength = -1;
			}
		else
			{
			// Grab the next token
			currentToken = data[pos];

			// Make sure that we have a token and a full parameter before continuing
			int remaining = length - (pos + 1);

			if (remaining < (int)sizeof(uint16_t))
				{
				// TODO: Log: Not all of the data present on this pass
				unprocessedLength = remaining;

				// Keep the old data
				memcpy((void*)unprocessedData, &data[pos + 1], remaining);

				break;
				}
			else if (pos < 0)
				pos = 0;

			// Bring in the parameter
			memcpy((void*)dataParameter.raw, &data[pos + 1], sizeof(uint16_t));

			}

		// NOTE: It is probably safe to do a value sanity check here to rule out missing or
		// corrupt data in the sensor readers. However, I've opted to perform the check in the
		// switch statement so that I can log the exact parameter, provided that it can be
		// pinned down (assuming the TOKEN is not the corrupt value)

		// If there is an error detected in one of the values then we'll make the assumption
		// that the data is one or more characters short. The way to re-align this is to back
		// up the position to the first recognized token, which will be beyond the index
		// of the most recent one.

		// FOR NOW: Just try backing up a single index. If there are multi-byte omissions
		// later, then this will need to be changed.


		// Start by checking which token (command) it is
		switch (currentToken)
			{
			case 0:
				break;

			case TOKEN_tmp_gearbox_front:

				if(checkSensorValueSanity(dataParameter.value))
					currentSensorValues.gearbox_front_temp = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_tmp_gearbox_rear:

				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.gearbox_rear_temp = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_tmp_bearing_front:

				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.bearing_front_temp = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_tmp_bearing_rear:

				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.bearing_rear_temp = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_tmp_room:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.weather_temperature = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_pressure:

				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.weather_pressure = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_humidity:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.weather_humidity = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_rpm_engine:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.RPM = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_tension_brake_front:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.brake_front_tension = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_tension_brake_rear:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.brake_rear_tension = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_afr:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.AFR = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_pos_lockpin_LR:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.lockpin_LR_pos = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_pos_lockpin_LF:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.lockpin_LF_pos = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_pos_lockpin_RF:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.lockpin_RF_pos = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_pos_lockpin_RR:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.lockpin_RR_pos = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_base_position:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.base_position = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_mains_detect:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.mains_detect = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_basemotor_prox_front:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.basemotor_prox_front = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_basemotor_prox_rear:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.basemotor_prox_rear = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_brake_front_encoder:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.brake_front_encoder = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_brake_rear_encoder:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.brake_rear_encoder = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_encoder_front_main:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.encoder_front_main = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_encoder_front_offset:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.encoder_front_offset = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_encoder_rear_main:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.encoder_rear_main = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_encoder_rear_offset:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.encoder_rear_offset = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_pendant_select:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.pendant_select = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_pendant_exec:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.pendant_exec = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_pendant_estop:
				
				if (checkSensorValueSanity(dataParameter.value))
					currentSensorValues.pendant_estop = dataParameter.value;
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			case TOKEN_current_state:

				if ((dataParameter.value >= DYNO_STATE_INIT) && (dataParameter.value <= DYNO_STATE_POWER_FAIL))
					*lastKnownDynoState = (char)(dataParameter.value);
				else
					{
					// TODO: Log error

					pos--;		// Loop will assume next data starts one byte earlier
					}

				break;

			default:		// Command not found

				// TODO: Log
				message.Format(_T("Processing data received. Unexpected token: %c (%d)"), data[pos], (int)data[pos]);
				m_status.SetWindowTextW(message);

				// If this happens, then it's possible that a single byte is missing from the stream.
				// The best thing to do is to adjust the pointer and re-align the loop. Using this approach,
				// only one data point is missed.
				
				// The question is which way to look; going backward, it would mean that the previous
				// value is incorrect. ASSUME FOR NOW that we can go forward to the next acceptable
				// token. For simplicity, just adjust the counter so that the loop moves onto
				// the very next byte, and checks itself.
				pos -= (int)sizeof(uint16_t);

//				MessageBox(message, _T("Data error"), MB_ICONEXCLAMATION);
			}
		}


	// Update the values on the screen
	updateSensorValues();

	m_status.SetWindowTextW(_T(""));

	return(unprocessedLength == -1);
	}

// This is a simple check to see if the sensor value is reasonable.
// It can optionally be specified, which allows for specific ranges to be
// checked for sensors with known ranges. The default is to look for anything
// that is somewhat sane.
bool DynoDiagnosticsScreen::checkSensorValueSanity(uint16_t value, int token)
	{
	// FOR NOW: Just looking for anything reasonable

	return (value == SENSOR_VALUE_UNKNOWN) || ((value >= 0) && (value < 1024));
	}


void DynoDiagnosticsScreen::OnDestroy()
	{
	CDialogEx::OnDestroy();

	// Terminate the polling loop if it's running
	continuousSet = false;
	streamingSensorData = false;

	// Kill the timer if it's running
	if (m_readPortTimer > 0)
		KillTimer(m_readPortTimer);
	}


void DynoDiagnosticsScreen::OnLvnItemchangedDiagnosticsProcessorList(NMHDR* pNMHDR, LRESULT* pResult)
	{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	// Set the button states
	updateControls();

	*pResult = 0;
	}

void DynoDiagnosticsScreen::OnLvnItemchangedDiagnosticsSensorList(NMHDR* pNMHDR, LRESULT* pResult)
	{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	// Set the button states
	updateControls();
	
	*pResult = 0;
	}

void DynoDiagnosticsScreen::OnLvnItemchangedDiagnosticsMotorList(NMHDR* pNMHDR, LRESULT* pResult)
	{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	// Set the button states
	updateControls();

	*pResult = 0;
	}

void DynoDiagnosticsScreen::OnLvnItemchangedDiagnosticsOtherDeviceList(NMHDR* pNMHDR, LRESULT* pResult)
	{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	
	// Set the button states
	updateControls();


	*pResult = 0;
	}


// Clicking the Low button will cause a signal to be sent to the corresponding motor
void DynoDiagnosticsScreen::OnBnClickedDiagnosticsSetLowMotorsButton()
	{
	POSITION curRecord = m_motorList.GetFirstSelectedItemPosition();

	// Allocated extra buffer space to rule out possible pointer issues from COM bug
	char sendData[100];
	memset(sendData, 0, 100);

	if (curRecord)
		{
		int nItem = m_motorList.GetNextItem(-1, LVNI_SELECTED);

		if (nItem > -1)
			{
			char* valuePtr;

			valuePtr = (char*)m_motorList.GetItemData(nItem);

			// Send the LOW command to the selected motor
			sendData[0] = valuePtr[1];
			sendData[1] = valuePtr[2];
			sendData[2] = TOKEN_CMD_signal_LOW;
			sendData[3] = 0;
			sendData[4] = 0;

			int length = globalPorts->Send(openPortIndex, sendData, 5);

			if (length < 5)
				{
				// TODO: Log error

				m_status.SetWindowTextW(globalPorts->lastErrorString);
				}
			else
				{
				// Set it to low
				valuePtr[0] = TOKEN_CMD_signal_LOW;
				}
			}
		}

	// Update value column
	updateMotorValues();
	}

void DynoDiagnosticsScreen::OnBnClickedDiagnosticsSetHighMotorsButton()
	{
	POSITION curRecord = m_motorList.GetFirstSelectedItemPosition();

	if (curRecord)
		{
		int nItem = m_motorList.GetNextItem(-1, LVNI_SELECTED);

		if (nItem > -1)
			{
			char* valuePtr;
			char sendData[5];

			valuePtr = (char*)m_motorList.GetItemData(nItem);

			// Send the HIGH command to the selected motor

			sendData[0] = valuePtr[1];
			sendData[1] = valuePtr[2];
			sendData[2] = TOKEN_CMD_signal_HIGH;
			sendData[3] = 0;
			sendData[4] = 0;

			int length = globalPorts->Send(openPortIndex, sendData, 5);

			if (length < 5)
				{
				// TODO: Log error

				m_status.SetWindowTextW(globalPorts->lastErrorString);
				}
			else
				{
				// Set it to high
				valuePtr[0] = TOKEN_CMD_signal_HIGH;
				}
			}
		}

	// Update value column
	updateMotorValues();
	}

void DynoDiagnosticsScreen::OnBnClickedDiagnosticsOtherPwmButton()
	{
	// TODO: Add your control notification handler code here
	}

void DynoDiagnosticsScreen::OnBnClickedDiagnosticsSetLowOtherButton()
	{
	POSITION curRecord = m_otherDevice_list.GetFirstSelectedItemPosition();

	if (curRecord)
		{
		int nItem = m_otherDevice_list.GetNextItem(-1, LVNI_SELECTED);

		if (nItem > -1)
			{
			char* valuePtr;
			char sendData[5];

			valuePtr = (char*)m_otherDevice_list.GetItemData(nItem);

			// Send the LOW command to the selected device
			sendData[0] = valuePtr[1];
			sendData[1] = TOKEN_CMD_signal_LOW;
			sendData[2] = 0;
			sendData[3] = 0;
			sendData[4] = 0;

			int length = globalPorts->Send(openPortIndex, sendData, 5);

			if (length < 5)
				{
				// TODO: Log error

				m_status.SetWindowTextW(globalPorts->lastErrorString);
				}
			else
				{
				// Set it to low
				valuePtr[0] = TOKEN_CMD_signal_LOW;
				}
			}
		}

	// Update value column
	updateOtherDeviceValues();
	}

void DynoDiagnosticsScreen::OnBnClickedDiagnosticsSetHighOtherButton()
	{
	POSITION curRecord = m_otherDevice_list.GetFirstSelectedItemPosition();

	if (curRecord)
		{
		int nItem = m_otherDevice_list.GetNextItem(-1, LVNI_SELECTED);

		if (nItem > -1)
			{
			char* valuePtr;
			char sendData[5];

			valuePtr = (char*)m_otherDevice_list.GetItemData(nItem);

			// Send the HIGH command to the selected motor

			sendData[0] = valuePtr[1];
			sendData[1] = TOKEN_CMD_signal_HIGH;
			sendData[2] = 0;
			sendData[3] = 0;
			sendData[4] = 0;

			int length = globalPorts->Send(openPortIndex, sendData, 5);

			if (length < 5)
				{
				// TODO: Log error

				m_status.SetWindowTextW(globalPorts->lastErrorString);
				}
			else
				{
				// Set it to high
				valuePtr[0] = TOKEN_CMD_signal_HIGH;
				}
			}
		}

	// Update value column
	updateOtherDeviceValues();
	}


void DynoDiagnosticsScreen::OnLvnItemchangedDiagnosticsProcedureTesting(NMHDR* pNMHDR, LRESULT* pResult)
	{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
	}

// Test Procedure button was pressed
void DynoDiagnosticsScreen::OnBnClickedDiagnosticsTestProcedureButton()
	{
	POSITION curRecord = m_procedureTestingList.GetFirstSelectedItemPosition();

	if (curRecord)
		{
		int nItem = m_procedureTestingList.GetNextItem(-1, LVNI_SELECTED);

		if (nItem > -1)
			{
			char* valuePtr;
			char sendData[5];

			valuePtr = (char*)m_procedureTestingList.GetItemData(nItem);

			// Build the commands for the selected sequence
			sendData[0] = valuePtr[1];
			sendData[1] = valuePtr[2];
			sendData[2] = 0;
			sendData[3] = 0;
			sendData[4] = 0;

			int length = globalPorts->Send(openPortIndex, sendData, 5);

			if (length < 5)
				{
				// TODO: Log error

				m_status.SetWindowTextW(globalPorts->lastErrorString);
				}
			else
				{
				// Set it to low
				valuePtr[0] = RESULT_executeProc_run;
				}
			}
		}

	// Update value column
	updateProcedureTestStatusValues();
	}


void DynoDiagnosticsScreen::OnBnClickedDiagnosticsContinuousSensorCheck()
	{
	continuousSet = m_continousSensorRead.GetCheck() == BST_CHECKED;
	}

float DynoDiagnosticsScreen::convertAnalogToTemperature(unsigned int analogReadValue)
	{
/*
	float R1 = 10000;
	float logR2, R2, T, Tc, Tf;
	float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

	// If analogReadValue is 1023, we would otherwise cause a Divide-By-Zero,
	// Treat as crazy out-of-range temperature.
	if (analogReadValue == 1023) return 1000.0;


	R2 = R1 * (1023.0 / (float)analogReadValue - 1.0);
	logR2 = log(R2);
	T = (1.0 / (c1 + c2 * logR2 + c3 * logR2 * logR2 * logR2));
	Tc = T - 273.15;
	Tf = (Tc * 9.0) / 5.0 + 32.0;

	return Tc;
*/

//	return (1.0 / ((log(((10000.0 * analogReadValue) / (1023.0 - analogReadValue)) / 10000.0) / 3950.0) + (1 / (273.15 + 25.000)))) - 273.15;

	float average = (float)analogReadValue;
	float seriesResistor = 10000;
	float thermistorNominal = 25;
	float bCoefficient = 3950;

	  // convert the value to resistance
	average = 1023.0 / average - 1.0;
	average = seriesResistor / average;
	

	float steinhart;
	steinhart = average / thermistorNominal;     // (R/Ro)
	steinhart = log(steinhart);                  // ln(R/Ro)
	steinhart /= bCoefficient;                   // 1/B * ln(R/Ro)
	steinhart += 1.0 / (thermistorNominal + 273.15); // + (1/To)
	steinhart = 1.0 / steinhart;                 // Invert
	steinhart -= 273.15;                         // convert absolute temp to C


	return(steinhart);
	}


// Read from the buffer, displaying the data, until it stops.
// Alternatively, the read can be manually stopped by pressing the button again
void DynoDiagnosticsScreen::OnBnClickedDiagnosticsDraininputButton()
	{
	CString statusMessage;
	MSG uiMessage;
	DWORD bytesRead = 0, bytesWaiting = 0;
	unsigned long totalRead = 0;
	char buffer[100001];
	static bool readingBuffer = false;

	// If the buffer is already being read then stop it
	if (readingBuffer)
		{
		// Stop the reading 
		readingBuffer = false;

		return;
		}

	// Start it up
	m_drainInputButton.SetWindowTextW(_T("Stop draining"));
	m_status.SetWindowTextW(_T("Starting drain..."));

	// Read from the open COM port
	do
		{
		memset(buffer, 0, 100000);

		// Read the data and if there isn't any the exit the loop
		if (!(bytesRead = globalPorts->Read(openPortIndex, buffer, 100000)))
			break;

		// Keep track of how many bytes were read in total
		totalRead += bytesRead;

		// Windows message pump while we're in this loop
		while (PeekMessage(&uiMessage, NULL, 0, 0, PM_REMOVE))
			{
			TranslateMessage(&uiMessage);
			DispatchMessage(&uiMessage);
			}

		// FOR NOW: Ignore the incoming data. Perhaps print it in the future, if helpful.
		bytesWaiting = globalPorts->getInputBufferCount(openPortIndex);

		statusMessage.Format(_T("Draining buffer: %u read now, %u total, %u waiting"), bytesRead, totalRead, bytesWaiting);
		m_status.SetWindowTextW(statusMessage);

		// If the stop button has been pressed then exit
		if (!readingBuffer)
			break;

		} while (bytesWaiting);

	statusMessage.Format(_T("Drain complete: %u last read, %u total, %u waiting"), bytesRead, totalRead, bytesWaiting);
	m_status.SetWindowTextW(statusMessage);

	m_drainInputButton.SetWindowTextW(_T("Drain input"));
	}

// Flush the input/output buffers on the COM port
void DynoDiagnosticsScreen::OnBnClickedDiagnosticsFlushioButton()
	{
	CString message;

	m_status.SetWindowTextW(_T("Flushing buffers..."));

	COMMPROP properties_start, properties_end;

	// Call the clear operation
	globalPorts->ClearBuffers(openPortIndex, properties_start, properties_end);
	m_status.SetWindowTextW(_T("Flush operation complete."));

	// Build the output message
	message.Format(_T("Packet length: %d / %d\n"
		"Packet version:  %d / %d\n"
		"Service mask: %u / %u\n"
		"Max Tx queue: %u / %u\n"
		"Max Rx queue: %u / %u\n"
		"Max baud: %u / %u\n"
		"Prov sub-type: %u / %u\n"
		"Prov capabilities: %u / %u\n"
		"Settable parameters: %u / %u\n"
		"Settable baud: %u / %u\n"
		"Settable data: %d / %d\n"
		"Settable stop parity: %d / %d\n"
		"Current Tx queue: %u / %u\n"
		"Current Rx queue: %u / %u\n"
		"Prov spec 1: %u / %u\n"
		"Prov spec 2: %u / %u\n"	
		), properties_start.wPacketLength, properties_end.wPacketLength,
			properties_start.wPacketVersion, properties_end.wPacketVersion,
			properties_start.dwServiceMask, properties_end.dwServiceMask,
			properties_start.dwMaxTxQueue, properties_end.dwMaxTxQueue,
			properties_start.dwMaxRxQueue, properties_end.dwMaxRxQueue,
			properties_start.dwMaxBaud, properties_end.dwMaxBaud,
			properties_start.dwProvSubType, properties_end.dwProvSubType,
			properties_start.dwProvCapabilities, properties_end.dwProvCapabilities,
			properties_start.dwSettableParams, properties_end.dwSettableParams,
			properties_start.dwSettableBaud, properties_end.dwSettableBaud,
			properties_start.wSettableData, properties_end.wSettableData,
			properties_start.wSettableStopParity, properties_end.wSettableStopParity,
			properties_start.dwCurrentTxQueue, properties_end.dwCurrentTxQueue,
			properties_start.dwCurrentRxQueue, properties_end.dwCurrentRxQueue,
			properties_start.dwProvSpec1, properties_end.dwProvSpec1,
			properties_start.dwProvSpec2, properties_end.dwProvSpec2);

	MessageBox(message, _T("COM Properties"), MB_ICONINFORMATION);

	m_status.SetWindowTextW(_T("Flush operation complete."));
	}



// Reset the COM port completely
void DynoDiagnosticsScreen::OnBnClickedDiagnosticsResetPortButton()
	{


	}

// Ask the Arduino for the current dyno state
void DynoDiagnosticsScreen::OnBnClickedDiagnosticsGetStateButton()
	{
	CString msg(TOKEN_getDynoState);
	POSITION curRecord = m_processorList.GetFirstSelectedItemPosition();
	MSG uiMessage;

	// Turn off the button until the state has been retrieved
	m_getStateButton.EnableWindow(false);

	// Make sure that a processor is selected
	if (!curRecord)
		return;

	// Point to the processor in the array
	int nItem = m_processorList.GetNextItem(-1, LVNI_SELECTED);

	if (nItem < 0)
		return;

	// Keep a pointer to the item's data
	char* valuePtr = (char*)m_processorList.GetItemData(nItem);

	// Query the processor for the current state
	int length = globalPorts->Send(openPortIndex, msg);

	if (length < msg.GetLength())
		{
		// TODO: Log error

		m_status.SetWindowTextW(globalPorts->lastErrorString);

		// Try to clear the error
		//globalPorts->

		// If the COM port is open then close it
		globalPorts->Reset(openPortIndex);
		}
	else
		{
		// Set the semaphore
		poll_waitingForRead = true;

		// Set it to "running"
		*lastKnownDynoState = (char)DYNO_STATE_UNKNOWN;

		// Update value column
		updateProcessorValues();
		}


	do {
		// After leaving first block, check if a read is needed. If it isn't, use an else because it can be
		// set inside the conditional block.
		if (poll_waitingForRead)
			{
			// With at least one port OPEN, start the timer if it hasn't already been done
			if (m_readPortTimer == 0)
				{
				m_readPortTimer = SetTimer(1, 100, NULL);		// 100ms timer for COM polling

				// If there is an error starting the timer then log and display it
				if (m_readPortTimer == 0)
					{
					CString errorMessage = GetLastErrorAsString();

					errorMessage.Format(_T("Error: No timer available for COM port polling\r\n--> %s"), errorMessage);

					// Add it to the log then display it on the screen
					m_status.SetWindowTextW(errorMessage);
					MessageBox(errorMessage, _T("COM Port error"), MB_ICONERROR);
					}
				}

			int a = 1;		// Poll should be false by now, from the time
			}

		// Windows message pump while we're in this loop
		while (PeekMessage(&uiMessage, NULL, 0, 0, PM_REMOVE))
			{
			TranslateMessage(&uiMessage);
			DispatchMessage(&uiMessage);
			}

		} while (poll_waitingForRead);

	// Kill the timer if it's running
	if (m_readPortTimer > 0)
		{
		KillTimer(m_readPortTimer);
		m_readPortTimer = 0;
		}

	poll_waitingForRead = false;

	// Update value column
	updateProcessorValues();

	m_getStateButton.EnableWindow(true);
	}
