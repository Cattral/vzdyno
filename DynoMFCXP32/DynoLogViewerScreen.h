#pragma once


// DynoLogViewerScreen dialog

class DynoLogViewerScreen : public CDialogEx
{
	DECLARE_DYNAMIC(DynoLogViewerScreen)

public:
	DynoLogViewerScreen(CWnd* pParent = nullptr);   // standard constructor
	virtual ~DynoLogViewerScreen();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LOG_VIEWER };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_list;
	};
