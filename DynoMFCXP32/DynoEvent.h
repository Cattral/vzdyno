#pragma once
#include <afx.h>

// This class stores information specific to a single Dynanometer event
class DynoEvent : public CObject
	{
	public:

		enum EventType {
			NotSet,						// No Event set
			Timestamp,
			Speed,
			RPM_drum, RPM_engine,		// RPM Events
			Torque,
			Horsepower,
			Voltage,
			Sound,
			Vibration,
			Temperature,
			Humidity,
			Pressure,
			CO
			};

		// Constructors

		DynoEvent();
		DynoEvent(int type, double parameter, int unitType, SYSTEMTIME time, double timeDeltaMs, DynoEvent *previousRPM_drum = nullptr);

		~DynoEvent();

		DynoEvent& operator=(const DynoEvent& cfg);

	protected:

		// The sysTime variable is an internally recorded timestamp that cannot be modified
		SYSTEMTIME sysTime;

	public:

		// To facilitate smoothing, internally each RPM_drum Event keeps
		// a pointer to the next one and the previous one. This allows quick
		// reference to prior and subsequent values. The only one that is expected
		// to be modified is the current one, so we don't care about the POSITION
		// within the larger CList container.
		DynoEvent* previousRPM_drum, * nextRPM_drum;
		int RPM_drum_index;

		SYSTEMTIME timestamp;				// Time the Event was created
		bool finalSmoothed;					// Indicates it is finalized
		bool graphable;						// Indicates it is ready to be graphed

		int type;							// The type of Event
		int units;							// Measurement system
		double absTime;						// Absolute time of Event (sum of time deltas to this point)
		double originalAcceleration;
		double smoothedAcceleration;

		double originalParameter;
		double smoothedParameter_sma;
		double smoothedParameter_ema;

		double originalTimeDelta;
		double smoothedTimeDelta_sma;
		double smoothedTimeDelta_ema;

		// Functions
		CString GetString();
		CString UnitString();
		
		// Set the Event links
		void setLinks(DynoEvent* prevEvent = nullptr, DynoEvent* nextEvent = nullptr)
			{
			previousRPM_drum = prevEvent;
			nextRPM_drum = nextEvent;
			}

		// Return the creation time
		SYSTEMTIME creationTime()
			{
			return sysTime; 
			}

		// Serialization
		void Serialize(CArchive& ar);
		DECLARE_SERIAL(DynoEvent);


	};

