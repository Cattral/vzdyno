// CMainForm.cpp : implementation file
//

//Hasan started working on this on 21st Feb 2021


#include "pch.h"

#include "DynoMFCXP32.h"
#include "VZDynoDefines.h"

#include "CMainForm.h"

#include "DynoComPortDialog.h"
#include "ConnectionSettings.h"
#include "CCustomizeUIDialog.h"
#include "CModelessReplayDialog.h"
#include "CFileDetailsDialog.h"
#include "DynoMasterConfigurationScreen.h"
#include "VehicleLookupScreen.h"
#include "DynoDiagnosticsScreen.h"
#include "DynoLogViewerScreen.h"


#include "interpolation.h"

using namespace alglib;


// CMainForm

IMPLEMENT_DYNCREATE(CMainForm, CFormView)

CMainForm::CMainForm()
	: CFormView(IDD_MAINFORMVIEW)
{

	// Member initialization
	m_pollTimer = -1;

}

CMainForm::~CMainForm()
{
}

void CMainForm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATICMETER, m_3DMeterCtrl);
	DDX_Control(pDX, IDC_VZ_BIGNAME, m_bigname);
	DDX_Control(pDX, IDC_TACHOMETER, m_tachometer);
	DDX_Control(pDX, IDC_SPEEDOMETER, m_speedometerCtrl);
	DDX_Control(pDX, IDC_TEXT_HORSEPOWER, m_leftOfTopChart);
	DDX_Control(pDX, IDC_TEXT_AFR, m_leftOfLowerChart);
	DDX_Control(pDX, IDC_TEXT_TORQUE, m_rightOfTopChart);
	DDX_Control(pDX, IDC_TEXT_RPM, m_bottomOfUpperChart);
	DDX_Control(pDX, IDC_RICH_GRAPH, m_richGraph);
	DDX_Control(pDX, IDC_LEAN_GRAPH, m_leanGraph);
	DDX_Control(pDX, IDC_SMALL_LEANRICH, m_smallLeanRich);
	DDX_Control(pDX, IDC_RICH_LEAN_BORDER, m_richLeanBorder);
	DDX_Control(pDX, IDC_RICH_LEAD_DIVIDER, m_richLeanDivider);
	DDX_Control(pDX, IDC_CHART_HP_TO_TORQUE, m_mainChart);
	DDX_Control(pDX, IDC_CHART_AFR_TO_RPM, m_afrChart);
	DDX_Control(pDX, IDC_STATIC_HP, m_hpLabel);
	DDX_Control(pDX, IDC_STATIC_TQ, m_tqLabel);
	DDX_Control(pDX, IDC_STATIC_AFR, m_afrLabel);
	DDX_Control(pDX, IDC_STATIC_TQAREA, m_tqareaLabel);
	DDX_Control(pDX, IDC_STATIC_ACCEL, m_accelLabel);

	DDX_Control(pDX, IDC_STATIC_HP_VAL, m_hpLabel_value);

	DDX_Control(pDX, IDC_STATIC_TQ_VAL, m_tqLabel_value);
	DDX_Control(pDX, IDC_STATIC_AFR_VAL, m_afrLabel_value);
	DDX_Control(pDX, IDC_STATIC_TQAREA_VAL, m_tqareaLabel_value);
	DDX_Control(pDX, IDC_STATIC_ACCEL_VAL, m_accelLabel_value);

	DDX_Control(pDX, IDC_STATIC_RIGHT_HORIZONTAL_DIVIDER, m_RightHorizonatalDivider);
	DDX_Control(pDX, IDC_STATIC_RIGHT_VERTICAL_DIVIDER, m_RightVerticalDivider );
	}

BEGIN_MESSAGE_MAP(CMainForm, CFormView)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_WM_CTLCOLOR()
	ON_WM_CREATE()
	ON_COMMAND(ID_CONNECT_SETTINGS, &CMainForm::OnConnectSettings)
	ON_COMMAND(ID_CONNECT_TESTCOMPORTS, &CMainForm::OnConnectTestcomports)
	ON_COMMAND(ID_CONNECT_AUTOCONNECT, &CMainForm::OnConnectAutoconnect)
	ON_COMMAND(ID_AUTHORIZATION_AUTOAUTHORIZE, &CMainForm::OnAuthorizationAutoauthorize)
	ON_WM_RBUTTONDBLCLK()
	ON_COMMAND(ID_CONNECT_IDENTIFYPORTS, &CMainForm::OnConnectIdentifyports)
	ON_COMMAND(ID_AUTHORIZATION_MANUALAUTHORIZE, &CMainForm::OnAuthorizationManualauthorize)
	ON_COMMAND(ID_AUTHORIZATION_CONTACTSUPPORT, &CMainForm::OnAuthorizationContactsupport)
	ON_COMMAND(ID_AUTHORIZATION_LIVECHAT, &CMainForm::OnAuthorizationLivechat)
	ON_COMMAND(ID_AUTHORIZATION_SETTINGS, &CMainForm::OnAuthorizationSettings)
	ON_COMMAND(ID_CONNECT_PAUSEALLPOLLING, &CMainForm::OnConnectPauseAllPolling)
	ON_UPDATE_COMMAND_UI(ID_CONNECT_PAUSEALLPOLLING, &CMainForm::OnUpdateConnectPauseallpolling)
	ON_COMMAND_RANGE(WM_USER+1, WM_USER+25, &CMainForm::OnMenuClickRange)
	ON_WM_MDIACTIVATE()
	ON_STN_DBLCLK(IDC_SPEEDOMETER, &CMainForm::OnStnDblclickSpeedometer)
	ON_STN_DBLCLK(IDC_TACHOMETER, &CMainForm::OnStnDblclickTachometer)
	ON_COMMAND(ID_VIEW_METERVALUEBORDERS, &CMainForm::OnViewMetervalueborders)
	ON_UPDATE_COMMAND_UI(ID_VIEW_METERVALUEBORDERS, &CMainForm::OnUpdateViewMetervalueborders)
	ON_COMMAND(ID_VIEW_CUSTOMIZEINTERFACE, &CMainForm::OnViewCustomizeinterface)
	ON_COMMAND(ID_FILE_OPEN, &CMainForm::OnFileOpen)
	ON_COMMAND(ID_FILE_CLOSE, &CMainForm::OnFileClose)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, &CMainForm::OnUpdateFileOpen)
	ON_UPDATE_COMMAND_UI(ID_FILE_CLOSE, &CMainForm::OnUpdateFileClose)
	ON_UPDATE_COMMAND_UI(ID_AUTHORIZATION_AUTOAUTHORIZE, &CMainForm::OnUpdateAuthorizationAutoauthorize)
	ON_UPDATE_COMMAND_UI(ID_CONNECT_AUTOCONNECT, &CMainForm::OnUpdateConnectAutoconnect)
	ON_COMMAND(ID_FILE_REPLAY, &CMainForm::OnFileReplay)
	ON_UPDATE_COMMAND_UI(ID_FILE_REPLAY, &CMainForm::OnUpdateFileReplay)
	ON_COMMAND(ID_FILE_EXAMINE_SESSION, &CMainForm::OnFileExamineSession)
	ON_UPDATE_COMMAND_UI(ID_FILE_EXAMINE_SESSION, &CMainForm::OnUpdateFileExamineSession)
	ON_BN_CLICKED(IDC_REPLAY_CONTROL_PLAY_BUTTON, &CMainForm::OnBnClickedReplayControlPlayButton)
	ON_BN_CLICKED(IDC_REPLAY_CONTROL_CLOSE_BUTTON, &CMainForm::OnBnClickedReplayControlCloseButton)
	ON_COMMAND(ID_VIEW_RESET, &CMainForm::OnViewReset)
	ON_UPDATE_COMMAND_UI(ID_VIEW_CUSTOMIZEINTERFACE, &CMainForm::OnUpdateViewCustomizeinterface)
	ON_COMMAND(ID_FILE_SAVE, &CMainForm::OnFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, &CMainForm::OnUpdateFileSave)
	ON_COMMAND(ID_FILE_SAVE_AS, &CMainForm::OnFileSaveAs)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, &CMainForm::OnUpdateFileSaveAs)
	ON_COMMAND(ID_FILE_IMPORT, &CMainForm::OnFileImport)
	ON_UPDATE_COMMAND_UI(ID_FILE_IMPORT, &CMainForm::OnUpdateFileImport)
	ON_COMMAND(ID_FILE_EXPORT, &CMainForm::OnFileExport)
	ON_UPDATE_COMMAND_UI(ID_FILE_EXPORT, &CMainForm::OnUpdateFileExport)
	ON_COMMAND(ID_GRAPH_TORQUEANDHORSEPOWER, &CMainForm::OnGraphTorqueandhorsepower)
	ON_UPDATE_COMMAND_UI(ID_GRAPH_TORQUEANDHORSEPOWER, &CMainForm::OnUpdateGraphTorqueandhorsepower)
	ON_COMMAND(ID_GRAPH_RPMANDSPEED, &CMainForm::OnGraphRpmandspeed)
	ON_UPDATE_COMMAND_UI(ID_GRAPH_RPMANDSPEED, &CMainForm::OnUpdateGraphRpmandspeed)
	ON_COMMAND(ID_VIEW_AUTOSCROLLGRAPH, &CMainForm::OnViewAutoscrollgraph)
	ON_UPDATE_COMMAND_UI(ID_VIEW_AUTOSCROLLGRAPH, &CMainForm::OnUpdateViewAutoscrollgraph)
	ON_COMMAND(ID_AUTHORIZATION_ADMINISTRATIVEOPTIONS, &CMainForm::OnAuthorizationAdministrativeoptions)
	ON_COMMAND(ID_OPTIONS_MASTER_CONFIG, &CMainForm::OnOptionsMasterConfig)
	ON_COMMAND(ID_LEGEND_TOP, &CMainForm::OnLegendTop)
	ON_UPDATE_COMMAND_UI(ID_LEGEND_TOP, &CMainForm::OnUpdateLegendTop)
	ON_COMMAND(ID_LEGEND_BOTTOM, &CMainForm::OnLegendBottom)
	ON_UPDATE_COMMAND_UI(ID_LEGEND_BOTTOM, &CMainForm::OnUpdateLegendBottom)
	ON_COMMAND(ID_LEGEND_NONE, &CMainForm::OnLegendNone)
	ON_UPDATE_COMMAND_UI(ID_LEGEND_NONE, &CMainForm::OnUpdateLegendNone)
	ON_COMMAND(ID_VEHICLE_LOOKUP, &CMainForm::OnVehicleLookup)
	ON_COMMAND(ID_CONNECT_DIAGNOSTICS, &CMainForm::OnConnectDiagnostics)
	ON_COMMAND(ID_VIEW_JUMPTOORIGIN, &CMainForm::OnViewJumptoorigin)
	ON_COMMAND(ID_HELP_LOGVIEWER, &CMainForm::OnHelpLogviewer)
	ON_COMMAND(ID_CONNECT_STARTPOLLING, &CMainForm::OnConnectStartpolling)
	ON_UPDATE_COMMAND_UI(ID_CONNECT_STARTPOLLING, &CMainForm::OnUpdateConnectStartpolling)
	ON_UPDATE_COMMAND_UI(ID_CONNECT_DIAGNOSTICS, &CMainForm::OnUpdateConnectDiagnostics)
	ON_COMMAND(ID_VIEW_SHOWVOLTMETER, &CMainForm::OnViewShowvoltmeter)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHOWVOLTMETER, &CMainForm::OnUpdateViewShowvoltmeter)
END_MESSAGE_MAP()


// CMainForm diagnostics

#ifdef _DEBUG
void CMainForm::AssertValid() const
	{
	CFormView::AssertValid();
	}

#ifndef _WIN32_WCE
void CMainForm::Dump(CDumpContext& dc) const
	{
	CFormView::Dump(dc);
	}
#endif
#endif //_DEBUG


// CMainForm message handlers

COLORREF CMainForm::getMappedColour(int mapIndex)
	{
	COLORREF colour;

	// Use the value in the map
	if (!currentFormConfig->colourRefsMap.Lookup(mapIndex, colour))
		colour = RGB(0, 0, 0);	// Return black if it fails

	// Return the colour that was pulled from the map
	return(colour);
	}


// For controlling colour, substitute our own brush
HBRUSH CMainForm::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
	{
	//HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO: If the background needs to be changed then we'll take care of it here

	/*
	// Depending on what is being drawn, return the appropriate brush
	switch (nCtlColor)
		{
			case CTLCOLOR_STATIC:

				CWnd* what = pWnd;
				pDC->SetDCBrushColor(RGB(0, 0, 255));
				return (HBRUSH)GetStockObject(WHITE_BRUSH);R

		}
	*/
	
	// Return our own brush
	return m_brush;
	}

// Update the window title
void CMainForm::updateWindowTitle()
	{
	CDocument* currentDoc = GetDocument();
	CString newTitle;

	if (openPortIndex == -1)
		{
		newTitle = Constants.String_not_connected.c_str();
		}
	else
		{
		// Update based on whether polling is active
		if(m_pollTimer > 0)
			newTitle = Constants.String_pollingActive.c_str();
		else
			newTitle = Constants.String_pollingPaused.c_str();
		}

	// if there is a file open with data for replay then include the filename
	if (streamingInputFile)
		{
		newTitle += _T(" <Replay file: ");
		newTitle += loadedFileDetails.fileTitle;
		newTitle += _T(">");
		}

	// If the data is being recorded then include the filename
	if (recording)
		{
		newTitle += _T(" <Recording file: ");
		newTitle += loadedFileDetails.fileTitle;
		newTitle += _T(">");
		}

	currentDoc->SetTitle(newTitle);
	}

void CMainForm::OnInitialUpdate()
	{
	// Meter test code
	CFormView::OnInitialUpdate();

	// Maintain a pointer to the DynoApp (parent)
	DynoApp = static_cast<CDynoMFCXP32App*>(AfxGetApp());
	currentFormConfig = &DynoApp->CurrentDynoConfiguration;		// Point to the parent app configuration

	m_remoteControl = new CModelessReplayDialog(this);
	m_remoteControl->Create(IDD_MODELESS_REPLAY_DIALOG);

	// Create a brush for the background colour and use the value in the map
	m_brush.CreateSolidBrush(getMappedColour(Constants.colour_target_Background));

		// Initialize the colour map
	colourNamesMap.SetAt(Constants.colour_target_Background, CString(Constants.String_Background.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_Labels, CString(Constants.String_Labels.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_Graph_names, CString(Constants.String_Graph_names.c_str()));

	colourNamesMap.SetAt(Constants.colour_target_Meter_FaceBorder, CString(Constants.String_Meter_FaceBorder.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_Meter_Tickmark, CString(Constants.String_Meter_Tickmark.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_Meter_valueText, CString(Constants.String_Meter_valueText.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_Meter_valueBorderHighlite, CString(Constants.String_Meter_valueHighlight.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_Meter_valueBorderShadow, CString(Constants.String_Meter_valueBorderShadow.c_str()));

	colourNamesMap.SetAt(Constants.colour_target_SpdNeedleNormal, CString(Constants.String_SpdNeedleNormal.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_SpdNeedleHigh, CString(Constants.String_SpdNeedleHigh.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_TachNeedleNormal, CString(Constants.String_TachNeedleNormal.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_TachNeedleHigh, CString(Constants.String_TachNeedleHigh.c_str()));

	colourNamesMap.SetAt(Constants.colour_target_RichLeanFill, CString(Constants.String_RichLeanFill.c_str()));

	colourNamesMap.SetAt(Constants.colour_target_htrChart_border, CString(Constants.String_THPChart_border.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_htrChart_background, CString(Constants.String_THPChart_background.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_htrChart_grid, CString(Constants.String_THPChart_grid.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_htrChart_torqueAxisLabels, CString(Constants.String_THPChart_torqueLabels.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_htrChart_hpAxisLabels, CString(Constants.String_THPChart_hpLabels.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_htrChart_rpmAxisLabels, CString(Constants.String_THPChart_rpmLabels.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_htrChart_torqueLine, CString(Constants.String_THPChart_torqueLine.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_htrChart_hpLine, CString(Constants.String_THPChart_hpLine.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_htrChart_rpmLine, CString(Constants.String_THPChart_rpmLine.c_str()));
	colourNamesMap.SetAt(Constants.colour_target_htrChart_speedLine, CString(Constants.String_THPChart_speedLine.c_str()));


	updateWindowTitle();

	// Set up the text and the font for the title (Company name)
	m_bigname.SubclassDlgItem(IDC_STATIC, this);
	m_bigname.SetWindowTextW(Constants.String_companyName.c_str());
	m_bigname.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));

	// The colour for the company name is fixed
	m_bigname.SetTextColor(RGB(255, 0, 0));


	// Load the font that is used for the title (Company name)
	// TODO: This will probably move to the application level so that it doesn't open
	// once for each of the MainForm windows
	HINSTANCE hResInstance = (HINSTANCE)GetModuleHandle(NULL);
	HRSRC res = FindResource(NULL, MAKEINTRESOURCE(IDR_TTF_RETRRG), RT_FONT);

	if (res)
		{
		HGLOBAL mem = LoadResource(hResInstance, res);
		void* data = LockResource(mem);
		size_t len = SizeofResource(hResInstance, res);

		DWORD nFonts;
		titleFontHandle = AddFontMemResourceEx(
			data,		// font resource
			len,		// number of bytes in font resource 
			NULL,		// Reserved. Must be 0.
			&nFonts		// number of fonts installed
		);

		if (titleFontHandle == 0)
			{
			MessageBox(_T("Font add fails"), _T("Error"));
			}
		else
			{

			m_bigname.SetItalic(true, false);
			m_bigname.SetFont(_T("BoomBox"), 50);
			}
		}
	else
		{
		CString error = GetLastErrorAsString();

		// Use a default font if we couldn't load it for whatever reason
		m_bigname.SetFont(_T("Ariel"), 50);
		}

	// Initialize the other titles and labels
	DrawGraphLabels();
	
	// Initialize the charts
	pBottomAxis = m_mainChart.CreateStandardAxis(CChartCtrl::BottomAxis);
	pLeftAxis = m_mainChart.CreateStandardAxis(CChartCtrl::LeftAxis);
	pRightAxis = m_mainChart.CreateStandardAxis(CChartCtrl::RightAxis);

	afrBottomAxis= m_afrChart.CreateStandardAxis(CChartCtrl::BottomAxis);
	afrLeftAxis= m_afrChart.CreateStandardAxis(CChartCtrl::LeftAxis);

	// Draw the charts
	DrawCharts();

	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();


	// Attempt to enumerate the ports
	if (autoEnumerate)
		{
		if (DynoApp->currentComPorts.enumerateComPorts())
			{
			// If any COM ports were found then the menu has to be updated, but the construction
			// hasn't been finished yet. Set a timer and do in shortly, once the user gets control.
			m_pollTimer = SetTimer(Constants.timer_initial, 2000, NULL);
			}

		// TODO: If this fails then we'll want to log it, print a message, etc

		}
	}

void CMainForm::DrawGraphLabels()
	{
	// Torque or RPM
	m_leftOfTopChart.SubclassDlgItem(IDC_STATIC, this);
	m_leftOfTopChart.SetWindowTextW(chartShown == Constants.Chart_t_hp_vs_rpm ? Constants.String_torque_v.c_str() : Constants.String_rpm_v.c_str());
	m_leftOfTopChart.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_leftOfTopChart.SetTextColor(getMappedColour(Constants.colour_target_Labels));
	m_leftOfTopChart.SetBold(true, false);
	m_leftOfTopChart.SetFont(_T("Ariel"), 20);

	// Horsepower or Speed
	m_rightOfTopChart.SubclassDlgItem(IDC_STATIC, this);
//	m_rightOfTopChart.SetWindowTextW(Constants.String_horsepower_v.c_str());
	m_rightOfTopChart.SetWindowTextW(chartShown == Constants.Chart_t_hp_vs_rpm ? Constants.String_horsepower_v.c_str() : Constants.String_speed_v.c_str());
	m_rightOfTopChart.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_rightOfTopChart.SetTextColor(getMappedColour(Constants.colour_target_Labels));
	m_rightOfTopChart.SetBold(true, false);
	m_rightOfTopChart.SetFont(_T("Ariel"), 20);

	// RPM or Time
	m_bottomOfUpperChart.SubclassDlgItem(IDC_STATIC, this);
	//	m_bottomOfUpperChart.SetWindowTextW(Constants.String_rpm_h.c_str());
	m_bottomOfUpperChart.SetWindowTextW(chartShown == Constants.Chart_t_hp_vs_rpm ? Constants.String_rpm_h.c_str() : Constants.String_time_h.c_str());
	m_bottomOfUpperChart.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_bottomOfUpperChart.SetTextColor(getMappedColour(Constants.colour_target_Labels));
	m_bottomOfUpperChart.SetBold(true, false);
	m_bottomOfUpperChart.SetFont(_T("Ariel"), 20);

	// AFR
	m_leftOfLowerChart.SubclassDlgItem(IDC_STATIC, this);
	m_leftOfLowerChart.SetWindowTextW(Constants.String_afr_v.c_str());
	m_leftOfLowerChart.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_leftOfLowerChart.SetTextColor(getMappedColour(Constants.colour_target_Labels));
	m_leftOfLowerChart.SetBold(true, false);
	m_leftOfLowerChart.SetFont(_T("Ariel"), 20);






	// Small and vertical LeanRich
	m_smallLeanRich.SubclassDlgItem(IDC_STATIC, this);
	m_smallLeanRich.SetWindowTextW(Constants.String_smallLeanRich.c_str());
	m_smallLeanRich.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_smallLeanRich.SetTextColor(getMappedColour(Constants.colour_target_Labels));
	m_smallLeanRich.SetBold(true, false);
	m_smallLeanRich.SetFont(_T("Ariel"), 6);

	// Right HP Label
	m_hpLabel.SubclassDlgItem(IDC_STATIC, this);
	m_hpLabel.SetWindowTextW(Constants.String_hp_label.c_str());
	m_hpLabel.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_hpLabel.SetTextColor(getMappedColour(Constants.colour_target_Graph_names));
	m_hpLabel.SetBold(true, false);
	m_hpLabel.SetFont(_T("Ariel"), 16);

	// Right TQ Label
	m_tqLabel.SubclassDlgItem(IDC_STATIC, this);
	m_tqLabel.SetWindowTextW(Constants.String_tq_label.c_str());
	m_tqLabel.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_tqLabel.SetTextColor(getMappedColour(Constants.colour_target_Graph_names));
	m_tqLabel.SetBold(true, false);
	m_tqLabel.SetFont(_T("Ariel"), 16);

	// Right AFR Label
	m_afrLabel.SubclassDlgItem(IDC_STATIC, this);
	m_afrLabel.SetWindowTextW(Constants.String_afr_label.c_str());
	m_afrLabel.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_afrLabel.SetTextColor(getMappedColour(Constants.colour_target_Graph_names));
	m_afrLabel.SetBold(true, false);
	m_afrLabel.SetFont(_T("Ariel"), 16);


	// Right TQAREA Label
	m_tqareaLabel.SubclassDlgItem(IDC_STATIC, this);
	m_tqareaLabel.SetWindowTextW(Constants.String_tqarea_label.c_str());
	m_tqareaLabel.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_tqareaLabel.SetTextColor(getMappedColour(Constants.colour_target_Graph_names));
	m_tqareaLabel.SetBold(true, false);
	m_tqareaLabel.SetFont(_T("Ariel"), 16);

	// Right ACCEL Label
	m_accelLabel.SubclassDlgItem(IDC_STATIC, this);
	m_accelLabel.SetWindowTextW(Constants.String_accel_label.c_str());
	m_accelLabel.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_accelLabel.SetTextColor(getMappedColour(Constants.colour_target_Graph_names));
	m_accelLabel.SetBold(true, false);
	m_accelLabel.SetFont(_T("Ariel"), 16);

	
	
	// Right HP Label Value
	m_hpLabel_value.SubclassDlgItem(IDC_STATIC, this);	
	m_hpLabel_value.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));

	
	// Right TQ Label Value
	m_tqLabel_value.SubclassDlgItem(IDC_STATIC, this);	
	m_tqLabel_value.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	

	// Right AFR Label Value
	m_afrLabel_value.SubclassDlgItem(IDC_STATIC, this);	
	m_afrLabel_value.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_afrLabel_value.SetTextColor(getMappedColour(Constants.colour_target_SpdNeedleNormal));
	m_afrLabel_value.SetBold(true, false);
	m_afrLabel_value.SetFont(_T("Ariel"), 16);


	// Right TQAREA Label Value
	m_tqareaLabel_value.SubclassDlgItem(IDC_STATIC, this);	
	m_tqareaLabel_value.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_tqareaLabel_value.SetTextColor(getMappedColour(Constants.colour_target_SpdNeedleNormal));
	m_tqareaLabel_value.SetBold(true, false);
	m_tqareaLabel_value.SetFont(_T("Ariel"), 16);

	// Right ACCEL Label Value
	m_accelLabel_value.SubclassDlgItem(IDC_STATIC, this);	
	m_accelLabel_value.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));
	m_accelLabel_value.SetTextColor(getMappedColour(Constants.colour_target_SpdNeedleNormal));
	m_accelLabel_value.SetBold(true, false);
	m_accelLabel_value.SetFont(_T("Ariel"), 16);
	


	m_RightHorizonatalDivider.RedrawWindow();
	m_RightVerticalDivider.RedrawWindow();

	


	}


void CMainForm::DrawCharts()
	{

	m_richGraph.SetName(CString(_T("RICH")));
	m_richGraph.SetGraphNameColour(getMappedColour(Constants.colour_target_Graph_names));
	m_richGraph.SetFillColour(getMappedColour(Constants.colour_target_RichLeanFill));
	m_richGraph.SetWindowColour(getMappedColour(Constants.colour_target_Background));
	

	m_leanGraph.SetName(CString(_T("LEAN")));
	m_leanGraph.SetGraphNameColour(getMappedColour(Constants.colour_target_Graph_names));
	m_leanGraph.SetFillColour(getMappedColour(Constants.colour_target_RichLeanFill));
	m_leanGraph.SetWindowColour(getMappedColour(Constants.colour_target_Background));




	// Initialize the speedometer and tachometer controls
	m_speedometerCtrl.SetName(CString(_T("SPEED")));
	m_speedometerCtrl.SetGraphNameColour(getMappedColour(Constants.colour_target_Graph_names));
	m_speedometerCtrl.SetValueDecimals(1);										// Only 1 place after the decimal
	int defaultSpeedValues[9];

	buildTickValueArray(defaultSpeedValues, 0, currentFormConfig->speedometerMax);

	m_speedometerCtrl.SetTickValues(defaultSpeedValues);
	m_speedometerCtrl.SetRange(0.0, currentFormConfig->speedometerMax);
	m_speedometerCtrl.SetWindowColour(getMappedColour(Constants.colour_target_Background));
	m_speedometerCtrl.SetValueColour(getMappedColour(Constants.colour_target_Meter_valueText), getMappedColour(Constants.colour_target_Background));
	m_speedometerCtrl.SetFaceBorderColour(getMappedColour(Constants.colour_target_Meter_FaceBorder));
	m_speedometerCtrl.SetNeedleColour(getMappedColour(Constants.colour_target_SpdNeedleNormal));
	m_speedometerCtrl.SetValueBorderColours(getMappedColour(Constants.colour_target_Meter_valueBorderHighlite), getMappedColour(Constants.colour_target_Meter_valueBorderShadow));
	m_speedometerCtrl.SetTickMarkColour(getMappedColour(Constants.colour_target_Meter_Tickmark));
	m_speedometerCtrl.SetTickLabelColour(getMappedColour(Constants.colour_target_Labels));

	m_tachometer.SetName(CString(_T("RPM")));
	m_tachometer.SetGraphNameColour(getMappedColour(Constants.colour_target_Graph_names));
	m_tachometer.SetValueDecimals(0);					// Whole number only
	m_tachometer.SetRange(0.0, currentFormConfig->rpmMax);
	m_tachometer.SetWindowColour(getMappedColour(Constants.colour_target_Background));
	m_tachometer.SetValueColour(getMappedColour(Constants.colour_target_Meter_valueText), getMappedColour(Constants.colour_target_Background));
	m_tachometer.SetFaceBorderColour(getMappedColour(Constants.colour_target_Meter_FaceBorder));
	m_tachometer.SetNeedleColour(getMappedColour(Constants.colour_target_TachNeedleNormal));
	m_tachometer.SetValueBorderColours(getMappedColour(Constants.colour_target_Meter_valueBorderHighlite), getMappedColour(Constants.colour_target_Meter_valueBorderShadow));
	m_tachometer.SetTickMarkColour(getMappedColour(Constants.colour_target_Meter_Tickmark));
	m_tachometer.SetTickLabelColour(getMappedColour(Constants.colour_target_Labels));

	// Initialize the HP/TORQUE to RPM chart

	// Remove any series that exist on the chart
	m_mainChart.RemoveAllSeries();
	m_mainChart.EnableRefresh(false);
	m_mainChart.SetBackColor(getMappedColour(Constants.colour_target_htrChart_background));
	m_mainChart.SetBorderColor(getMappedColour(Constants.colour_target_htrChart_border));

	// Depending on the type of chart, set up the axes accordingly
	if (chartShown == Constants.Chart_t_hp_vs_rpm)
		{
		// Bottom axis (RPM)
		pBottomAxis->SetMinMax(0, currentFormConfig->rpmMax);				// RPM 0 - 8000 by default
		pBottomAxis->SetTickIncrement(true, 1000.0);
		pBottomAxis->SetDiscrete(false);
		pBottomAxis->EnableScrollBar(false);
		pBottomAxis->GetGrid()->SetVisible(currentFormConfig->showGrid_hpTorqueRpm);
		pBottomAxis->SetVisible(currentFormConfig->rpmXAxisVisible);
		pBottomAxis->SetTextColor(getMappedColour(Constants.colour_target_htrChart_rpmAxisLabels));

		// Left axis (Torque)
		pLeftAxis->SetMinMax(0, currentFormConfig->torqueMax);				// Torque 0 - 500 by default
		pLeftAxis->GetGrid()->SetVisible(currentFormConfig->showGrid_hpTorqueRpm);
		pLeftAxis->SetVisible(currentFormConfig->torqueAxisVisible);
		pLeftAxis->SetTextColor(getMappedColour(Constants.colour_target_htrChart_torqueAxisLabels));

		// Right axis (Horsepower)
		pRightAxis->SetMinMax(0, currentFormConfig->horsepowerMax);			// HP 0 - 500 by default
		pRightAxis->GetGrid()->SetVisible(currentFormConfig->showGrid_hpTorqueRpm);
		pRightAxis->SetVisible(currentFormConfig->hpAxisVisible);
		pRightAxis->SetTextColor(getMappedColour(Constants.colour_target_htrChart_hpAxisLabels));

		// Create the torque_rpm series
		pSeries_torqueAtRpm = m_mainChart.CreateLineSerie();

		// Configure torque series display
		pSeries_torqueAtRpm->SetWidth(currentFormConfig->torqueLineWidth);								// Torque line width
		pSeries_torqueAtRpm->SetColor(getMappedColour(Constants.colour_target_htrChart_torqueLine));	// Torque line colour
		pSeries_torqueAtRpm->SetPenStyle(currentFormConfig->torqueLineType);							// Torque pen type
		pSeries_torqueAtRpm->SetName(_T("Torque"));

		// Create the horsepower_rpm series
		pSeries_hpAtRpm = m_mainChart.CreateLineSerie(false, true);

		// Configure HP series display
		pSeries_hpAtRpm->SetWidth(currentFormConfig->hpLineWidth);										// HP line width
		pSeries_hpAtRpm->SetColor(getMappedColour(Constants.colour_target_htrChart_hpLine));			// HP line colour
		pSeries_hpAtRpm->SetPenStyle(currentFormConfig->hpLineType);									// HP pen type
		pSeries_hpAtRpm->SetName(_T("Horsepower"));
		}
	else if (chartShown == Constants.Chart_rpm_spd_vs_time)
		{
		chart_timeZero = 0.0;

		// Bottom axis (Time)
		// TODO: An x-axis time scale needs to be built in
		pBottomAxis->SetMinMax(0, currentFormConfig->timeAxisMax);
		pBottomAxis->SetTickIncrement(true, 100.0);
		pBottomAxis->SetDiscrete(false);
		pBottomAxis->EnableScrollBar(false);
		pBottomAxis->GetGrid()->SetVisible(currentFormConfig->showGrid_rpmSpeedTime);
		pBottomAxis->SetVisible(currentFormConfig->timeAxisVisible);

		// TODO: The time axis labels are just a copy of the RPM axis labels (for now)
		pBottomAxis->SetTextColor(getMappedColour(Constants.colour_target_htrChart_rpmAxisLabels));

		// Left axis (RPM)
		pLeftAxis->SetMinMax(0, currentFormConfig->rpmMax);								// RPM 0 - 8000 by default
		pLeftAxis->GetGrid()->SetVisible(currentFormConfig->showGrid_rpmSpeedTime);
		pLeftAxis->SetVisible(currentFormConfig->rpmYAxisVisible);

		// TODO: The RPM axis labels are just a copy of the torque axis labels (for now)
		pLeftAxis->SetTextColor(getMappedColour(Constants.colour_target_htrChart_torqueAxisLabels));

		// Right axis (Speed)
		pRightAxis->SetMinMax(0, currentFormConfig->speedometerMax);					// Speed 0 - 200 by default
		pRightAxis->GetGrid()->SetVisible(currentFormConfig->showGrid_rpmSpeedTime);
		pRightAxis->SetVisible(currentFormConfig->speedAxisVisible);

		// TODO: The speed axis labels are just a copy of the hp axis labels (for now)
		pRightAxis->SetTextColor(getMappedColour(Constants.colour_target_htrChart_hpAxisLabels));

		// Create the drum_rpm_time series
		pSeries_drumRpmAtTime = m_mainChart.CreateLineSerie();

		// Configure drum RPM series display
		pSeries_drumRpmAtTime->SetWidth(currentFormConfig->rpmLineWidth);								// Drum RPM line width
		pSeries_drumRpmAtTime->SetColor(getMappedColour(Constants.colour_target_htrChart_rpmLine));		// Drum RPM line colour
		pSeries_drumRpmAtTime->SetPenStyle(currentFormConfig->rpmLineType);								// Drum RPM pen type
		pSeries_drumRpmAtTime->SetName(_T("Drum RPM"));

		// Create the engine_rpm_time series
		pSeries_engineRpmAtTime = m_mainChart.CreateLineSerie();

		// Configure engine RPM series display
		pSeries_engineRpmAtTime->SetWidth(currentFormConfig->rpmLineWidth);								// Engine RPM line width
		pSeries_engineRpmAtTime->SetColor(getMappedColour(Constants.colour_target_htrChart_rpmLine));	// Engine RPM line colour
		pSeries_engineRpmAtTime->SetPenStyle(currentFormConfig->rpmLineType);							// Engine RPM pen type
		pSeries_engineRpmAtTime->SetName(_T("Engine RPM"));

		// Create the speed_time series
		pSeries_speedAtTime = m_mainChart.CreateLineSerie(false, true);

		// Configure Speed series display
		pSeries_speedAtTime->SetWidth(currentFormConfig->speedLineWidth);								// Speed line width
		pSeries_speedAtTime->SetColor(getMappedColour(Constants.colour_target_htrChart_speedLine));		// Speed line colour
		pSeries_speedAtTime->SetPenStyle(currentFormConfig->speedLineType);								// Speed pen type
		pSeries_speedAtTime->SetName(_T("Speed"));
		}

	// Bring up the legend
	if (currentFormConfig->legend > -1)
		{
		m_mainChart.GetLegend()->SetVisible(true);
		m_mainChart.GetLegend()->DockLegend((CChartLegend::DockSide)currentFormConfig->legend);
		m_mainChart.GetLegend()->SetHorizontalMode(true);
		//m_mainChart.GetLegend()->SetBackColor(getMappedColour(Constants.colour_target_htrChart_background));
		}
	else
		{
		m_mainChart.GetLegend()->SetVisible(false);
		}





	if (currentFormConfig->showVoltMeter == false)
		m_3DMeterCtrl.ShowWindow(SW_HIDE);
	else
		m_3DMeterCtrl.ShowWindow(SW_SHOW);


	//m_richGraph.FillBackground();

	// Draw the chart
	m_mainChart.EnableRefresh(true);


	// Remove any series that exist on the chart
	m_afrChart.RemoveAllSeries();
	m_afrChart.EnableRefresh(false);
	m_afrChart.SetBackColor(getMappedColour(Constants.colour_target_htrChart_background));
	m_afrChart.SetBorderColor(getMappedColour(Constants.colour_target_htrChart_border));

	// TODO: An x-axis time scale needs to be built in
	afrBottomAxis->SetMinMax(0, currentFormConfig->timeAxisMax);
	afrBottomAxis->SetTickIncrement(true, 1000);
	afrBottomAxis->SetDiscrete(false);
	afrBottomAxis->EnableScrollBar(false);
	afrBottomAxis->GetGrid()->SetVisible(true);
	afrBottomAxis->SetVisible(true);
	afrBottomAxis->SetTextColor(getMappedColour(Constants.colour_target_htrChart_hpAxisLabels));

	// Left axis (Rich Lean)
	afrLeftAxis->SetMinMax(0, ceil(currentFormConfig->AFRValue*2));					// Reach Lean -100 to  100 by default	
	afrLeftAxis->GetGrid()->SetVisible(currentFormConfig->showGrid_rpmSpeedTime);
	afrLeftAxis->SetVisible(currentFormConfig->speedAxisVisible);	
	afrLeftAxis->SetTextColor(getMappedColour(Constants.colour_target_htrChart_hpAxisLabels));


	// Right axis (Rich Lean)
	//afrRightAxis->SetMinMax(0, ceil(currentFormConfig->AFRValue * 2));					// Reach Lean -100 to  100 by default
	//afrRightAxis->GetGrid()->SetVisible(currentFormConfig->showGrid_rpmSpeedTime);
	//afrRightAxis->SetVisible(currentFormConfig->speedAxisVisible);
	//afrRightAxis->SetTextColor(getMappedColour(Constants.colour_target_htrChart_hpAxisLabels));



	// Create the afr_time series
	//pSeries_AfrAtTime = m_afrChart.CreateLineSerie();

	


	// Create the afr static serie
	pSeries_AfrStatic = m_afrChart.CreateLineSerie();

	// Configure AFR series display
	pSeries_AfrStatic->SetWidth(1);								// Speed line width
	pSeries_AfrStatic->SetColor(getMappedColour(Constants.colour_target_htrChart_speedLine));		// Speed line colour
	pSeries_AfrStatic->SetPenStyle(currentFormConfig->speedLineType);								// Speed pen type
	pSeries_AfrStatic->SetName(_T("Static"));

	int len = (int)currentFormConfig->timeAxisMax;
	double* YValues = new double[len];
	double* XValues = new double[len];

	

	for (int i = 0;i < len;i++)
	{
		XValues[i]= i;
		YValues[i] = currentFormConfig->AFRValue;
	}
	pSeries_AfrStatic->SetPoints(XValues, YValues, len);
	


	m_afrChart.EnableRefresh(true);








//	m_richLeanBorder.SetBackgroundColor(RGB(100, 100, 50));
	}

// Fill the incoming array with evenly spaced values, as much as practical
void CMainForm::buildTickValueArray(int *defaultSpeedValues, int startVal, int maxVal)
	{
	defaultSpeedValues[0] = startVal;
	defaultSpeedValues[8] = maxVal;

	double increment = (maxVal - startVal) / 8.0;

	// FOR NOW: Hard code the number of values to 9, but this will later change for different types of charts
	for (int i = 1; i < 8; i++)
		{
		defaultSpeedValues[i] = static_cast<int>(increment * i);
		}
	}

void CMainForm::OnDestroy()
	{
	CFormView::OnDestroy();

	// Kill the timer if it's running
	if (m_pollTimer > 0)
		KillTimer(m_pollTimer);

	// Just in case it's running, signal the background task to stop
	bkgComProcessMessage.forceExit = true;

	// If the COM port is open then close it
	if (openPortIndex > -1)
		{
		DynoApp->currentComPorts.Close(openPortIndex);
		}

	if (titleFontHandle)
		{
		BOOL removed = RemoveFontMemResourceEx(titleFontHandle);
		}
	}

// All of the controls will be updated on the timer
void CMainForm::OnTimer(UINT_PTR nIDEvent)
	{
	// If the timer is checking for the initial setup period to finish then deal with it
	if (nIDEvent == Constants.timer_initial)
		{
		// Kill the timer
		KillTimer(m_pollTimer);
		m_pollTimer = 0;

		// Update the menu (ports should have been enumerated before now)
		updateComMenu();
		updateWindowTitle();

		CFormView::OnTimer(nIDEvent);
		return;
		}

	// TODO: We may have different timer events for different ports, etc. If so, check here.

	// If data needs to be read and maybe processed then kill the timer first.
	// This is necessary because sometimes reading data takes a long time.
	// Don't reset the m_pollTimer variable because we don't want the other program behaviour to change.
	KillTimer(m_pollTimer);

	// Check to see if there are any events that are ready to go
	if (bkgComProcessMessage.readyToGraph > 0)
		{
		// Processing the incoming data will remove the events from the input queue and display them
		processIncomingData();
		}

	// Restart the timer once the processing is complete
	if(!bkgComProcessMessage.forceExit)
		m_pollTimer = SetTimer(Constants.timer_polling, currentFormConfig->pollingInterval, NULL);

	CFormView::OnTimer(nIDEvent);
	}

// COM-Port polling and processing thread.
// This function builds a queue continuously in the background. The timer-polling function
// will access the members of the queue that this function builds. This threat does not care
// what is in the queue and only adds to the end of it.
UINT CMainForm::backgroundPollCOMPort(LPVOID dataList)
	{
	backgroundComProcessing* bkgData = (backgroundComProcessing*)dataList;

	int DEBUG_count_1 = 0;
	int DEBUG_count_2 = 0;


	long valuesRead = 0;

	MSG msg;
	bool processedAlternativeStream = false;

	SYSTEMTIME currentTime;
	DWORD dataRead;
	CString dataPrefixes = TOKEN_Full_String;

	std::shared_ptr<char[]>incomingData;

	// If there is an alternative data stream then don't bother allocating new memory
	if (bkgData->altDataBufferLength)
		{
		incomingData = bkgData->dataStream;
		}
	else
		{
		// Default to a 5K buffer
		incomingData.reset(new char[5000]);
		}

	// Maintain an internal Event list. I didn't think that this was necessary until after
	// the introduction of non-time delta events, such as a timestamp or sensor reading.
	// These other types would require an extra and complicated layer of control in the
	// external Event list that is ultimately unnecessary.
	// Events will be put into the internal list until they are finalized. After that,
	// they will be copied out (by this function, which avoids concurrency issues) and
	// no longer referenced here.
	CList <DynoEvent, DynoEvent&> internalEventList;
	DynoEvent* previous_RPM_drum_event = nullptr;

	// Everything is calculated based on the time delta
	double timeDelta;
	
	// The derived values
	double rpm_o, rpm_ss, rpm_se;
	double radsPerSecond_o, radsPerSecond_ss, radsPerSecond_se;
	double angularAcceleration_o, angularAcceleration_ss, angularAcceleration_se;

	// Incoming data index
	int dataReadIndex = 0;

	// Smoothing-related
	auto smoothingArray = std::make_unique<double[]>(bkgData->sessionConfig->smoothingWindowSize);
	int smoothCount = 0;
	int smoothIndex = 0;

	// Exponential moving average
	double previous_EMA;
	double emaMultiplier = 2.0 / (bkgData->sessionConfig->smoothingWindowSize + 1.0);

	// Keep running this loop until we're told to stop
	while (!bkgData->forceExit)
		{
		// Before reading from the COM port, make sure that there aren't too many events queued up
		while (bkgData->haveSmoothed > bkgData->sessionConfig->incomingEventQueueLimit)
			{
			if (bkgData->forceExit)
				{
				goto ExitBackgroundCom;
				}

			Sleep(100);
			}

		// Windows message pump while we're in this loop
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			}

		// NOTE: The following check is used to specify an alternative source of data to the COM
		// port. Really, all of this processing should be moved to its own function, but during
		// development, just a quick fix is needed for testing. It will probably not be
		// used heavily enough to warrant anything other than a quick hack.

		// Select and use the appropriate data source
		if (bkgData->altDataBufferLength)
			{
			// If we've already processed the alternative stream then exit
			if (processedAlternativeStream)
				break;

			// Use the alternative data stream instead of reading one in
			dataRead = bkgData->altDataBufferLength;

			processedAlternativeStream = true;
			}
		else
			{
			// Read any data that is buffered at the specified COM port.
			// Offset the read into the buffer to remember overflow from the previous read.
			dataRead = bkgData->activePorts->Read(bkgData->portIndex, incomingData.get()+dataReadIndex, bkgData->sessionConfig->comReadBufferSize);
			}

		// After the data has been read, reset the read index
		dataReadIndex = 0;
/*
		incomingData[0] = incomingData[3] = incomingData[6] = incomingData[9] = incomingData[12] = incomingData[15] = incomingData[18] = incomingData[21] = incomingData[24] = incomingData[27] = incomingData[30] = incomingData[33] = incomingData[36] = incomingData[39] = incomingData[42] = incomingData[45] = incomingData[48] = TOKEN_timeDelta;
		incomingData[2] = incomingData[5] = incomingData[8] = incomingData[11] = incomingData[14] = incomingData[17] = incomingData[20] = incomingData[23] = incomingData[26] = incomingData[29] = incomingData[32] = incomingData[35] = incomingData[38] = incomingData[41] = incomingData[44] = incomingData[47] = incomingData[50] = 0;

		incomingData[1] = 100;
		incomingData[4] = 90;
		incomingData[7] = 85;
		incomingData[10] = 80;
		incomingData[13] = 70;
		incomingData[16] = 60;
		incomingData[19] = 50;
		incomingData[22] = 40;
		incomingData[25] = 30;
		incomingData[28] = 25;
		incomingData[31] = 15;
		incomingData[34] = 30;
		incomingData[37] = 60;
		incomingData[40] = 90;
		incomingData[43] = 120;
		incomingData[46] = 150;
		incomingData[49] = 200;

		dataRead = 51;
*/		

		// If there is data read then process it
		if (dataRead)
			{
			// Process the incoming data

			// Look at the incoming data stream as:
			//	[Single byte to indicate what type of data it is] + 4 bytes of data
			//	NOTE: This implies that (at least for now) everything is in chunks of 5 bytes
			//		DXXXX = Time delta indicated by a 32-bit value # of ticks
			//
			int numValues = dataRead / bkgData->sessionConfig->timeDeltaByteLength+1;			// Calculate how many values are in the stream	
			bool isEven = !(dataRead % bkgData->sessionConfig->timeDeltaByteLength + 1);		// Keep track of whether the message appears to be complete, length-wise

			uint32_t inputValue;					// Temporary input value that matches Arduino output
			long double inputLong;					// Variable used to do the math on the input

			// Roll through the points (each is the time delta based on the previous one)
			while ((static_cast<DWORD>(dataReadIndex + bkgData->sessionConfig->timeDeltaByteLength+1) <= dataRead) && !bkgData->forceExit)
				{
				// Check to make sure that the first character is a valid identifier.
				if(dataPrefixes.Find(incomingData[dataReadIndex]) < 0)
					{
					dataReadIndex++;
					continue;
					}

				// With a valid identifier found, copy the value.
				// NOTE: At last update, this is a 4-byte uint32_t value from the Arduino that
				// matches the architecture here. If that should change in the future then this
				// could not be a straight copy. Some kind of manipulation would be necessary.
				memcpy((void*)&inputValue, (void*)(incomingData.get()+dataReadIndex + 1), bkgData->sessionConfig->timeDeltaByteLength);


				// If it's a Sound Event then deal with it and move on
				if (incomingData[dataReadIndex] == TOKEN_sound)
					{
					GetLocalTime(&currentTime);
					internalEventList.AddTail(DynoEvent(DynoEvent::EventType::Sound, inputValue, 0, currentTime, bkgData->totalTime));
					dataReadIndex += bkgData->sessionConfig->timeDeltaByteLength + 1;

					continue;
					}

				// FOR NOW: If the time delta is 0.0 then skip this one
				if (!inputValue)
					{
					dataReadIndex += bkgData->sessionConfig->timeDeltaByteLength + 1;

					// Check to see if the processing has been halted
					if (bkgData->forceExit)
						break;

					continue;		// Move on to reading the next three bytes
					}

				// DEBUG
				if (!(++valuesRead % 1000))
					int x = 999;

				// As long as there is a backlog of unprocessed Events that have already been smoothed,
				// just sleep and wait for it to clear.
				while (bkgData->haveSmoothed > bkgData->sessionConfig->incomingEventQueueLimit)
					{
					if (bkgData->forceExit)
						{
						goto ExitBackgroundCom;
						}

					Sleep(100);
					}

				// Take action based on the identifier
				switch (incomingData[dataReadIndex])
					{
					POSITION lastTailPosition;
					bool beyondHeaderEvents;

						case TOKEN_timeDelta_mf:		// TODO: Condense all of the math later

							// Get the current system time for any Events that are created.
							// It may not mean much right now but we can tell which events
							// are linked together if they are re-arranged.
							GetLocalTime(&currentTime);

							// The input value is now the timeDelta measured in microseconds.
							// It needs to be converted to a fraction of seconds.
							inputLong = inputValue;

							// The following comments are to test specific values.
//							inputLong *= 8.0;
//							inputLong *= 64.0;		// Nano prescaler 512 and Mega prescaler 1024
//							inputLong *= 0.0625;	// No prescaler on the Mega means that a tick is 62.5 nanoseconds

							inputLong *= bkgData->sessionConfig->tickMultiplier;

							timeDelta = inputLong / 1000000.0;		// Fixed value because we're always converting to microseconds

							// If the timeDelta is zero but the input value was not then just make it very small
							if (!timeDelta && inputValue)
								{
								// TODO: Log this
								timeDelta = .000001;
								}

							// NOTE: If in the future there is a system that can be paused and restarted,
							// we may not want the smoothing to bridge the break in time. In this case,
							// we'll have to keep track of how long, time-wise, since the last Event was
							// created. If it is over a certain threshold then it won't be included in the
							// smoothing from this point on.

							// The smoothingArray will contain the raw data until. This array will roll
							// over because it will only be used to quickly calculate the average when
							// needed. The difference between radsPerSecond won't come from here.
							smoothingArray[smoothIndex] = timeDelta;
							smoothCount++;

							// Roll the values around when the array reaches the end
							if (++smoothIndex >= bkgData->sessionConfig->smoothingWindowSize)
								smoothIndex = 0;

							// Add a new event with -1 RPM to indicate that it hasn't been calculated yet.
							// Also, store a pointer to the previous drum Event in here. Immediately afterwards, this new one becomes the "previous" one.
							lastTailPosition = internalEventList.AddTail(DynoEvent(DynoEvent::EventType::RPM_drum, -1.0, 0, currentTime, timeDelta, previous_RPM_drum_event));

							// After exiting the switch but before the next iteration of the loop, see if there are enough
							// values to do some smoothing and add the other Events. Up to this point, there will be no
							// torque or horsepower Events.
							if (smoothCount >= bkgData->sessionConfig->smoothingWindowSize)
								{
								double TimeDelta_SMA = 0.0;
								double TimeDelta_EMA = 0.0;

								// Calculate the SMA time delta for the last set
								for (int avgIndex = 0; avgIndex < bkgData->sessionConfig->smoothingWindowSize; avgIndex++)
									TimeDelta_SMA += smoothingArray[avgIndex];

								TimeDelta_SMA /= static_cast<double>(bkgData->sessionConfig->smoothingWindowSize);

								// Calculate the EMA time delta for the last set
								// NOTE: The EMA needs one extra point of observation (smoothingWindowSize+1), and it uses the SMA
								// as the "last EMA" for the first calculate.
								if (smoothCount == bkgData->sessionConfig->smoothingWindowSize)
									{
									previous_EMA = TimeDelta_SMA;
									}
								else
									{
									// EMA From: https://www.investopedia.com/ask/answers/122314/what-exponential-moving-average-ema-formula-and-how-ema-calculated.asp
									TimeDelta_EMA = (timeDelta * emaMultiplier) + (previous_EMA * (1.0 - emaMultiplier));
									previous_EMA = TimeDelta_EMA;
									}

								// Every time we get a new average, the total time increases
								bkgData->totalTime += timeDelta;

								if (bkgData->forceExit)
									break;

								// Since the smoothArray has been filled up to the specific limit, start rolling through the list and calculating
								// the various values. We have to start relative to the back of the list, since the front may be ready
								// to be pulled out but not yet gone. From the very first element in the list (or the last element of
								// a set, but it doesn't matter), we need to pull out the lastRadsPerSecond.


								// Roll backwards from the last-added Event until we hit some that have been smoothed already, or the head
								DynoEvent *tempEvent;
								POSITION holdPos;
								int stepsBackwards = 0;

								DEBUG_count_1++;
								if (DEBUG_count_1 == 7)
									int a = 10;

								do
									{
									// Keep track of the last Event before stepping back
									holdPos = lastTailPosition;

									// Get the previous Event
									tempEvent = &internalEventList.GetPrev(lastTailPosition);
									stepsBackwards++;

									// When we hit one that has been smoothed already, go back to the previous and exit the loop
									if (tempEvent->finalSmoothed)
										{
										tempEvent = &internalEventList.GetNext(holdPos);
										break;
										}

									} while (!bkgData->forceExit && lastTailPosition);

								if (bkgData->forceExit)
									break;

								// Make sure that we aren't positioned on a sensor event
								while (tempEvent->type > 7)
									{
									tempEvent = &internalEventList.GetNext(holdPos);					// Advance
									}


								// When this loop exits, we will be on the first Event in the last set before the one that we want to smooth.
								// The holdPos will be set to the same Event (element in the list).
							
								lastTailPosition = holdPos;

								///////////
								// TODO: The lastRadsPerSecond may no longer be needed
								// Calculate what the radsPerSecond would be and store it (keep both an original and a smoothed version for diagnostics)
								///////////

								bkgData->lastRadsPerSecond_o = ((60.0 * 2.0 * 3.14159 / 360.0) * 60.0 / tempEvent->originalTimeDelta) / 60.0;

								// If there isn't a smoothed delta (such as in the first RPM event) then just use the original
								bkgData->lastRadsPerSecond_ss = ((60.0 * 2.0 * 3.14159 / 360.0) * 60.0 / (tempEvent->smoothedTimeDelta_sma ? tempEvent->smoothedTimeDelta_sma : tempEvent->originalTimeDelta)) / 60.0;
								bkgData->lastRadsPerSecond_se = ((60.0 * 2.0 * 3.14159 / 360.0) * 60.0 / (tempEvent->smoothedTimeDelta_ema ? tempEvent->smoothedTimeDelta_ema : tempEvent->originalTimeDelta)) / 60.0;
								//////////////////////////////////////


								// At this point, we have to start looking at a subset of the points that have been smoothed.
								// FOR NOW: This is fixed at three points that are the subject of a Lagrangian polynomial.

								// Depending on where we are, record the values accordingly
								beyondHeaderEvents = false;

								if (smoothCount == bkgData->sessionConfig->smoothingWindowSize)
									{
									// Are we on the first point that makes up the beginning?
									bkgData->tdSum[0] = bkgData->totalTime;
									
									// NOTE: Using original timeDelta instead of EMA to evaluate psplines
									bkgData->tdValues[0] = Constants.RADS_6 / timeDelta;

//									bkgData->tdValues[0] = Constants.RADS_6 / TimeDelta_EMA;

									}
								else if(smoothCount == bkgData->sessionConfig->smoothingWindowSize+1)
									{
									// Are we on the second point that makes up the beginning?
									bkgData->tdSum[1] = bkgData->totalTime;

									// NOTE: Using original timeDelta instead of EMA to evaluate psplines
									bkgData->tdValues[1] = Constants.RADS_6 / timeDelta;

//									bkgData->tdValues[1] = Constants.RADS_6 / TimeDelta_EMA;
									}
								else if (smoothCount == bkgData->sessionConfig->smoothingWindowSize + 2)
									{
									// Are we on the third point that makes up the beginning?
									bkgData->tdSum[2] = bkgData->totalTime;

									// NOTE: Using original timeDelta instead of EMA to evaluate psplines
									bkgData->tdValues[2] = Constants.RADS_6 / timeDelta;

//									bkgData->tdValues[2] = Constants.RADS_6 / TimeDelta_EMA;

									beyondHeaderEvents = true;
									}
								else
									{
									// Beyond the initial three points, we just keep rolling

									bkgData->tdSum[0] = bkgData->tdSum[1];
									bkgData->tdSum[1] = bkgData->tdSum[2];
									bkgData->tdSum[2] = bkgData->totalTime;

									bkgData->tdValues[0] = bkgData->tdValues[1];
									bkgData->tdValues[1] = bkgData->tdValues[2];


									// NOTE: Using original timeDelta instead of EMA to evaluate psplines
									bkgData->tdValues[2] = Constants.RADS_6 / timeDelta;

//									bkgData->tdValues[2] = Constants.RADS_6 / TimeDelta_EMA;

									beyondHeaderEvents = true;
									}

								// If we're beyond the initial position then we have to advance the hold pointer to the horsepower Event
								if (tempEvent->finalSmoothed && ((internalEventList.GetCount()-stepsBackwards) >= (bkgData->sessionConfig->smoothingWindowSize)))
									{
									DynoEvent* skipEvent;

									// Keep looping until we get to the next Horsepower Event and stop on it
									while (holdPos)
										{
										skipEvent = &internalEventList.GetNext(holdPos);					// Advance

										if (holdPos)
											{
											skipEvent = &internalEventList.GetAt(holdPos);					// Check it, don't advance

											if (skipEvent->type == DynoEvent::EventType::Horsepower)
												break;
											}
										}
									}

								// We want to make sure that we skip past exactly one RPM_drum Event.
								// It was simpler before other types of Events from
								// the sensors were added, but now, we have to skip over them.
								while (lastTailPosition)
									{
									tempEvent = &internalEventList.GetNext(lastTailPosition);				// Advance

									// If we didn't skip over an RPM_drum Event then try again, otherwise stop
									if (tempEvent->type == DynoEvent::EventType::RPM_drum)
										break;

									// If it doesn't break then it will keep reading. If there is some kind of error
									// and it hits the end of the list then it will be caught by the while() stop condition.
									// If there are other sensor Events immediately after this then it won't matter
									// because the next loop finds the first RPM_drum.
									}

								DEBUG_count_2++;

								if (DEBUG_count_2 == 7)
									int a = 10;

								while (!bkgData->forceExit && lastTailPosition)
									{
									DynoEvent* rpmEvent_drum = &internalEventList.GetNext(lastTailPosition);		// Grab the RPM Event and position on the next Event
									DynoEvent* torqueEvent;
									DynoEvent* hpEvent;
									DynoEvent* rpmEvent_engine;

									// Verify that this is an RPM event in case things got out of sequence.
									// It might be normal if we're advancing to a new set in the list an the RPM Event that we just visited
									// is the first in the set of Events for the same timestamp.

									while (lastTailPosition && (rpmEvent_drum->type != DynoEvent::EventType::RPM_drum))
										{
										rpmEvent_drum = &internalEventList.GetNext(lastTailPosition);		// Grab the RPM Event and position on the next Event
										}

									if (!lastTailPosition && (rpmEvent_drum->type != DynoEvent::EventType::RPM_drum))
										break;

									// Start by putting the averaged time delta in
									rpmEvent_drum->smoothedTimeDelta_sma = TimeDelta_SMA;
									rpmEvent_drum->smoothedTimeDelta_ema = TimeDelta_EMA;
									rpmEvent_drum->absTime = bkgData->totalTime;

									// Calculate the RPM based on the smoothed data
									// Note: Math was ((60.0 / 360.0) * 60.0) / timeDelta
									rpm_o = 10 / rpmEvent_drum->originalTimeDelta;
									rpm_ss = 10 / rpmEvent_drum->smoothedTimeDelta_sma;

									// NOTE: Using original timeDelta instead of EMA to evaluate psplines
									rpm_se = 10 / rpmEvent_drum->originalTimeDelta;
//									rpm_se = 10 / rpmEvent_drum->smoothedTimeDelta_ema;

									// Calculate the velocity: RADS_6 = (2 * PI) / 6
									radsPerSecond_o = Constants.RADS_6 / rpmEvent_drum->originalTimeDelta;
									radsPerSecond_ss = Constants.RADS_6 / rpmEvent_drum->smoothedTimeDelta_sma;

									// NOTE: Using original timeDelta instead of EMA to evaluate psplines
									radsPerSecond_se = Constants.RADS_6 / rpmEvent_drum->originalTimeDelta;
//									radsPerSecond_se = Constants.RADS_6 / rpmEvent_drum->smoothedTimeDelta_ema;


									// If we're not beyond the header events then this does not yet apply
									if (beyondHeaderEvents)
										{
										// Calculate the angular acceleration and torque
										angularAcceleration_o = (radsPerSecond_o - bkgData->lastRadsPerSecond_o) / rpmEvent_drum->originalTimeDelta;		// Angular acceleration (dO/dt)
//										angularAcceleration_s = (radsPerSecond_s - bkgData->lastRadsPerSecond_s) / averageTimeDelta;				// Angular acceleration (dO/dt)


										double L_a = (bkgData->tdSum[0]* bkgData->tdSum[0])-(bkgData->tdSum[2]* bkgData->tdSum[0]) - (bkgData->tdSum[1] * bkgData->tdSum[0]) +(bkgData->tdSum[1]* bkgData->tdSum[2]);
										double L_b = (bkgData->tdSum[1] * bkgData->tdSum[1]) - (bkgData->tdSum[2] * bkgData->tdSum[1]) - (bkgData->tdSum[0] * bkgData->tdSum[1]) + (bkgData->tdSum[0] * bkgData->tdSum[2]);
										double L_c = (bkgData->tdSum[2] * bkgData->tdSum[2]) - (bkgData->tdSum[1] * bkgData->tdSum[2]) - (bkgData->tdSum[0] * bkgData->tdSum[2]) + (bkgData->tdSum[0] * bkgData->tdSum[1]);
										double L_A = ((bkgData->tdValues[0] / L_a) + (bkgData->tdValues[1] / L_b) + (bkgData->tdValues[2] / L_c));
										double L_B = -1*(((bkgData->tdValues[0] * bkgData->tdSum[2] + bkgData->tdValues[0] * bkgData->tdSum[1]) / L_a) + ((bkgData->tdValues[1] * bkgData->tdSum[2] + bkgData->tdValues[1] * bkgData->tdSum[0]) / L_b) + ((bkgData->tdValues[2] * bkgData->tdSum[1] + bkgData->tdValues[2] * bkgData->tdSum[0]) / L_c));
										double L_C = ((bkgData->tdValues[0] * bkgData->tdSum[1] * bkgData->tdSum[2]) / L_a) + ((bkgData->tdValues[1] * bkgData->tdSum[0] * bkgData->tdSum[2]) / L_b) + ((bkgData->tdValues[2] * bkgData->tdSum[0] * bkgData->tdSum[1]) / L_c);
										double L_Y = L_A * bkgData->tdSum[1] * bkgData->tdSum[1] + L_B * bkgData->tdSum[1] + L_C;
										double L_A_2 = L_A * 2;
										double L_B_2 = L_B;
										double dYdT = L_A_2 * bkgData->tdSum[1] + L_B_2;


										// NOTE: These angular acceleration parameters are no longer used
										angularAcceleration_ss = (radsPerSecond_ss - bkgData->lastRadsPerSecond_ss) / TimeDelta_SMA;	// Angular acceleration (dO/dt)
										angularAcceleration_se = (radsPerSecond_se - bkgData->lastRadsPerSecond_se) / TimeDelta_EMA;	// Angular acceleration (dO/dt)

										// Record the acceleration and then set it as an active (pre-smoothed) RPM Drum Event
										rpmEvent_drum->originalAcceleration = dYdT;
										bkgData->RPM_drum_total++;

/* Moved to calculateEventValues()*/ 

										// Update the data in all three of the Events

										// Start with the drum RPM event
										rpmEvent_drum->originalParameter = rpm_o;
										rpmEvent_drum->smoothedParameter_sma = rpm_ss;
										rpmEvent_drum->smoothedParameter_ema = rpm_se;

										// Set the nextRPM_drum Event of the previous one to be the current one
										if (previous_RPM_drum_event)
											{
											rpmEvent_drum->RPM_drum_index = previous_RPM_drum_event->RPM_drum_index + 1;
											rpmEvent_drum->previousRPM_drum = previous_RPM_drum_event;
											previous_RPM_drum_event->nextRPM_drum = rpmEvent_drum;
											previous_RPM_drum_event = rpmEvent_drum;
											}
										else
											{
											// Set the index to be the first one (1-based)
											rpmEvent_drum->RPM_drum_index = 1;

											// Mark the current one as the previous for the next iteration
											previous_RPM_drum_event = rpmEvent_drum;
											}

										// Create the torque Event in the list
										// NOTE: This is drum torque but we want engine torque, so the torque
										// is modified at the end, after horsepower is derived.
										lastTailPosition = internalEventList.AddTail(DynoEvent(DynoEvent::EventType::Torque, -1.0, Constants.Units_imperial, currentTime, timeDelta));

										torqueEvent = &internalEventList.GetAt(lastTailPosition);				// Grab the Torque Event and position on HP
										torqueEvent->smoothedTimeDelta_sma = TimeDelta_SMA;						// Put the averaged time delta in
										torqueEvent->smoothedTimeDelta_ema = TimeDelta_EMA;
										torqueEvent->absTime = bkgData->totalTime;

										// Create the HP Event in the list
										lastTailPosition = internalEventList.AddTail(DynoEvent(DynoEvent::EventType::Horsepower, -1.0, Constants.Units_imperial, currentTime, timeDelta));

										hpEvent = &internalEventList.GetNext(lastTailPosition);					// Grab the HorsePower Event and position on the next RPM
										hpEvent->smoothedTimeDelta_sma = TimeDelta_SMA;							// Put the averaged time delta in
										hpEvent->smoothedTimeDelta_ema = TimeDelta_EMA;
										hpEvent->absTime = bkgData->totalTime;

										// Calculate engine RPM
										// NOTE: There are several ways that engine RPM is going to be determined or derived.
										//	1) The drum RPM will be synchronized with the engine RPM. This is the best way to do it.
										//	2) Hook an induction clamp to the distributor lead. Some cars have problems with this. Things like misfires cause noise.
										//	3) The car specifications will be used in conjunction with drum RPM to derive it.
										//	4) An ODB-II feed will supply it.
										//	5) FOR TESTING: An default equation, much like the one that will be used to derive engine RPM from drum RPM, will be used.
										//		The difference is that it is based on absolute time, rather than drum speed. It only works for Chris' Camaro dyno run.

										// Create the engine RPM event
										lastTailPosition = internalEventList.AddTail(DynoEvent(DynoEvent::EventType::RPM_engine, -1.0, Constants.Units_imperial, currentTime, timeDelta));

										rpmEvent_engine = &internalEventList.GetNext(lastTailPosition);					// Grab the HorsePower Event and position on the next RPM

										// REMEMBER: This equation is only valid for Chris' dyno run - the one instance
										rpmEvent_engine->originalParameter = (-0.000606748841 * pow(bkgData->totalTime, 6)) + (0.029927472838 * pow(bkgData->totalTime, 5)) - (0.62653233148 * pow(bkgData->totalTime, 4)) + (6.347046004844 * pow(bkgData->totalTime, 3)) - (27.843495209402 * pow(bkgData->totalTime, 2)) + (357.64883421901 * bkgData->totalTime) + 2141.74711683313;
										rpmEvent_engine->absTime = bkgData->totalTime;
										}

									bkgData->lastRadsPerSecond_o = radsPerSecond_o;					// Keep track of the last rads per second reading
									bkgData->lastRadsPerSecond_ss = radsPerSecond_ss;
									bkgData->lastRadsPerSecond_se = radsPerSecond_se;
									}
									// After exiting this loop, we should be pointing either to NULL after the end of the tail

									if (bkgData->forceExit)
										break;

									// At the end of the smoothing run, make sure that everything up to the point before this loop has been marked Final
									// pos = bkgData->incomingEventList.GetHeadPosition();
									bool hitSmoothed = false;
									do
										{
										if (!holdPos)
											{
											// Log this error because it should never happen
											int a = 5;
											break;
											}

										// Get the event that was immediate before the recently smoothed list
										POSITION possibleUpdate = holdPos;
										tempEvent = &internalEventList.GetPrev(holdPos);

										// If it has already been marked final then stop going backward
										if (tempEvent->finalSmoothed)
											{
											hitSmoothed = true;
											holdPos = possibleUpdate;
											break;
											}

										tempEvent->finalSmoothed = true;

										/*
										// Right before the Event is marked final, we will do the P-Spline smoothing.
										// It only happens for RPM_drum Events.

										if (tempEvent->type == DynoEvent::EventType::RPM_drum)
											{

											// The requires the acceleration from the previous RPM_Drum Event, as well
											// as the next Event. It doesn't change either of those, but needs them.
											// If the next one does not exist yet, then don't finalize at this point.
											// Instead, leave it for the next iteration.

											if (tempEvent->countForwardDrumEvents() >= (bkgData->psplines_N/2))
												{*/
												/*
													We can now traverse backwards through the RPM_Drum Events and grab the values
													for P-Splines. The X is the absTime, and Y is the originalAcceleration.
													Using the Event prior to this, and the one after this, calculate the P-Spline
													values and just keep the middle one to update here. This becomes the
													smoothed acceleration.

													Once the new acceleration has been calculated, the other values (torque, etc)
													have to be re-calculated and updated. The three Events immediately after this
													one, which are the Torque, Horsepower, and Engine RPM, also have to be updated.
													Since this was walking backward through the list, they will already be marked as final,
													but they won't have been taken because the HaveSmoothed was not incremented. Also,
													they won't be marked as Graphable.
													
													Just make sure to return back to this one, positioned on the previous,
													to continue the original traversal backward.																		
												*//*
												
												// Update the Events and mark them as final
												calculateEventValues(&internalEventList, possibleUpdate, bkgData);
												}
											else
												tempEvent->finalSmoothed = true;

											}
										else
											{
											// Mark it as final and move it to the external list
											tempEvent->finalSmoothed = true;
											}
										*/
												

										} while (holdPos && !bkgData->forceExit);		// If we just processed the Head then stop

									// NOTE: In the above loop, I had been adding the events to the tail of the incomingEventList as I marked them final,
									// but realized that there were being put in out of order. The graph was still ok in general because it is only
									// mixed up in small subsets, like a jitter.
									
									// If there is no holdPos then we start at the head, otherwise we advance one and then start
									if (!hitSmoothed)
										{
										holdPos = internalEventList.GetHeadPosition();
										}
									else
										{
										// Advance past the one that had been marked final in a previous iteration
										tempEvent = &internalEventList.GetNext(holdPos);
										}

									// TODO: The next couple of loops should be optimized into one, keeping a count as to how far
									// back the pointer travels and knowing where to set the graphable points. Take care
									// of this when there is time.

									// Mark the start of these so that we can send eligible Events to the graph
									POSITION markPos = holdPos = (bkgData->lastGraphable ? bkgData->lastGraphable : internalEventList.GetHeadPosition());

									// Roll forward, adding the ones that are smoothed to the incomingEventList
									tempEvent = &internalEventList.GetNext(holdPos);

									// Of the smoothed Events, a subset of them will be ready to sent to the graph.
									// It is determined by the size of the P-Spline smoothing window
									while(tempEvent->finalSmoothed)
										{
										if ((tempEvent->type == DynoEvent::EventType::RPM_drum) && (tempEvent->RPM_drum_index))
											{
											// After setting the Events as smoothed, see if there are any that should be sent to the graph.
											// To be eligible, they have to be before the N/2 mark of the semi-finalized points.
											if ((bkgData->RPM_drum_total - tempEvent->RPM_drum_index) > (bkgData->psplines_N / 2))
												{
												// Update the Events and mark them as final
												calculateEventValues(&internalEventList, markPos, bkgData);

												tempEvent->graphable = true;
												bkgData->lastGraphable = holdPos;

												bkgData->incomingEventList->AddTail(*tempEvent);

												// Mark the current node as finalized
												bkgData->haveSmoothed++;

												// FOR NOW: Just destroy the links in the newly created event.
												// Later, we'll build them up as they're inserted.
												//bkgData->incomingEventList->GetTail().setLinks();

												bkgData->readyToGraph++;
												}
											else
												{

												// If we hit an RPM total that is closer to the end than half the window size then stop
												break;
												}
											}
										else
											{
											tempEvent->graphable = true;
											bkgData->lastGraphable = holdPos;
											bkgData->incomingEventList->AddTail(*tempEvent);

											// Mark the current node as finalized
											bkgData->haveSmoothed++;

											// FOR NOW: Just destroy the links in the newly created event.
											// Later, we'll build them up as they're inserted.
											//bkgData->incomingEventList->GetTail().setLinks();

											bkgData->readyToGraph++;
											}

										// Make sure that we're not at the tail
										if (!holdPos)
											break;

										// Move on to the next one
										markPos = holdPos;
										tempEvent = &internalEventList.GetNext(holdPos);
										}

								}
								else
									{
									// Until the smoothing window has been filled, build the total time
									bkgData->totalTime += timeDelta;
									}

								break;

						default:		// Unknown identifier

							// Just ignore it for now
							;
					}

					// Advance the index
					dataReadIndex += bkgData->sessionConfig->timeDeltaByteLength+1;

					// Check to see if the processing has been halted
					if (bkgData->forceExit)
						break;
				}

			// When the while loop exits, check to see if we've read to the end. If not, it means there
			// was data at the end that was cut off (ex: in the middle of a time delta Event).
			// NOTE: This should never happen for an alternative data stream but we'll forbid it
			// regardless because we don't want the buffer being modified.
			if(!processedAlternativeStream && (dataReadIndex < static_cast<int>(dataRead)))
				{
				// Copy the data from the end of the buffer to the beginning and leave the read index the
				// way it is. When the next read from the COM happens, it will be offset and then the
				// read index will be reset.
				memcpy((void*)incomingData.get(), (void*)(incomingData.get()+dataReadIndex), dataRead - dataReadIndex);
				dataReadIndex = dataRead - dataReadIndex;
				}

			/* This DEBUG code can be deleted soon.
				It was used to look at all of the values of the events mid-loop.
			
			CString TESTingCheckValues;

			// Add all of the events
			POSITION pos = bkgData->incomingEventList->GetHeadPosition();

			for (int index = 0; index < bkgData->incomingEventList->GetCount(); index++)
				{
				TESTingCheckValues += bkgData->incomingEventList->GetNext(pos).GetString();
				TESTingCheckValues += "\n";
				}
			*/
			}
		else
			{
			// If there was no data read from the COM port then Sleep briefly
			Sleep(100 /*3500*/);
			}
		}

	// Keep a GOTO label here to jump out of the deeply nested loops and exit immediately
	ExitBackgroundCom:

	bkgData->running = false;

	// If this function is exiting then copy all of the non-finalized Events out. The
	// calling function will have the option to process them, knowing that they
	// have not been fully smoothed.
	// NOTE: The smoothed parameter will be filled so that the calling function doesn't
	// have to change things to chart it. However, it will be the original value.
	// The calling function will have to check "finalSmoothed" to know the truth.
	POSITION pos = internalEventList.GetHeadPosition();

	while (pos)
		{
		DynoEvent* tempEvent = &internalEventList.GetNext(pos);

		// If it hasn't been smoothed then move it and just copy the non-smoothed parameter
		if (!tempEvent->finalSmoothed)
			{
			tempEvent->smoothedParameter_sma = tempEvent->originalParameter;
			tempEvent->smoothedParameter_ema = tempEvent->originalParameter;
			bkgData->incomingEventList->AddTail(*tempEvent);
			}
		}

	return(ERROR_SUCCESS);
	}

// This function is called with the CList position of an RPM_drum Event that is just about
// to be finalized. Also, the torque, horsepower, and RPM_engine Events that follow it also
// need to be updated. This function will only be called when there are enough Events
// to allow for graph smoothing, which necessarily means that there is both one before and one
// after it, at the minimum.
void CMainForm::calculateEventValues(CList <DynoEvent, DynoEvent&> *internalEventList, POSITION pos, backgroundComProcessing *bkgData)
	{
	POSITION initialPos = pos;

	DynoEvent* rpmEvent_drum;
	DynoEvent* torqueEvent = nullptr;
	DynoEvent* hpEvent = nullptr;
	DynoEvent* rpmEvent_engine = nullptr;
	double torqueNm_o, torqueNm_ss, torqueNm_se;
	double torqueFtLbs_o, torqueFtLbs_ss, torqueFtLbs_se;

	// Read in the four Events and error-check along the way
	rpmEvent_drum = &internalEventList->GetNext(pos);

	if (pos)
		{
		torqueEvent = &internalEventList->GetNext(pos);

		if (pos && torqueEvent->type == DynoEvent::EventType::Torque)
			{
			hpEvent = &internalEventList->GetNext(pos);
			
			if (pos && hpEvent->type == DynoEvent::EventType::Horsepower)
				{
				rpmEvent_engine = &internalEventList->GetNext(pos);

				if(rpmEvent_engine->type != DynoEvent::EventType::RPM_engine)
					rpmEvent_engine = nullptr;
				}
			}
		}

	// Start with the drum RPM event
	rpmEvent_drum->finalSmoothed = true;	// Mark it as final	

	// If all of the Events were not read then we're still in the beginning stages, so just 
	// exit because the Event has been marked as final.
	if (!(rpmEvent_drum && torqueEvent && hpEvent && rpmEvent_engine))
		{
		return;
		}

	// If the P-Splines parameters are not set then don't use any smoothing
	if (bkgData->psplines_N > 0)
		{
		// TODO: The following P-Splines code needs to be optimized
		std::shared_ptr<double[]> x_values;
		std::shared_ptr<double[]> y_values;

		x_values.reset(new double[bkgData->psplines_N]);
		y_values.reset(new double[bkgData->psplines_N]);

		int x_count = 0;
		int travelDistance = (bkgData->psplines_N - 1) / 2;
		DynoEvent* tempEvent = rpmEvent_drum;

		// Count how far back we can go
		while (tempEvent->previousRPM_drum && (x_count < travelDistance))
			{
			x_count++;
			tempEvent = tempEvent->previousRPM_drum;
			}

		// Starting at the back, roll forward and store the values
		for (x_count = 0; x_count < bkgData->psplines_N; x_count++)
			{
			x_values[x_count] = tempEvent->absTime;
			y_values[x_count] = tempEvent->originalAcceleration;
			
			tempEvent = tempEvent->nextRPM_drum;

			if (!tempEvent)
				break;
			}

		real_1d_array x;
		real_1d_array y;

		x.attach_to_ptr(x_count, x_values.get());
		y.attach_to_ptr(x_count, y_values.get());

		ae_int_t info;
		double v;
		spline1dinterpolant s;
		spline1dfitreport rep;

		//
		// Fit with VERY small amount of smoothing (rho = -5.0)
		// and large number of basis functions (M=50).
		//
		// With such small regularization penalized spline almost fully reproduces function values
		//
		spline1dfitpenalized(x, y, bkgData->psplines_M, bkgData->psplines_RHO, info, s, rep);
		v = spline1dcalc(s, rpmEvent_drum->absTime);

		rpmEvent_drum->smoothedAcceleration = v;												// Use P-Spline

		// Keep a running total of how much different the original and smoothed values are
		bkgData->totalAbsError += abs(rpmEvent_drum->originalAcceleration - rpmEvent_drum->smoothedAcceleration);
		bkgData->totalRegError += rpmEvent_drum->originalAcceleration - rpmEvent_drum->smoothedAcceleration;
		}
	else
		{
		rpmEvent_drum->smoothedAcceleration = rpmEvent_drum->originalAcceleration;				// Use Original
		//	rpmEvent_drum->smoothedAcceleration = (y_values[0]+ y_values[1] + y_values[2]) / 3;		// Use average
		}

	// TODO: Select the appropriate inertiaConstant
	torqueNm_o = rpmEvent_drum->originalAcceleration * bkgData->sessionConfig->inertiaConstant_AWD;		// AA x Inertia constant
	torqueNm_ss = rpmEvent_drum->smoothedAcceleration * bkgData->sessionConfig->inertiaConstant_AWD;
	torqueNm_se = rpmEvent_drum->smoothedAcceleration * bkgData->sessionConfig->inertiaConstant_AWD;

	torqueFtLbs_o = torqueNm_o * Constants.torqueConversion;
	torqueFtLbs_ss = torqueNm_ss * Constants.torqueConversion;
	torqueFtLbs_se = torqueNm_se * Constants.torqueConversion;

	// Modify the torque Event
	torqueEvent->originalParameter = torqueFtLbs_o;					// Put in the torque
	torqueEvent->smoothedParameter_sma = torqueFtLbs_ss;
	torqueEvent->smoothedParameter_ema = torqueFtLbs_se;

	// Modify the HP Event
	hpEvent->originalParameter = bkgData->sessionConfig->dynoToEngineRPMMultiplier * torqueFtLbs_o * rpmEvent_drum->originalParameter / Constants.horsepowerConstant;				// Calculate and put in the horsepower
	hpEvent->smoothedParameter_sma = bkgData->sessionConfig->dynoToEngineRPMMultiplier * torqueFtLbs_ss * rpmEvent_drum->smoothedParameter_sma / Constants.horsepowerConstant;
	hpEvent->smoothedParameter_ema = bkgData->sessionConfig->dynoToEngineRPMMultiplier * torqueFtLbs_se * rpmEvent_drum->smoothedParameter_ema / Constants.horsepowerConstant;
	hpEvent->finalSmoothed = true;	// Mark it as final

	// Modify the engine RPM Event.
	// FOR NOW: Until the engine RPM is better defined, it is done in the main loop.
	// This means that we only have to mark it as finished here.

	rpmEvent_engine->finalSmoothed = true;	// Mark it as final

	// After the engine RPM has been determined, re-calculate torque so that we end up with Engine Torque

	// TODO: This is going directly into the smoothed EMA parameter for graphing. This will change.
//	torqueEvent->smoothedParameter_ema = (hpEvent->smoothedParameter_ema * Constants.horsepowerConstant) / rpmEvent_engine->originalParameter;					// Put in the new torque
//	torqueEvent->finalSmoothed = true;	// Mark it as final
	}

// Parse the file that is open in memory and build the DynoFileDetails structure.
void CMainForm::parseSessionFile()
	{
	m_remoteControl->SetStatus(Constants.PlaybackStatus_File_Parsing);

	// First, clear out any data that may be remaining from a previous file
	loadedFileDetails.Reset(currentFormConfig, dataStream, loadedFileDetails.dataLength);
	
	if (!loadedFileDetails.ParseData(&m_remoteControl->m_progress))
		{
		MessageBox(_T("Could not parse session file."), _T("File error"), MB_ICONERROR);
		}

	m_remoteControl->SetStatus(Constants.PlaybackStatus_Not_started);
	}

// Process the incoming data from the COM port that has been created by the background process.
// There now exists a sequential list of Events, and this function isn't called unless there is at least one Event.
// These will be stored in the current SessionFile, which will be saved after the run is complete.
// This function is called on real-time data for the purpose of populating a DynoFileDetails class.
void CMainForm::processIncomingData()
	{
	MSG msg;
	DynoEvent *showEvent;

	// As long as there are smoothed Events ready to be pulled, keep going.
	// Because it is possible that the background task is adding Events at
	// the same time this function is pulling them, this loop may keep
	// running for longer than it originally looked.
	while ((bkgComProcessMessage.haveSmoothed > 0) && !forceStopProcessing)
		{
		// Check to see if any Events have already been displayed
		if (!bkgComProcessMessage.lastDisplayedPos)
			{
			// If this is the first Event then display the head
			bkgComProcessMessage.lastDisplayedPos = bkgComProcessMessage.incomingEventList->GetHeadPosition();
			showEvent = &bkgComProcessMessage.incomingEventList->GetAt(bkgComProcessMessage.lastDisplayedPos);
			}
		else
			{
			// If this is a subsequent Event the get the one that immediately follows the last one we
			// displayed.
			showEvent = &bkgComProcessMessage.incomingEventList->GetNext(bkgComProcessMessage.lastDisplayedPos);
			}

		// Display it on the screen and decrement the count		
		displayEvent(*showEvent);
		bkgComProcessMessage.readyToGraph--;
		bkgComProcessMessage.haveSmoothed--;


		// Windows message pump while we're in this loop
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			}
		}

	// Check to see if the processing has been halted
	if (forceStopProcessing)
		{
		// If it has then stop the background process as well
		bkgComProcessMessage.forceExit = true;
		}
	}

// Play the currently loaded session by scrolling through the Events
void CMainForm::playLoadedSession(int startingEventIndex)
	{
	MSG msg;

	// Set the position to the first event
	POSITION pos = loadedFileDetails.eventList.FindIndex(startingEventIndex);
	CString displayText;
	int index;

	// Roll through all of the events and execute them
	for (index = startingEventIndex; index < loadedFileDetails.eventList.GetCount(); index++)
		{
		// Windows message pump while we're in this loop
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			}

		// Check to see if the playback has been stopped (assume paused)
		if (forceStopPlayback)
			break;

		// Grab the current Event
		DynoEvent &curEvent = loadedFileDetails.eventList.GetNext(pos);

		// Update the UI to show the next event
		displayEvent(curEvent);

		// Update the slider control and the text
		m_remoteControl->SetFilePos(index);

		displayText.Format(_T("Event #%d:\n%s"), index + 1, curEvent.GetString());
		m_remoteControl->SetText(displayText);

		// Sleep for the number of milliseconds that are specified in the remote control
		Sleep(m_remoteControl->GetSpeedSelection());
		}

	// If the loop exits before it reaches the last Event then the playback
	// was paused (or perhaps stopped)
	if (index < loadedFileDetails.eventList.GetCount())
		{
		m_remoteControl->SetStatus(Constants.PlaybackStatus_Paused);
		}
	else
		{
		m_remoteControl->SetStatus(Constants.PlaybackStatus_Ended);
		}
	}

// Update the UI to reflect the specified Event
void CMainForm::displayEvent(DynoEvent& event)
	{
	// FOR NOW: If the event is not marked as "Final smoothed" then don't show it
	if (!event.finalSmoothed)
		return;

	// Create the string based on the type
	if (event.type == DynoEvent::EventType::NotSet)
		{
		// If the type is not set then ignore it for now
		;
		}
	else if (event.type == DynoEvent::EventType::Timestamp)
		{
		// TODO: Some kind of clock will update here.

		}
	else if (event.type == DynoEvent::EventType::Speed)
		{
		// Speed reading
		// TODO: If the speed is read from a file and not derived from a time delta series
		// then it will be the "originalParameter". Deal with this later if we need to.
		UpdateSpeedometer(event.smoothedParameter_ema);

		// If it's the Speed vs Time chart then create a point
		if (chartShown == Constants.Chart_rpm_spd_vs_time)
			UpdateMainChart(event);
		}
	else if (event.type == DynoEvent::EventType::RPM_drum)
		{
		// Tachometer reading
		UpdateTachometer(event.smoothedParameter_ema);
		
		// Whenever there is an RPM event on the drum, also update the speedometer
		double curSpeed;
		
		// TODO: The original speed can alternatively be displayed
		curSpeed = event.smoothedParameter_ema * currentFormConfig->dynoShaftDiameter * Constants.rpmToMph * (currentFormConfig->units == Constants.Units_metric ? Constants.milesToKm : 1.0);
		UpdateSpeedometer(curSpeed);

		// Update the main chart
		UpdateMainChart(event);
		}
	else if (event.type == DynoEvent::EventType::RPM_engine)
		{

		// Tachometer reading
		// FOR NOW: Don't update the tachometer, quite yet
		//UpdateTachometer(event.smoothedParameter_ema);

		// Update the main chart
		UpdateMainChart(event);
		}
	else if (event.type == DynoEvent::EventType::Torque)
		{
		// If it's the Speed vs Time chart then create a point
		if (chartShown == Constants.Chart_t_hp_vs_rpm)
			UpdateMainChart(event);
		}
	else if (event.type == DynoEvent::EventType::Horsepower)
		{
		// If it's the Speed vs Time chart then create a point
		if (chartShown == Constants.Chart_t_hp_vs_rpm)
			UpdateMainChart(event);
		}
	else if (event.type == DynoEvent::EventType::Voltage)
		{
		// Voltage reading
		UpdateVoltMeter(event.originalParameter);
		}
	else if (event.type == DynoEvent::EventType::Sound)
		{
		// Voltage reading
		UpdateVoltMeter(event.originalParameter);
		}
	else
		{
		// If it isn't recognized then just ignore it for now
		;
		}
	}

// Update the speedometer control with the specific value
void CMainForm::UpdateSpeedometer(double speed)
	{
	// Set the needle colour accordingly
	if(speed < currentFormConfig->speedometerRedline)
		m_speedometerCtrl.SetNeedleColour(getMappedColour(Constants.colour_target_SpdNeedleNormal));
	else
		m_speedometerCtrl.SetNeedleColour(getMappedColour(Constants.colour_target_SpdNeedleHigh));

	// Update the position of the needle
	m_speedometerCtrl.UpdateNeedle(speed);
	}

// Update the tachometer control with the specific value
void CMainForm::UpdateTachometer(double rpm)
	{
	// Set the needle colour accordingly
	if (rpm < currentFormConfig->tachometerRedline)
		m_tachometer.SetNeedleColour(getMappedColour(Constants.colour_target_TachNeedleNormal));
	else
		m_tachometer.SetNeedleColour(getMappedColour(Constants.colour_target_TachNeedleHigh));

	// Update the position of the needle
	m_tachometer.UpdateNeedle(rpm);
	}

// Update the Reah / Lean Graph Background  with specific value percentage

void CMainForm::UpdateRichLean(CString gname, double fill)
{
	if (gname == "RICH")
	{
		m_richGraph.SetRichLeanValue(fill); // Must be between 0-100
		m_leanGraph.SetRichLeanValue(0.0);
		

	}
	else if (gname == "LEAN")
	{
		m_richGraph.SetRichLeanValue(0.0);
		m_leanGraph.SetRichLeanValue(fill); // Must be between 0-100

	}
}


// Update HP/TQ/AFR/TQAREA and ACCEL Values

void CMainForm::UpdateOutputValues(CString pname, CString value)
{
	if (pname == "HP")
	{
		
		m_hpLabel_value.SetWindowTextW(value);
		m_hpLabel_value.SetTextColor(getMappedColour(Constants.colour_target_SpdNeedleNormal));
		m_hpLabel_value.SetBold(true, false);
		m_hpLabel_value.SetFont(_T("Ariel"), 16);
		
	}
	else if (pname == "TQ")
	{
		m_tqLabel_value.SetWindowTextW(value);
		m_tqLabel_value.SetTextColor(getMappedColour(Constants.colour_target_SpdNeedleNormal));
		m_tqLabel_value.SetBold(true, false);
		m_tqLabel_value.SetFont(_T("Ariel"), 16);
		
	}
	else if (pname == "AFR")
	{
		m_afrLabel_value.SetWindowTextW(value);
		m_afrLabel_value.SetTextColor(getMappedColour(Constants.colour_target_SpdNeedleNormal));
		m_afrLabel_value.SetBold(true, false);
		m_afrLabel_value.SetFont(_T("Ariel"), 16);


	}
	else if (pname == "TQAREA")
	{
		m_tqareaLabel_value.SetWindowTextW(value);
		m_tqareaLabel_value.SetTextColor(getMappedColour(Constants.colour_target_SpdNeedleNormal));
		m_tqareaLabel_value.SetBold(true, false);
		m_tqareaLabel_value.SetFont(_T("Ariel"), 16);
	}
	else if (pname == "ACCEL")
	{
		m_accelLabel_value.SetWindowTextW(value);
		m_accelLabel_value.SetTextColor(getMappedColour(Constants.colour_target_SpdNeedleNormal));
		m_accelLabel_value.SetBold(true, false);
		m_accelLabel_value.SetFont(_T("Ariel"), 16);


	}
}

// Update the volt meter control with the specific value
void CMainForm::UpdateVoltMeter(double volts)
	{
	COLORREF needleColour;

	// Set the need colour accordingly.
	// Since there is no stored max value for voltage, wing it for now.
	// If the needle is within 10% of the max then make it red, otherwise green
	if (volts > (m_3DMeterCtrl.m_dMaxValue * .75))
		{
		needleColour = RGB(255, 0, 0);
		}
	else
		{
		needleColour = RGB(0, 255, 0);
		}

	m_3DMeterCtrl.SetNeedleColor(needleColour);


	// Redraw the needle
	m_3DMeterCtrl.UpdateNeedle(volts);
	}

// Update the main chart based on the event
void CMainForm::UpdateMainChart(DynoEvent &event)
	{
	static double lastRPMTimestamp;

	// Update the chart based on the type of event
	if (event.type == DynoEvent::EventType::RPM_drum)
		{
		// NOTE: The copying of the parameter to the smoothed_ema is a hack that is needed
		// for the import function to work properly. At some point in the future, this
		// will be a proper check. There will also be options to show smoothed data,
		// or automatically decided which is the correct value to use.
//		if (!event.smoothedParameter_ema)
//			event.smoothedParameter_ema = event.originalParameter;

		// Check to see which chart is being displayed
		if (chartShown == Constants.Chart_t_hp_vs_rpm)
			{
			// Update the TORQUE_HP vs RPM chart

			// For this chart, we expect Events in the order: Drum RPM, Torque, Horsepower, Engine RPM

			// If the Event is an RPM update then just record it because an RPM reading does not create a point on it's own.
			// It serves only as the X-axis. There could be some additional error checking here to see if there are
			// dangling Torque or HP events, but it shouldn't be necessary
			chart_currentRpm = event.smoothedParameter_ema;
			}
		else
			{
			// Update the RPM vs Time chart.
			// The RPM will also be used to calculate the speed.
			double currentAxisMin, currentAxisMax;
			double timePoint;

			pBottomAxis->GetMinMax(currentAxisMin, currentAxisMax);

			// If this is the first Event then record it as the X-axis (time) baseline
			if (!chart_timeZero)
				{
				chart_timeZero = event.absTime * 1000.0;
				pSeries_drumRpmAtTime->AddPoint(chart_timeZero, event.smoothedParameter_ema);

				// If the point isn't inside of the current axis then shift the axis.
				// Since it is time and we don't expect it to go backwards, just reset
				// to the default (0.0, 10000.0).
				pBottomAxis->SetMinMax(0.0, currentFormConfig->timeAxisMax);
				timePoint = chart_timeZero;
				}
			else
				{
				// Calculate the x-coordinate
				timePoint = event.absTime * 1000.0;		// ABS time is in fractional seconds and multiplying by 1000 brings it to milliseconds

				// If there isn't a point at the current timestamp then create one
				if (timePoint - lastRPMTimestamp)
					{
					pSeries_drumRpmAtTime->AddPoint(timePoint, event.smoothedParameter_ema);

					// If the units are in metric then convert the speed before displaying it
//					pSeries_speedAtTime->AddPoint(timePoint,
//						event.smoothedParameter_ema * currentFormConfig->dynoShaftDiameter * Constants.rpmToMph * (currentFormConfig->units == Constants.Units_metric ? Constants.milesToKm : 1.0));

					// If the point isn't inside of the current axis then shift the axis.
					// This could be really fancy and shift based on what the rate has been, but there
					// is no point in over complicating it because the time axis is more often used
					// for troubleshooting than by the customer.

					// FOR NOW: 
					if (currentFormConfig->autoScrollChart)
						{
						if (timePoint > currentAxisMax)
							{
							// Just shift the screen in the appropriate direction leaving 25 %
							// of the direction it is going open. The remaining 75% will have been
							// in the direction that the point was coming from.
							// pBottomAxis->SetMinMax(timePoint - (Constants.timeAxisMax * .75), timePoint + (Constants.timeAxisMax * .25));
							
							// FOR NOW: Ignore the above because the single-point smooth scrolling looks better. Later, there can
							// be a configuration option that allows for a greater jump, like the 25/75 code that I have above.							
							pBottomAxis->SetMinMax(timePoint + 1.0 - currentFormConfig->timeAxisMax, timePoint + 1.0);
							}
						else if (timePoint < currentAxisMin)
							{
							pBottomAxis->SetMinMax(timePoint - (currentFormConfig->timeAxisMax * .25), timePoint + (currentFormConfig->timeAxisMax * .75));
							}
						}
					}
				}

			// Update the last RPM time
			lastRPMTimestamp = timePoint;
			}
		}
	else if (event.type == DynoEvent::EventType::RPM_engine)
		{
		// FOR NOW: Just pop the engine RPM up on the vs TIME graph
//		if(chartShown == Constants.Chart_rpm_spd_vs_time)
//			pSeries_engineRpmAtTime->AddPoint(event.absTime*1000.0, event.originalParameter);
		}
	else if (event.type == DynoEvent::EventType::Torque)
		{
		// If the Event is a Torque update then check to see if we have a current RPM reading
		if (chart_currentRpm > 0.0)
			{
			// Add the torque point at the current RPM
			// TODO: We'll have a way to choose between showing raw and smoothed points
			pSeries_torqueAtRpm->AddPoint(chart_currentRpm, event.smoothedParameter_sma);
			}
		else
			{
			// TODO: Hitting a Torque with no RPM is an error that should be logged

			}
		}
	else if (event.type == DynoEvent::EventType::Horsepower)
		{
		// If the Event is a Horsepower update then check to see if we have a current RPM reading
		if (chart_currentRpm > 0.0)
			{
			// Add the horsepower point at the current RPM
			// TODO: We'll have a way to choose between showing raw and smoothed points
			pSeries_hpAtRpm->AddPoint(chart_currentRpm, event.smoothedParameter_sma);
			}
		else
			{
			// TODO: Hitting an HP with no RPM is an error that should be logged

			}
		}
	else
		{
		// If it isn't relevant to this chart then just ignore it for now
		;
		}
	}

/* NOTE: Saving this old version for now. It relied on the system time, which we will no longer
		 be doing in the typical case. It will all be geared off of the time delta.

	// Update the main chart based on the event
	void CMainForm::UpdateMainChart(DynoEvent& event)
		{
		static ULARGE_INTEGER lastRPMTimestamp;
		static ULARGE_INTEGER lastSpeedTimestamp;

		// Update the chart based on the type of event
		if (event.type == DynoEvent::EventType::RPM_drum)
			{
			// Check to see which chart is being displayed
			if (chartShown == Constants.Chart_t_hp_vs_rpm)
				{
				// Update the TORQUE_HP vs RPM chart

				// For this chart, we expect Events in the order: Drum RPM, Torque, Horsepower, Engine RPM

				// If the Event is an RPM update then just record it because an RPM reading does not create a point on it's own.
				// It serves only as the X-axis. There could be some additional error checking here to see if there are
				// dangling Torque or HP events, but it shouldn't be necessary
				chart_currentRpm = event.smoothedParameter_ema;
				}
			else
				{
				// Update the RPM vs Time chart.
				// The RPM will also be used to calculate the speed.
				double currentAxisMin, currentAxisMax;

				pBottomAxis->GetMinMax(currentAxisMin, currentAxisMax);

				// Calculate the difference between the two times
				FILETIME ft;
				ULARGE_INTEGER curTime, difftime;

				if (!SystemTimeToFileTime(&event.timestamp, &ft))
					{
					// TODO: Log this type of error
					return;
					}

				// Convert FILETIME to ULARGE_INTEGER and then calculate the difference
				curTime.LowPart = ft.dwLowDateTime;
				curTime.HighPart = ft.dwHighDateTime;

				// If this is the first Event then record it as the X-axis (time) baseline
				if (!chart_timeZero.QuadPart)
					{
					chart_timeZero.QuadPart = curTime.QuadPart;
					pSeries_drumRpmAtTime->AddPoint(0.0, event.smoothedParameter_ema);

					// If the point isn't inside of the current axis then shift the axis.
					// Since it is time and we don't expect it to go backwards, just reset
					// to the default (0.0, 10000.0).
					pBottomAxis->SetMinMax(0.0, Constants.timeAxisMax);
					}
				else
					{
					// Calculate the difference between them
					difftime.QuadPart = curTime.QuadPart - lastRPMTimestamp.QuadPart;

					// If there isn't a point at the current timestamp then create one
					if (difftime.QuadPart)
						{
						// We want to base our X-axis on the different in time between start and now,
						// and then convert to milliseconds (unit is 100-nanoseconds)
						difftime.QuadPart = (curTime.QuadPart - chart_timeZero.QuadPart) / 10000;

						double timePoint = static_cast<double>(difftime.QuadPart);

						// FOR NOW: This is ignoring everything to do with the chart
						timePoint = event.absTime;

						pSeries_drumRpmAtTime->AddPoint(timePoint, event.smoothedParameter_ema);

						// If the units are in metric then convert the speed before displaying it
						pSeries_speedAtTime->AddPoint(timePoint,
							event.smoothedParameter_ema * currentFormConfig->dynoShaftDiameter * Constants.rpmToMph * (currentFormConfig->units == Constants.Units_metric ? Constants.milesToKm : 1.0));

						// If the point isn't inside of the current axis then shift the axis.
						// This could be really fancy and shift based on what the rate has been, but there
						// is no point in over complicating it because the time axis is more often used
						// for troubleshooting than by the customer.

						// FOR NOW: 
						if (currentFormConfig->autoScrollChart)
							{
							if (timePoint > currentAxisMax)
								{
								// Just shift the screen in the appropriate direction leaving 25 %
								// of the direction it is going open. The remaining 75% will have been
								// in the direction that the point was coming from.
								// pBottomAxis->SetMinMax(timePoint - (Constants.timeAxisMax * .75), timePoint + (Constants.timeAxisMax * .25));

								// FOR NOW: Ignore the above because the single-point smooth scrolling looks better. Later, there can
								// be a configuration option that allows for a greater jump, like the 25/75 code that I have above.							
								pBottomAxis->SetMinMax(timePoint + 1.0 - Constants.timeAxisMax, timePoint + 1.0);
								}
							else if (timePoint < currentAxisMin)
								{
								pBottomAxis->SetMinMax(timePoint - (Constants.timeAxisMax * .25), timePoint + (Constants.timeAxisMax * .75));
								}
							}
						}
					}

				// Update the last RPM time
				lastRPMTimestamp = curTime;
				}
			}
		else if (event.type == DynoEvent::EventType::RPM_engine)
			{
			// FOR NOW: Just pop the engine RPM up on the vs TIME graph
			if (chartShown == Constants.Chart_rpm_spd_vs_time)
				pSeries_engineRpmAtTime->AddPoint(event.smoothedParameter_ema, event.originalParameter);
			}
		else if (event.type == DynoEvent::EventType::Torque)
			{
			// If the Event is a Torque update then check to see if we have a current RPM reading
			if (chart_currentRpm > 0.0)
				{
				// Add the torque point at the current RPM
				// TODO: We'll have a way to choose between showing raw and smoothed points
				pSeries_torqueAtRpm->AddPoint(chart_currentRpm, event.smoothedParameter_ema);
				}
			else
				{
				// TODO: Hitting a Torque with no RPM is an error that should be logged

				}
			}
		else if (event.type == DynoEvent::EventType::Horsepower)
			{
			// If the Event is a Horsepower update then check to see if we have a current RPM reading
			if (chart_currentRpm > 0.0)
				{
				// Add the horsepower point at the current RPM
				// TODO: We'll have a way to choose between showing raw and smoothed points
				pSeries_hpAtRpm->AddPoint(chart_currentRpm, event.smoothedParameter_ema);
				}
			else
				{
				// TODO: Hitting an HP with no RPM is an error that should be logged

				}
			}
		else
			{
			// If it isn't relevant to this chart then just ignore it for now
			;
			}
		}
*/

// Bring up the COM port testing dialog
void CMainForm::OnConnectTestcomports()
	{
	DynoComPortDialog dcp;

	// Make a local copy of the entire configuration
	dcp.localCfg = *currentFormConfig;

	// Pass in COM port group
	dcp.globalPorts = &DynoApp->currentComPorts;

	if (dcp.DoModal() != IDOK)
		return;

	// If the user saved the changes then bring them into effect

	// Start by copying the changes back out
	*currentFormConfig = dcp.localCfg;

	// Save the configuration
	DynoApp->saveDynoConfig(true);

	// If there are any on-screen changes to make then do them here
	}

// Bring up the Connection Settings dialog
void CMainForm::OnConnectSettings()
	{
	ConnectionSettings cs;

	// Make a local copy of the entire configuration
	cs.localCfg = *currentFormConfig;

	// Pass in COM port group
	cs.globalPorts = &DynoApp->currentComPorts;

	if (cs.DoModal() != IDOK)
		return;

	// If the user saved the changes then bring them into effect

	// Start by copying the changes back out
	*currentFormConfig = cs.localCfg;

	// Save the configuration
	DynoApp->saveDynoConfig(true);

	// If there are any on-screen changes to make then do them here
	}

// Easter-egg car sounds
void CMainForm::OnRButtonDblClk(UINT nFlags, CPoint point)
	{
	// TODO: Add your message handler code here and/or call default

	PlaySound(MAKEINTRESOURCEW(IDR_StartCar), NULL, SND_RESOURCE | SND_ASYNC);

	CFormView::OnRButtonDblClk(nFlags, point);
	}

// Authorization and support functions

void CMainForm::OnAuthorizationAutoauthorize()
	{
	// Toggle the auto authorization setting
	currentFormConfig->autoAuthorize = !currentFormConfig->autoAuthorize;
	
	// Save the configuration
	DynoApp->saveDynoConfig(true);
	}

// Have the check mark reflect the current option for auto authorization
void CMainForm::OnUpdateAuthorizationAutoauthorize(CCmdUI* pCmdUI)
	{
	pCmdUI->SetCheck(currentFormConfig->autoAuthorize);
	}

// TODO: Manual authorization may need to bring up some kind of dialog, etc...
void CMainForm::OnAuthorizationManualauthorize()
	{

	}

// Bring up the dialog to assist with contacting support
void CMainForm::OnAuthorizationContactsupport()
	{


	}


void CMainForm::OnAuthorizationLivechat()
	{

	}

// Bring up the Authorization Settings screen
void CMainForm::OnAuthorizationSettings()
	{
	
	}

// Connection functions

void CMainForm::OnConnectAutoconnect()
	{
	// Toggle the auto polling setting
	currentFormConfig->autoStartPolling = !currentFormConfig->autoStartPolling;

	// Save the configuration
	DynoApp->saveDynoConfig(true);
	}

// Have the check mark reflect the current option for auto polling
void CMainForm::OnUpdateConnectAutoconnect(CCmdUI* pCmdUI)
	{
	pCmdUI->SetCheck(currentFormConfig->autoStartPolling);
	}

// Enumerate the available COM ports and add them to the pop-up menu
void CMainForm::OnConnectIdentifyports()
	{

	bool enumResult = DynoApp->currentComPorts.enumerateComPorts();
	updateComMenu();
	}

// Enable the polling item if there is a COM open but no ports are being polled
void CMainForm::OnUpdateConnectStartpolling(CCmdUI* pCmdUI)
	{	
	pCmdUI->Enable((openPortIndex > -1) && (m_pollTimer == 0));
	}

void CMainForm::OnUpdateConnectDiagnostics(CCmdUI* pCmdUI)
	{
	pCmdUI->Enable((openPortIndex > -1) && (m_pollTimer == 0));
	}


// Pause the timer that is doing the polling
void CMainForm::OnConnectPauseAllPolling()
	{
	// Kill the timer if it's running
	if (m_pollTimer > 0)
		{
		KillTimer(m_pollTimer);
		m_pollTimer = 0;
		}

	// Halt any background processing
	forceStopProcessing = true;
	bkgComProcessMessage.forceExit = true;

	// Record the "stop time" in the Session details.
	// NOTE: The recording of the end time may be done by the Event system at some point, so this
	// time stamping may not be done here forever.
	GetLocalTime(&recordFileDetails.endTime);


	CMenu* connectMenu, * openPortMenu;

	// Start by finding the "Connect" menu
	if (!(connectMenu = findMenu(CString(Constants.String_menu_connect.c_str()), DynoApp->m_pMainWnd->GetMenu())))
		return;

	// Inside "Connect", find the "Start Polling" menu
	if (!(openPortMenu = findMenu(CString(Constants.String_menu_openPort.c_str()), connectMenu)))
		return;

	// Roll through all of the items and remove the check mark
	for (int index = openPortMenu->GetMenuItemCount(); index; index--)
		{
		// Remove the check mark beside the menu item, regardless of whether it had been checked or not
		openPortMenu->CheckMenuItem(WM_USER+index+1, MF_UNCHECKED);
		}

	// Close the open COM ports
	DynoApp->currentComPorts.Close(0);
	openPortIndex = -1;

	// Set the title of the doc
	updateWindowTitle();
	}

// Enable the control if the timer is active
void CMainForm::OnUpdateConnectPauseallpolling(CCmdUI* pCmdUI)
	{
	pCmdUI->Enable(m_pollTimer == Constants.timer_polling);
	}

void CMainForm::updateComMenu()
	{
	CMenu* connectMenu, * openPortMenu;

	// Start by finding the "Connect" menu
	if (!(connectMenu = findMenu(CString(Constants.String_menu_connect.c_str()), DynoApp->m_pMainWnd->GetMenu())))
		return;

	// Inside "Connect", find the "Open Port" menu
	if (!(openPortMenu = findMenu(CString(Constants.String_menu_openPort.c_str()), connectMenu)))
		return;

	int originalCount = openPortMenu->GetMenuItemCount();

	// Start by removing all of the ports in the list
	while (originalCount = openPortMenu->GetMenuItemCount())
		{
		openPortMenu->RemoveMenu(0, MF_BYPOSITION);
		}

	// Add each of the COM ports that were discovered
	CString tempString;

	for (const auto& port : DynoApp->currentComPorts.portAndNames)
		{
		#pragma warning(suppress: 26489)
		tempString.Format(_T("COM%u <%s>\n"), port.first, port.second.c_str());
		originalCount++;

		if (!openPortMenu->AppendMenuW(MF_STRING, WM_USER + originalCount + 1, tempString))
			{
			// TODO: Log an error than indicates the menu could not be created
			AfxMessageBox(_T("System error: Could not expand menu."), MB_ICONERROR);
			}
		}
	}
/*
// Called when the default port is selected
// TODO: This will be changed or removed later
void CMainForm::OnStartpollingDefaultport()
	{
	// Toggle the value and then update the check mark
	chart_lastRadsPerSecond = 0.0;
	m_pollTimer = SetTimer(Constants.timer_polling, currentFormConfig->pollingInterval, NULL);

	updateWindowTitle();
	}
*/


// The dynamically created menu for starting COM polling will link to this function.
// The nID specifies which item in the list has been selected.
void CMainForm::OnMenuClickRange(UINT nID)
	{
	CMenu* connectMenu, * openPortMenu;

	// Start by finding the "Connect" menu
	if(!(connectMenu = findMenu(CString(Constants.String_menu_connect.c_str()), DynoApp->m_pMainWnd->GetMenu())))
		return;

	// Inside "Connect", find the "Start Polling" menu
	if (!(openPortMenu = findMenu(CString(Constants.String_menu_openPort.c_str()), connectMenu)))
		return;

	// Put a check mark beside the menu item
	DWORD oldState = openPortMenu->CheckMenuItem(nID, MF_CHECKED);

	// If it hadn't already been checked 
	if (oldState == 0)
		{
		// Open this port
		int check = WM_USER;
		int check_2 = nID - WM_USER;
		openPortIndex = DynoApp->currentComPorts.Open(DynoApp->currentComPorts.portAndNames[nID - WM_USER - 2].first, currentFormConfig->baudRate);

		// If the port was opened then the other functions will be enabled, otherwise return an error.
		// Display the system error message.
		// TODO: Change the error message to something VZ and log the real error.
		if (openPortIndex == -1)
			{
			openPortMenu->CheckMenuItem(nID, MF_UNCHECKED);		// Uncheck it since it isn't polling

			// Get the error message
			CString errorMessage = DynoApp->currentComPorts.lastErrorString;

			// Add it to the log then display it on the screen
			//log(errorMessage);
			MessageBox(errorMessage, _T("Error opening COM Port"), MB_ICONERROR);

			// Exit
			return;
			}

		updateWindowTitle();
		}
	else
		{
		// Don't worry about dealing with a click for an open port, yet.
		// Maybe in the future, we'll close this port.

		}
	}



void CMainForm::OnConnectStartpolling()
	{
	// Start polling
	if (m_pollTimer < 1)
		{
		chart_lastRadsPerSecond = 0.0;

		// Verify that the process is not already running
		if (bkgComProcessMessage.running)
			{
			MessageBox(_T("Background thread already running."), _T("COM Processing error"), MB_ICONERROR);
			return;
			}

		// Record the current system time in the Session details as the start time
		GetLocalTime(&recordFileDetails.startTime);

		// Set up the process and start running it in the background (10 is the default smoothing windows size for now)
		bkgComProcessMessage.init(&recordFileDetails.eventList, currentFormConfig, &DynoApp->currentComPorts, openPortIndex);

		bkgComProcessMessage.psplines_N = 23;
		bkgComProcessMessage.psplines_M = 58;
		bkgComProcessMessage.psplines_RHO = 6;


		bkgComProcessMessage.comThread = AfxBeginThread(backgroundPollCOMPort, (LPVOID)&bkgComProcessMessage);

		// Star the polling timer
		m_pollTimer = SetTimer(Constants.timer_polling, currentFormConfig->pollingInterval, NULL);
		updateWindowTitle();
		}




	}


// Oddly, the index of top-level menus in MDI Child Frames seem to change. The only
// time I've actually seen it is when I try to use a GetSubMenu() on the top-level
// during the initialization. There seems to be an extra menu that is "" at position 0.
// Just to be extra-safe, rather than put in a workaround for that specific case,
// I'm using a function to find the menu by name without knowing or caring about
// the position.
CMenu* CMainForm::findMenu(CString menuName, CMenu* root)
	{
	CString tempString;

	// Roll through all of the sub-menus and compare names. If there is a match then return.
	for (int index = 0, itemCount = root->GetMenuItemCount(); index < itemCount; index++)
		{
		if (root->GetMenuStringW(index, tempString, MF_BYPOSITION) > 0)
			{
			// Compare the strings
			if (!menuName.Compare(tempString))
				{
				// Return a pointer to the sub-menu
				return root->GetSubMenu(index);
				}
			}
		}

	// If it makes it this far then no match was found
	return nullptr;
	}


// Toggle the meter value border display
void CMainForm::OnViewMetervalueborders()
	{
	currentFormConfig->showCircularMeterValueBorders = !currentFormConfig->showCircularMeterValueBorders;

	// Redraw the control
	m_speedometerCtrl.SetDrawNumberBorder(currentFormConfig->showCircularMeterValueBorders);
	m_tachometer.SetDrawNumberBorder(currentFormConfig->showCircularMeterValueBorders);
	}

// Update the check mark for the meter value border display
void CMainForm::OnUpdateViewMetervalueborders(CCmdUI* pCmdUI)
	{
	pCmdUI->SetCheck(currentFormConfig->showCircularMeterValueBorders);
	}

// Double clicking the Speedometer
void CMainForm::OnStnDblclickSpeedometer()
	{
	UpdateRichLean(CString(_T("RICH")),5);
	UpdateOutputValues(CString(_T("HP")), CString(_T("500/300.55")));
	UpdateOutputValues(CString(_T("TQ")), CString(_T("625/200.01")));
	UpdateOutputValues(CString(_T("AFR")), CString(_T("14.1/25.01/35.2")));
	}

// Double clicking the Tachometer
void CMainForm::OnStnDblclickTachometer()
	{
	UpdateRichLean(CString(_T("LEAN")), 50);
	UpdateOutputValues(CString(_T("TQAREA")), CString(_T("58462500 U2")));
	UpdateOutputValues(CString(_T("ACCEL")), CString(_T("6.7 m/s2")));
	
	}

// Customization is only available when it isn't polling
void CMainForm::OnUpdateViewCustomizeinterface(CCmdUI* pCmdUI)
	{
	pCmdUI->Enable(!m_pollTimer);
	}

// Re-draw all of the components on the screen
void CMainForm::RedrawEverything()
	{
	// Create a brush for the background colour and use the value in the map
	m_brush.DeleteObject();
	m_brush.CreateSolidBrush(getMappedColour(Constants.colour_target_Background));

	// Redraw the other components
	DrawGraphLabels();
	DrawCharts();
	m_bigname.SetBackgroundColor(getMappedColour(Constants.colour_target_Background));

	Invalidate();
	}

// Bring up the Customize Interface dialog
void CMainForm::OnViewCustomizeinterface()
	{
	CCustomizeUIDialog cd;

	// Prepare the dialog
	// FOR NOW: Just populate the target list manually

	// Point to the parent-copy of the colour map names
	cd.colourNamesMap = &colourNamesMap;

	// Make a local copy of the entire configuration
	cd.localCfg = *currentFormConfig;


	// Bring up the customization dialog
	if (cd.DoModal() != IDOK)
		return;

	// If nothing has been modified then just return
	if (!cd.isModified())
		return;

	// If the user saved the changes then bring them into effect
	
	// Start by copying the changes back out
	*currentFormConfig = cd.localCfg;

	// Save the configuration
	DynoApp->saveDynoConfig(true);

	// Redraw all of the controls
	RedrawEverything();
	}

// Prompt for a .VZD file to load a saved data stream from
void CMainForm::OnFileOpen()
	{
	CFileDialog dlg(TRUE, Constants.String_datafile_ext.c_str(), Constants.String_datafile_wc_ext.c_str(), OFN_OVERWRITEPROMPT, Constants.String_datafile_filter.c_str());

	auto result = dlg.DoModal();

	if (result == IDOK)
		{
		CString path = dlg.GetPathName();

		auto fileOpened = dataInputFile.Open(path, CFile::modeRead);

		// If the file opens properly then read in all of the data and close it.
		// The "Close File" function from the menu just releases the memory.
		if (fileOpened)
			{
			// Change the title of this window
			streamingInputFile = true;

			// Create a Session-specific configuration
			loadedFileDetails.Reset(&loadedFileConfig);

			// Finish off
			if (loadedFileDetails.loadSession(dataInputFile))
				{
				CString displayString;

				// Set the file size to be the number of events
				m_remoteControl->SetFileSize(loadedFileDetails.eventList.GetCount() - 1);

				// Show the replay control
				m_remoteControl->ShowWindow(SW_SHOW);
				displayString.Format(_T("Loaded Session replay ready. Total events: #%d"), loadedFileDetails.eventList.GetCount());
				m_remoteControl->SetText(displayString);

				// Keep the title for display in the title bar
				loadedFileDetails.fileTitle = dataInputFile.GetFileTitle();

				dataInputFile.Close();		// Close the file				
				updateWindowTitle();		// Update the window title

				// Switch the Configuration to the one that belongs to the file
				currentFormConfig = &loadedFileConfig;

				// Clear all of the controls
				UpdateSpeedometer(0.0);
				UpdateTachometer(0.0);
				UpdateVoltMeter(0.0);
				UpdateRichLean(_T("RICH"),0.0);
				UpdateRichLean(_T("LEAN"), 0.0);
				

				

				m_mainChart.RemoveAllSeries();

/*
				// Clear the series that are on the graph
				if (chartShown == Constants.Chart_rpm_spd_vs_time)
					{
					pSeries_torqueAtRpm->ClearSerie();
					pSeries_hpAtRpm->ClearSerie();
					}
				else if (chartShown == Constants.Chart_rpm_spd_vs_time)
					{
					pSeries_drumRpmAtTime->ClearSerie();
					pSeries_engineRpmAtTime->ClearSerie();
					pSeries_speedAtTime->ClearSerie();
					}
*/
				// Redraw all of the controls
				RedrawEverything();
				}
			else
				{
				MessageBox(_T("Error reading file contents."), _T("File read error"), MB_ICONERROR);
				}
			}
		else
			{
			MessageBox(_T("Could not open replay file."), _T("File open error"), MB_ICONEXCLAMATION);
			}
		}
	else
		{
		// If the file wasn't opened then figure out why
		auto dlgError = CommDlgExtendedError();

		// If the user pressed Cancel then ignore it, otherwise log the error
		if (dlgError)
			{
			MessageBox(_T("Error creating File Open dialog"));
			}

		streamingInputFile = false;
		}
	}

// Import a human-readable text-based Session file.
void CMainForm::OnFileImport()
	{
	CFileDialog dlg(TRUE, Constants.String_importfile_ext.c_str(), Constants.String_importfile_wc_ext.c_str(), OFN_OVERWRITEPROMPT, Constants.String_importfile_filter.c_str());

	auto result = dlg.DoModal();

	if (result == IDOK)
		{
		CString path = dlg.GetPathName();

		auto fileOpened = dataInputFile.Open(path, CFile::modeRead);

		// If the file opens properly then read in all of the data and close it.
		// The "Close File" function from the menu just releases the memory.
		if (fileOpened)
			{
			UINT size;
			UINT bytesRead;

			// Rewind the file pointer
			dataInputFile.SeekToBegin();

			size = static_cast<UINT>(dataInputFile.GetLength());

			// Grab a large enough chunk of memory to read in the entire file
			dataStream.reset(new char[size]);

			// Load the contents of the file and send it off to be parsed
			bytesRead = dataInputFile.Read(&dataStream[0], size);

			// Adjust the controls based on whether any data was read
			if (bytesRead > 1)
				{
				CString displayString;

				// Change the title of this window
				streamingInputFile = true;
				loadedFileDetails.dataLength = bytesRead;

				// Finish off
				parseSessionFile();			// Parse the session file

				// Set the file size to be the number of events
				m_remoteControl->SetFileSize(loadedFileDetails.eventList.GetCount() - 1);

				// Show the replay control
				m_remoteControl->ShowWindow(SW_SHOW);
				displayString.Format(_T("Imported Session replay ready. Total events: #%d"), loadedFileDetails.eventList.GetCount());
				m_remoteControl->SetText(displayString);

				// TODO: If the file is longer than the number of bytes read then deal with it later
				if (bytesRead != size)
					{
					MessageBox(_T("Could not read entire file contents."), _T("File read warning"), MB_ICONEXCLAMATION);
					}
				}
			else
				{
				// No data was read
				MessageBox(_T("Could not read data from file."), _T("File error"), MB_ICONASTERISK);
				}

			// Keep the title for display in the title bar
			loadedFileDetails.fileTitle = dataInputFile.GetFileTitle();

			dataInputFile.Close();		// Close the file
			updateWindowTitle();		// Update the window title
			}
		else
			{
			MessageBox(_T("Could not open replay file."), _T("File open error"), MB_ICONEXCLAMATION);
			}
		}
	else
		{
		// If the file wasn't opened then figure out why
		auto dlgError = CommDlgExtendedError();

		// If the user pressed Cancel then ignore it, otherwise log the error
		if (dlgError)
			{
			MessageBox(_T("Error creating File Import dialog"));
			}

		streamingInputFile = false;
		}
	}

void CMainForm::OnUpdateFileImport(CCmdUI* pCmdUI)
	{
	pCmdUI->Enable(!streamingInputFile);
	}

// Close the file(s) that are open
void CMainForm::OnFileClose()
	{
	// If there is a replay file open the close it
	if (streamingInputFile)
		{
		currentFormConfig = &DynoApp->CurrentDynoConfiguration;		// Point to the parent app configuration

		// Release the memory used to store the current file
		dataStream.reset();

		streamingInputFile = false;

		// Clear all of the controls
		UpdateSpeedometer(0.0);
		UpdateTachometer(0.0);
		UpdateVoltMeter(0.0);
		UpdateRichLean(CString(_T("RICH")), 0.0);
		UpdateRichLean(CString(_T("LEAN")), 0.0);
		 

		m_mainChart.RemoveAllSeries();

/*
		// Clear the series that are on the graph
		pSeries_torqueAtRpm->ClearSerie();
		pSeries_hpAtRpm->ClearSerie();
		pSeries_drumRpmAtTime->ClearSerie();
		pSeries_engineRpmAtTime->ClearSerie();
		pSeries_speedAtTime->ClearSerie();
*/
		// Hide the replay control
		m_remoteControl->ShowWindow(SW_HIDE);

		// Redraw all of the controls
		RedrawEverything();
		}
		
	// If there is a recording file open the close it
	if (recording)
		{
		recording = false;

		// Close the file
		dataOutputFile.Close();
		}

	// Update the title
	updateWindowTitle();
	}

void CMainForm::OnUpdateFileOpen(CCmdUI* pCmdUI)
	{
	pCmdUI->Enable(!streamingInputFile);
	}

void CMainForm::OnUpdateFileClose(CCmdUI* pCmdUI)
	{
	pCmdUI->Enable(streamingInputFile);
	}

// Play the file that is currently open
void CMainForm::OnFileReplay()
	{
	// Update the UI control to indicate we're starting
	m_remoteControl->SetStatus(Constants.PlaybackStatus_Playing);

	// TODO: Later, when we replay a session, we'll load the configuration to show what it looked like at that time,
	// including the colours, speed settings, red-line, etc.
	// There can be an option to use the current colour scheme or other numbers, but reproducibility is important.
	// ---> loadTemporaryConfiguration();
	// (*) This may happen when the file is opened, rather than here.

	// Send the data through the UI as if polling were happening

	do
		{
		// Play the loaded session file continuously unless it is not auto-looping or it is paused
		playLoadedSession(m_remoteControl->GetPosition());

		if (m_remoteControl->playLoop() && (m_remoteControl->GetStatus() == Constants.PlaybackStatus_Ended))
			{
			m_remoteControl->SetFilePos(0);
			m_remoteControl->SetStatus(Constants.PlaybackStatus_Playing);
			}

		} while (m_remoteControl->playing);

	// TODO: Here, we'll restore the original configuration

	// Display whether playback is paused or ended
	CString displayString;
	POSITION pos = loadedFileDetails.eventList.FindIndex(m_remoteControl->GetPosition());

	// Double-check that the position is valid
	if (!pos)
		{
		displayString.Format(_T("Invalid playback position"));
		}
	else
		{
		DynoEvent& curEvent = loadedFileDetails.eventList.GetNext(pos);

		if (m_remoteControl->GetStatus() == Constants.PlaybackStatus_Paused)
			displayString.Format(_T("Playback paused at event #%d:\n%s"), m_remoteControl->GetPosition()+1, curEvent.GetString());
		else
			displayString.Format(_T("Playback complete at event #%d:\n%s"), m_remoteControl->GetPosition()+1, curEvent.GetString());

		// Update the screen control
		m_remoteControl->SetText(displayString);
		}
	}

void CMainForm::OnUpdateFileReplay(CCmdUI* pCmdUI)
	{
	pCmdUI->Enable(streamingInputFile);
	}

// Calculate and show details about the open file
void CMainForm::OnFileExamineSession()
	{
	CFileDetailsDialog cfdd;

	cfdd.fileDetails = &loadedFileDetails;

	(void)cfdd.DoModal();
	}

void CMainForm::OnUpdateFileExamineSession(CCmdUI* pCmdUI)
	{
	pCmdUI->Enable(streamingInputFile);
	}

// The Play Button on the Session Replay remote control was pressed
void CMainForm::OnBnClickedReplayControlPlayButton()
	{
	int status = m_remoteControl->GetStatus();

	// If it's already playing then pause it
	if (status == Constants.PlaybackStatus_Playing)
		{
		forceStopPlayback = true;
		return;
		}
	else if (status == Constants.PlaybackStatus_Paused)
		{
		// If it was paused then resume from where it was 
		forceStopPlayback = false;
		}
	else if(status == Constants.PlaybackStatus_Ended)
		{
		// If the playback had ended then set the position to the beginning before replaying
		m_remoteControl->SetFilePos(0);	
		forceStopPlayback = false;
		}

	OnFileReplay();
	}

// The Close File button on the Session Replay remote control was pressed
void CMainForm::OnBnClickedReplayControlCloseButton()
	{
	// Call the local Close() function
	OnFileClose();
	}

// Zero-out all of the meters and clear the graphs
void CMainForm::OnViewReset()
	{
	// Clear the internal variables
	chart_lastRadsPerSecond = 0.0;
	chart_currentRpm = 0.0;

	// Clear all of the controls
	UpdateSpeedometer(0.0);
	UpdateTachometer(0.0);
	UpdateVoltMeter(0.0);
	UpdateRichLean(CString(_T("RICH")),0.0);
	UpdateRichLean(CString(_T("LEAN")), 0.0);

	// Reset back to point 0,0
	if (chartShown == Constants.Chart_t_hp_vs_rpm)
		{
		// Clear the series that are on the graph
		pSeries_torqueAtRpm->ClearSerie();
		pSeries_hpAtRpm->ClearSerie();

		pBottomAxis->SetMinMax(0, currentFormConfig->rpmMax);				// RPM 0 - 8000 by default
		pLeftAxis->SetMinMax(0, currentFormConfig->torqueMax);				// Torque 0 - 500 by default
		pRightAxis->SetMinMax(0, currentFormConfig->horsepowerMax);			// HP 0 - 500 by default
		}
	else if (chartShown == Constants.Chart_rpm_spd_vs_time)
		{
		// Clear the series that are on the graph
		pSeries_drumRpmAtTime->ClearSerie();
		pSeries_engineRpmAtTime->ClearSerie();
		pSeries_speedAtTime->ClearSerie();

		chart_timeZero = 0.0;
		pBottomAxis->SetMinMax(0, currentFormConfig->timeAxisMax);
		pLeftAxis->SetMinMax(0, currentFormConfig->rpmMax);					// RPM 0 - 8000 by default
		pRightAxis->SetMinMax(0, currentFormConfig->speedometerMax);		// Speed 0 - 200 by default
		}

	// If the Session Reply widget is open then reset it to the beginning
	if (streamingInputFile)
		{
		CString displayString;

		displayString.Format(_T("Session replay reset. Total events: #%d"), loadedFileDetails.eventList.GetCount());
		m_remoteControl->SetText(displayString);
		m_remoteControl->SetFilePos(0);
		}
	}

// Move the chart to the original starting position
void CMainForm::OnViewJumptoorigin()
	{
	// Depending on which chart is shown, reset the axes accordingly
	if (chartShown == Constants.Chart_t_hp_vs_rpm)
		{
		pBottomAxis->SetMinMax(0, currentFormConfig->rpmMax);			// Bottom axis (RPM)
		pLeftAxis->SetMinMax(0, currentFormConfig->torqueMax);			// Left axis (Torque)
		pRightAxis->SetMinMax(0, currentFormConfig->horsepowerMax);		// Right axis (Horsepower)
		}
	else if (chartShown == Constants.Chart_rpm_spd_vs_time)
		{
		pBottomAxis->SetMinMax(0, currentFormConfig->timeAxisMax);		// Bottom axis (Time)
		pLeftAxis->SetMinMax(0, currentFormConfig->rpmMax);				// Left axis (Torque)
		pRightAxis->SetMinMax(0, currentFormConfig->speedometerMax);	// Right axis (Speed)
		}
	}

// Save the current session
void CMainForm::OnFileSave()
	{
	// FOR NOW: Just call Save As...
	OnFileSaveAs();
	}

void CMainForm::OnUpdateFileSave(CCmdUI* pCmdUI)
	{
	pCmdUI->Enable(!m_pollTimer && (recordFileDetails.eventList.GetCount() || loadedFileDetails.eventList.GetCount()));
	}

// Save the current recorded session
void CMainForm::OnFileSaveAs()
	{
	CFileDialog dlg(FALSE, Constants.String_datafile_ext.c_str(), Constants.String_datafile_wc_ext.c_str(), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, Constants.String_datafile_filter.c_str());

	auto result = dlg.DoModal();

	if (result == IDOK)
		{
		CString path = dlg.GetPathName();

		if (dataOutputFile.Open(path, CFile::modeCreate | CFile::modeWrite))
			{
			DynoFileDetails* dfd;

			// If the recordFile is empty then it must have been imported
			dfd = (recordFileDetails.eventList.GetCount() ? &recordFileDetails : &loadedFileDetails);

			// Point to the configuration that is being used right now
			dfd->currentFormConfig = currentFormConfig;

			if (!dfd->saveSession(dataOutputFile))
				{
				MessageBox(_T("Error saving session history."), _T("Session save error"), MB_ICONERROR);
				}

			dataOutputFile.Close();		// Close the recording file
			}
		else
			{
			MessageBox(_T("Could not open output file."), _T("Session save error"), MB_ICONERROR);
			}
		}
	else
		{
		// If the file wasn't opened then figure out why
		auto dlgError = CommDlgExtendedError();

		// If the user pressed Cancel then ignore it, otherwise log the error
		if (dlgError)
			{
			MessageBox(_T("Error creating File Save As dialog"));
			}
		}
	}

void CMainForm::OnUpdateFileSaveAs(CCmdUI* pCmdUI)
	{
	// Enable the Save As button if the System is not polling and there are Events to be saved
	pCmdUI->Enable(!m_pollTimer && (recordFileDetails.eventList.GetCount() || loadedFileDetails.eventList.GetCount()));
	}

// Export the open session into human-readable text format
void CMainForm::OnFileExport()
	{

	}

void CMainForm::OnUpdateFileExport(CCmdUI* pCmdUI)
	{
	// Enable the Export button if the System is not polling and there are Events to be saved
	pCmdUI->Enable(!m_pollTimer && recordFileDetails.eventList.GetCount());
	}

// The menu open for changing the main chart to the THP was selected
void CMainForm::OnGraphTorqueandhorsepower()
	{
	// If it was already set then ignore the click
	if (chartShown == Constants.Chart_t_hp_vs_rpm)
		return;

	// Change to the THP chart
	chartShown = Constants.Chart_t_hp_vs_rpm;

	// Redraw the board
	DrawGraphLabels();
	DrawCharts();
	}

void CMainForm::OnUpdateGraphTorqueandhorsepower(CCmdUI* pCmdUI)
	{
	pCmdUI->SetCheck(chartShown == Constants.Chart_t_hp_vs_rpm);
	}

void CMainForm::OnGraphRpmandspeed()
	{
	// If it was already set then ignore the click
	if (chartShown == Constants.Chart_rpm_spd_vs_time)
		return;

	// Change to the THP chart
	chartShown = Constants.Chart_rpm_spd_vs_time;
	
	// Redraw the board
	DrawGraphLabels();
	DrawCharts();
	}

void CMainForm::OnUpdateGraphRpmandspeed(CCmdUI* pCmdUI)
	{
	pCmdUI->SetCheck(chartShown == Constants.Chart_rpm_spd_vs_time);
	}

// Toggle the auto scroll option in the menu
void CMainForm::OnViewAutoscrollgraph()
	{
	currentFormConfig->autoScrollChart = !currentFormConfig->autoScrollChart;

	// Save the configuration
	DynoApp->saveDynoConfig(true);
	}

void CMainForm::OnUpdateViewAutoscrollgraph(CCmdUI* pCmdUI)
	{
	pCmdUI->SetCheck(currentFormConfig->autoScrollChart);
	}

void CMainForm::OnAuthorizationAdministrativeoptions()
	{
	// TODO: Add your command handler code here
	}

// Bring up the master configuration screen
void CMainForm::OnOptionsMasterConfig()
	{
	DynoMasterConfigurationScreen dmcs;

	// Make a local copy of the entire configuration
	dmcs.localCfg = *currentFormConfig;

	// Bring up the dialog
	if (dmcs.DoModal() != IDOK)
		return;

	// Save the changes if required

		// If nothing has been modified then just return
	if (!dmcs.isModified())
		return;

	// If the user saved the changes then bring them into effect

	// Start by copying the changes back out
	*currentFormConfig = dmcs.localCfg;

	// Save the configuration
	DynoApp->saveDynoConfig(true);

	// NOTE: If this later affects something visual they it will be necessary to redraw the controls
	}

// Basic log functionality
void CMainForm::log(CString& text)
	{
	CStdioFile logFile;

	if (!logFile.Open(_T("VZD.log"), CFile::modeCreate | CFile::modeWrite | CFile::typeText | CFile::modeNoTruncate))
		{
//		MessageBox(NULL, _T("Error opening log file."), _T("File error"), MB_ICONERROR);
		return;
		}

	logFile.SeekToEnd();
	logFile.WriteString(text);
	logFile.Close();


	}

void CMainForm::OnLegendTop()
	{
	currentFormConfig->legend = CChartLegend::dsDockTop;

	// Save the configuration and redraw the screen
	DynoApp->saveDynoConfig(true);	
	DrawCharts();
	}

void CMainForm::OnUpdateLegendTop(CCmdUI* pCmdUI)
	{
	pCmdUI->SetCheck(currentFormConfig->legend == CChartLegend::dsDockTop);
	}

void CMainForm::OnLegendBottom()
	{
	currentFormConfig->legend = CChartLegend::dsDockBottom;

	// Save the configuration and redraw the screen
	DynoApp->saveDynoConfig(true);
	DrawCharts();
	}

void CMainForm::OnUpdateLegendBottom(CCmdUI* pCmdUI)
	{
	pCmdUI->SetCheck(currentFormConfig->legend == CChartLegend::dsDockBottom);
	}

void CMainForm::OnLegendNone()
	{
	currentFormConfig->legend = -1;

	// Save the configuration and redraw the screen
	DynoApp->saveDynoConfig(true);
	DrawCharts();
	}

void CMainForm::OnUpdateLegendNone(CCmdUI* pCmdUI)
	{
	pCmdUI->SetCheck(currentFormConfig->legend == -1);
	}

void CMainForm::OnVehicleLookup()
	{
	VehicleLookupScreen vls;

	if (vls.DoModal() != IDOK)
		return;
	}

void CMainForm::OnConnectDiagnostics()
	{
	DynoDiagnosticsScreen dds;

	// Pass in COM port group
	dds.globalPorts = &DynoApp->currentComPorts;
	dds.openPortIndex = openPortIndex;


	if (dds.DoModal() != IDOK)
		return;
	}

void CMainForm::OnHelpLogviewer()
	{
	DynoLogViewerScreen dlvs;

	if (dlvs.DoModal() != IDOK)
		return;
	}


// Called when you click on the menu item
void CMainForm::OnViewShowvoltmeter()
	{
	currentFormConfig->showVoltMeter = !currentFormConfig->showVoltMeter;

	// Save the configuration and redraw the screen
	DynoApp->saveDynoConfig(true);

	
	if (currentFormConfig->showVoltMeter == false)
		m_3DMeterCtrl.ShowWindow(SW_HIDE);
	else
		m_3DMeterCtrl.ShowWindow(SW_SHOW);
	

	}





// This is called when the menu is displayed to determine whether to show this item, and how
void CMainForm::OnUpdateViewShowvoltmeter(CCmdUI* pCmdUI)
	{

	pCmdUI->SetCheck(currentFormConfig->showVoltMeter);



	}
