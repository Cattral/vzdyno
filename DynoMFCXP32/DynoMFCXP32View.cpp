
// DynoMFCXP32View.cpp : implementation of the CDynoMFCXP32View class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "DynoMFCXP32.h"
#endif

#include "DynoMFCXP32Doc.h"
#include "DynoMFCXP32View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CDynoMFCXP32View

IMPLEMENT_DYNCREATE(CDynoMFCXP32View, CView)

BEGIN_MESSAGE_MAP(CDynoMFCXP32View, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CDynoMFCXP32View construction/destruction

CDynoMFCXP32View::CDynoMFCXP32View() noexcept
{
	// TODO: add construction code here

}

CDynoMFCXP32View::~CDynoMFCXP32View()
{
}

BOOL CDynoMFCXP32View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CDynoMFCXP32View drawing

void CDynoMFCXP32View::OnDraw(CDC* /*pDC*/)
{
	CDynoMFCXP32Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CDynoMFCXP32View printing

BOOL CDynoMFCXP32View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CDynoMFCXP32View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CDynoMFCXP32View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CDynoMFCXP32View diagnostics

#ifdef _DEBUG
void CDynoMFCXP32View::AssertValid() const
{
	CView::AssertValid();
}

void CDynoMFCXP32View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CDynoMFCXP32Doc* CDynoMFCXP32View::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDynoMFCXP32Doc)));
	return (CDynoMFCXP32Doc*)m_pDocument;
}
#endif //_DEBUG


// CDynoMFCXP32View message handlers
